# studiokit-scaffolding-js

**studiokit-scaffolding-js** provides a common scaffolding for react apps.

1. [Installation](#installation)
1. [Usage](#usage)
1. [Development](#development)

# Installation

## Install this library and call startup methods
1. `yarn add studiokit-scaffolding-js`
1. (Optional) Call configurable extensions in your main entry file
	* `redux/sagas/rootSaga`
		* `setOtherDependentSagas` to add more sagas
	* `redux/sagas/postLoginDataSaga`
		* `setOnPostLogin` to load more data after login
	* `redux/configureReducers`
		* `updatePersistBlacklist` and `setOtherReducers` to add more reducers
1. In your main entry file call `startup`
	```js
	import { startup, endpointMappings } from 'studiokit-scaffolding-js'

	const appConfig = {/* AppConfiguration */}
	const endpointMappings = {...endpointMappings, .../* App EndpointMappings */}
	const { history, store, persistor } = startup(appConfig, endpointMappings)

	// render using history, store, persistor
	ReactDOM.render(
		...
	)
	```
## Styles

### Basic Setup

In your project’s startup `index.tsx` file, import the scaffolding styles as follows

```ts
...third party css...

import 'studiokit-scaffolding-js/lib/css/index-with-variables.css'

...project specific css...
```

In `config-overrides.js` when setting up `react-app-require-postcss`, in order for css variables to work, the `importFrom` array should look like the following:

```js
importFrom: [
	'node_modules/studiokit-scaffolding-js/lib/css/variables.css'
]
```
### Variable Override Setup

In order to override certain color variables from `variables.css`, they must be overridden _before_ scaffolding’s `index.css` file is imported.

In your project’s startup `index.tsx` file, import the scaffolding styles as follows

```ts
...third party css...

import 'studiokit-scaffolding-js/lib/css/variables.css'
import './css/variables.css'
import 'studiokit-scaffolding-js/lib/css/index.css'
import './css/index.css'
```

In `config-overrides.js` when setting up `react-app-require-postcss`, in order for css variables to work, the `importFrom` array should look like the following:

```js
importFrom: [
	'node_modules/studiokit-scaffolding-js/lib/css/variables.css',
	'src/css/variables.css'
]
```

# Usage

## Components and Utils

Components and utils can be imported from `studiokit-scaffolding-js/lib/...`

# Development

## Branches

1. Create a new branch
	* **Feature/Issue**: In GitLab from an issue, create a branch off of `develop`
	* **Hotfix**: Create a `hotfix` branch off of `master` (manually or in GitLab by making an MR)
1. Update the version number
	* **Feature/Issue**
		* append `-next.1.1`
		* e.g. If the current version is `1.0.0`, the new branch should be `1.0.0-next.1.1`
	* **Hotfix**
		* increase the version number and append `-alpha.1`
		* e.g. If the current version is `1.0.0`, the new branch should be `1.0.1-alpha.1` for hotfix
	* **NOTE:** if **more than one** branch is being developed on at the same time, simply increment the **first** number after "next"
		* e.g. `1.0.0-next.2.1` for the second issue branch (this won't apply for hotfixes)
1. Development Loop
	1. Add/update new components, utils, or styles
	1. Add/update unit tests for those components or utils to confirm basic functionality
	1. Increment the **last** number in the version
		* e.g. `1.0.0-next.2.1` => `1.0.0-next.2.2` for a feature/issue branch
		* e.g. `1.0.1-alpha.1` => `1.0.1-alpha.2` for a hotfix branch
	1. Push to gitlab. Azure DevOps will run a pipeline and publish this version to npmjs.org
	1. Install the new version in the project(s) you are working on by updating its `package.json` and running `yarn`
		  * As an alternative to the two steps above, if you don't need to make changes available to other devs yet you can:
		    1. In the studiokit directory, run `yarn build` then `yarn pack`. You'll still need to have incremented the version number to avoid having it cached.
			1. Over in the Circuit/Variate/etc. directory, run `yarn add ../studiokit-scaffolding-js/package.tgz`. 
	1. Repeat as needed
1. Merging
	* After the Merge Request is approved, **remove** the "next" or "alpha" suffix from the version **before** merging to develop or master
	* For **hotfix** branches, the version number should be the new patch number, e.g. `1.0.1`
1. Release
	* **Feature/Issue**: After merging, create a new "release" branch from `develop` and follow the "git flow" release steps as normal
	* **Hotfix**: Finish the "git flow" hotfix steps as normal from the hotfix branch
	* Azure DevOps will run a pipeline to publish the version merged to `master` to npmjs.org

## Styles

Styles are organized into the following folders and files inside of `src/css`

1. `variables.css`
	* Global constant variables
	* Common app-specific variables
1. `utils`
	* Assorted utility styles, similar to tachyons
	* Break out similar styles into separate files once a few similar ones exist
		* e.g. `_display.css` contains all `display: ...;` related modifier styles
1. `base`
	* `_base.css`: Generic tags, .skip-main link, and .main-content
	* `_typography.css`: Default fonts, font scale, and text colors
1. `components`
	* Global component specific styles (e.g. not functional like tachyons)

Your project’s css folders and files should follow this same pattern.
