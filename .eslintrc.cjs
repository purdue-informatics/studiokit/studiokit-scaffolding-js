module.exports = {
	extends: [require.resolve('./src/config/eslint/react.cjs')],
	overrides: [
		{
			files: ['**/*.ts?(x)'],
			parser: '@typescript-eslint/parser',
			parserOptions: {
				project: 'tsconfig.json'
			}
		}
	],
	ignorePatterns: ['coverage/', 'lib/']
}
