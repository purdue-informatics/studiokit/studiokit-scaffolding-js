import { useCallback, useEffect } from 'react'
import {
	CollectionCommonProps,
	CollectionCommonState,
	CollectionCreateParams,
	CollectionDeleteParams,
	CollectionDerivedProps,
	CollectionLoadParams,
	CollectionMethods,
	CollectionReduxResponse,
	CollectionUpdateParams,
	Model,
	ModelCollection
} from '../types'
import {
	cleanupCollectionGuidKey,
	createItemInCollection,
	deleteItemFromCollection,
	handleCollectionParamsChange,
	initializeCollection,
	loadCollection,
	selectCollectionFromState,
	stopCollectionPeriodicLoad,
	updateItemInCollection
} from '../utils/collection'
import { useCollectionConfiguration } from './useCollectionConfiguration'

export interface UseCollectionResponse<TModel extends Model>
	extends CollectionCommonState,
		CollectionReduxResponse<TModel>,
		CollectionDerivedProps<TModel>,
		CollectionMethods {
	guid: string
}

export function useCollection<TModel extends Model>(props: CollectionCommonProps): UseCollectionResponse<TModel> {
	const { modelName, disableAutoLoadOnParamsChange } = props

	// state, route, and redux
	const config = useCollectionConfiguration<TModel>(props, selectCollectionFromState)
	const {
		previousModelName,
		guid,
		model,
		pathParams,
		previousPathParams,
		queryParams,
		previousQueryParams,
		fetchingId,
		modelStatus,
		previousModelStatus,
		isInitialized,
		setIsInitialized,
		methodConfig,
		modelArray
	} = config

	// decorated methods
	const load = useCallback(
		(params: CollectionLoadParams = {}) => {
			loadCollection(methodConfig, params)
		},
		[methodConfig]
	)

	const create = useCallback(
		(params: CollectionCreateParams) => {
			createItemInCollection(methodConfig, params)
		},
		[methodConfig]
	)

	const update = useCallback(
		(params: CollectionUpdateParams) => {
			updateItemInCollection(methodConfig, params)
		},
		[methodConfig]
	)

	const deleteItem = useCallback(
		(params: CollectionDeleteParams) => {
			deleteItemFromCollection(methodConfig, params)
		},
		[methodConfig]
	)

	// initialize and auto-load, if needed (runs once)
	useEffect(() => {
		if (!isInitialized) {
			initializeCollection(model, load, methodConfig)
			setIsInitialized(true)
		}
	}, [isInitialized, load, methodConfig, model, setIsInitialized])

	// re-load if `pathParams` or `queryParams` change, unless `disableAutoLoadOnParamsChange` is true
	useEffect(() => {
		if (!disableAutoLoadOnParamsChange) {
			handleCollectionParamsChange(
				modelName,
				previousModelName,
				pathParams,
				previousPathParams,
				queryParams,
				previousQueryParams,
				load
			)
		}
	}, [
		disableAutoLoadOnParamsChange,
		load,
		modelName,
		pathParams,
		previousModelName,
		previousPathParams,
		previousQueryParams,
		queryParams
	])

	// cleanup on unmount
	useEffect(() => {
		return function cleanup() {
			cleanupCollectionGuidKey(model as ModelCollection<TModel>, methodConfig)
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []) // this effect should NOT be called more than once

	return {
		guid,
		model: model as ModelCollection<TModel>,
		modelArray: modelArray as TModel[],
		modelStatus,
		previousModelStatus,
		fetchingId,
		load,
		stopPeriodicLoad: stopCollectionPeriodicLoad,
		create,
		update,
		delete: deleteItem
	}
}
