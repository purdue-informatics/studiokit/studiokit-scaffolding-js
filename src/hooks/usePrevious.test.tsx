import { mount } from 'enzyme'
import React from 'react'
import { usePrevious } from './usePrevious'

const TestComponent = ({ value }: { value: string }) => {
	const prevValue = usePrevious(value)
	return (
		<div>
			<div id="value">{value}</div>
			<div id="prevValue">{prevValue}</div>
		</div>
	)
}

describe('usePrevious', () => {
	it('should render with a `prevValue` after `value` is changed', () => {
		const wrapper = mount(<TestComponent value="first" />)
		expect(wrapper.find('#value').text()).toEqual('first')
		expect(wrapper.find('#prevValue').text()).toEqual('first')
		wrapper.setProps({
			value: 'second'
		})
		expect(wrapper.find('#value').text()).toEqual('second')
		expect(wrapper.find('#prevValue').text()).toEqual('first')
		wrapper.setProps({
			value: 'third'
		})
		expect(wrapper.find('#value').text()).toEqual('third')
		expect(wrapper.find('#prevValue').text()).toEqual('second')
	})
})
