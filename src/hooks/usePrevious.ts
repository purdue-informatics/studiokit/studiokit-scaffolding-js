import { useEffect, useRef } from 'react'

/**
 * `usePrevious` persists a previous `value` when the `value` changes.
 * @param value A value, e.g. a prop or a value from `useState`.
 */
export function usePrevious<T>(value: T) {
	const ref = useRef<T>(value) // uses value as the initial value

	// Store current value in ref
	useEffect(() => {
		ref.current = value
	}, [value]) // Only re-run if value changes

	// Return previous value (happens before update in useEffect above)
	return ref.current
}
