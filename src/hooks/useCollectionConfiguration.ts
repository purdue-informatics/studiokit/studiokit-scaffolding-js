import { isEqual } from 'lodash'
import { useCallback, useEffect, useMemo, useState } from 'react'
import { useSelector } from 'react-redux'
import { useParams } from 'react-router'
import { MODEL_STATUS } from '../constants/modelStatus'
import {
	BaseReduxState,
	CollectionCommonProps,
	CollectionCommonState,
	CollectionMethodConfiguration,
	CollectionSelectorMethod,
	Model,
	ModelCollection
} from '../types'
import { getModelArray, getModelMinusRelations, handleModelFetchFinish } from '../utils/model'
import { getPathParamsFromRouteMatchParams } from '../utils/route'
import { useGuid } from './useGuid'
import { usePrevious } from './usePrevious'

/** Configuration returned by `useCollectionConfiguration` for use in `useCollection` and `useCollectionItem` */
export interface CollectionConfiguration<TModel extends Model> extends CollectionCommonState {
	previousModelName: string
	guid: string
	model: TModel | ModelCollection<TModel>
	modelArray?: TModel[]
	modelMinusRelations?: Partial<TModel>
	previousModel: TModel | ModelCollection<TModel>
	pathParams: Array<string | number>
	previousPathParams: Array<string | number>
	queryParams?: Record<string, string | number | boolean>
	previousQueryParams?: Record<string, string | number | boolean>
	methodConfig: CollectionMethodConfiguration
	isInitialized: boolean
	setIsInitialized: (value: React.SetStateAction<boolean>) => void
}

/**
 * `useCollectionConfiguration` shares common logic between `useCollection` and `useCollectionItem`,
 * handling most of the state, route, and redux configuration.
 *
 * @param props The props used to call the outer hook, `useCollection` or `useCollectionItem`
 * @param selectorFunction The function that will be used to select the `model` from redux.
 */
export function useCollectionConfiguration<TModel extends Model>(
	props: CollectionCommonProps,
	selectorFunction: CollectionSelectorMethod
): CollectionConfiguration<TModel> {
	const { modelName, pathParams: propPathParams, queryParams: incomingQueryParams, disableAutoLoad } = props
	const guid = useGuid()
	const routeMatchParams = useParams()

	const [isInitialized, setIsInitialized] = useState(false)
	const [modelStatus, setModelStatus] = useState<MODEL_STATUS>(MODEL_STATUS.UNINITIALIZED)
	const previousModelStatus = usePrevious(modelStatus)
	const [fetchingId, setFetchingId] = useState<string | number>()

	const incomingPathParams = useMemo(
		() => propPathParams || getPathParamsFromRouteMatchParams(routeMatchParams, modelName),
		[propPathParams, modelName, routeMatchParams]
	)
	// keep track of pathParams and queryParams values in state
	// the prop/calculated values will almost always be a new array or object, which causes issues for hook deps
	const [persistedPathParams, setPersistedPathParams] = useState(incomingPathParams)
	const [persistedQueryParams, setPersistedQueryParams] = useState(incomingQueryParams)
	// immediately use the incoming value if it is different than the persisted value
	// this allows components using this hook to avoid waiting for another state update before using the new values
	const pathParams = useMemo(
		() => (isEqual(incomingPathParams, persistedPathParams) ? persistedPathParams : incomingPathParams),
		[incomingPathParams, persistedPathParams]
	)
	const queryParams = useMemo(
		() => (isEqual(incomingQueryParams, persistedQueryParams) ? persistedQueryParams : incomingQueryParams),
		[incomingQueryParams, persistedQueryParams]
	)
	// update the persisted pathParams and queryParams if they have changed for later comparisons with incoming values
	useEffect(() => {
		if (!isEqual(persistedPathParams, pathParams)) {
			setPersistedPathParams(pathParams)
		}
	}, [persistedPathParams, pathParams])
	useEffect(() => {
		if (!isEqual(persistedQueryParams, queryParams)) {
			setPersistedQueryParams(queryParams)
		}
	}, [persistedQueryParams, queryParams])

	const { model, isCollectionItem } = useSelector((state: BaseReduxState) => {
		return selectorFunction<TModel>({ guid, modelName, pathParams, routeMatchParams, state })
	})
	const previousModelName = usePrevious(modelName)
	const previousModel = usePrevious(model)
	const previousPathParams = usePrevious(pathParams)
	const previousQueryParams = usePrevious(queryParams)

	const modelArray = useMemo(
		() => (!isCollectionItem ? getModelArray<TModel>(model as ModelCollection<TModel>, guid) : undefined),
		[guid, isCollectionItem, model]
	)
	const modelMinusRelations = useMemo(
		() => (isCollectionItem ? getModelMinusRelations(model as TModel) : undefined),
		[isCollectionItem, model]
	)

	const changeModelStatus = useCallback((newModelStatus: MODEL_STATUS, newFetchingId?: string | number) => {
		setFetchingId(newFetchingId)
		setModelStatus(newModelStatus)
	}, [])

	const methodConfig: CollectionMethodConfiguration = useMemo(
		() => ({
			modelName,
			pathParams,
			queryParams,
			guid,
			isInitialized,
			changeModelStatus,
			disableAutoLoad
		}),
		[modelName, pathParams, queryParams, guid, isInitialized, changeModelStatus, disableAutoLoad]
	)

	// update `modelStatus` when `model` fetch finishes
	useEffect(() => {
		handleModelFetchFinish(model, previousModel, fetchingId, changeModelStatus)
	}, [fetchingId, model, previousModel, changeModelStatus])

	return {
		previousModelName,
		guid,
		model,
		modelArray,
		modelMinusRelations,
		previousModel,
		pathParams,
		previousPathParams,
		queryParams,
		previousQueryParams,
		modelStatus,
		previousModelStatus,
		fetchingId,
		methodConfig,
		isInitialized,
		setIsInitialized
	}
}
