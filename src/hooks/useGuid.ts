import { useState } from 'react'
import { v4 as uuidv4 } from 'uuid'

/**
 * `useGuid` generates and persists a unique guid for the lifetime of the component.
 */
export function useGuid() {
	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	const [guid, setGuid] = useState<string>(uuidv4())
	return guid
}
