import { mount } from 'enzyme'
import React, { FunctionComponent } from 'react'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import { setEndpointMappings } from '../constants/configuration'
import { MODEL_STATUS } from '../constants/modelStatus'
import { BaseReduxState, CollectionCommonProps, Model, ModelCollection } from '../types'
import { useCollectionItem } from './useCollectionItem'

interface TestModel extends Model {
	id: number
	name: string
}

const TestLoader = () => <div id="testLoader" />

const TestComponent: FunctionComponent<CollectionCommonProps> = (props: CollectionCommonProps) => {
	const { model, modelStatus } = useCollectionItem<TestModel>(props)
	if (modelStatus === MODEL_STATUS.UNINITIALIZED) {
		return <TestLoader />
	}
	return (
		<div id="testComponent">
			{model.id} - {model.name}
		</div>
	)
}

const setup = (testModels: ModelCollection<TestModel> = {}, extraProps?: Partial<CollectionCommonProps>) => {
	const mockState: Partial<BaseReduxState> = {
		models: {
			testModels
		}
	}
	const mockProps: Partial<CollectionCommonProps> = {
		modelName: 'testModels',
		pathParams: [1],
		...extraProps
	}
	return mount(
		<Provider store={createStore((state: any) => state, mockState)}>
			<TestComponent {...(mockProps as CollectionCommonProps)} />
		</Provider>
	)
}

jest.mock('../redux/actionCreator')

jest.mock('react-router', () => ({
	// @ts-ignore: allow spread of unknown type
	...jest.requireActual('react-router'),
	useParams: jest.fn().mockReturnValue({ testModelId: '1' })
}))

beforeAll(() => {
	setEndpointMappings({
		testModels: {
			_config: {
				isCollection: true
			}
		}
	})
})

describe('useCollectionItem', () => {
	it('should render <TestLoader> component on first render', () => {
		const wrapper = setup()
		expect(wrapper.find('TestLoader').length).toEqual(1)
	})
	it('should render <TestComponent> immediately if `disableAutoLoad = true`', () => {
		const wrapper = setup(
			{
				'1': {
					id: 1,
					name: 'Bob'
				}
			},
			{ disableAutoLoad: true }
		)
		expect(wrapper.find('TestComponent').length).toEqual(1)
		expect(wrapper.find('#testComponent').text()).toEqual('1 - Bob')
	})
})
