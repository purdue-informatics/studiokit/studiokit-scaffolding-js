import { useCallback, useEffect } from 'react'
import {
	CollectionCommonProps,
	CollectionCommonState,
	CollectionCreateParams,
	CollectionItemDeleteParams,
	CollectionItemDerivedProps,
	CollectionItemLoadParams,
	CollectionItemMethods,
	CollectionItemReduxResponse,
	CollectionItemUpdateParams,
	Model
} from '../types'
import {
	createCollectionItem,
	deleteCollectionItem,
	handleCollectionItemParamsChange,
	initializeCollectionItem,
	loadCollectionItem,
	selectCollectionItemFromState,
	stopCollectionPeriodicLoad,
	updateCollectionItem
} from '../utils/collection'
import { useCollectionConfiguration } from './useCollectionConfiguration'

export interface UseCollectionItemResponse<TModel extends Model>
	extends CollectionCommonState,
		CollectionItemReduxResponse<TModel>,
		CollectionItemDerivedProps<TModel>,
		CollectionItemMethods {
	guid: string
}

export function useCollectionItem<TModel extends Model>(
	props: CollectionCommonProps
): UseCollectionItemResponse<TModel> {
	const { modelName, disableAutoLoadOnParamsChange } = props

	// state, route, and redux
	const config = useCollectionConfiguration<TModel>(props, selectCollectionItemFromState)
	const {
		previousModelName,
		guid,
		model,
		pathParams,
		previousPathParams,
		queryParams,
		previousQueryParams,
		modelStatus,
		previousModelStatus,
		isInitialized,
		setIsInitialized,
		methodConfig,
		modelMinusRelations
	} = config

	// decorated methods
	const load = useCallback(
		(params: CollectionItemLoadParams = {}) => {
			loadCollectionItem(methodConfig, params)
		},
		[methodConfig]
	)

	const create = useCallback(
		(params: CollectionCreateParams) => {
			createCollectionItem(model, methodConfig, params)
		},
		[model, methodConfig]
	)

	const update = useCallback(
		(params: CollectionItemUpdateParams) => {
			updateCollectionItem(model, methodConfig, params)
		},
		[model, methodConfig]
	)

	const deleteItem = useCallback(
		(params: CollectionItemDeleteParams = {}) => {
			deleteCollectionItem(model, methodConfig, params)
		},
		[model, methodConfig]
	)

	// initialize and auto-load, if needed (runs once)
	useEffect(() => {
		if (!isInitialized) {
			initializeCollectionItem(model, load, methodConfig)
			setIsInitialized(true)
		}
	}, [isInitialized, load, methodConfig, model, setIsInitialized])

	// re-load if `pathParams` or `queryParams` change, unless `disableAutoLoadOnParamsChange` is true
	useEffect(() => {
		if (!disableAutoLoadOnParamsChange) {
			handleCollectionItemParamsChange(
				modelName,
				previousModelName,
				pathParams,
				previousPathParams,
				queryParams,
				previousQueryParams,
				load
			)
		}
	}, [
		disableAutoLoadOnParamsChange,
		load,
		modelName,
		pathParams,
		previousModelName,
		previousPathParams,
		previousQueryParams,
		queryParams
	])

	return {
		guid,
		model: model as TModel,
		modelMinusRelations: modelMinusRelations as Partial<TModel>,
		modelStatus,
		previousModelStatus,
		load,
		stopPeriodicLoad: stopCollectionPeriodicLoad,
		create,
		update,
		delete: deleteItem
	}
}
