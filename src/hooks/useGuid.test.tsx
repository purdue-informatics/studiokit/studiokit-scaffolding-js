import { shallow } from 'enzyme'
import React, { FunctionComponent } from 'react'
import { useGuid } from './useGuid'

jest.mock('uuid', () => {
	return {
		v4: jest.fn().mockImplementation(() => 'new-guid')
	}
})

const TestComponent: FunctionComponent = () => {
	const guid = useGuid()
	return <div id="testComponent">{guid}</div>
}

describe('useGuid', () => {
	it('should render with a `guid`', () => {
		const wrapper = shallow(<TestComponent />)
		expect(wrapper.find('#testComponent').text()).toEqual('new-guid')
	})
})
