import { connectRouter } from 'connected-react-router'
import { History } from 'history'
import { Dictionary } from 'lodash'
import { AnyAction, combineReducers, Reducer, ReducersMapObject } from 'redux'
import { PersistedState, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native
import { BaseModelsState, BaseReduxState } from '../types'
import { ModelStoreAction } from './actions/ModelAction'
import { authReducer, modalsReducer, modelsReducer, notificationsReducer, searchReducer } from './reducers'

//#region Setup

const defaultPersistBlacklist = [
	'logs',
	'statusMessages',
	'env',
	'modals',
	'models',
	'router',
	// blacklist nested persists
	'auth',
	'notifications'
]

let persistBlacklist: string[] = [...defaultPersistBlacklist]

const rootPersistConfig = {
	key: 'root',
	storage,
	blacklist: persistBlacklist
}

// create nested persist configs for nested key blacklisting

const authPersistConfig = {
	key: 'auth',
	storage,
	blacklist: ['isInitialized']
}

const notificationsPersistConfig = {
	key: 'notifications',
	storage,
	blacklist: ['queue']
}

export const updatePersistBlacklist = (blacklist: string[]) => {
	persistBlacklist = [...defaultPersistBlacklist, ...blacklist]
	rootPersistConfig.blacklist = persistBlacklist
}

let otherReducers: Dictionary<Reducer> = {}

export const setOtherReducers = (value: Dictionary<Reducer>) => {
	otherReducers = value
}

type ConfigureModelsReducerFunction = <TModelsState extends BaseModelsState>(
	reducer: Reducer<TModelsState, ModelStoreAction>
) => Reducer<TModelsState, ModelStoreAction>

let configureModelsReducer: ConfigureModelsReducerFunction

/**
 * Sets or clears a "middleware" function that is called when constructing the "models"
 * redux reducer, allowing for modification by the app.
 * If provided, the function will be called and its output used as the final value for the reducer.
 * Otherwise, the default "fetchReducer" from "studiokit-net-js" is used.
 * @param value The "middleware" function
 */
export const setConfigureModelsReducer = (value: ConfigureModelsReducerFunction) => {
	configureModelsReducer = value
}

//#endregion Setup

const getModelsReducer = <TModelsState extends BaseModelsState>() => {
	const localModelsReducer = modelsReducer as Reducer<TModelsState, ModelStoreAction>
	if (configureModelsReducer) {
		return configureModelsReducer(localModelsReducer)
	}
	return localModelsReducer
}

const configureReducers = <TReduxState extends BaseReduxState>(
	history: History
): Reducer<TReduxState & PersistedState, any> =>
	persistReducer(
		rootPersistConfig,
		combineReducers({
			auth: persistReducer(authPersistConfig, authReducer),
			notifications: persistReducer(notificationsPersistConfig, notificationsReducer),
			modals: modalsReducer,
			models: getModelsReducer(),
			router: connectRouter(history),
			search: searchReducer,
			...otherReducers
		} as ReducersMapObject<TReduxState, AnyAction>)
	)

export default configureReducers
