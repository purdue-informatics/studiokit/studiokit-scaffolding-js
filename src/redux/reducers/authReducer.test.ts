import { OAuthToken } from '../../types'
import { MODEL_REMOVE_KEY_ACTION_TYPE } from '../actions'
import { AUTH_ACTION_TYPE, AUTH_TOKEN_ACTION_TYPE, AUTH_TOKEN_SUCCESS_ACTION_TYPE } from '../actions/AuthAction'
import authReducer from './authReducer'

const initialState = {
	isInitialized: false,
	isAuthenticating: false,
	isAuthenticated: false,
	didFail: false
}

describe('authReducer', () => {
	describe('default action', () => {
		it('should do nothing for other types of actions', () => {
			const state = authReducer(initialState, {
				type: MODEL_REMOVE_KEY_ACTION_TYPE
			} as any)
			expect(state).toEqual(initialState)
		})

		it('should handle no state parameter passed', () => {
			const state = authReducer(undefined, {
				type: AUTH_TOKEN_ACTION_TYPE.AUTH_INITIALIZED,
				oauthToken: null
			})
			expect(state).toEqual({
				isInitialized: true,
				isAuthenticating: false,
				isAuthenticated: false,
				didFail: false
			})
		})
	})

	describe('GET_TOKEN_SUCCEEDED', () => {
		it('should update state', () => {
			const state = authReducer(initialState, {
				type: AUTH_TOKEN_SUCCESS_ACTION_TYPE.GET_TOKEN_SUCCEEDED,
				oauthToken: {} as OAuthToken
			})
			expect(state).toEqual({
				isInitialized: false,
				isAuthenticating: false,
				isAuthenticated: true,
				didFail: false
			})
		})
	})

	describe('TOKEN_REFRESH_SUCCEEDED', () => {
		it('should update state', () => {
			const state = authReducer(initialState, {
				type: AUTH_TOKEN_SUCCESS_ACTION_TYPE.TOKEN_REFRESH_SUCCEEDED,
				oauthToken: {} as OAuthToken
			})
			expect(state).toEqual({
				isInitialized: false,
				isAuthenticating: false,
				isAuthenticated: true,
				didFail: false
			})
		})
	})

	describe('LOGIN_REQUESTED', () => {
		it('should update state', () => {
			const state = authReducer(initialState, { type: AUTH_ACTION_TYPE.LOGIN_REQUESTED })
			expect(state).toEqual({
				isInitialized: false,
				isAuthenticating: true,
				isAuthenticated: false,
				didFail: false
			})
		})
	})

	describe('LOG_OUT_REQUESTED', () => {
		it('should update state', () => {
			const state = authReducer(initialState, { type: AUTH_ACTION_TYPE.LOG_OUT_REQUESTED })
			expect(state).toEqual({
				isInitialized: false,
				isAuthenticating: false,
				isAuthenticated: false,
				didFail: false
			})
		})
	})

	describe('LOGIN_FAILED', () => {
		it('should update state', () => {
			const state = authReducer(initialState, { type: AUTH_ACTION_TYPE.LOGIN_FAILED })
			expect(state).toEqual({
				isInitialized: false,
				isAuthenticating: false,
				isAuthenticated: false,
				didFail: true
			})
		})
	})

	describe('AUTH_INITIALIZED w/ no oauthToken', () => {
		it('should set isInitialized = true and isAuthenticated = false', () => {
			const state = authReducer(initialState, { type: AUTH_TOKEN_ACTION_TYPE.AUTH_INITIALIZED, oauthToken: null })
			expect(state).toEqual({
				isInitialized: true,
				isAuthenticating: false,
				isAuthenticated: false,
				didFail: false
			})
		})
	})
})
