import { AuthState } from '../../types/auth/AuthState'
import {
	AUTH_ACTION_TYPE,
	AUTH_TOKEN_ACTION_TYPE,
	AUTH_TOKEN_SUCCESS_ACTION_TYPE,
	AuthAction,
	AuthTokenAction,
	AuthTokenSuccessAction
} from '../actions/AuthAction'

const initialState: AuthState = {
	isInitialized: false,
	isAuthenticating: false,
	isAuthenticated: false,
	didFail: false
}

/**
 * Reducer for updating `auth` in redux.
 */
export default function authReducer(
	state: AuthState = initialState,
	action: AuthAction | AuthTokenAction | AuthTokenSuccessAction
) {
	switch (action.type) {
		case AUTH_TOKEN_ACTION_TYPE.AUTH_INITIALIZED:
			return Object.assign({}, state, {
				isInitialized: true,
				isAuthenticated: !!action.oauthToken
			})

		case AUTH_TOKEN_SUCCESS_ACTION_TYPE.GET_TOKEN_SUCCEEDED:
		case AUTH_TOKEN_SUCCESS_ACTION_TYPE.TOKEN_REFRESH_SUCCEEDED:
			return Object.assign({}, state, {
				isAuthenticating: false,
				isAuthenticated: true,
				didFail: false
			})

		case AUTH_ACTION_TYPE.LOGIN_REQUESTED:
			return Object.assign({}, state, {
				isAuthenticating: true,
				isAuthenticated: false,
				didFail: false
			})

		case AUTH_ACTION_TYPE.LOG_OUT_REQUESTED:
			return Object.assign({}, state, {
				isAuthenticating: false,
				isAuthenticated: false,
				didFail: false
			})

		case AUTH_ACTION_TYPE.LOGIN_FAILED:
			return Object.assign({}, state, {
				isAuthenticating: false,
				isAuthenticated: false,
				didFail: true
			})

		default:
			return state
	}
}
