import { Dictionary } from 'lodash'
import { MODAL_ACTION_TYPE, ModalAction } from '../actions/ModalAction'

export interface ModalActionValue {
	index: number
	isFullscreen: boolean
}

export interface ModalsState extends Dictionary<ModalActionValue> {}

/// state is a dictionary where keys are modal guids, for easy lookup
const initialState: ModalsState = {}

export default function modalsReducer(state = initialState, action: ModalAction) {
	let guid: string
	let index: number | undefined
	let isFullscreen: boolean | undefined
	switch (action.type) {
		case MODAL_ACTION_TYPE.MODAL_ENTERING: {
			guid = action.guid
			index = action.index
			isFullscreen = action.isFullscreen
			if (state[guid]) {
				return state
			}
			const newState = { ...state, ...{ [`${guid}`]: { index, isFullscreen } } }
			return newState
		}
		case MODAL_ACTION_TYPE.MODAL_EXITED: {
			guid = action.guid
			if (!state[guid]) {
				return state
			}
			// destructure `state`, separating the value of key `guid` to `removed`, and the rest of the keys to `stateCopy`
			const { [guid]: removed, ...stateCopy } = state
			return stateCopy
		}
		default:
			return state
	}
}
