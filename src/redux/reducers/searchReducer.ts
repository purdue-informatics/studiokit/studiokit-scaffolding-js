import { isArray, mergeWith } from 'lodash'
import { Dictionary } from 'lodash'
import { SEARCH_ACTION_TYPE, SearchAction } from '../actions/SearchAction'

export interface SearchState extends Dictionary<any> {}

/**
 * Store the state of a manage component's search parameters so the params can be
 * re-applied later when revisiting the same component
 *
 * @export
 * @param {*} [state={}]
 * @param {*} action
 * @returns
 */
export default function searchReducer(state: SearchState = {}, action: SearchAction) {
	switch (action.type) {
		case SEARCH_ACTION_TYPE.PERSIST_SEARCH:
			return mergeWith({}, state, action.data, (target, source) => (isArray(source) ? source : undefined))

		default:
			return state
	}
}
