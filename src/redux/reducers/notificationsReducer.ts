import { merge } from 'lodash'
import { Notification } from '../../types/Notification'
import { NOTIFICATION_ACTION_TYPE, NotificationAction } from '../actions/NotificationAction'

export interface NotificationState {
	queue: Notification[]
}

const initialState: NotificationState = {
	queue: []
}

export default function notificationsReducer(state = initialState, action: NotificationAction) {
	let updatedState: NotificationState
	switch (action.type) {
		case NOTIFICATION_ACTION_TYPE.ADD_NOTIFICATION:
			updatedState = merge({}, state)
			updatedState.queue.push(action.notification)
			return updatedState
		case NOTIFICATION_ACTION_TYPE.REMOVE_NOTIFICATION:
			updatedState = merge({}, state)
			updatedState.queue = updatedState.queue.filter(n => n.id !== action.notification.id)
			return updatedState
		default:
			return state
	}
}
