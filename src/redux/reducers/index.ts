import authReducer from './authReducer'
import modalsReducer from './modalsReducer'
import modelsReducer from './modelsReducer'
import notificationsReducer from './notificationsReducer'
import searchReducer from './searchReducer'

export { authReducer, modelsReducer, modalsReducer, notificationsReducer, searchReducer }
