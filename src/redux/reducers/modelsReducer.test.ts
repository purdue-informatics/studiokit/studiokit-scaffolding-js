import MockDate from 'mockdate'
import { v4 as uuidv4 } from 'uuid'
import { unknownErrorData } from '../../constants/fetchErrorData'
import { Metadata } from '../../types'
import {
	MODEL_FETCH_ERROR_ACTION_TYPE,
	MODEL_FETCH_REQUEST_ACTION_TYPE,
	MODEL_FETCH_RESULT_ACTION_TYPE,
	MODEL_FETCH_START_ACTION_TYPE,
	MODEL_REMOVE_KEY_ACTION_TYPE
} from '../actions'
import modelsReducer, { getMetadata, isCollection, merge, updateMetadataForChildCollections } from './modelsReducer'

describe('supporting functions', () => {
	describe('getMetadata', () => {
		it('should return metadata entity', () => {
			const state = { foo: { bar: 'bar', _metadata: { baz: 'quux' } } }
			expect(getMetadata(state, ['foo'])).toEqual({ baz: 'quux' })
		})

		it('should return empty object for no metadata', () => {
			const state = { foo: { bar: 'bar' } }
			expect(getMetadata(state, ['foo'])).toEqual({})
		})

		it('should return empty object for nonexistent path', () => {
			const state = { foo: { bar: 'bar' } }
			expect(getMetadata(state, ['nada'])).toEqual({})
		})
	})

	describe('isCollection', () => {
		it('should return true for a collection obj', () => {
			const obj = {
				1: {
					id: 1
				},
				2: {
					id: 2
				}
			}
			expect(isCollection(obj)).toEqual(true)
		})

		it('should return true for a collection obj with _metadata', () => {
			const obj = {
				1: {
					id: 1
				},
				2: {
					id: 2
				},
				_metadata: {
					isFetching: false,
					hasError: false,
					lastFetchErrorData: undefined
				}
			}
			expect(isCollection(obj)).toEqual(true)
		})

		it('should return false for a collection array', () => {
			const obj = [
				{
					id: 1
				},
				{
					id: 2
				}
			]
			expect(isCollection(obj)).toEqual(false)
		})

		it('should return false for a non collection', () => {
			const obj = {
				foo: 'bar',
				baz: { quux: 7 },
				bleb: 4,
				boop: [1, 2, { three: 4 }],
				_metadata: {
					isFetching: false,
					hasError: false,
					lastFetchErrorData: undefined
				}
			}
			expect(isCollection(obj)).toEqual(false)
		})

		it('should return false for an empty object', () => {
			const obj = {}
			expect(isCollection(obj)).toEqual(false)
		})

		it('should return false for item with single relation that has id, but is not a collection', () => {
			expect(
				isCollection({
					child: {
						id: 1,
						foo: false
					}
				})
			).toEqual(false)
		})
	})

	describe('merge', () => {
		it('should succeed with empty objects', () => {
			const current = {}
			const incoming = {}
			expect(merge(current, incoming)).toEqual({})
		})

		it('should succeed if incoming is `undefined`', () => {
			const current = {
				foo: 'bar'
			}
			expect(merge(current, undefined)).toEqual({ foo: 'bar' })
		})

		it('should update existing property', () => {
			const current = { foo: 'bar' }
			const incoming = { foo: 'baz' }
			expect(merge(current, incoming)).toEqual({ foo: 'baz' })
		})

		it('should replace existing collection item if incoming is a null response object', () => {
			const current = { 1: { id: 1, child: { foo: 'bar' } } }
			const incoming = { 1: { response: null } }
			expect(merge(current, incoming)).toEqual({ 1: { response: null } })
		})

		it('should add new property, keeping existing', () => {
			const current = { foo: 'bar' }
			const incoming = { buzz: 'baz' }
			expect(merge(current, incoming)).toEqual({ foo: 'bar', buzz: 'baz' })
		})

		it('should set existing property to null', () => {
			const current = { foo: 'bar' }
			const incoming = { foo: null }
			expect(merge(current, incoming)).toEqual({ foo: null })
		})

		it('should remove collection items not included in incoming', () => {
			const current = { 1: { id: 1, child: { foo: 'bar' } }, 2: { id: 2 } }
			const incoming = { 1: { id: 1 } }
			expect(merge(current, incoming)).toEqual({ 1: { id: 1, child: { foo: 'bar' } } })
		})

		it('should remove nested collection items not included in incoming', () => {
			const current = {
				1: { id: 1, children: { 1: { id: 1, foo: 'bar' }, 2: { id: 2, boo: 'bah' } } }
			}
			const incoming = { 1: { id: 1, children: { 2: { id: 2 } } } }
			expect(merge(current, incoming)).toEqual({
				1: { id: 1, children: { 2: { id: 2, boo: 'bah' } } }
			})
		})

		it('should preserve non-collection relations', () => {
			const current = { child: { foo: 'bar' } }
			const incoming = {}
			expect(merge(current, incoming)).toEqual({ child: { foo: 'bar' } })
		})

		it('should set non-collection relations to `null` if incoming value is `null`', () => {
			const current = {
				child1: {
					name: 'Jim',
					job: {
						name: 'Coffee Guy'
					}
				},
				child2: {
					name: 'Bob',
					job: {
						name: 'Builder'
					}
				}
			}
			const incoming = {
				child1: null,
				child2: {
					name: 'Bobby',
					job: null
				}
			}
			const expected = {
				child1: null,
				child2: {
					name: 'Bobby',
					job: null
				}
			}
			expect(merge(current, incoming)).toEqual(expected)
		})

		it('should preserve collection relations if incoming does not include the property', () => {
			const current = { 1: { id: 1, foo: 'bar', children: { 1: { foo: 'bar' } } } }
			const incoming = { 1: { id: 1, foo: 'baz' } }
			expect(merge(current, incoming)).toEqual({
				1: { id: 1, foo: 'baz', children: { 1: { foo: 'bar' } } }
			})
		})

		it('should preserve child relation', () => {
			const current = {
				childArray: [3, 4],
				child: {
					id: 1,
					foo: false
				}
			}
			const incoming = { id: 1, name: 'Bob', childArray: [1] }
			expect(merge(current, incoming)).toEqual({
				id: 1,
				name: 'Bob',
				childArray: [1],
				child: {
					id: 1,
					foo: false
				}
			})
		})

		it('should merge nested data with _metadata', () => {
			const current = {
				id: 1,
				foo: 'bar',
				childEntities: {
					1: {
						id: 1,
						name: 'foo',
						roles: ['foo', 'bar'],
						grandchildEntities: {
							1: {
								id: 1,
								name: 'g-foo',
								_metadata: {
									isFetching: false,
									hasError: false,
									lastFetchError: undefined
								}
							},
							2: {
								id: 2,
								name: 'g-bar',
								_metadata: {
									isFetching: false,
									hasError: false,
									lastFetchError: undefined
								}
							},
							_metadata: {
								isFetching: false,
								hasError: false,
								lastFetchError: undefined
							}
						},
						_metadata: {
							isFetching: false,
							hasError: false,
							lastFetchError: undefined
						}
					},
					2: {
						id: 2,
						name: 'bar',
						roles: ['foo', 'bar'],
						grandchildEntities: {
							3: {
								id: 3,
								name: 'g-foo-2',
								_metadata: {
									isFetching: false,
									hasError: false,
									lastFetchError: undefined
								}
							},
							4: {
								id: 4,
								name: 'g-bar-2',
								_metadata: {
									isFetching: false,
									hasError: false,
									lastFetchError: undefined
								}
							},
							_metadata: {
								isFetching: false,
								hasError: false,
								lastFetchError: undefined
							}
						},
						_metadata: {
							isFetching: false,
							hasError: false,
							lastFetchError: undefined
						}
					},
					_metadata: {
						isFetching: false,
						hasError: false,
						lastFetchError: undefined
					}
				},
				_metadata: {
					isFetching: false,
					hasError: false,
					lastFetchError: undefined
				}
			}
			const incoming = {
				id: 1,
				foo: 'bar',
				childEntities: {
					1: {
						id: 1,
						name: 'foo',
						roles: ['foo', 'bar'],
						grandchildEntities: {
							1: {
								id: 1,
								name: 'g-foo',
								_metadata: {
									isFetching: false,
									hasError: false,
									lastFetchError: undefined
								}
							},
							2: {
								id: 2,
								name: 'g-bar',
								_metadata: {
									isFetching: false,
									hasError: false,
									lastFetchError: undefined
								}
							},
							_metadata: {
								isFetching: false,
								hasError: false,
								lastFetchError: undefined
							}
						},
						_metadata: {
							isFetching: false,
							hasError: false,
							lastFetchError: undefined
						}
					},
					2: {
						id: 2,
						name: 'bar',
						grandchildEntities: {
							3: {
								id: 3,
								name: 'g-foo-2',
								_metadata: {
									isFetching: false,
									hasError: false,
									lastFetchError: undefined
								}
							},
							4: {
								id: 4,
								name: 'g-bar-2',
								_metadata: {
									isFetching: false,
									hasError: false,
									lastFetchError: undefined
								}
							},
							_metadata: {
								isFetching: false,
								hasError: false,
								lastFetchError: undefined
							}
						},
						_metadata: {
							isFetching: false,
							hasError: false,
							lastFetchError: undefined
						}
					},
					_metadata: {
						isFetching: false,
						hasError: false,
						lastFetchError: undefined
					}
				},
				_metadata: {
					isFetching: false,
					hasError: false,
					lastFetchError: undefined
				}
			}
			expect(merge(current, incoming)).toEqual({
				id: 1,
				foo: 'bar',
				childEntities: {
					1: {
						id: 1,
						name: 'foo',
						roles: ['foo', 'bar'],
						grandchildEntities: {
							1: {
								id: 1,
								name: 'g-foo',
								_metadata: {
									isFetching: false,
									hasError: false,
									lastFetchError: undefined
								}
							},
							2: {
								id: 2,
								name: 'g-bar',
								_metadata: {
									isFetching: false,
									hasError: false,
									lastFetchError: undefined
								}
							},
							_metadata: {
								isFetching: false,
								hasError: false,
								lastFetchError: undefined
							}
						},
						_metadata: {
							isFetching: false,
							hasError: false,
							lastFetchError: undefined
						}
					},
					2: {
						id: 2,
						name: 'bar',
						roles: ['foo', 'bar'],
						grandchildEntities: {
							3: {
								id: 3,
								name: 'g-foo-2',
								_metadata: {
									isFetching: false,
									hasError: false,
									lastFetchError: undefined
								}
							},
							4: {
								id: 4,
								name: 'g-bar-2',
								_metadata: {
									isFetching: false,
									hasError: false,
									lastFetchError: undefined
								}
							},
							_metadata: {
								isFetching: false,
								hasError: false,
								lastFetchError: undefined
							}
						},
						_metadata: {
							isFetching: false,
							hasError: false,
							lastFetchError: undefined
						}
					},
					_metadata: {
						isFetching: false,
						hasError: false,
						lastFetchError: undefined
					}
				},
				_metadata: {
					isFetching: false,
					hasError: false,
					lastFetchError: undefined
				}
			})
		})
	})

	describe('updateMetadataForChildCollections', () => {
		it('should set _metadata on any nested collections and collection items in the data', () => {
			const incoming = {
				id: 1,
				name: 'foo',
				childCollection: {
					1: { id: 1, grandchildCollection: { 1: { id: 1 }, 2: { id: 2 } } },
					2: { id: 2, grandchildCollection: { 1: { id: 1 }, 2: { id: 2 } } }
				}
			}
			const metadata: Metadata = {
				isFetching: false,
				hasError: false,
				lastFetchErrorData: undefined,
				fetchedAt: new Date()
			}
			updateMetadataForChildCollections(incoming, metadata)
			expect(incoming).toEqual({
				id: 1,
				name: 'foo',
				childCollection: {
					_metadata: metadata,
					1: {
						id: 1,
						_metadata: metadata,
						grandchildCollection: {
							_metadata: metadata,
							1: { id: 1, _metadata: metadata },
							2: { id: 2, _metadata: metadata }
						}
					},
					2: {
						id: 2,
						_metadata: metadata,
						grandchildCollection: {
							_metadata: metadata,
							1: { id: 1, _metadata: metadata },
							2: { id: 2, _metadata: metadata }
						}
					}
				}
			})
		})
	})
})

describe('modelsReducer', () => {
	it('should do nothing without action.modelPath', () => {
		const state = modelsReducer(
			{ foo: 'bar' },
			{
				modelPath: '',
				type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED
			}
		)
		expect(state).toEqual({ foo: 'bar' })
	})

	describe('FETCH_START', () => {
		it('should update single level', () => {
			const state = modelsReducer({}, { type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START, modelPath: 'test' })
			expect(state).toEqual({
				test: {
					_metadata: {
						isFetching: true,
						hasError: false,
						lastFetchErrorData: undefined
					}
				}
			})
		})

		it('should include guid if passed in "action.guid"', () => {
			const guid = uuidv4()
			const state = modelsReducer(
				{},
				{ type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START, modelPath: 'test', guid }
			)
			expect(state).toEqual({
				test: {
					guid,
					_metadata: {
						isFetching: true,
						hasError: false,
						lastFetchErrorData: undefined
					}
				}
			})
		})

		it('should update nested level', () => {
			const state = modelsReducer({}, { type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START, modelPath: 'user.test' })
			expect(state).toEqual({
				user: {
					test: {
						_metadata: {
							isFetching: true,
							hasError: false,
							lastFetchErrorData: undefined
						}
					}
				}
			})
		})

		it('should update nested level with numbers', () => {
			const state = modelsReducer({}, { type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START, modelPath: 'user.1' })
			expect(state).toEqual({
				user: {
					'1': {
						_metadata: {
							isFetching: true,
							hasError: false,
							lastFetchErrorData: undefined
						}
					}
				}
			})
		})

		it('should update nested level with multiple level numbers', () => {
			const state = modelsReducer(
				{},
				{ type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START, modelPath: 'user.1.info.2' }
			)
			expect(state).toEqual({
				user: {
					'1': {
						info: {
							'2': {
								_metadata: {
									isFetching: true,
									hasError: false,
									lastFetchErrorData: undefined
								}
							}
						}
					}
				}
			})
		})

		it('should merge nested level state', () => {
			const state = modelsReducer(
				{ foo: 'bar' },
				{ type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START, modelPath: 'user.test' }
			)
			expect(state).toEqual({
				foo: 'bar',
				user: {
					test: {
						_metadata: {
							isFetching: true,
							hasError: false,
							lastFetchErrorData: undefined
						}
					}
				}
			})
		})

		it('should replace nested level state', () => {
			const state = modelsReducer(
				{ user: { foo: 'bar' } },
				{ type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START, modelPath: 'user.test' }
			)
			expect(state).toEqual({
				user: {
					foo: 'bar',
					test: {
						_metadata: {
							isFetching: true,
							hasError: false,
							lastFetchErrorData: undefined
						}
					}
				}
			})
		})

		it('should preserve data and fetchedAt key while fetching', () => {
			const fetchedDate = new Date()

			const state = modelsReducer(
				{
					test: {
						foo: 'bar',
						_metadata: {
							isFetching: false,
							hasError: false,
							fetchedAt: fetchedDate
						}
					}
				},
				{ type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START, modelPath: 'test' }
			)
			expect(state).toEqual({
				test: {
					foo: 'bar',
					_metadata: {
						isFetching: true,
						hasError: false,
						fetchedAt: fetchedDate
					}
				}
			})
		})

		it('should preserve data in nested level', () => {
			const fetchedDate = new Date()

			const state = modelsReducer(
				{
					test: {
						foo: {
							bar: 'baz'
						},
						_metadata: {
							isFetching: false,
							hasError: false,

							fetchedAt: fetchedDate
						}
					}
				},
				{ type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START, modelPath: 'test.qux.1.corge' }
			)
			expect(state).toEqual({
				test: {
					foo: {
						bar: 'baz'
					},
					qux: {
						'1': {
							corge: {
								_metadata: {
									isFetching: true,
									hasError: false,
									lastFetchErrorData: undefined
								}
							}
						}
					},
					_metadata: {
						isFetching: false,
						hasError: false,
						fetchedAt: fetchedDate
					}
				}
			})
		})
	})

	describe('FETCH_RESULT_RECEIVED', () => {
		let fetchedAtDate: Date
		let fetchedAtDateOld: Date
		beforeEach(() => {
			fetchedAtDate = new Date()
			fetchedAtDateOld = new Date()
			fetchedAtDateOld.setDate(fetchedAtDateOld.getDate() - 1)
			MockDate.set(fetchedAtDate)
		})
		afterEach(() => {
			MockDate.reset()
		})

		it('should update single level', () => {
			const state = modelsReducer(
				{},
				{
					type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
					modelPath: 'test',
					data: { key: 'value' }
				}
			)
			expect(state).toEqual({
				test: {
					_metadata: {
						isFetching: false,
						hasError: false,
						fetchedAt: fetchedAtDate
					},
					key: 'value'
				}
			})
		})

		it('should update nested level', () => {
			const state = modelsReducer(
				{},
				{
					type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
					modelPath: 'user.test',
					data: { key: 'value' }
				}
			)
			expect(state).toEqual({
				user: {
					test: {
						_metadata: {
							isFetching: false,
							hasError: false,
							fetchedAt: fetchedAtDate
						},
						key: 'value'
					}
				}
			})
		})

		it('should add nested sibling key', () => {
			const state = modelsReducer(
				{ foo: 'bar' },
				{
					type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
					modelPath: 'user.test',
					data: { key: 'value' }
				}
			)
			expect(state).toEqual({
				foo: 'bar',
				user: {
					test: {
						_metadata: {
							isFetching: false,
							hasError: false,
							fetchedAt: fetchedAtDate
						},
						key: 'value'
					}
				}
			})
		})

		it('should replace existing nested level key', () => {
			const state = modelsReducer(
				{
					user: {
						test: { key: 'oldValue' }
					}
				},
				{
					type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
					modelPath: 'user.test',
					data: { key: 'value' }
				}
			)
			expect(state).toEqual({
				user: {
					test: {
						_metadata: {
							isFetching: false,
							hasError: false,
							fetchedAt: fetchedAtDate
						},
						key: 'value'
					}
				}
			})
		})

		it('should merge existing nested level key', () => {
			const state = modelsReducer(
				{
					user: {
						existingKey: { foo: 'bar' }
					}
				},
				{
					type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
					modelPath: 'user.test',
					data: { key: 'value' }
				}
			)
			expect(state).toEqual({
				user: {
					existingKey: { foo: 'bar' },
					test: {
						_metadata: {
							isFetching: false,
							hasError: false,
							fetchedAt: fetchedAtDate
						},
						key: 'value'
					}
				}
			})
		})

		it('should update existing nested level data on same key', () => {
			const state = modelsReducer(
				{
					user: {
						test: {
							_metadata: {
								isFetching: false,
								hasError: false,
								fetchedAt: fetchedAtDate
							},
							key: 'value1',
							key2: 'value2'
						}
					}
				},
				{
					type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
					modelPath: 'user.test',
					data: { key: 'value3' }
				}
			)
			expect(state).toEqual({
				user: {
					test: {
						_metadata: {
							isFetching: false,
							hasError: false,
							lastFetchErrorData: undefined,
							fetchedAt: fetchedAtDate
						},
						key: 'value3',
						key2: 'value2'
					}
				}
			})
		})

		it('should update existing collection nested level data on same key', () => {
			const state = modelsReducer(
				{
					groups: {
						1: {
							_metadata: {
								isFetching: false,
								hasError: false,
								fetchedAt: fetchedAtDateOld
							},
							key: 'value1',
							key2: 'value2'
						}
					}
				},
				{
					type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
					modelPath: 'groups.1',
					data: { key: 'value3' }
				}
			)
			expect(state).toEqual({
				groups: {
					1: {
						_metadata: {
							isFetching: false,
							hasError: false,
							fetchedAt: fetchedAtDate
						},
						key: 'value3',
						key2: 'value2'
					}
				}
			})
		})

		it('should preserve data in nested level', () => {
			const state = modelsReducer(
				{
					test: {
						foo: {
							bar: 'baz'
						},
						qux: {
							'1': {
								corge: {
									_metadata: {
										isFetching: true,
										hasError: false,
										lastFetchErrorData: undefined
									}
								}
							}
						},
						_metadata: {
							isFetching: false,
							hasError: false,
							fetchedAt: fetchedAtDateOld
						}
					}
				},
				{
					type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
					modelPath: 'test.qux.1.corge',
					data: { key: 'value' }
				}
			)
			expect(state).toEqual({
				test: {
					foo: {
						bar: 'baz'
					},
					qux: {
						'1': {
							corge: {
								key: 'value',
								_metadata: {
									isFetching: false,
									hasError: false,
									fetchedAt: fetchedAtDate
								}
							}
						}
					},
					_metadata: {
						isFetching: false,
						hasError: false,
						fetchedAt: fetchedAtDateOld
					}
				}
			})
		})

		it('should preserve nested collection relation that contains an id, but is not its key', () => {
			let state = {}
			state = modelsReducer(state, {
				type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
				modelPath: 'groups.1.child',
				data: { id: 1, foo: false }
			})
			expect(state).toEqual({
				groups: {
					1: {
						child: {
							id: 1,
							foo: false,
							_metadata: {
								isFetching: false,
								hasError: false,
								fetchedAt: fetchedAtDate
							}
						}
					}
				}
			})
			state = modelsReducer(state, {
				type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
				modelPath: 'groups.1',
				data: { id: 1, name: 'Group 1' }
			})
			expect(state).toEqual({
				groups: {
					1: {
						id: 1,
						name: 'Group 1',
						child: {
							id: 1,
							foo: false,
							_metadata: {
								isFetching: false,
								hasError: false,
								fetchedAt: fetchedAtDate
							}
						},
						_metadata: {
							isFetching: false,
							hasError: false,
							fetchedAt: fetchedAtDate
						}
					}
				}
			})
		})

		it('should remove nested collection key, if incoming is a collection and key is not included', () => {
			let state: any = {
				groups: {
					2: {
						child: {
							id: 2,
							foo: false,
							_metadata: {
								isFetching: false,
								hasError: false,
								fetchedAt: fetchedAtDateOld
							}
						}
					}
				}
			}
			// add group 1
			state = modelsReducer(state, {
				type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
				modelPath: 'groups.1.child',
				data: { id: 1, foo: false }
			})
			expect(state).toEqual({
				groups: {
					1: {
						child: {
							id: 1,
							foo: false,
							_metadata: {
								isFetching: false,
								hasError: false,
								fetchedAt: fetchedAtDate
							}
						}
					},
					2: {
						child: {
							id: 2,
							foo: false,
							_metadata: {
								isFetching: false,
								hasError: false,
								fetchedAt: fetchedAtDateOld
							}
						}
					}
				}
			})
			// reload collection, remove group 1
			state = modelsReducer(state, {
				type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
				modelPath: 'groups',
				data: { 2: { id: 2, name: 'Group 2' } }
			})
			expect(state).toEqual({
				groups: {
					2: {
						id: 2,
						name: 'Group 2',
						child: {
							id: 2,
							foo: false,
							_metadata: {
								isFetching: false,
								hasError: false,
								fetchedAt: fetchedAtDateOld
							}
						},
						_metadata: {
							isFetching: false,
							hasError: false,
							fetchedAt: fetchedAtDate
						}
					},
					_metadata: {
						isFetching: false,
						hasError: false,
						fetchedAt: fetchedAtDate
					}
				}
			})
		})

		it('should clear properties on collection item, if incoming is null', () => {
			const state = {
				groups: {
					1: {
						id: 1,
						foo: 'bar',
						_metadata: {
							isFetching: true,
							hasError: false,
							fetchedAt: fetchedAtDateOld
						}
					},
					_metadata: {
						isFetching: false,
						hasError: false,
						fetchedAt: fetchedAtDateOld
					}
				}
			}
			expect(
				modelsReducer(state, {
					type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
					modelPath: 'groups.1',
					data: null
				})
			).toEqual({
				groups: {
					1: {
						response: null,
						_metadata: {
							isFetching: false,
							hasError: false,
							fetchedAt: fetchedAtDate
						}
					},
					_metadata: {
						isFetching: false,
						hasError: false,
						fetchedAt: fetchedAtDateOld
					}
				}
			})
		})

		it('should handle string response', () => {
			const state = modelsReducer(
				{},
				{
					type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
					modelPath: 'class',
					data: 'value'
				}
			)
			expect(state).toEqual({
				class: {
					_metadata: {
						isFetching: false,
						hasError: false,
						fetchedAt: fetchedAtDate
					},
					response: 'value'
				}
			})
		})

		it('should preserve children at any level', () => {
			const state = modelsReducer(
				{
					user: {
						testChildren: {
							_metadata: {
								isFetching: false,
								lastFetchErrorData: undefined,
								hasError: false,
								fetchedAt: fetchedAtDate
							},
							key: 'value',
							key2: 'value2',
							child1: [1, 2, 3],
							child2: {
								eeny: 'meeny',
								miney: 'mo'
							}
						}
					}
				},
				{
					type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
					modelPath: 'user.testChildren',
					data: {
						key: 'new value'
					}
				}
			)
			expect(state).toEqual({
				user: {
					testChildren: {
						_metadata: {
							isFetching: false,
							lastFetchErrorData: undefined,
							hasError: false,
							fetchedAt: fetchedAtDate
						},
						key: 'new value',
						key2: 'value2',
						child1: [1, 2, 3],
						child2: {
							eeny: 'meeny',
							miney: 'mo'
						}
					}
				}
			})
		})

		it('should replace value completely if action.replaceValue is true', () => {
			const state = modelsReducer(
				{
					user: {
						testChildren: {
							_metadata: {
								isFetching: false,
								lastFetchErrorData: undefined,
								hasError: false,
								fetchedAt: fetchedAtDate
							},
							key: 'value',
							key2: 'value2',
							child1: [1, 2, 3],
							child2: {
								eeny: 'meeny',
								miney: 'mo'
							}
						}
					}
				},
				{
					type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
					modelPath: 'user.testChildren',
					replaceValue: true,
					data: {
						key: 'new value'
					}
				}
			)
			expect(state).toEqual({
				user: {
					testChildren: {
						_metadata: {
							isFetching: false,
							lastFetchErrorData: undefined,
							hasError: false,
							fetchedAt: fetchedAtDate
						},
						key: 'new value'
					}
				}
			})
		})
	})

	describe('FETCH_FAILED', () => {
		it('should update single level with fetch error data', () => {
			const state = modelsReducer(
				{},
				{
					type: MODEL_FETCH_ERROR_ACTION_TYPE.FETCH_FAILED,
					modelPath: 'test',
					errorData: unknownErrorData
				}
			)
			expect(state).toEqual({
				test: {
					_metadata: {
						isFetching: false,
						hasError: true,
						lastFetchErrorData: unknownErrorData
					}
				}
			})
		})

		it('should update single level with no fetch error data', () => {
			const state = modelsReducer({}, { type: MODEL_FETCH_ERROR_ACTION_TYPE.FETCH_FAILED, modelPath: 'test' })
			expect(state).toEqual({
				test: {
					_metadata: {
						isFetching: false,
						hasError: true,
						lastFetchErrorData: undefined
					}
				}
			})
		})

		it('should include guid if passed in "action.guid"', () => {
			const guid = uuidv4()
			const state = modelsReducer(
				{},
				{ type: MODEL_FETCH_ERROR_ACTION_TYPE.FETCH_FAILED, modelPath: 'test', guid }
			)
			expect(state).toEqual({
				test: {
					guid,
					_metadata: {
						isFetching: false,
						hasError: true,
						lastFetchErrorData: undefined
					}
				}
			})
		})

		it('should update nested level', () => {
			const state = modelsReducer(
				{},
				{
					type: MODEL_FETCH_ERROR_ACTION_TYPE.FETCH_FAILED,
					modelPath: 'user.test',
					errorData: unknownErrorData
				}
			)
			expect(state).toEqual({
				user: {
					test: {
						_metadata: {
							isFetching: false,
							hasError: true,
							lastFetchErrorData: unknownErrorData
						}
					}
				}
			})
		})

		it('should merge nested level state', () => {
			const state = modelsReducer(
				{ foo: 'bar' },
				{
					type: MODEL_FETCH_ERROR_ACTION_TYPE.FETCH_FAILED,
					modelPath: 'user.test',
					errorData: unknownErrorData
				}
			)
			expect(state).toEqual({
				foo: 'bar',
				user: {
					test: {
						_metadata: {
							isFetching: false,
							hasError: true,
							lastFetchErrorData: unknownErrorData
						}
					}
				}
			})
		})

		it('should replace nested level state', () => {
			const state = modelsReducer(
				{ user: { foo: 'bar' } },
				{
					type: MODEL_FETCH_ERROR_ACTION_TYPE.FETCH_FAILED,
					modelPath: 'user.test',
					errorData: unknownErrorData
				}
			)
			expect(state).toEqual({
				user: {
					foo: 'bar',
					test: {
						_metadata: {
							isFetching: false,
							hasError: true,
							lastFetchErrorData: unknownErrorData
						}
					}
				}
			})
		})

		it('should replace collection nested level state', () => {
			const fetchedAtDate = new Date()
			MockDate.set(fetchedAtDate)

			const state = modelsReducer(
				{
					groups: {
						1: {
							id: 1,
							foo: 'bar',
							_metadata: {
								isFetching: true,
								hasError: false,
								fetchedAt: fetchedAtDate
							}
						}
					}
				},
				{
					type: MODEL_FETCH_ERROR_ACTION_TYPE.FETCH_FAILED,
					modelPath: 'groups.1',
					errorData: unknownErrorData
				}
			)
			expect(state).toEqual({
				groups: {
					1: {
						id: 1,
						foo: 'bar',
						_metadata: {
							isFetching: false,
							hasError: true,
							lastFetchErrorData: unknownErrorData,
							fetchedAt: fetchedAtDate
						}
					}
				}
			})
		})
	})

	describe('MODEL_REMOVE_KEY', () => {
		it('should remove key', () => {
			const state = modelsReducer(
				{ test: { foo: 'bar' } },
				{ type: MODEL_REMOVE_KEY_ACTION_TYPE, modelPath: 'test' }
			)
			expect(state).toEqual({})
		})

		it('should remove nested key', () => {
			const state = modelsReducer(
				{ test: { foo: 'bar' }, test2: { baz: 'bat' } },
				{ type: MODEL_REMOVE_KEY_ACTION_TYPE, modelPath: 'test' }
			)
			expect(state).toEqual({ test2: { baz: 'bat' } })
		})

		it('should remove nested collection key', () => {
			const state = modelsReducer(
				{ groups: { 1: { id: 1, name: 'group name' } } },
				{ type: MODEL_REMOVE_KEY_ACTION_TYPE, modelPath: 'groups.1' }
			)
			expect(state).toEqual({ groups: {} })
		})
	})

	describe('full lifecycle', () => {
		it('should update state for default flow', () => {
			const state = {}

			const state2 = modelsReducer(state, {
				type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START,
				modelPath: 'test'
			})
			expect(state2).toEqual({
				test: {
					_metadata: {
						isFetching: true,
						hasError: false,
						lastFetchErrorData: undefined
					}
				}
			})

			const state3 = modelsReducer(state2, {
				type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
				modelPath: 'test',
				data: { foo: 'bar' }
			})
			let fetchedAtDate = new Date()
			MockDate.set(fetchedAtDate)
			expect(state3).toEqual({
				test: {
					_metadata: {
						isFetching: false,
						hasError: false,
						fetchedAt: fetchedAtDate
					},
					foo: 'bar'
				}
			})

			fetchedAtDate = new Date()
			const state4 = modelsReducer(state3, {
				type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
				modelPath: 'test',
				data: { baz: 'quux', bleb: 'fleb' }
			})
			expect(state4).toEqual({
				test: {
					_metadata: {
						isFetching: false,
						hasError: false,
						fetchedAt: fetchedAtDate,
						lastFetchErrorData: undefined
					},
					baz: 'quux',
					bleb: 'fleb',
					foo: 'bar'
				}
			})
		})
	})

	describe('default action', () => {
		it('should do nothing for other action type', () => {
			const state = modelsReducer({ test: { foo: 'bar' } }, {
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				modelPath: 'test'
			} as any)
			expect(state).toEqual({ test: { foo: 'bar' } })
		})

		it('should handle no state parameter', () => {
			const state = modelsReducer(undefined, {
				type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START,
				modelPath: 'test'
			})
			expect(state).toEqual({
				test: {
					_metadata: {
						isFetching: true,
						hasError: false,
						lastFetchErrorData: undefined
					}
				}
			})
		})
	})
})
