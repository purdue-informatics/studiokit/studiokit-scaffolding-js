import * as Sentry from '@sentry/react'
import { routerMiddleware } from 'connected-react-router'
import { History } from 'history'
import { isArray, isPlainObject, transform } from 'lodash'
import { AnyAction, applyMiddleware, compose, createStore, Store } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly'
import { createLogger } from 'redux-logger'
import { Persistor, PersistState, persistStore } from 'redux-persist'
import createSagaMiddleware from 'redux-saga'
import { getAppConfig } from '../constants/configuration'
import { BaseReduxState } from '../types'
import { setStore as setStoreForActionCreator } from './actionCreator'
import { MODAL_ACTION_TYPE } from './actions'
import configureReducers from './configureReducers'
import rootSaga from './sagas/rootSaga'

export type CustomStore<TReduxState extends BaseReduxState> = Store<
	TReduxState & {
		_persist: PersistState
	},
	any
> & {
	dispatch: unknown
}

const keysToFilter = [
	'puid',
	'employeeNumber',
	'access_token',
	'refresh_token',
	'password',
	'Password',
	'content',
	'data'
]

const filterData = (obj: any) => {
	return transform(obj, (result: any, value: any, key: any) => {
		if (keysToFilter.includes(key)) {
			result[key] = '[Filtered]'
			return
		}
		result[key] = isPlainObject(value) || isArray(value) ? filterData(value) : value
	})
}

export const stripState = (obj: any, parentKey?: any) => {
	return transform(obj, (result: any, value: any, key: any) => {
		if (isPlainObject(value)) {
			result[key] = stripState(value, key)
		} else if (key === 'id' || parentKey === '_metadata') {
			result[key] = value
		}
	})
}

export const actionTransformer = (action: AnyAction) => {
	// action breadcrumb filtering
	if (
		// do not track modal actions
		action.type === MODAL_ACTION_TYPE.MODAL_ENTERING ||
		action.type === MODAL_ACTION_TYPE.MODAL_EXITED
	) {
		return null
	}
	return filterData(action)
}

const configureStore = <TReduxState extends BaseReduxState>(history: History<any>) => {
	const appConfig = getAppConfig()
	const sentryReduxEnhancer = Sentry.createReduxEnhancer({
		stateTransformer: state => {
			// Transform the state to remove unneeded data
			return stripState(state)
		},
		actionTransformer: actionTransformer
	})
	const sagaMiddleware = createSagaMiddleware()
	const reduxRouterMiddleware = routerMiddleware(history)
	const commonMiddlewares = [sagaMiddleware, reduxRouterMiddleware]

	let enchancers
	if (appConfig.NODE_ENV !== 'production') {
		const logger = createLogger({
			collapsed: (getState, action, logEntry) => {
				if (!logEntry) {
					return false
				}
				return !logEntry || !logEntry.error
			}
		})
		enchancers = composeWithDevTools(applyMiddleware(...commonMiddlewares, logger), sentryReduxEnhancer)
	} else {
		enchancers = compose(applyMiddleware(...commonMiddlewares), sentryReduxEnhancer)
	}

	const store: CustomStore<TReduxState> = createStore(configureReducers<TReduxState>(history), enchancers)
	setStoreForActionCreator(store)
	const persistor: Persistor = persistStore(store, undefined, () => {
		// Finished rehydration
		// start the sagas after rehydration so
		// rehydrate is guaranteed to be the first action
		sagaMiddleware.run(rootSaga)
	})

	return { store, persistor }
}

export default configureStore
