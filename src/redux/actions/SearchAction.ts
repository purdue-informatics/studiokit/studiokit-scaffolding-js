import { Dictionary } from 'lodash'
import { Action } from 'redux'

export enum SEARCH_ACTION_TYPE {
	PERSIST_SEARCH = 'manage/PERSIST_SEARCH'
}
export interface SearchAction extends Action<SEARCH_ACTION_TYPE> {
	data: Dictionary<any>
}
