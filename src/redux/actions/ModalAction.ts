import { Action } from 'redux'

export enum MODAL_ACTION_TYPE {
	MODAL_ENTERING = 'modals/ENTERING',
	MODAL_EXITED = 'modals/EXITED'
}

export interface ModalActionValue {
	index: number
	isFullscreen: boolean
}

export interface ModalEnterAction extends Action<MODAL_ACTION_TYPE.MODAL_ENTERING>, ModalActionValue {
	guid: string
}

export interface ModalExitAction extends Action<MODAL_ACTION_TYPE.MODAL_EXITED> {
	guid: string
}

export type ModalAction = ModalEnterAction | ModalExitAction
