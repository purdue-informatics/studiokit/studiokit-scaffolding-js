import { Action } from 'redux'
import { Notification } from '../../types/Notification'

export enum NOTIFICATION_ACTION_TYPE {
	ADD_NOTIFICATION = 'notifications/ADD_NOTIFICATION',
	REMOVE_NOTIFICATION = 'notifications/REMOVE_NOTIFICATION'
}

export interface NotificationAction extends Action<NOTIFICATION_ACTION_TYPE> {
	notification: Notification
}
