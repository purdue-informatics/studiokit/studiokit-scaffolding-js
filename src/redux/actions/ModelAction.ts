import { Action, AnyAction } from 'redux'
import { FetchErrorData } from '../../types/net/FetchErrorData'
import { HTTPMethod } from '../../types/net/HTTPMethod'

//#region Model Fetch Requests

export enum MODEL_FETCH_REQUEST_ACTION_TYPE {
	FETCH_REQUEST = 'models/FETCH_REQUEST',
	PERIODIC_FETCH_REQUEST = 'models/PERIODIC_FETCH_REQUEST'
}

export interface BaseModelFetchRequestAction<T> extends Action<T> {
	/**
	 * The generic path (no Ids) to an `EndpointMapping` and to where the model will be stored in redux under `state.models` (levels separated by a '.').
	 */
	modelName: string
	/**
	 * An array of values to be replaced in the fetch path and redux model path using pattern matching, in order, e.g.
	 * `"/collection/{}/subcollection/{}" => "/collection/1/subcollection/2"`
	 */
	pathParams?: Array<string | number | undefined | null>
	/** A unique identifier used for action dispatchers to correlate responses to requests. Will be attached to the corresponding `ModelFetchResultAction.data`. */
	guid?: string
	/** The HTTP Method to use for the fetch. Defaults to use value from EndpointMapping, or 'GET'. */
	method?: HTTPMethod
	/** Key/value pairs of headers to be sent with the request. */
	headers?: Record<string, string>
	/** Key/value pairs to be added to query as query params. */
	queryParams?: Record<string, string | number | boolean>
	/** A value to send as the HTTP Body. */
	body?: any
	/** If true, make the request but do not store the response in redux. */
	noStore?: boolean
	/** Prevent the use of the default logarithmic backoff retry strategy. */
	noRetry?: boolean
	/** The contentType to be set in the headers. Defaults to `application/json` */
	contentType?: string
	/** If true, the `modelsReducer` will replace the existing value in redux, instead of merging the incoming with existing. Passed to `ModelFetchResultAction` */
	replaceValue?: boolean
}

//#region Model Fetch Request

/**
 * Action used to trigger a fetch request for a model. Uses `modelName` to find an `EndpointMapping`, and stores results in the corresponding location in redux under `state.models`.
 */
export interface ModelFetchRequestAction
	extends BaseModelFetchRequestAction<MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST> {}

//#endregion Model Fetch Request

//#region Periodic Model Fetch Request

/**
 * Action used to trigger a recurring fetch request for a model. Uses `modelName` to find an `EndpointMapping`, and stores results in the corresponding location in redux under `state.models`.
 */
export interface PeriodicModelFetchRequestAction
	extends BaseModelFetchRequestAction<MODEL_FETCH_REQUEST_ACTION_TYPE.PERIODIC_FETCH_REQUEST> {
	/** How often in `ms` milliseconds to re-fetch when used in a recurring fetch scenario. */
	period: number
	/** An id, from your application, used to cancel the recurring fetch task at a later time. */
	taskId: string
}

export enum PERIODIC_MODEL_FETCH_TERMINATION_ACTION_TYPE {
	TERMINATE = 'models/PERIODIC_MODEL_FETCH_REQUEST_TERMINATE',
	SUCCEEDED = 'models/PERIODIC_MODEL_FETCH_REQUEST_TERMINATE_SUCCEEDED'
}

/**
 * Action used to terminate a periodic model fetch request task, or notify of termination success.
 */
export interface PeriodicModelFetchTerminationAction extends Action<PERIODIC_MODEL_FETCH_TERMINATION_ACTION_TYPE> {
	/** The id, from your application, used to cancel a recurring task. */
	taskId: string
}

//#endregion Periodic Model Fetch Request

//#endregion Model Fetch Requests

//#region Model Store Actions

interface BaseModelStoreAction {
	/** The full object path used to store the result in the redux store under 'state.models'. Includes keys and ids. */
	modelPath: string
	/** The pre-generated GUID, from your application, that was provided to the `ModelFetchRequestAction`. */
	guid?: string
}

//#region Model Remove Key

export const MODEL_REMOVE_KEY_ACTION_TYPE = 'models/REMOVE_KEY'

/**
 * Action used to remove the model at the target redux `modelPath` key.
 */
export interface ModelRemoveKeyAction
	extends Action<typeof MODEL_REMOVE_KEY_ACTION_TYPE>,
		Omit<BaseModelStoreAction, 'guid'> {}

//#endregion Model Remove Key

//#region Model Fetch Start

export enum MODEL_FETCH_START_ACTION_TYPE {
	FETCH_START = 'models/FETCH_START',
	TRANSIENT_FETCH_START = 'models/TRANSIENT_FETCH_START'
}

/**
 * Action used to update the target redux `modelPath` `_metadata` when a fetch is about to start.
 */
export interface ModelFetchStartAction extends Action<MODEL_FETCH_START_ACTION_TYPE>, BaseModelStoreAction {}

//#endregion Model Fetch Start

//#region Model Fetch Result

export enum MODEL_FETCH_RESULT_ACTION_TYPE {
	FETCH_RESULT_RECEIVED = 'models/FETCH_RESULT_RECEIVED',
	TRANSIENT_FETCH_RESULT_RECEIVED = 'models/TRANSIENT_FETCH_RESULT_RECEIVED'
}

/**
 * Action used to update the target redux `modelPath` when a successful fetch result is received.
 */
export interface ModelFetchResultAction extends Action<MODEL_FETCH_RESULT_ACTION_TYPE>, BaseModelStoreAction {
	/** If true, the `modelsReducer` will replace the existing value in redux, instead of merging the incoming with existing. */
	replaceValue?: boolean
	/** The data returned from a request. */
	data?: any
}

export const isModelFetchResultAction = (action: AnyAction): action is ModelFetchResultAction =>
	action.type === MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED ||
	action.type === MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED

export const isTransientModelFetchResultAction = (action: AnyAction): action is ModelFetchResultAction =>
	action.type === MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED

//#endregion Model Fetch Result

//#region Model Fetch Error

export enum MODEL_FETCH_ERROR_ACTION_TYPE {
	FETCH_FAILED = 'models/FETCH_FAILED',
	TRANSIENT_FETCH_FAILED = 'models/TRANSIENT_FETCH_FAILED',
	TRY_FETCH_FAILED = 'models/TRY_FETCH_FAILED'
}

/**
 * Action used to update the target redux `modelPath` `_metadata` for a failed fetch.
 */
export interface ModelFetchErrorAction extends Action<MODEL_FETCH_ERROR_ACTION_TYPE>, BaseModelStoreAction {
	/** The error data returned from a failed request. */
	errorData?: FetchErrorData
}

export const isModelFetchErrorAction = (action: AnyAction): action is ModelFetchErrorAction =>
	action.type === MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED ||
	action.type === MODEL_FETCH_ERROR_ACTION_TYPE.FETCH_FAILED ||
	action.type === MODEL_FETCH_ERROR_ACTION_TYPE.TRANSIENT_FETCH_FAILED

export const isTransientModelFetchErrorAction = (action: AnyAction): action is ModelFetchErrorAction =>
	action.type === MODEL_FETCH_ERROR_ACTION_TYPE.TRANSIENT_FETCH_FAILED

export const isModelTryFetchErrorAction = (action: AnyAction): action is ModelFetchErrorAction =>
	action.type === MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED

//#endregion Model Fetch Error

/**
 * A redux action related to a model fetch request, result, error, or remove key action. To be used by `modelsReducer`.
 */
export type ModelStoreAction =
	| ModelFetchStartAction
	| ModelFetchResultAction
	| ModelFetchErrorAction
	| ModelRemoveKeyAction

//#endregion Model Store Actions
