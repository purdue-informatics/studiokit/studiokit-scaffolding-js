import { Action } from 'redux'
import { CasV1LoginRequestBody, IdentityProvider } from '../../types'
import { LocalLoginRequestBody } from '../../types/auth/LocalLoginRequestBody'
import { OAuthToken, OAuthTokenOrNull } from '../../types/net'

export enum AUTH_ACTION_TYPE {
	CAS_LOGIN_REQUESTED = 'auth/CAS_LOGIN_REQUESTED',
	LOGIN_REQUESTED = 'auth/LOGIN_REQUESTED',
	LOG_OUT_REQUESTED = 'auth/LOG_OUT_REQUESTED',
	TOKEN_REFRESH_FAILED = 'auth/TOKEN_REFRESH_FAILED',
	LOGIN_FAILED = 'auth/LOGIN_FAILED'
}

/**
 * Action with no additional properties.
 */
export interface AuthAction extends Action<AUTH_ACTION_TYPE> {}

export enum AUTH_TOKEN_ACTION_TYPE {
	AUTH_INITIALIZED = 'auth/INITIALIZED'
}

/**
 * Action with a nullable `oauthToken`.
 */
export interface AuthTokenAction extends Action<AUTH_TOKEN_ACTION_TYPE> {
	oauthToken: OAuthTokenOrNull
}

export enum AUTH_TOKEN_SUCCESS_ACTION_TYPE {
	GET_TOKEN_SUCCEEDED = 'auth/GET_TOKEN_SUCCEEDED',
	TOKEN_REFRESH_SUCCEEDED = 'auth/TOKEN_REFRESH_SUCCEEDED'
}

/**
 * Action with an `oauthToken`.
 */
export interface AuthTokenSuccessAction extends Action<AUTH_TOKEN_SUCCESS_ACTION_TYPE> {
	oauthToken: OAuthToken
}

export enum AUTH_IDENTITY_PROVIDER_REQUEST_ACTION_TYPE {
	IDENTITY_PROVIDER_LOGIN_REQUEST = 'auth/IDENTITY_PROVIDER_LOGIN_REQUEST'
}

export interface AuthIdentityProviderRequestAction extends Action<AUTH_IDENTITY_PROVIDER_REQUEST_ACTION_TYPE> {
	identityProvider: IdentityProvider
}

export enum AUTH_CAS_V1_LOGIN_REQUEST_ACTION_TYPE {
	CAS_V1_LOGIN_REQUEST = 'auth/CAS_V1_LOGIN_REQUEST'
}

export interface AuthCasV1LoginRequestAction extends Action<AUTH_CAS_V1_LOGIN_REQUEST_ACTION_TYPE> {
	body: CasV1LoginRequestBody
}

export enum AUTH_LOCAL_LOGIN_REQUEST_ACTION_TYPE {
	LOCAL_LOGIN_REQUEST = 'auth/LOCAL_LOGIN_REQUEST'
}

export interface AuthLocalLoginRequestAction extends Action<AUTH_LOCAL_LOGIN_REQUEST_ACTION_TYPE> {
	body: LocalLoginRequestBody
}
