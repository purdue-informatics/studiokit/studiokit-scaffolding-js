import * as Sentry from '@sentry/react'
import { Dictionary } from 'lodash'
import { SagaIterator } from 'redux-saga'
import { all, AllEffect, ForkEffect, takeEvery } from 'redux-saga/effects'
import { getAppConfig, getEndpointMappings } from '../../constants/configuration'
import { codeProviderService } from '../../services/codeProviderService'
import { persistenceService } from '../../services/persistenceService'
import { ticketProviderService } from '../../services/ticketProviderService'
import { ErrorHandler, FetchConfig, FetchErrorData, FetchResult } from '../../types'
import { AUTH_TOKEN_ACTION_TYPE } from '../actions/AuthAction'
import { ModelFetchRequestAction, PeriodicModelFetchRequestAction } from '../actions/ModelAction'
import appInsightsSaga from './appInsightsSaga'
import authSaga, { getOAuthToken } from './authSaga'
import caliperSaga from './caliperSaga'
import clockOffsetSaga from './clockOffsetSaga'
import configurationSaga from './configurationSaga'
import downtimeApiErrorSaga from './downtimeApiErrorSaga'
import errorSaga from './errorSaga'
import googleAnalyticsSaga from './googleAnalyticsSaga'
import identityProviderSaga from './identityProviderSaga'
import initialDataLoadSaga from './initialDataLoadSaga'
import lockDownBrowserErrorSaga from './lockDownBrowserErrorSaga'
import modelFetchSaga from './modelFetchSaga'
import noStoreSaga from './noStoreSaga'
import postLoginDataSaga from './postLoginDataSaga'
import postLoginRedirectSaga from './postLoginRedirectSaga'
import sentrySaga from './sentrySaga'
import userIdSaga from './userIdSaga'

//#region Setup

let otherDependentSagas: Dictionary<SagaIterator> = {}

export const setOtherDependentSagas = (value: Dictionary<SagaIterator>) => {
	otherDependentSagas = value
}

//#endregion Setup

function* dependentSagas() {
	yield all({
		noStoreSaga: noStoreSaga(),
		configurationSaga: configurationSaga(),
		caliperSaga: caliperSaga(),
		userIdSaga: userIdSaga(),
		sentrySaga: sentrySaga(),
		googleAnalyticsSaga: googleAnalyticsSaga(),
		appInsightsSaga: appInsightsSaga(),
		initialDataLoadSaga: initialDataLoadSaga(),
		identityProviderSaga: identityProviderSaga(),
		postLoginRedirectSaga: postLoginRedirectSaga(),
		postLoginDataSaga: postLoginDataSaga(),
		clockOffsetSaga: clockOffsetSaga(),
		lockDownBrowserErrorSaga: lockDownBrowserErrorSaga(),
		downtimeApiErrorSaga: downtimeApiErrorSaga(),
		...otherDependentSagas
	})
}

const errorHandler: ErrorHandler = (
	error: Error,
	modelFetchRequestAction: ModelFetchRequestAction | PeriodicModelFetchRequestAction,
	fetchConfig?: FetchConfig,
	fetchResult?: FetchResult,
	fetchErrorData?: FetchErrorData
) => {
	Sentry.captureException(error, {
		contexts: {
			'Fetch Action': { ...modelFetchRequestAction },
			'Fetch Config': (fetchConfig || {}) as Record<string, unknown>,
			'Fetch Result': (fetchResult || {}) as Record<string, unknown>,
			'Fetch Error Data': (fetchErrorData || {}) as Record<string, unknown>
		}
	})
}

export default function* rootSaga(): IterableIterator<ForkEffect | AllEffect<any>> {
	const appConfig = getAppConfig()
	const endpointMappings = getEndpointMappings()

	if (appConfig.IS_DOWNTIME) {
		return
	}

	// Don’t start the remaining sagas until net and auth are initialized
	// takeEvery is used because we wouldn’t want to block on `take`
	yield takeEvery(AUTH_TOKEN_ACTION_TYPE.AUTH_INITIALIZED, dependentSagas)
	yield all({
		errorSaga: errorSaga(),
		modelFetchSaga: modelFetchSaga(endpointMappings, appConfig.API_BASE_URL, getOAuthToken, errorHandler),
		authSaga: authSaga(
			{
				client_id: appConfig.CLIENT_ID,
				client_secret: appConfig.CLIENT_SECRET
			},
			persistenceService,
			ticketProviderService,
			codeProviderService
		)
	})
}
