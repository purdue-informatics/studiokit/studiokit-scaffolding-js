import { v4 as uuidv4 } from 'uuid'
import { internalServerErrorData } from '../../constants/mockData'
import { FetchErrorData } from '../../types'
import {
	MODEL_FETCH_ERROR_ACTION_TYPE,
	MODEL_FETCH_REQUEST_ACTION_TYPE,
	MODEL_FETCH_RESULT_ACTION_TYPE,
	ModelFetchErrorAction,
	ModelFetchRequestAction,
	ModelFetchResultAction,
	PeriodicModelFetchRequestAction
} from '../actions'
import {
	handleAction,
	matchesFailedNoStoreHookAction,
	matchesNoStoreAction,
	matchesReceivedNoStoreHookAction,
	registerNoStoreActionHook,
	takeMatchesFailedNoStoreHookAction,
	takeMatchesReceivedNoStoreHookAction,
	unregisterNoStoreActionHook
} from './noStoreSaga'

describe('noStoreSaga', () => {
	describe('helpers', () => {
		let guid: string
		let noStoreModelFetchRequestAction: ModelFetchRequestAction
		let modelFetchRequestAction: ModelFetchRequestAction
		let periodicModelFetchRequestAction: PeriodicModelFetchRequestAction
		let transientModelFetchErrorAction: ModelFetchErrorAction
		let otherTransientModelFetchErrorAction: ModelFetchErrorAction
		let transientModelFetchResultAction: ModelFetchResultAction
		let otherTransientModelFetchResultAction: ModelFetchResultAction

		beforeAll(() => {
			guid = uuidv4()
			noStoreModelFetchRequestAction = {
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				modelName: 'someModel',
				noStore: true,
				guid
			}
			modelFetchRequestAction = {
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				modelName: 'someModel'
			}
			periodicModelFetchRequestAction = {
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.PERIODIC_FETCH_REQUEST,
				modelName: 'someModel',
				noStore: true,
				period: 500,
				taskId: 'someModel-refresh'
			}

			transientModelFetchErrorAction = {
				type: MODEL_FETCH_ERROR_ACTION_TYPE.TRANSIENT_FETCH_FAILED,
				modelPath: 'someModel',
				guid
			}
			otherTransientModelFetchErrorAction = {
				type: MODEL_FETCH_ERROR_ACTION_TYPE.TRANSIENT_FETCH_FAILED,
				modelPath: 'someModel',
				guid: uuidv4()
			}

			transientModelFetchResultAction = {
				type: MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED,
				modelPath: 'someModel',
				guid
			}
			otherTransientModelFetchResultAction = {
				type: MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED,
				modelPath: 'someModel',
				guid: uuidv4()
			}
		})

		describe('matchesNoStoreAction', () => {
			it('matches correctly', () => {
				expect(matchesNoStoreAction(noStoreModelFetchRequestAction)).toEqual(true)
			})
			it('does not match when `noStore` is `false`', () => {
				expect(matchesNoStoreAction(modelFetchRequestAction)).toEqual(false)
			})
			it('does not match other action types', () => {
				expect(matchesNoStoreAction(periodicModelFetchRequestAction)).toEqual(false)
			})
		})

		describe('matchesFailedNoStoreHookAction', () => {
			it('matches failed action correctly', () => {
				expect(
					matchesFailedNoStoreHookAction(transientModelFetchErrorAction, noStoreModelFetchRequestAction)
				).toEqual(true)
			})
			it('does not match other action types', () => {
				expect(
					matchesFailedNoStoreHookAction(periodicModelFetchRequestAction, noStoreModelFetchRequestAction)
				).toEqual(false)
			})
			it('does not match other `guid`', () => {
				expect(
					matchesFailedNoStoreHookAction(otherTransientModelFetchErrorAction, noStoreModelFetchRequestAction)
				).toEqual(false)
			})
			it('should call matchesFailedNoStoreHookAction from takeMatchesFailedNoStoreHookAction', () => {
				expect(
					takeMatchesFailedNoStoreHookAction(noStoreModelFetchRequestAction)(transientModelFetchErrorAction)
				).toEqual(true)
			})
		})

		describe('matchesReceivedNoStoreHookAction', () => {
			it('matches correctly', () => {
				expect(
					matchesReceivedNoStoreHookAction(transientModelFetchResultAction, noStoreModelFetchRequestAction)
				).toEqual(true)
			})
			it('does not match other action types', () => {
				expect(
					matchesReceivedNoStoreHookAction(periodicModelFetchRequestAction, noStoreModelFetchRequestAction)
				).toEqual(false)
			})
			it('does not match other `guid`', () => {
				expect(
					matchesReceivedNoStoreHookAction(
						otherTransientModelFetchResultAction,
						noStoreModelFetchRequestAction
					)
				).toEqual(false)
			})
			it('should call matchesReceivedNoStoreHookAction from takeMatchesReceivedNoStoreHookAction', () => {
				expect(
					takeMatchesReceivedNoStoreHookAction(noStoreModelFetchRequestAction)(
						transientModelFetchResultAction
					)
				).toEqual(true)
			})
		})
	})

	describe('handleAction', () => {
		const firstKey = uuidv4()
		const secondKey = uuidv4()
		let hookCalled: string | undefined
		let hookData: any
		const firstHook = (data: string | FetchErrorData | null) => {
			hookCalled = 'first'
			hookData = data
		}
		const secondHook = (data: string | FetchErrorData | null) => {
			hookCalled = 'second'
			hookData = data
		}

		beforeEach(() => {
			registerNoStoreActionHook(firstKey, firstHook)
			registerNoStoreActionHook(secondKey, secondHook)
		})

		afterEach(() => {
			unregisterNoStoreActionHook(firstKey)
			unregisterNoStoreActionHook(secondKey)
			hookCalled = undefined
			hookData = undefined
		})

		it('should call correct hook with `data` on success', () => {
			const action: ModelFetchRequestAction = {
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				modelName: 'someModel',
				noStore: true,
				guid: firstKey
			}
			const gen = handleAction(action)
			gen.next() // raceEffect
			const sagaDone = gen.next({
				resultAction: {
					type: MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED,
					modelPath: action.modelName,
					guid: action.guid,
					data: 'blah'
				}
			})
			expect(sagaDone.done).toEqual(true)
			expect(hookCalled).toEqual('first')
			expect(hookData).toEqual('blah')
		})

		it('should call two different hooks with `data` on success', () => {
			const firstAction: ModelFetchRequestAction = {
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				modelName: 'someModel',
				noStore: true,
				guid: firstKey
			}
			const gen = handleAction(firstAction)
			gen.next() // raceEffect
			const sagaDone = gen.next({
				resultAction: {
					type: MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED,
					modelPath: firstAction.modelName,
					guid: firstAction.guid,
					data: 'blah'
				}
			})
			expect(sagaDone.done).toEqual(true)
			expect(hookCalled).toEqual('first')
			expect(hookData).toEqual('blah')

			const secondAction: ModelFetchRequestAction = {
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				modelName: 'someModel',
				noStore: true,
				guid: secondKey
			}
			const gen2 = handleAction(secondAction)
			gen2.next() // raceEffect2
			const sagaDone2 = gen2.next({
				resultAction: {
					type: MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED,
					modelPath: secondAction.modelName,
					guid: secondAction.guid,
					data: 'bloo'
				}
			})
			expect(sagaDone2.done).toEqual(true)
			expect(hookCalled).toEqual('second')
			expect(hookData).toEqual('bloo')
		})

		it('should call correct hook with `errorData` on failure', () => {
			const action: ModelFetchRequestAction = {
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				modelName: 'someModel',
				noStore: true,
				guid: firstKey
			}
			const gen = handleAction(action)
			gen.next() // raceEffect
			const sagaDone = gen.next({
				errorAction: {
					type: MODEL_FETCH_ERROR_ACTION_TYPE.TRANSIENT_FETCH_FAILED,
					modelPath: action.modelName,
					guid: action.guid,
					errorData: internalServerErrorData
				}
			})
			expect(sagaDone.done).toEqual(true)
			expect(hookCalled).toEqual('first')
			expect(hookData).toEqual(internalServerErrorData)
		})

		it('should call correct hook with `null` if there is no resultAction.data', () => {
			const action: ModelFetchRequestAction = {
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				modelName: 'someModel',
				noStore: true,
				guid: firstKey
			}
			const gen = handleAction(action)
			gen.next() // raceEffect
			const sagaDone = gen.next({
				resultAction: {
					type: MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED,
					modelPath: action.modelName,
					guid: action.guid,
					data: undefined
				}
			})
			expect(sagaDone.done).toEqual(true)
			expect(hookCalled).toEqual('first')
			expect(hookData).toEqual(null)
		})

		it('should finish if no `guid` on action', () => {
			const action: ModelFetchRequestAction = {
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				modelName: 'someModel',
				noStore: true
			}
			const gen = handleAction(action)
			const sagaDone = gen.next()
			expect(sagaDone.done).toEqual(true)
			expect(hookCalled).toEqual(undefined)
			expect(hookData).toEqual(undefined)
		})

		it('should finish if no hook found for action', () => {
			const action: ModelFetchRequestAction = {
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				modelName: 'someModel',
				noStore: true,
				guid: uuidv4()
			}
			const gen = handleAction(action)
			const sagaDone = gen.next()
			expect(sagaDone.done).toEqual(true)
			expect(hookCalled).toEqual(undefined)
			expect(hookData).toEqual(undefined)
		})

		it('should not call if hook is unregistered while requesting', () => {
			const action: ModelFetchRequestAction = {
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				modelName: 'someModel',
				noStore: true,
				guid: firstKey
			}
			const gen = handleAction(action)
			gen.next() // raceEffect
			unregisterNoStoreActionHook(firstKey)
			const sagaDone = gen.next({
				resultAction: {
					type: MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED,
					modelPath: action.modelName,
					guid: action.guid,
					data: 'blah'
				}
			})
			expect(sagaDone.done).toEqual(true)
			expect(hookCalled).toEqual(undefined)
			expect(hookData).toEqual(undefined)
		})
	})
})
