/* eslint-disable @typescript-eslint/no-unused-vars */

import { push } from 'connected-react-router'
import { call, put, take } from 'redux-saga/effects'
import { defaultLocation } from '../../constants/mockData'
import { persistenceService, returnUrlKey } from '../../services/persistenceService'
import { windowService } from '../../services/windowService'
import { SimpleLocation } from '../../types'
import { AUTH_ACTION_TYPE } from '../actions'
import postLoginRedirectSaga from './postLoginRedirectSaga'

const rootPathname = '/'
const coursePathname = '/courses/1'
const ltiLaunchPathname = '/lti-launch/2'

describe('postLoginRedirectSaga', () => {
	afterAll(() => {
		windowService.setLocation()
	})

	it('should not redirect if no `returnUrl`', () => {
		const courseLocation: SimpleLocation = { ...defaultLocation, pathname: coursePathname }
		windowService.setLocation(courseLocation)

		const gen = postLoginRedirectSaga()
		const authActionEffect = gen.next()
		const callGetItemEffect = gen.next()
		// no redirect, no item to remove, go directly to log out action
		const logOutEffect = gen.next(null)
		expect(logOutEffect.value).toEqual(take(AUTH_ACTION_TYPE.LOG_OUT_REQUESTED))
	})

	it('should not redirect if `returnUrl` and `pathname` match', () => {
		const courseLocation: SimpleLocation = { ...defaultLocation, pathname: coursePathname }
		windowService.setLocation(courseLocation)

		const gen = postLoginRedirectSaga()
		const authActionEffect = gen.next()
		const callGetItemEffect = gen.next()
		// no redirect, go directly to removeItem
		const callRemoveItemEffect = gen.next(coursePathname)
		expect(callRemoveItemEffect.value).toEqual(call(persistenceService.removeItem, returnUrlKey))
	})

	it('should redirect to course `returnUrl` when `pathname` is root (allow: root => non-root)', () => {
		windowService.setLocation(defaultLocation)

		const gen = postLoginRedirectSaga()
		const authActionEffect = gen.next()
		const callGetItemEffect = gen.next()
		const putRouterEffect = gen.next(coursePathname)
		expect(putRouterEffect.value).toEqual(put(push(coursePathname)))
		const callRemoveItemEffect = gen.next()
		expect(callRemoveItemEffect.value).toEqual(call(persistenceService.removeItem, returnUrlKey))
	})

	it('should not redirect to root `returnUrl` when `pathname` is at a course (deny: non-root => root)', () => {
		const courseLocation: SimpleLocation = { ...defaultLocation, pathname: coursePathname }
		windowService.setLocation(courseLocation)

		const gen = postLoginRedirectSaga()
		const authActionEffect = gen.next()
		const callGetItemEffect = gen.next()
		// no redirect, go directly to removeItem
		const callRemoveItemEffect = gen.next(rootPathname)
		expect(callRemoveItemEffect.value).toEqual(call(persistenceService.removeItem, returnUrlKey))
	})

	it('should not redirect to course `returnUrl` when `pathname` is at an ltiLaunch (deny: non-root => non-root)', () => {
		const courseLocation: SimpleLocation = { ...defaultLocation, pathname: ltiLaunchPathname }
		windowService.setLocation(courseLocation)

		const gen = postLoginRedirectSaga()
		const authActionEffect = gen.next()
		const callGetItemEffect = gen.next()
		// no redirect, go directly to removeItem
		const callRemoveItemEffect = gen.next(coursePathname)
		expect(callRemoveItemEffect.value).toEqual(call(persistenceService.removeItem, returnUrlKey))
	})
})
