/* eslint-disable @typescript-eslint/no-unused-vars */

import { SagaIterator } from '@redux-saga/core'
import { all, call, put, race, take, takeEvery } from 'redux-saga/effects'
import { badRequestErrorData, internalServerErrorData, unauthorizedErrorData } from '../../constants/mockData'
import { ticketProviderService as defaultTicketProviderService } from '../../services/ticketProviderService'
import { tokenPersistenceService as defaultTokenPersistenceService } from '../../services/tokenPersistenceService'
import {
	CasV1LoginRequestBody,
	FetchErrorData,
	OAuthToken,
	OAuthTokenOrNull,
	TokenPersistenceService
} from '../../types'
import { LocalLoginRequestBody } from '../../types/auth/LocalLoginRequestBody'
import {
	AUTH_ACTION_TYPE,
	AUTH_CAS_V1_LOGIN_REQUEST_ACTION_TYPE,
	AUTH_LOCAL_LOGIN_REQUEST_ACTION_TYPE,
	AUTH_TOKEN_ACTION_TYPE,
	AUTH_TOKEN_SUCCESS_ACTION_TYPE,
	AuthAction,
	AuthCasV1LoginRequestAction,
	AuthLocalLoginRequestAction,
	AuthTokenAction,
	AuthTokenSuccessAction,
	MODEL_FETCH_ERROR_ACTION_TYPE,
	MODEL_FETCH_REQUEST_ACTION_TYPE,
	MODEL_FETCH_RESULT_ACTION_TYPE,
	MODEL_REMOVE_KEY_ACTION_TYPE,
	ModelFetchErrorAction,
	ModelFetchRequestAction,
	ModelFetchResultAction,
	ModelRemoveKeyAction
} from '../actions'
import authSaga, {
	casTicketLoginFlow,
	casV1LoginFlow,
	credentialsLoginFlow,
	getOAuthToken,
	getTokenFromCode,
	getTokenFromRefreshToken,
	handleAuthFailure,
	localLoginFlow,
	loginFlow,
	matchesModelFetchFailed,
	matchesModelFetchReceived,
	performTokenRefresh,
	takeMatchesModelFetchFailed,
	takeMatchesModelFetchReceived
} from './authSaga'

const clientCredentials = { client_id: 'test', client_secret: 'secret' }

const sampleOAuthToken: OAuthToken = {
	access_token: 'some-access-token',
	refresh_token: 'some-refresh-token',
	client_id: 'web',
	token_type: 'Bearer',
	expires_in: 3600,
	'.expires': '2019-01-02',
	'.issued': '2019-01-01'
}

const casV1RequestBody: CasV1LoginRequestBody = {
	username: 'some-user',
	password: '*****'
}

const casV1Action: AuthCasV1LoginRequestAction = {
	type: AUTH_CAS_V1_LOGIN_REQUEST_ACTION_TYPE.CAS_V1_LOGIN_REQUEST,
	body: casV1RequestBody
}

const localLoginRequestBody: LocalLoginRequestBody = {
	username: 'some-user',
	isInstructor: true
}

const localLoginAction: AuthLocalLoginRequestAction = {
	type: AUTH_LOCAL_LOGIN_REQUEST_ACTION_TYPE.LOCAL_LOGIN_REQUEST,
	body: localLoginRequestBody
}

describe('helpers', () => {
	const modelName = 'someModel'
	let modelFetchResultAction: ModelFetchResultAction
	let modelFetchErrorAction: ModelFetchErrorAction

	beforeEach(() => {
		modelFetchResultAction = {
			type: MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED,
			modelPath: modelName
		}
		modelFetchErrorAction = {
			type: MODEL_FETCH_ERROR_ACTION_TYPE.TRANSIENT_FETCH_FAILED,
			modelPath: modelName
		}
	})

	it('matchesModelFetchReceived matches by modelName', () => {
		expect(matchesModelFetchReceived(modelFetchResultAction, modelName)).toEqual(true)
	})
	it('matchesModelFetchReceived ignores other action types for modelName', () => {
		expect(matchesModelFetchReceived(modelFetchErrorAction, modelName)).toEqual(false)
	})
	it('matchesModelFetchReceived ignores other modelNames', () => {
		modelFetchResultAction.modelPath = 'someOtherModel'
		expect(matchesModelFetchReceived(modelFetchResultAction, modelName)).toEqual(false)
	})
	it('should call matchesModelFetchReceived from takeMatchesModelFetchReceived', () => {
		expect(takeMatchesModelFetchReceived(modelName)(modelFetchResultAction)).toEqual(true)
	})

	it('matchesModelFetchFailed matches by modelName', () => {
		expect(matchesModelFetchFailed(modelFetchErrorAction, modelName)).toEqual(true)
	})
	it('matchesModelFetchFailed ignores other action types for modelName', () => {
		expect(matchesModelFetchFailed(modelFetchResultAction, modelName)).toEqual(false)
	})
	it('matchesModelFetchFailed ignores other modelNames', () => {
		modelFetchErrorAction.modelPath = 'someOtherModel'
		expect(matchesModelFetchFailed(modelFetchErrorAction, modelName)).toEqual(false)
	})
	it('should call matchesModelFetchFailed from takeMatchesModelFetchFailed', () => {
		expect(takeMatchesModelFetchFailed(modelName)(modelFetchErrorAction)).toEqual(true)
	})
})

describe('getTokenFromCode', () => {
	let authSagaGen: SagaIterator
	beforeEach(() => {
		authSagaGen = authSaga(clientCredentials)
		const callGetPersistedTokenEffect = authSagaGen.next()
		const putAuthInitializedEffect = authSagaGen.next(undefined)
	})

	it('should put FETCH_REQUEST with modelName and body string from clientCredentials and code', () => {
		const gen = getTokenFromCode('some-code')
		const putDataRequestedEffect = gen.next()
		expect(putDataRequestedEffect.value).toEqual(
			put<ModelFetchRequestAction>({
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				modelName: 'getToken',
				body: 'grant_type=authorization_code&client_id=test&client_secret=secret&code=some-code',
				noStore: true
			})
		)
	})

	it('should return null and finish if fetch fails', () => {
		const gen = getTokenFromCode('some-code')
		const putDataRequestedEffect = gen.next()
		const raceFetchEffect = gen.next()
		const modelFetchErrorAction: ModelFetchErrorAction = {
			type: MODEL_FETCH_ERROR_ACTION_TYPE.TRANSIENT_FETCH_FAILED,
			modelPath: 'getToken'
		}
		const fetchFailedEffect = gen.next({
			fetchErrorAction: modelFetchErrorAction
		})
		expect(fetchFailedEffect.value).toEqual(null)
		const sagaDone = gen.next()
		expect(sagaDone.done).toEqual(true)
	})

	it('should return null and finish if fetch returns no data', () => {
		const gen = getTokenFromCode('some-code')
		const putDataRequestedEffect = gen.next()
		const raceFetchEffect = gen.next()
		const modelFetchResultAction: ModelFetchResultAction = {
			type: MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED,
			modelPath: 'getToken'
		}
		const fetchReceivedEffect = gen.next({
			fetchResultAction: modelFetchResultAction
		})
		expect(fetchReceivedEffect.value).toEqual(null)
		const sagaDone = gen.next()
		expect(sagaDone.done).toEqual(true)
	})

	it('should return data and finish if fetch returns successfully', () => {
		const gen = getTokenFromCode('some-code')
		const putDataRequestedEffect = gen.next()
		const raceFetchEffect = gen.next()
		const modelFetchResultAction: ModelFetchResultAction = {
			type: MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED,
			modelPath: 'getToken',
			data: {
				foo: 'bar'
			}
		}
		const fetchReceivedEffect = gen.next({
			fetchResultAction: modelFetchResultAction
		})
		expect(fetchReceivedEffect.value).toEqual(modelFetchResultAction.data)
		const sagaDone = gen.next()
		expect(sagaDone.done).toEqual(true)
	})
})

describe('getTokenFromRefreshToken', () => {
	let authSagaGen: SagaIterator
	beforeEach(() => {
		authSagaGen = authSaga(clientCredentials)
		const callGetPersistedTokenEffect = authSagaGen.next()
		const putAuthInitializedEffect = authSagaGen.next(undefined)
	})

	it('should put FETCH_REQUEST with modelName and body string from clientCredentials and code', () => {
		const gen = getTokenFromRefreshToken(sampleOAuthToken)
		const putDataRequestedEffect = gen.next()
		expect(putDataRequestedEffect.value).toEqual(
			put<ModelFetchRequestAction>({
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				modelName: 'getToken',
				body: 'grant_type=refresh_token&client_id=test&client_secret=secret&refresh_token=some-refresh-token',
				noStore: true
			})
		)
	})

	it('should return null and finish if fetch fails', () => {
		const gen = getTokenFromRefreshToken(sampleOAuthToken)
		const putDataRequestedEffect = gen.next()
		const raceFetchEffect = gen.next()
		const modelFetchErrorAction: ModelFetchErrorAction = {
			type: MODEL_FETCH_ERROR_ACTION_TYPE.TRANSIENT_FETCH_FAILED,
			modelPath: 'getToken'
		}
		const fetchFailedEffect = gen.next({
			fetchErrorAction: modelFetchErrorAction
		})
		expect(fetchFailedEffect.value).toEqual(null)
		const sagaDone = gen.next()
		expect(sagaDone.done).toEqual(true)
	})

	it('should return null and finish if fetch returns no data', () => {
		const gen = getTokenFromRefreshToken(sampleOAuthToken)
		const putDataRequestedEffect = gen.next()
		const raceFetchEffect = gen.next()
		const modelFetchResultAction: ModelFetchResultAction = {
			type: MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED,
			modelPath: 'getToken'
		}
		const fetchReceivedEffect = gen.next({
			fetchResultAction: modelFetchResultAction
		})
		expect(fetchReceivedEffect.value).toEqual(null)
		const sagaDone = gen.next()
		expect(sagaDone.done).toEqual(true)
	})

	it('should return data and finish if fetch returns successfully', () => {
		const gen = getTokenFromRefreshToken(sampleOAuthToken)
		const putDataRequestedEffect = gen.next()
		const raceFetchEffect = gen.next()
		const modelFetchResultAction: ModelFetchResultAction = {
			type: MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED,
			modelPath: 'getToken',
			data: {
				foo: 'bar'
			}
		}
		const fetchReceivedEffect = gen.next({
			fetchResultAction: modelFetchResultAction
		})
		expect(fetchReceivedEffect.value).toEqual({
			foo: 'bar'
		})
		const sagaDone = gen.next()
		expect(sagaDone.done).toEqual(true)
	})

	it('should return original oauthToken if fetch fails with a server error', () => {
		const gen = getTokenFromRefreshToken(sampleOAuthToken)
		const putDataRequestedEffect = gen.next()
		const raceFetchEffect = gen.next()
		const modelFetchErrorAction: ModelFetchErrorAction = {
			type: MODEL_FETCH_ERROR_ACTION_TYPE.TRANSIENT_FETCH_FAILED,
			modelPath: 'getToken',
			errorData: internalServerErrorData
		}
		const fetchFailedEffect = gen.next({
			fetchErrorAction: modelFetchErrorAction
		})
		expect(fetchFailedEffect.value).toEqual(sampleOAuthToken)
		const sagaDone = gen.next()
		expect(sagaDone.done).toEqual(true)
	})
})

describe('performTokenRefresh', () => {
	let authSagaGen: SagaIterator
	beforeEach(() => {
		defaultTokenPersistenceService.persistToken(null)
		authSagaGen = authSaga(clientCredentials, defaultTokenPersistenceService)
		const callGetPersistedTokenEffect = authSagaGen.next()
		const putAuthInitializedEffect = authSagaGen.next(sampleOAuthToken)
	})

	it('should set refreshLock to true and call getTokenFromRefreshToken', () => {
		const gen = performTokenRefresh()
		const callGetTokenFromRefreshTokenEffect = gen.next()
		// locked
		expect(callGetTokenFromRefreshTokenEffect.value).toEqual(call(getTokenFromRefreshToken, sampleOAuthToken))
		const allSuccessActions = gen.next(sampleOAuthToken)
		const sagaDone = gen.next()
		expect(sagaDone.done).toEqual(true)
	})

	it('should return race condition if refreshLock is already true', () => {
		const gen = performTokenRefresh()
		const callGetTokenFromRefreshTokenEffect = gen.next()
		const gen2 = performTokenRefresh()
		const raceEffect = gen2.next()

		// anonymous function comparison seems to break this test, possibly from ts-jest
		// expect(raceEffect.value).toEqual(
		// 	race({
		// 		refreshSuccess: take(takeMatchesTokenRefreshSucceeded()),
		// 		refreshFailed: take(takeMatchesTokenRefreshFailed()),
		// 	})
		// )

		const allSuccessActions = gen.next(sampleOAuthToken)
		const sagaDone = gen.next()
		expect(sagaDone.done).toEqual(true)

		const saga2Done = gen2.next()
		expect(saga2Done.done).toEqual(true)
	})

	it('should allow refresh again after first refresh is finished', () => {
		// first refresh - start
		const gen = performTokenRefresh()
		const callGetTokenFromRefreshTokenEffect = gen.next()

		// first refresh - finish
		const allSuccessActions = gen.next(sampleOAuthToken)
		const sagaDone = gen.next()
		expect(sagaDone.done).toEqual(true)

		// unlocked

		// second refresh - success
		const gen2 = performTokenRefresh()
		const callGetTokenFromRefreshTokenEffect2 = gen2.next()
		expect(callGetTokenFromRefreshTokenEffect2.value).toEqual(call(getTokenFromRefreshToken, sampleOAuthToken))
		const allSuccessActions2 = gen2.next(sampleOAuthToken)
		const sagaDone2 = gen2.next()
		expect(sagaDone2.done).toEqual(true)
	})

	it('should call success effects if refresh succeeds', () => {
		const gen = performTokenRefresh()
		const callGetTokenFromRefreshTokenEffect = gen.next()
		const newAccessToken = {
			...sampleOAuthToken,
			...{
				access_token: 'some-new-token'
			}
		}
		const callPersistTokenEffect = gen.next(newAccessToken)
		expect(callPersistTokenEffect.value).toEqual(call(defaultTokenPersistenceService.persistToken, newAccessToken))
		const putRefreshSuccessEffect = gen.next()
		expect(putRefreshSuccessEffect.value).toEqual(
			put<AuthTokenSuccessAction>({
				type: AUTH_TOKEN_SUCCESS_ACTION_TYPE.TOKEN_REFRESH_SUCCEEDED,
				oauthToken: newAccessToken
			})
		)
		const sagaDone = gen.next()
		expect(sagaDone.done).toEqual(true)
	})

	it('should not change oauthToken if it did not change due to failure', () => {
		const gen = performTokenRefresh()
		const callGetTokenFromRefreshTokenEffect = gen.next()
		const allSuccessActions = gen.next(sampleOAuthToken)
		const sagaDone = gen.next()
		expect(sagaDone.done).toEqual(true)
	})

	it('should call all failure effects if refresh fails', () => {
		const gen = performTokenRefresh()
		const callGetTokenFromRefreshTokenEffect = gen.next()
		const allFailureActions = gen.next(null)
		expect(allFailureActions.value).toEqual(
			all({
				refreshFailed: put<AuthAction>({ type: AUTH_ACTION_TYPE.TOKEN_REFRESH_FAILED }),
				logOut: put<AuthAction>({ type: AUTH_ACTION_TYPE.LOG_OUT_REQUESTED })
			})
		)
		const sagaDone = gen.next()
		expect(sagaDone.done).toEqual(true)
	})

	it('defaultTokenPersistenceService.persistToken should update token', () => {
		let currentToken = defaultTokenPersistenceService.getPersistedToken()
		expect(currentToken).toEqual(null)
		defaultTokenPersistenceService.persistToken(sampleOAuthToken)
		currentToken = defaultTokenPersistenceService.getPersistedToken()
		expect(currentToken).toEqual(sampleOAuthToken)
	})
})

describe('loginFlow', () => {
	const credentials = {
		Username: 'username',
		Password: 'password'
	}
	const modelName = 'some-model'
	const modelFetchRequestAction: ModelFetchRequestAction = {
		type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
		modelName,
		noStore: true,
		body: credentials
	}

	let authSagaGen: SagaIterator
	beforeEach(() => {
		authSagaGen = authSaga(clientCredentials)
		const callGetPersistedTokenEffect = authSagaGen.next()
		const putAuthInitializedEffect = authSagaGen.next(undefined)
	})

	it('should put modelFetchRequestAction', () => {
		const gen = loginFlow(modelFetchRequestAction)
		const putDataRequestedEffect = gen.next()
		expect(putDataRequestedEffect.value).toEqual(put(modelFetchRequestAction))
	})

	it('should return null and finish if loginFailed', () => {
		const gen = loginFlow(modelFetchRequestAction)
		const putDataRequestedEffect = gen.next()
		const raceFetchResultEffect = gen.next()
		const modelFetchErrorAction: ModelFetchErrorAction = {
			type: MODEL_FETCH_ERROR_ACTION_TYPE.TRANSIENT_FETCH_FAILED,
			modelPath: modelName
		}
		const sagaDone = gen.next({
			fetchErrorAction: modelFetchErrorAction
		})
		expect(sagaDone.value).toEqual(null)
		expect(sagaDone.done).toEqual(true)
	})

	it('should call getTokenFromCode if action.data.code exists', () => {
		const gen = loginFlow(modelFetchRequestAction)
		const putDataRequestedEffect = gen.next()
		const raceFetchResultEffect = gen.next()
		const modelFetchResultAction: ModelFetchResultAction = {
			type: MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED,
			modelPath: modelName,
			data: { code: 'some-code' }
		}
		const fetchReceivedEffect = gen.next({
			fetchResultAction: modelFetchResultAction
		})
		expect(fetchReceivedEffect.value).toEqual(call(getTokenFromCode, 'some-code'))
		const sagaDone = gen.next()
		expect(sagaDone.done).toEqual(true)
	})

	it('should return null and finish if successful fetch result has no data', () => {
		const gen = loginFlow(modelFetchRequestAction)
		const putDataRequestedEffect = gen.next()
		const raceFetchResultEffect = gen.next()
		const modelFetchResultAction: ModelFetchResultAction = {
			type: MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED,
			modelPath: modelName
		}
		const sagaDone = gen.next({
			fetchResultAction: modelFetchResultAction
		})
		expect(sagaDone.value).toEqual(null)
		expect(sagaDone.done).toEqual(true)
	})

	describe('casTicketLoginFlow', () => {
		it('should call loginFlow with given ticket and service as query params', () => {
			const gen = casTicketLoginFlow('some-ticket', 'some-service')
			const callLoginFlowEffect = gen.next()
			expect(callLoginFlowEffect.value).toEqual(
				call(loginFlow, {
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					modelName: 'codeFromCasTicket',
					noStore: true,
					queryParams: {
						ticket: 'some-ticket',
						service: 'some-service'
					}
				})
			)
			const sagaDone = gen.next()
			expect(sagaDone.done).toEqual(true)
		})
	})

	describe('credentialsLoginFlow', () => {
		it('should call loginFlow with given action properties, plus required defaults', () => {
			const gen = credentialsLoginFlow(casV1Action, 'some-model')
			const callLoginFlowEffect = gen.next()
			expect(callLoginFlowEffect.value).toEqual(
				call(loginFlow, {
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					modelName: 'some-model',
					noStore: true,
					noRetry: true,
					// passed thru
					body: casV1RequestBody
				})
			)
			const sagaDone = gen.next()
			expect(sagaDone.done).toEqual(true)
		})
	})

	describe('casV1LoginFlow', () => {
		it('should call credentialsLoginFlow with given action', () => {
			const gen = casV1LoginFlow(casV1Action)
			const callCredentialsLoginFlowEffect = gen.next()
			expect(callCredentialsLoginFlowEffect.value).toEqual(
				call(credentialsLoginFlow, casV1Action, 'codeFromCasV1')
			)
			const sagaDone = gen.next()
			expect(sagaDone.done).toEqual(true)
		})
	})

	describe('localLoginFlow', () => {
		it('should call credentialsLoginFlow with given action', () => {
			const gen = localLoginFlow(localLoginAction)
			const callCredentialsLoginFlowEffect = gen.next()
			expect(callCredentialsLoginFlowEffect.value).toEqual(
				call(credentialsLoginFlow, localLoginAction, 'codeFromLocalCredentials')
			)
			const sagaDone = gen.next()
			expect(sagaDone.done).toEqual(true)
		})
	})
})

describe('handleAuthFailure', () => {
	it('does nothing if authSaga has no token', () => {
		const authSagaGen = authSaga(clientCredentials)
		const callGetPersistedTokenEffect = authSagaGen.next()
		const putAuthInitializedEffect = authSagaGen.next(undefined)

		const gen = handleAuthFailure({
			type: MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED,
			modelPath: 'groups',
			errorData: unauthorizedErrorData
		})
		const sagaDone = gen.next()
		expect(sagaDone.value).toEqual(undefined)
		expect(sagaDone.done).toEqual(true)
	})

	it('does nothing if error code is not Unauthorized (401)', () => {
		const expiredDate = new Date()
		expiredDate.setMinutes(expiredDate.getMinutes() - 1)
		const oauthToken = {
			...sampleOAuthToken,
			...{
				access_token: 'some-access-token',
				'.expires': expiredDate.toISOString()
			}
		}
		const authSagaGen = authSaga(clientCredentials)
		const callGetPersistedTokenEffect = authSagaGen.next()
		const putAuthInitializedEffect = authSagaGen.next(oauthToken)

		const gen = handleAuthFailure({
			type: MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED,
			modelPath: 'groups',
			errorData: badRequestErrorData
		})
		const sagaDone = gen.next()
		expect(sagaDone.value).toEqual(undefined)
		expect(sagaDone.done).toEqual(true)
	})

	it('triggers refresh if token is expired and code is Unauthorized (401)', () => {
		const expiredDate = new Date()
		expiredDate.setMinutes(expiredDate.getMinutes() - 1)
		const oauthToken = {
			...sampleOAuthToken,
			...{
				access_token: 'some-access-token',
				'.expires': expiredDate.toISOString()
			}
		}
		const authSagaGen = authSaga(clientCredentials)
		const callGetPersistedTokenEffect = authSagaGen.next()
		const putAuthInitializedEffect = authSagaGen.next(oauthToken)

		const gen = handleAuthFailure({
			type: MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED,
			modelPath: 'groups',
			errorData: unauthorizedErrorData
		})
		const callPerformTokenRefreshEffect = gen.next()
		expect(callPerformTokenRefreshEffect.value).toEqual(call(performTokenRefresh))
		const sagaDone = gen.next()
		expect(sagaDone.done).toEqual(true)
	})
})

describe('getOAuthToken', () => {
	it('should return null if modelName equals "getToken"', () => {
		const gen = getOAuthToken('getToken')
		const token = gen.next()
		expect(token.value).toEqual(null)
	})

	it('should return oauthToken if authSaga has one', () => {
		const oauthToken = { access_token: 'some-access-token' }
		const authSagaGen = authSaga(clientCredentials)
		const callGetPersistedTokenEffect = authSagaGen.next()
		const putAuthInitializedEffect = authSagaGen.next(oauthToken)

		const gen = getOAuthToken('someModelName')
		const token = gen.next()
		expect(token.value).toEqual(oauthToken)
	})

	it('should return oauthToken if has ".expires", but is not within 30 seconds of expiration', () => {
		const notExpiredDate = new Date()
		notExpiredDate.setMinutes(notExpiredDate.getMinutes() + 1)
		const oauthToken = {
			access_token: 'some-access-token',
			'.expires': notExpiredDate.toISOString()
		}
		const authSagaGen = authSaga(clientCredentials)
		const callGetPersistedTokenEffect = authSagaGen.next()
		const putAuthInitializedEffect = authSagaGen.next(oauthToken)

		const gen = getOAuthToken('someModelName')
		const token = gen.next()
		expect(token.value).toEqual(oauthToken)
	})

	it('should trigger refresh of oauthToken if has ".expires" and is expired', () => {
		const expiresNowDate = new Date()
		const oauthToken = {
			access_token: 'some-access-token',
			'.expires': expiresNowDate.toISOString()
		}
		const authSagaGen = authSaga(clientCredentials)
		const callGetPersistedTokenEffect = authSagaGen.next()
		const putAuthInitializedEffect = authSagaGen.next(oauthToken)

		const gen = getOAuthToken('someModelName')
		const callPerformRefreshEffect = gen.next()
		expect(callPerformRefreshEffect.value).toEqual(call(performTokenRefresh))
	})

	it('should trigger refresh of oauthToken if has ".expires" and is within 30 seconds of expiration', () => {
		const almostExpiredDate = new Date()
		almostExpiredDate.setSeconds(almostExpiredDate.getSeconds() + 15)
		const oauthToken = {
			access_token: 'some-access-token',
			'.expires': almostExpiredDate.toISOString()
		}
		const authSagaGen = authSaga(clientCredentials)
		const callGetPersistedTokenEffect = authSagaGen.next()
		const putAuthInitializedEffect = authSagaGen.next(oauthToken)

		const gen = getOAuthToken('someModelName')
		const callPerformRefreshEffect = gen.next()
		expect(callPerformRefreshEffect.value).toEqual(call(performTokenRefresh))
	})

	it('should return token if refresh succeeds', () => {
		const almostExpiredDate = new Date()
		almostExpiredDate.setSeconds(almostExpiredDate.getSeconds() + 15)
		const oauthToken = {
			...sampleOAuthToken,
			...{
				access_token: 'some-access-token',
				'.expires': almostExpiredDate.toISOString()
			}
		}
		const authSagaGen = authSaga(clientCredentials)
		const callGetPersistedTokenEffect = authSagaGen.next()
		const putAuthInitializedEffect = authSagaGen.next(oauthToken)

		const gen = getOAuthToken('someModelName')
		const callPerformRefreshEffect = gen.next()
		const sagaDone = gen.next()
		expect(sagaDone.value).toEqual(oauthToken)
		expect(sagaDone.done).toEqual(true)
	})
})

describe('authSaga', () => {
	beforeEach(() => {
		defaultTokenPersistenceService.persistToken(null)
	})
	describe('init', () => {
		it('calls tokenPersistenceService.getPersistedToken to load oauthToken', () => {
			let storedToken: OAuthTokenOrNull = sampleOAuthToken
			const tokenPersistenceService: TokenPersistenceService = {
				getPersistedToken: () => {
					return storedToken
				},
				persistToken: token => {
					storedToken = token
				}
			}
			const gen = authSaga(clientCredentials, tokenPersistenceService)
			const callGetPersistedTokenEffect = gen.next()
			expect(callGetPersistedTokenEffect.value).toEqual(call(tokenPersistenceService.getPersistedToken))
			const putAuthInitializedEffect = gen.next(storedToken)
			expect(putAuthInitializedEffect.value).toEqual(
				put<AuthTokenAction>({ type: AUTH_TOKEN_ACTION_TYPE.AUTH_INITIALIZED, oauthToken: storedToken })
			)
		})

		it('defaultTokenPersistenceService.getPersistedToken does not return a token', () => {
			const gen = authSaga(clientCredentials, defaultTokenPersistenceService)
			const callGetPersistedTokenEffect = gen.next()
			expect(callGetPersistedTokenEffect.value).toEqual(call(defaultTokenPersistenceService.getPersistedToken))
			const putAuthInitializedEffect = gen.next(defaultTokenPersistenceService.getPersistedToken())
			expect(putAuthInitializedEffect.value).toEqual(
				put<AuthTokenAction>({ type: AUTH_TOKEN_ACTION_TYPE.AUTH_INITIALIZED, oauthToken: null })
			)
		})

		describe('casTicket init (no oauthToken)', () => {
			it('calls casTicketLoginFlow if ticketProviderService returns a ticket, then removes the ticket', () => {
				let didRemoveTicket = false
				const ticketProviderService = {
					getTicket: () => 'some-ticket',
					getAppServiceName: () => 'http://localhost:3000/',
					removeTicket: () => {
						didRemoveTicket = true
					}
				}
				const gen = authSaga(clientCredentials, defaultTokenPersistenceService, ticketProviderService)
				const callGetPersistedTokenEffect = gen.next()
				const casTicketLoginFlowEffect = gen.next(null)
				expect(casTicketLoginFlowEffect.value).toEqual(
					call(casTicketLoginFlow, 'some-ticket', 'http://localhost:3000/')
				)
				expect(didRemoveTicket).toEqual(true)
				const putAuthInitializedEffect = gen.next(null)
				expect(putAuthInitializedEffect.value).toEqual(
					put<AuthTokenAction>({ type: AUTH_TOKEN_ACTION_TYPE.AUTH_INITIALIZED, oauthToken: null })
				)
			})

			it('does not call casTicketLoginFlow if ticketProviderService does not return a ticket', () => {
				let didRemoveTicket = false
				const ticketProviderService = {
					getTicket: () => undefined,
					getAppServiceName: () => 'http://localhost:3000/',
					removeTicket: () => {
						didRemoveTicket = true
					}
				}
				const gen = authSaga(clientCredentials, defaultTokenPersistenceService, ticketProviderService)
				const callGetPersistedTokenEffect = gen.next()
				const putAuthInitializedEffect = gen.next(null)
				expect(putAuthInitializedEffect.value).toEqual(
					put<AuthTokenAction>({ type: AUTH_TOKEN_ACTION_TYPE.AUTH_INITIALIZED, oauthToken: null })
				)
			})
		})

		describe('code init', () => {
			it('with no oauthToken, if codeProviderService returns a code, calls getTokenFromCode, then removes the code', () => {
				let didRemoveCode = false
				const codeProviderService = {
					getCode: () => 'some-code',
					removeCode: () => {
						didRemoveCode = true
					}
				}
				const gen = authSaga(
					clientCredentials,
					defaultTokenPersistenceService,
					defaultTicketProviderService,
					codeProviderService
				)
				const callGetPersistedTokenEffect = gen.next()
				const codeLoginFlowEffect = gen.next()
				expect(codeLoginFlowEffect.value).toEqual(call(getTokenFromCode, 'some-code'))
				const putAuthInitializedEffect = gen.next(null)
				expect(didRemoveCode).toEqual(true)
				expect(putAuthInitializedEffect.value).toEqual(
					put<AuthTokenAction>({ type: AUTH_TOKEN_ACTION_TYPE.AUTH_INITIALIZED, oauthToken: null })
				)
			})

			it('with oauthToken, if codeProviderService returns a code, clears token, then calls getTokenFromCode, then removes the code', () => {
				defaultTokenPersistenceService.persistToken(sampleOAuthToken)

				let didRemoveCode = false
				const codeProviderService = {
					getCode: () => 'some-code',
					removeCode: () => {
						didRemoveCode = true
					}
				}

				const gen = authSaga(
					clientCredentials,
					defaultTokenPersistenceService,
					defaultTicketProviderService,
					codeProviderService
				)
				const callGetPersistedTokenEffect = gen.next()
				const allClearDataEffect = gen.next(sampleOAuthToken)
				expect(allClearDataEffect.value).toEqual(
					all({
						clearUserData: put<ModelRemoveKeyAction>({
							type: MODEL_REMOVE_KEY_ACTION_TYPE,
							modelPath: 'user'
						}),
						clearPersistentToken: call(defaultTokenPersistenceService.persistToken, null)
					})
				)
				const codeLoginFlowEffect = gen.next()
				expect(codeLoginFlowEffect.value).toEqual(call(getTokenFromCode, 'some-code'))
				const putAuthInitializedEffect = gen.next(null)
				expect(didRemoveCode).toEqual(true)
				expect(putAuthInitializedEffect.value).toEqual(
					put<AuthTokenAction>({ type: AUTH_TOKEN_ACTION_TYPE.AUTH_INITIALIZED, oauthToken: null })
				)
			})

			it('does not call getTokenFromCode if codeProviderService does not return a code', () => {
				let didRemoveCode = false
				const codeProviderService = {
					getCode: () => undefined,
					removeCode: () => {
						didRemoveCode = true
					}
				}
				const gen = authSaga(
					clientCredentials,
					defaultTokenPersistenceService,
					defaultTicketProviderService,
					codeProviderService
				)
				const callGetPersistedTokenEffect = gen.next()
				const putAuthInitializedEffect = gen.next(null)
				expect(putAuthInitializedEffect.value).toEqual(
					put<AuthTokenAction>({ type: AUTH_TOKEN_ACTION_TYPE.AUTH_INITIALIZED, oauthToken: null })
				)
			})
		})

		describe('init auth error handling', () => {
			it('takeEvery failure to handleAuthFailure', () => {
				const gen = authSaga(clientCredentials)
				const callGetPersistedTokenEffect = gen.next()
				const putAuthInitializedEffect = gen.next(null)
				const takeEveryFetchFailureEffect = gen.next()
				expect(takeEveryFetchFailureEffect.value).toEqual(
					takeEvery(MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED, handleAuthFailure)
				)
			})
		})
	})

	describe('run loop', () => {
		describe('no oauthToken', () => {
			let gen: SagaIterator
			beforeEach(() => {
				gen = authSaga(clientCredentials)
				const callGetPersistedTokenEffect = gen.next()
				const putAuthInitializedEffect = gen.next(undefined)
				const takeEveryFetchFailureEffect = gen.next()
			})

			it('race condition for login actions', () => {
				const raceLoginActionEffect = gen.next()
				expect(raceLoginActionEffect.value).toEqual(
					race({
						casV1Action: take(AUTH_CAS_V1_LOGIN_REQUEST_ACTION_TYPE.CAS_V1_LOGIN_REQUEST),
						localLoginAction: take(AUTH_LOCAL_LOGIN_REQUEST_ACTION_TYPE.LOCAL_LOGIN_REQUEST)
					})
				)
			})

			it('puts LOGIN_REQUESTED after race resolves', () => {
				const raceLoginActionEffect = gen.next()
				const putLoginRequestedEffect = gen.next({
					casV1Action,
					localLoginAction: null
				})
				expect(putLoginRequestedEffect.value).toEqual(
					put<AuthAction>({ type: AUTH_ACTION_TYPE.LOGIN_REQUESTED })
				)
			})

			it('calls casV1Action if won race', () => {
				const raceLoginActionEffect = gen.next()
				const putLoginRequestedEffect = gen.next({
					casV1Action,
					localLoginAction: null
				})
				const callActionEffect = gen.next()
				expect(callActionEffect.value).toEqual(call(casV1LoginFlow, casV1Action))
				const callPersistTokenEffect = gen.next(sampleOAuthToken)
			})

			it('calls localLoginFlow if won race', () => {
				const raceLoginActionEffect = gen.next()
				const putLoginRequestedEffect = gen.next({
					casV1Action: null,
					localLoginAction
				})
				const callActionEffect = gen.next()
				expect(callActionEffect.value).toEqual(call(localLoginFlow, localLoginAction))
				const callPersistTokenEffect = gen.next(sampleOAuthToken)
			})

			it('triggers all login effects after completing login flow', () => {
				const raceLoginActionEffect = gen.next()
				const putLoginRequestedEffect = gen.next({
					casV1Action,
					localLoginAction: null
				})
				const callActionEffect = gen.next()
				const callPersistTokenEffect = gen.next(sampleOAuthToken)
				expect(callPersistTokenEffect.value).toEqual(
					call(defaultTokenPersistenceService.persistToken, sampleOAuthToken)
				)
				const allLoginSuccessEffect = gen.next()
				expect(allLoginSuccessEffect.value).toEqual(
					all({
						loginSuccess: put<AuthTokenSuccessAction>({
							type: AUTH_TOKEN_SUCCESS_ACTION_TYPE.GET_TOKEN_SUCCEEDED,
							oauthToken: sampleOAuthToken
						}),
						getUserInfo: put<ModelFetchRequestAction>({
							type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
							modelName: 'user.userInfo'
						}),
						logOut: take(AUTH_ACTION_TYPE.LOG_OUT_REQUESTED)
					})
				)
			})

			it('puts LOGIN_FAILED if race condition fail', () => {
				const raceLoginActionEffect = gen.next()
				const putLoginRequestedEffect = gen.next({
					// no success
				})
				const putLoginFailedEffect = gen.next(undefined)
				expect(putLoginFailedEffect.value).toEqual(
					put<AuthAction>({ type: AUTH_ACTION_TYPE.LOGIN_FAILED })
				)
			})

			it('puts LOGIN_FAILED if no token after race condition success but login flow fail', () => {
				const raceLoginActionEffect = gen.next()
				const putLoginRequestedEffect = gen.next({
					casV1Action,
					localLoginAction: null
				})
				const callActionEffect = gen.next()
				const putLoginFailedEffect = gen.next(undefined)
				expect(putLoginFailedEffect.value).toEqual(
					put<AuthAction>({ type: AUTH_ACTION_TYPE.LOGIN_FAILED })
				)
			})
		})

		describe('with oauthToken', () => {
			const oauthToken = sampleOAuthToken
			let gen: SagaIterator
			beforeEach(() => {
				gen = authSaga(clientCredentials)
				const callGetPersistedTokenEffect = gen.next()
				const putAuthInitializedEffect = gen.next(oauthToken)
				const takeEveryFetchFailureEffect = gen.next()
			})

			it('triggers all login effects after loading oauthToken', () => {
				const callPersistTokenEffect = gen.next()
				expect(callPersistTokenEffect.value).toEqual(
					call(defaultTokenPersistenceService.persistToken, oauthToken)
				)
				const allLoginSuccessEffect = gen.next()
				expect(allLoginSuccessEffect.value).toEqual(
					all({
						loginSuccess: put<AuthTokenSuccessAction>({
							type: AUTH_TOKEN_SUCCESS_ACTION_TYPE.GET_TOKEN_SUCCEEDED,
							oauthToken
						}),
						getUserInfo: put<ModelFetchRequestAction>({
							type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
							modelName: 'user.userInfo'
						}),
						logOut: take(AUTH_ACTION_TYPE.LOG_OUT_REQUESTED)
					})
				)
			})
		})

		describe('LOG_OUT_REQUESTED', () => {
			const oauthToken = { access_token: 'some-access-token' }
			let gen: SagaIterator
			beforeEach(() => {
				gen = authSaga(clientCredentials)
				const callGetPersistedTokenEffect = gen.next()
				const putAuthInitializedEffect = gen.next(oauthToken)
				const takeEveryFetchFailureEffect = gen.next()
			})

			it('clears user data after log out requested, and restarts loop back to race effect', () => {
				const callPersistTokenEffect = gen.next()
				const allLoginSuccessEffect = gen.next()
				const allClearDataEffect = gen.next()
				expect(allClearDataEffect.value).toEqual(
					all({
						clearUserData: put<ModelRemoveKeyAction>({
							type: MODEL_REMOVE_KEY_ACTION_TYPE,
							modelPath: 'user'
						}),
						clearPersistentToken: call(defaultTokenPersistenceService.persistToken, null)
					})
				)
				const raceLoginActionEffect = gen.next()
				expect(raceLoginActionEffect.value).toEqual(
					race({
						casV1Action: take(AUTH_CAS_V1_LOGIN_REQUEST_ACTION_TYPE.CAS_V1_LOGIN_REQUEST),
						localLoginAction: take(AUTH_LOCAL_LOGIN_REQUEST_ACTION_TYPE.LOCAL_LOGIN_REQUEST)
					})
				)
			})
		})
	})
})
