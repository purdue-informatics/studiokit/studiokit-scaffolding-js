import { AnyAction } from 'redux'
import { SagaIterator } from 'redux-saga'
import { take } from 'redux-saga/effects'
import { getAppConfig, getAppInsights } from '../../constants'
import { AUTH_ACTION_TYPE, AUTH_TOKEN_SUCCESS_ACTION_TYPE, isModelFetchResultAction } from '../actions'

export default function* appInsightsSaga(): SagaIterator {
	const appConfig = getAppConfig()
	const appInsightsKey = appConfig.APP_INSIGHTS_KEY
	if (!appInsightsKey) {
		return
	}
	const appInsights = getAppInsights()
	while (true) {
		const userInfoResponse = yield take(
			(action: AnyAction) => isModelFetchResultAction(action) && action.modelPath === 'user.userInfo'
		)
		const userId = userInfoResponse.data.id
		appInsights.setAuthenticatedUserContext(userId, undefined, true)
		yield take(AUTH_ACTION_TYPE.LOG_OUT_REQUESTED)
		appInsights.clearAuthenticatedUserContext()
		yield take(AUTH_TOKEN_SUCCESS_ACTION_TYPE.GET_TOKEN_SUCCEEDED)
	}
}
