import { AnyAction } from 'redux'
import { SagaIterator } from 'redux-saga'
import { call, take } from 'redux-saga/effects'
import { setShardConfig } from '../../constants/configuration'
import { Configuration } from '../../types'
import { isModelFetchResultAction } from '../actions'

export default function* configurationSaga(): SagaIterator {
	const configurationResult = yield take(
		(action: AnyAction) => isModelFetchResultAction(action) && action.modelPath === 'configuration'
	)
	const configuration: Configuration = configurationResult.data
	yield call(setShardConfig, configuration)
}
