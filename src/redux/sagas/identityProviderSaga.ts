import { AnyAction } from 'redux'
import { SagaIterator } from 'redux-saga'
import { call, take } from 'redux-saga/effects'
import { identityProviderKey, persistenceService } from '../../services/persistenceService'
import {
	AUTH_ACTION_TYPE,
	AUTH_IDENTITY_PROVIDER_REQUEST_ACTION_TYPE,
	AuthIdentityProviderRequestAction,
	isModelFetchResultAction
} from '../actions'

export default function* identityProviderSaga(): SagaIterator {
	yield take((action: AnyAction) => isModelFetchResultAction(action) && action.modelPath === 'identityProviders')
	while (true) {
		const oauthToken = yield call(persistenceService.getPersistedToken)
		let identityProviderAction: AuthIdentityProviderRequestAction | undefined = yield call(
			persistenceService.getItem,
			identityProviderKey
		)
		if (oauthToken && identityProviderAction) {
			yield take(AUTH_ACTION_TYPE.LOG_OUT_REQUESTED)
			yield call(persistenceService.removeItem, identityProviderKey)
			window.location.href = identityProviderAction.identityProvider.logoutUrl
		}
		identityProviderAction = yield take(AUTH_IDENTITY_PROVIDER_REQUEST_ACTION_TYPE.IDENTITY_PROVIDER_LOGIN_REQUEST)
		if (identityProviderAction) {
			yield call(persistenceService.setItem, identityProviderKey, identityProviderAction)
			window.location.href = identityProviderAction.identityProvider.loginUrl
		}
	}
}
