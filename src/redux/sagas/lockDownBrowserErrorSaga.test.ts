import { push } from 'connected-react-router'
import { stringify } from 'query-string'
import { runSaga, stdChannel } from 'redux-saga'
import { MODEL_FETCH_ERROR_ACTION_TYPE } from '../actions'
import lockDownBrowserErrorSaga from './lockDownBrowserErrorSaga'

describe('lockDownBrowserErrorSaga', () => {
	let channel: any
	let dispatchedActions: any
	let fakeStore: any
	const errorDataDetailMessage = 'LockDown Browser 💩'

	beforeEach(() => {
		channel = stdChannel()
		dispatchedActions = []
		fakeStore = {
			channel,
			getState: () => 'initial',
			dispatch: (action: any) => dispatchedActions.push(action)
		}
	})

	it('should not dispatch push if action is not of expected type', () => {
		runSaga(fakeStore, lockDownBrowserErrorSaga, false)
		channel.put({ type: 'not-try-fetch-failed', errorData: { detail: errorDataDetailMessage } })
		expect(dispatchedActions.length).toEqual(0)
	})

	it('should not dispatch push if failed fetch is not LDB related', () => {
		runSaga(fakeStore, lockDownBrowserErrorSaga, false)
		channel.put({
			type: MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED,
			modelPath: 'foo',
			errorData: { detail: 'Yo, this has nothing to do with you' }
		})
		expect(dispatchedActions.length).toEqual(0)
	})

	it('should dispatch push if action is a failed fetch with an LDB error', () => {
		runSaga(fakeStore, lockDownBrowserErrorSaga, false)
		channel.put({
			type: MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED,
			modelPath: 'foo',
			errorData: { detail: errorDataDetailMessage }
		})
		expect(dispatchedActions.length).toEqual(1)
		expect(dispatchedActions[0]).toEqual(
			push({
				pathname: '/error',
				search: stringify({ message: errorDataDetailMessage })
			})
		)
	})
})
