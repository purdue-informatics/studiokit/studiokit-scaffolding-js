import * as Sentry from '@sentry/react'
import { AnyAction } from 'redux'
import { SagaIterator } from 'redux-saga'
import { take } from 'redux-saga/effects'
import { getAppConfig } from '../../constants/configuration'
import { AUTH_ACTION_TYPE, AUTH_TOKEN_SUCCESS_ACTION_TYPE, isModelFetchResultAction } from '../actions'

export default function* sentrySaga(): SagaIterator {
	const appConfig = getAppConfig()
	const sentryDsn = appConfig.SENTRY_DSN
	if (!sentryDsn) {
		return
	}
	while (true) {
		const userInfoResponse = yield take(
			(action: AnyAction) => isModelFetchResultAction(action) && action.modelPath === 'user.userInfo'
		)
		const userInfo = userInfoResponse.data
		let scope = Sentry.getCurrentScope()
		scope.setUser({
			id: userInfo.id,
			username: userInfo.userName,
			email: userInfo.email
		})
		yield take(AUTH_ACTION_TYPE.LOG_OUT_REQUESTED)
		scope = Sentry.getCurrentScope()
		scope.setUser({})
		yield take(AUTH_TOKEN_SUCCESS_ACTION_TYPE.GET_TOKEN_SUCCEEDED)
	}
}
