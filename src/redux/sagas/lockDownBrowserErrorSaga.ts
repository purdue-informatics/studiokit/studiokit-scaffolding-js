import { push } from 'connected-react-router'
import { stringify } from 'query-string'
import { AnyAction } from 'redux'
import { put, take } from 'redux-saga/effects'
import { isModelTryFetchErrorAction } from '../actions'

export default function* lockDownBrowserSaga(runIndefinitely = true) {
	do {
		let message = ''

		// wait for any LDB error
		yield take((action: AnyAction) => {
			if (!isModelTryFetchErrorAction(action)) return false
			message = action.errorData?.detail || ''
			return message?.includes('LockDown Browser')
		})

		// redirect to known LDB error route
		yield put(
			push({
				pathname: '/error',
				search: stringify({ message })
			})
		)
	} while (runIndefinitely)
}
