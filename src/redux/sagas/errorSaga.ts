import { LOCATION_CHANGE, push } from 'connected-react-router'
import _ from 'lodash'
import { parse, stringify } from 'query-string'
import { AnyAction } from 'redux'
import { put, take } from 'redux-saga/effects'
import { windowService } from '../../services/windowService'

export default function* errorSaga() {
	while (true) {
		// check for error messages immediately at app startup
		const location = windowService.getLocation()
		const parsedSearch = parse(location.search)
		// check possible error message keys
		const message = parsedSearch._ltierrormsg || parsedSearch.error
		if (!_.isNil(message)) {
			yield put(
				push({
					pathname: '/error',
					search: stringify({ message })
				})
			)
		}
		// check for error messages on location changes
		yield take((action: AnyAction) => action.type === LOCATION_CHANGE)
	}
}
