import moment from 'moment-timezone'
import { AnyAction } from 'redux'
import { SagaIterator } from 'redux-saga'
import { call, take } from 'redux-saga/effects'
import { setClockOffset } from '../../utils/date'
import { getLogger } from '../../utils/logger'
import { isTransientModelFetchResultAction } from '../actions'

/**
	Periodically, we fetch the current UTC server time. To insure against browser clock time being set
	incorrectly, we track the difference between server time and browser time by storing that
	difference in a module-level variable in the date utilities module. That module provides a method
	to get the current UTC time, according to the API server.

	If the calculated difference is less than one second, we can assume that's a result of network
	latency and will not store that offset.
*/
export default function* clockOffsetSaga(): SagaIterator {
	const logger = getLogger()

	while (true) {
		const timeResult = yield take(
			(action: AnyAction) => isTransientModelFetchResultAction(action) && action.modelPath === 'time'
		)
		const time: string = timeResult.data
		const offset = moment.utc(time).diff(moment.utc())
		if (Math.abs(offset) > 1000) {
			logger.debug('> 1 second server <-> browser time difference')
			logger.debug('server', time)
			logger.debug('browser', new Date().toISOString())
			logger.debug('calculated', new Date(Date.now() + offset).toISOString())
			yield call(setClockOffset, offset)
		} else {
			logger.debug('clear clock offset')
			yield call(setClockOffset, 0)
		}
	}
}
