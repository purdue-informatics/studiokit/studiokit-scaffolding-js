import { push } from 'connected-react-router'
import { SagaIterator } from 'redux-saga'
import { call, put, take } from 'redux-saga/effects'
import { persistenceService, returnUrlKey } from '../../services/persistenceService'
import { windowService } from '../../services/windowService'
import { AUTH_ACTION_TYPE, AUTH_TOKEN_SUCCESS_ACTION_TYPE } from '../actions'

/**
 * Redirect the user after a successful log in.
 * Uses the saved `returnUrl`, if any, if different from the current pathname and is not the root url.
 */
export default function* postLoginRedirectSaga(): SagaIterator {
	while (true) {
		yield take(AUTH_TOKEN_SUCCESS_ACTION_TYPE.GET_TOKEN_SUCCEEDED)
		const location = windowService.getLocation()
		const pathname = location.pathname
		const returnUrl = yield call(persistenceService.getItem, returnUrlKey)
		// redirect to the `returnUrl` if it is different than the current pathname and is not the root url
		if (returnUrl && returnUrl !== pathname && returnUrl !== '/' && pathname === '/') {
			yield put(push(returnUrl))
		}
		// always clear `returnUrl`
		if (returnUrl) {
			yield call(persistenceService.removeItem, returnUrlKey)
		}
		yield take(AUTH_ACTION_TYPE.LOG_OUT_REQUESTED)
	}
}
