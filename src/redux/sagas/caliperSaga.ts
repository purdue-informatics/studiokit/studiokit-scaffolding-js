import { Dictionary } from 'lodash'
import { AnyAction } from 'redux'
import { SagaIterator } from 'redux-saga'
import { call, cancel, delay, fork, race, take } from 'redux-saga/effects'
import { BrowserCaliperService } from 'studiokit-caliper-js'
import {
	CaliperSensorOptions,
	CaliperService,
	CaliperServiceOptions,
	CaliperToken
} from 'studiokit-caliper-js/lib/types'
import { getAppConfig, getShardConfig } from '../../constants/configuration'
import { persistenceService } from '../../services/persistenceService'
import { Configuration, UserInfo } from '../../types'
import { getServerNowUtc } from '../../utils/date'
import { dispatchModelFetchRequest } from '../actionCreator'
import {
	AUTH_ACTION_TYPE,
	isModelFetchResultAction,
	isTransientModelFetchErrorAction,
	isTransientModelFetchResultAction
} from '../actions'

//#region Local Variables

let service: CaliperService
let caliperToken: CaliperToken | undefined

//#endregion Local Variables

//#region CaliperService Helpers

function initializeCaliperService(shardConfig: Configuration) {
	const appConfig = getAppConfig()
	if (!shardConfig.caliperEventStoreHostname) {
		return
	}
	const sensorOptions: CaliperSensorOptions = Object.assign({}, appConfig.CALIPER_EVENT_STORE_OPTIONS, {
		hostname: shardConfig.caliperEventStoreHostname
	})
	const options: Partial<CaliperServiceOptions> = {
		sensorId: appConfig.CALIPER_SENSOR_ID,
		sensorOptions,
		appId: appConfig.CALIPER_SENSOR_APP_ID,
		appName: appConfig.CALIPER_SENSOR_APP_NAME,
		getToken: function () {
			return new Promise((resolve, reject) => {
				if (caliperToken) {
					resolve(caliperToken)
				} else {
					reject(new Error('`caliperToken` is undefined'))
				}
			})
		},
		// disable auto send, the saga will handle this
		autoSend: false
	}
	const service: CaliperService = new BrowserCaliperService(options)
	return service
}

function setCaliperPerson(userData: UserInfo) {
	const appConfig = getAppConfig()
	const shardConfig = getShardConfig()
	const personNamespace = shardConfig.caliperPersonNamespace
	const id = `${personNamespace}${userData.puid}`
	const extensions: Dictionary<any> = {}
	extensions[`${appConfig.CALIPER_EXTENSIONS_NAMESPACE}.userId`] = userData.id
	service.setPerson(id, userData.firstName ?? undefined, userData.lastName ?? undefined, extensions)
	return Promise.resolve()
}

function startCaliperSession() {
	const appConfig = getAppConfig()
	const extensions: Dictionary<any> = {}
	extensions[`${appConfig.CALIPER_EXTENSIONS_NAMESPACE}.userAgent`] = navigator.userAgent
	service.startSession(extensions)
	return Promise.resolve()
}

function endCaliperSession() {
	service.endSession()
	return Promise.resolve()
}

function setPersonAndStartCaliperSession(userData: UserInfo) {
	void setCaliperPerson(userData)
	void startCaliperSession()
}

function isTokenExpired(caliperToken: CaliperToken) {
	return new Date(caliperToken.expires) <= getServerNowUtc()
}

//#endregion CaliperService Helpers

/**
 * Load the persisted `caliperToken`, if any. This token is persisted by the CaliperService,
 * but we load it in order to check expiry.
 */
function* loadToken(): SagaIterator<CaliperToken | undefined> {
	const caliperToken: CaliperToken | null = yield call(persistenceService.getPersistedCaliperToken)
	if (caliperToken) return caliperToken
	return undefined
}

/**
 * Load a new `caliperToken` from the API. Returns `undefined` if the data request fails.
 */
function* getToken(): SagaIterator<CaliperToken | undefined> {
	dispatchModelFetchRequest({
		modelName: 'caliperToken',
		noStore: true
	})
	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	const { receivedResult, failedResult } = yield race({
		receivedResult: take(
			(action: AnyAction) => isTransientModelFetchResultAction(action) && action.modelPath === 'caliperToken'
		),
		failedResult: take(
			(action: AnyAction) => isTransientModelFetchErrorAction(action) && action.modelPath === 'caliperToken'
		)
	})
	if (receivedResult && receivedResult.data) {
		// convert to CaliperToken from OAuthToken
		const caliperToken: CaliperToken = {
			accessToken: receivedResult.data.access_token,
			expires: receivedResult.data['.expires']
		}
		return caliperToken
	}
	return undefined
}

/**
 * Calls `getToken`, but keeps retrying until a token is successfully loaded, using a back-off delay strategy.
 */
function* getTokenWithRetry(): SagaIterator<CaliperToken> {
	// get token
	let caliperToken = yield call(getToken)
	// handle failing to load token
	let tryCount = 1
	while (!caliperToken) {
		// back-off delay
		yield delay(Math.min(1000 * 60, 2 ^ (tryCount * 100))) // 100, 200, 400...
		caliperToken = yield call(getToken)
		tryCount++
	}
	return caliperToken
}

/**
 * Saga decorator for the `CaliperService.send` method
 */
function* send() {
	try {
		yield call(service.send)
	} catch {
		// catch error to avoid breaking the saga
		// ignore error since `CaliperService` already tracks send errors
	}
}

/**
 * Saga decorator for the `CaliperService.send` method,
 * which will refresh the `caliperToken` if needed before calling `send`
 */
function* sendWithTokenRefreshIfNeeded() {
	// refresh token, if needed
	if (!caliperToken || isTokenExpired(caliperToken)) {
		caliperToken = yield call(getTokenWithRetry)
	}
	// send
	yield call(send)
}

/**
 * Saga fork implementing the "auto send" functionality from `CaliperService.
 * Calls `send` every 10 seconds.
 */
function* autoSend() {
	while (true) {
		// try to send every 10 seconds
		yield delay(10 * 1000)
		yield call(sendWithTokenRefreshIfNeeded)
	}
}

export default function* caliperSaga(): SagaIterator {
	// on app start, get the configuration
	const configurationResult = yield take(
		(action: AnyAction) => isModelFetchResultAction(action) && action.modelPath === 'configuration'
	)
	const configuration: Configuration = configurationResult.data
	// do not proceed if caliper is not enabled or is missing hostname
	if (!configuration.caliperEnabled || !configuration.caliperEventStoreHostname) {
		return
	}

	// load persisted token, if any
	caliperToken = yield call(loadToken)
	// initialize caliper service
	service = yield call(initializeCaliperService, configuration)

	while (true) {
		// user is loaded and authenticated => can get token and can start session
		const userInfo = yield take(
			(action: AnyAction) => isModelFetchResultAction(action) && action.modelPath === 'user.userInfo'
		)
		const autoSendTask = yield fork(autoSend)
		setPersonAndStartCaliperSession(userInfo.data)

		// user logs out => stop auto-send and end session
		yield take(AUTH_ACTION_TYPE.LOG_OUT_REQUESTED)
		yield cancel(autoSendTask)
		void endCaliperSession()
		// attempt to send the session end event once, w/ no token refresh since logged out
		yield call(send)
	}
}
