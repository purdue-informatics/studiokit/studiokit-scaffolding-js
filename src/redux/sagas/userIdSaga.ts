import { AnyAction } from 'redux'
import { SagaIterator } from 'redux-saga'
import { take } from 'redux-saga/effects'
import { setUserId } from '../../constants'
import { AUTH_ACTION_TYPE, AUTH_TOKEN_SUCCESS_ACTION_TYPE, isModelFetchResultAction } from '../actions'

export default function* userIdSaga(): SagaIterator {
	while (true) {
		const userInfoResponse = yield take(
			(action: AnyAction) => isModelFetchResultAction(action) && action.modelPath === 'user.userInfo'
		)
		const userId = userInfoResponse.data.id
		setUserId(userId)
		yield take(AUTH_ACTION_TYPE.LOG_OUT_REQUESTED)
		setUserId(null)
		yield take(AUTH_TOKEN_SUCCESS_ACTION_TYPE.GET_TOKEN_SUCCEEDED)
	}
}
