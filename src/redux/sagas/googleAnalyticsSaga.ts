import ga from 'react-ga4'
import { AnyAction } from 'redux'
import { SagaIterator } from 'redux-saga'
import { take } from 'redux-saga/effects'
import { getAppConfig } from '../../constants'
import { isModelFetchResultAction } from '../actions'
import { AUTH_ACTION_TYPE, AUTH_TOKEN_SUCCESS_ACTION_TYPE } from '../actions/AuthAction'

export default function* googleAnalyticsSaga(): SagaIterator {
	const appConfig = getAppConfig()
	const gaTrackingId = appConfig.GOOGLE_ANALYTICS_TRACKING_ID
	if (!gaTrackingId) {
		return
	}
	while (true) {
		const userInfoResponse = yield take(
			(action: AnyAction) => isModelFetchResultAction(action) && action.modelPath === 'user.userInfo'
		)
		const userId = userInfoResponse.data.id
		ga.gtag('config', gaTrackingId, {
			user_id: userId
		})
		yield take(AUTH_ACTION_TYPE.LOG_OUT_REQUESTED)
		ga.gtag('config', gaTrackingId, {
			user_id: null
		})
		yield take(AUTH_TOKEN_SUCCESS_ACTION_TYPE.GET_TOKEN_SUCCEEDED)
	}
}
