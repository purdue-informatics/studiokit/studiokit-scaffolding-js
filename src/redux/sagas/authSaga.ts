import { SagaIterator } from '@redux-saga/core'
import { AnyAction } from 'redux'
import { all, call, put, race, take, takeEvery } from 'redux-saga/effects'
import { codeProviderService as defaultCodeProviderService } from '../../services/codeProviderService'
import { ticketProviderService as defaultTicketProviderService } from '../../services/ticketProviderService'
import { tokenPersistenceService as defaultTokenPersistenceService } from '../../services/tokenPersistenceService'
import {
	ClientCredentials,
	CodeProviderService,
	HTTP_STATUS_CODE,
	OAuthToken,
	OAuthTokenOrNull,
	TicketProviderService,
	TokenAccessFunction,
	TokenPersistenceService
} from '../../types'
import { getLogger, Logger } from '../../utils/logger'
import {
	AUTH_ACTION_TYPE,
	AUTH_CAS_V1_LOGIN_REQUEST_ACTION_TYPE,
	AUTH_LOCAL_LOGIN_REQUEST_ACTION_TYPE,
	AUTH_TOKEN_ACTION_TYPE,
	AUTH_TOKEN_SUCCESS_ACTION_TYPE,
	AuthAction,
	AuthCasV1LoginRequestAction,
	AuthLocalLoginRequestAction,
	AuthTokenAction,
	AuthTokenSuccessAction,
	isTransientModelFetchErrorAction,
	isTransientModelFetchResultAction,
	MODEL_FETCH_ERROR_ACTION_TYPE,
	MODEL_FETCH_REQUEST_ACTION_TYPE,
	MODEL_REMOVE_KEY_ACTION_TYPE,
	ModelFetchErrorAction,
	ModelFetchRequestAction,
	ModelFetchResultAction,
	ModelRemoveKeyAction
} from '../actions'

//#region Helpers

export const matchesModelFetchReceived = (action: AnyAction, modelName: string) =>
	isTransientModelFetchResultAction(action) && action.modelPath === modelName

export const takeMatchesModelFetchReceived = (modelName: string) => (incomingAction: AnyAction) =>
	matchesModelFetchReceived(incomingAction, modelName)

export const matchesModelFetchFailed = (action: AnyAction, modelName: string) =>
	isTransientModelFetchErrorAction(action) && action.modelPath === modelName

export const takeMatchesModelFetchFailed = (modelName: string) => (incomingAction: AnyAction) =>
	matchesModelFetchFailed(incomingAction, modelName)

//#endregion Helpers

//#region Local Variables

let clientCredentials: ClientCredentials
let oauthToken: OAuthTokenOrNull = null
let tokenPersistenceService: TokenPersistenceService
let refreshLock: boolean
let logger: Logger

//#endregion Local Variables

export function* getTokenFromCode(code: string): SagaIterator {
	const getTokenModelName = 'getToken'
	// Manually creating form-url-encoded body here because NOTHING else uses this content-type
	// but the OAuth spec requires it
	const formBody = [
		'grant_type=authorization_code',
		`client_id=${clientCredentials.client_id}`,
		`client_secret=${clientCredentials.client_secret}`,
		`code=${encodeURIComponent(code)}`
	]
	const formBodyString = formBody.join('&')
	yield put<ModelFetchRequestAction>({
		type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
		modelName: getTokenModelName,
		body: formBodyString,
		noStore: true
	})
	const {
		fetchResultAction,
		fetchErrorAction
	}: { fetchResultAction?: ModelFetchResultAction; fetchErrorAction?: ModelFetchErrorAction } = yield race({
		fetchResultAction: take(takeMatchesModelFetchReceived(getTokenModelName)),
		fetchErrorAction: take(takeMatchesModelFetchFailed(getTokenModelName))
	})
	if (fetchErrorAction || !fetchResultAction?.data) {
		return null
	}
	return fetchResultAction.data
}

export function* getTokenFromRefreshToken(oauthTokenParam: OAuthToken): SagaIterator {
	const getTokenModelName = 'getToken'
	// Manually creating form-url-encoded body here because NOTHING else uses this content-type
	// but the OAuth spec requires it
	const formBody = [
		'grant_type=refresh_token',
		`client_id=${clientCredentials.client_id}`,
		`client_secret=${clientCredentials.client_secret}`,
		`refresh_token=${encodeURIComponent(oauthTokenParam.refresh_token)}`
	]
	const formBodyString = formBody.join('&')
	yield put<ModelFetchRequestAction>({
		type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
		modelName: getTokenModelName,
		body: formBodyString,
		noStore: true
	})
	const {
		fetchResultAction,
		fetchErrorAction
	}: { fetchResultAction?: ModelFetchResultAction; fetchErrorAction?: ModelFetchErrorAction } = yield race({
		fetchResultAction: take(takeMatchesModelFetchReceived(getTokenModelName)),
		fetchErrorAction: take(takeMatchesModelFetchFailed(getTokenModelName))
	})
	// any error response
	if (fetchErrorAction) {
		// ignore server errors
		if (
			fetchErrorAction.errorData?.status &&
			fetchErrorAction.errorData.status >= HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR
		) {
			return oauthTokenParam
		}
		return null
	}
	// for some reason the response had no body
	if (!fetchResultAction?.data) {
		return null
	}
	return fetchResultAction.data
}

export function* performTokenRefresh(): SagaIterator {
	if (refreshLock || !oauthToken) {
		// already refreshing. wait for the current refresh to succeed or fail.
		yield race({
			refreshSuccess: take(AUTH_TOKEN_SUCCESS_ACTION_TYPE.TOKEN_REFRESH_SUCCEEDED),
			refreshFailed: take(AUTH_ACTION_TYPE.TOKEN_REFRESH_FAILED)
		})
		return
	}
	logger.debug('Refreshing OAuth token')
	refreshLock = true
	// oauthToken will be set to:
	// 1. new token (success)
	// 2. same token (failed from timeout or server error)
	// 3. null (fail)
	const originalAccessToken = oauthToken.access_token
	oauthToken = yield call(getTokenFromRefreshToken, oauthToken)
	if (!!oauthToken && oauthToken.access_token !== originalAccessToken) {
		logger.debug('OAuth token refreshed')
		yield call(tokenPersistenceService.persistToken, oauthToken)
		yield put<AuthTokenSuccessAction>({ type: AUTH_TOKEN_SUCCESS_ACTION_TYPE.TOKEN_REFRESH_SUCCEEDED, oauthToken })
	} else if (oauthToken === null) {
		logger.debug('OAuth token failed to refresh')
		// This should never happen outside of the token having been revoked on the server side
		yield all({
			refreshFailed: put<AuthAction>({ type: AUTH_ACTION_TYPE.TOKEN_REFRESH_FAILED }),
			logOut: put<AuthAction>({ type: AUTH_ACTION_TYPE.LOG_OUT_REQUESTED })
		})
	}
	refreshLock = false
}

export function* loginFlow(modelFetchRequestAction: ModelFetchRequestAction): SagaIterator {
	yield put(modelFetchRequestAction)
	const {
		fetchResultAction,
		fetchErrorAction
	}: { fetchResultAction?: ModelFetchResultAction; fetchErrorAction?: ModelFetchErrorAction } = yield race({
		fetchResultAction: take(takeMatchesModelFetchReceived(modelFetchRequestAction.modelName)),
		fetchErrorAction: take(takeMatchesModelFetchFailed(modelFetchRequestAction.modelName))
	})
	if (fetchErrorAction) {
		return null
	}
	const code = fetchResultAction?.data?.code
	if (!code) {
		return null
	}
	return yield call(getTokenFromCode, code)
}

export function* credentialsLoginFlow(
	action: AuthCasV1LoginRequestAction | AuthLocalLoginRequestAction,
	modelName: string
): SagaIterator {
	const modelFetchRequestAction: ModelFetchRequestAction = {
		// set required defaults
		type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
		modelName,
		noStore: true,
		// force no retry
		noRetry: true,
		// pass thru body
		body: action.body
	}
	return yield call(loginFlow, modelFetchRequestAction)
}

export function* casV1LoginFlow(action: AuthCasV1LoginRequestAction): SagaIterator {
	return yield call(credentialsLoginFlow, action, 'codeFromCasV1')
}

export function* localLoginFlow(action: AuthLocalLoginRequestAction): SagaIterator {
	return yield call(credentialsLoginFlow, action, 'codeFromLocalCredentials')
}

export function* casTicketLoginFlow(ticket: string, service: string): SagaIterator {
	const modelFetchRequestAction: ModelFetchRequestAction = {
		type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
		modelName: 'codeFromCasTicket',
		noStore: true,
		queryParams: {
			ticket,
			service
		}
	}
	return yield call(loginFlow, modelFetchRequestAction)
}

export function* handleAuthFailure(action: ModelFetchErrorAction): SagaIterator {
	// This should be unlikely since we normally have a refresh token loop happening
	// but if the app is backgrounded, the loop might not be caught up yet
	if (oauthToken && action.errorData && action.errorData.status === HTTP_STATUS_CODE.UNAUTHORIZED) {
		logger.debug('token expired - refreshing')
		yield call(performTokenRefresh)
	}
}

export const getOAuthToken: TokenAccessFunction = function* (modelName: string) {
	// Don't try to refresh the token if we're already in a request to refresh the token
	if (modelName === 'getToken') {
		return null
	}
	if (oauthToken && oauthToken['.expires']) {
		const thirtySecondsFromNow = new Date()
		thirtySecondsFromNow.setSeconds(thirtySecondsFromNow.getSeconds() + 30)
		if (new Date(oauthToken['.expires']) < thirtySecondsFromNow) {
			// start a token refresh and wait for the success action in case another refresh is currently happening
			yield call(performTokenRefresh)
			return oauthToken
		}
	}
	return oauthToken
}

export default function* authSaga(
	clientCredentialsParam: ClientCredentials,
	tokenPersistenceServiceParam: TokenPersistenceService = defaultTokenPersistenceService,
	ticketProviderService: TicketProviderService = defaultTicketProviderService,
	codeProviderService: CodeProviderService = defaultCodeProviderService
): SagaIterator {
	logger = getLogger()

	/* istanbul ignore if */
	if (!clientCredentialsParam) {
		throw new Error("'clientCredentialsParam' is required for authSaga")
	}
	clientCredentials = clientCredentialsParam
	tokenPersistenceService = tokenPersistenceServiceParam

	// Try to get persisted token (normally in AsyncStorage or LocalStorage)
	oauthToken = yield call(tokenPersistenceService.getPersistedToken)

	// If no token, try to get CAS ticket (normally in the URL), use it to get a token
	if (!oauthToken) {
		const casTicket = ticketProviderService.getTicket()
		ticketProviderService.removeTicket()
		const service = ticketProviderService.getAppServiceName()
		if (casTicket && service) {
			oauthToken = yield call(casTicketLoginFlow, casTicket, service)
		}
	}

	// If OAuth Code exists (normally in the URL), use it to get a token
	// e.g. LTI, Shibboleth, Facebook, Google
	const code = codeProviderService.getCode()
	if (code) {
		// Log out the current user if a new token is about to be generated from the OAuth Code
		if (oauthToken) {
			yield all({
				clearUserData: put<ModelRemoveKeyAction>({ type: MODEL_REMOVE_KEY_ACTION_TYPE, modelPath: 'user' }),
				clearPersistentToken: call(tokenPersistenceService.persistToken, null)
			})
			oauthToken = null
		}
		oauthToken = yield call(getTokenFromCode, code)
	}
	codeProviderService.removeCode()

	yield put<AuthTokenAction>({ type: AUTH_TOKEN_ACTION_TYPE.AUTH_INITIALIZED, oauthToken })

	yield takeEvery(MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED, handleAuthFailure)

	do {
		if (!oauthToken) {
			const {
				casV1Action,
				localLoginAction
			}: {
				casV1Action?: AuthCasV1LoginRequestAction
				localLoginAction?: AuthLocalLoginRequestAction
			} = yield race({
				casV1Action: take(AUTH_CAS_V1_LOGIN_REQUEST_ACTION_TYPE.CAS_V1_LOGIN_REQUEST),
				localLoginAction: take(AUTH_LOCAL_LOGIN_REQUEST_ACTION_TYPE.LOCAL_LOGIN_REQUEST)
			})

			yield put<AuthAction>({ type: AUTH_ACTION_TYPE.LOGIN_REQUESTED })
			if (casV1Action) {
				oauthToken = yield call(casV1LoginFlow, casV1Action)
			} else if (localLoginAction) {
				oauthToken = yield call(localLoginFlow, localLoginAction)
			}
		}

		if (oauthToken) {
			yield call(tokenPersistenceService.persistToken, oauthToken)
			yield all({
				loginSuccess: put<AuthTokenSuccessAction>({
					type: AUTH_TOKEN_SUCCESS_ACTION_TYPE.GET_TOKEN_SUCCEEDED,
					oauthToken
				}),
				getUserInfo: put<ModelFetchRequestAction>({
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					modelName: 'user.userInfo'
				}),
				logOut: take(AUTH_ACTION_TYPE.LOG_OUT_REQUESTED)
			})
		} else {
			yield put<AuthAction>({ type: AUTH_ACTION_TYPE.LOGIN_FAILED })
		}

		yield all({
			clearUserData: put<ModelRemoveKeyAction>({ type: MODEL_REMOVE_KEY_ACTION_TYPE, modelPath: 'user' }),
			clearPersistentToken: call(tokenPersistenceService.persistToken, null)
		})
		oauthToken = null
	} while (true)
}
