import { SagaIterator } from '@redux-saga/core'
import { isNil, isPlainObject, merge } from 'lodash'
import moment from 'moment-timezone'
import { AnyAction } from 'redux'
import { call, cancel, cancelled, delay, fork, put, take, takeEvery } from 'redux-saga/effects'
import { networkOfflineErrorData, unknownErrorData } from '../../constants/fetchErrorData'
import { getLastActivityDate } from '../../services/dateService'
import { sendFetch, setApiRoot } from '../../services/fetchService'
import { windowService } from '../../services/windowService'
import {
	EndpointMapping,
	EndpointMappings,
	ErrorHandler,
	FetchErrorData,
	FetchResult,
	HTTP_STATUS_CODE,
	Model,
	ModelCollection,
	OAuthTokenOrNull,
	TokenAccessFunction
} from '../../types'
import { constructErrorFromCaughtError } from '../../utils/error'
import { isFetchErrorData, prepareFetch, PrepareFetchResult } from '../../utils/fetch'
import { getLogger, Logger } from '../../utils/logger'
import {
	MODEL_FETCH_ERROR_ACTION_TYPE,
	MODEL_FETCH_REQUEST_ACTION_TYPE,
	MODEL_FETCH_RESULT_ACTION_TYPE,
	MODEL_FETCH_START_ACTION_TYPE,
	MODEL_REMOVE_KEY_ACTION_TYPE,
	ModelFetchErrorAction,
	ModelFetchRequestAction,
	ModelFetchResultAction,
	ModelFetchStartAction,
	ModelRemoveKeyAction,
	PERIODIC_MODEL_FETCH_TERMINATION_ACTION_TYPE,
	PeriodicModelFetchRequestAction,
	PeriodicModelFetchTerminationAction
} from '../actions'

/** The total number of tries for `fetchData`, including the initial request. */
export const TRY_LIMIT = 5

//#region Helpers

export const matchesTerminationAction = (
	incomingAction: AnyAction,
	dataRequestAction: PeriodicModelFetchRequestAction
) =>
	incomingAction.type === PERIODIC_MODEL_FETCH_TERMINATION_ACTION_TYPE.TERMINATE &&
	incomingAction.taskId === dataRequestAction.taskId

export const takeMatchesTerminationAction = (dataRequestAction: PeriodicModelFetchRequestAction) => (
	incomingAction: AnyAction
) => matchesTerminationAction(incomingAction, dataRequestAction)

/* istanbul ignore next */
/* eslint-disable-next-line require-yield */
export const defaultTokenAccessFunction: TokenAccessFunction = function* () {
	return null
}

/* istanbul ignore next */
export const defaultErrorHandler: ErrorHandler = () => {
	return
}

interface GenericModel extends Model, Record<string, any> {}

const getChildCollectionKeys = (endpointMapping: EndpointMapping) =>
	Object.keys(endpointMapping).reduce((out: string[], key) => {
		const value = endpointMapping[key]
		if (!value || !Object.prototype.hasOwnProperty.call(value, '_config')) {
			return out
		}
		const em = value as EndpointMapping
		if (!!em._config && !!em._config.isCollection) {
			out.push(key)
		}
		return out
	}, [])

const convertCollections = (endpointMapping: EndpointMapping, prepareFetchResult: PrepareFetchResult, data: any) => {
	const { endpointConfig, fetchConfig, isCollectionItemCreate, isCollectionItemFetch } = prepareFetchResult
	const childCollectionKeys = getChildCollectionKeys(endpointMapping)
	const hasChildCollections = childCollectionKeys.length > 0
	// no collections, return as-is
	if (!endpointConfig.isCollection && !hasChildCollections) {
		return data
	}
	// delete returns no data
	if (fetchConfig.method === 'DELETE') {
		return {}
	}
	// data is a collection item, convert any child collections in item response
	if (isCollectionItemFetch || isCollectionItemCreate) {
		return convertDataCollectionItem(data, childCollectionKeys, endpointMapping)
	}
	// data is a collection, convert collection response, and any child collections in each item
	return endpointConfig.isCollection
		? convertDataToCollection(data, childCollectionKeys, endpointMapping)
		: convertDataCollectionItem(data, childCollectionKeys, endpointMapping)
}

const convertDataCollectionItem = (data: any, collectionKeys: string[], endpointMapping: EndpointMapping) => {
	if (isNil(data)) {
		return data
	}
	return Object.keys(data).reduce((out: GenericModel, key) => {
		let value = data[key]
		if (collectionKeys.includes(key)) {
			const childEndpointMapping = endpointMapping[key] as EndpointMapping
			const childCollectionKeys = getChildCollectionKeys(childEndpointMapping)
			value = convertDataToCollection(value, childCollectionKeys, childEndpointMapping)
		}
		out[key] = value
		return out
	}, {})
}

const convertDataToCollection = (data: any, collectionKeys: string[], endpointMapping: EndpointMapping) => {
	if (isNil(data)) {
		return data
	}
	return Object.keys(data).reduce((out: ModelCollection<any>, key) => {
		const value = convertDataCollectionItem(data[key], collectionKeys, endpointMapping)
		/* istanbul ignore else */
		if (value && value.id) {
			out[value.id] = value
		}
		return out
	}, {})
}

/**
 * Using the `lastActivityDate` set in local storage by "studiokit-caliper-js",
 * determine if the user has been idle, e.g. has had no activity in the last 15 minutes.
 */
function isUserIdle() {
	const lastActivityDateString = getLastActivityDate()
	if (!lastActivityDateString) {
		return false
	}

	const lastActivityDateUtc = moment.utc(lastActivityDateString)
	const nowUtc = moment.utc()
	const idleMinutesThreshold = 1000 * 60 * 15 // 15 minutes

	return nowUtc.diff(lastActivityDateUtc) >= idleMinutesThreshold
}

//#endregion Helpers

//#region Local Variables

let endpointMappings: EndpointMappings
let tokenAccessFunction: TokenAccessFunction
let errorHandler: ErrorHandler
let logger: Logger

//#endregion Local Variables

/** Set the shared EndpointMappings variable, exposed for testability */
export function setEndpointMappings(endpointMappingsParam: EndpointMappings) {
	endpointMappings = endpointMappingsParam
}

/**
 * Construct a request based on the provided dataRequestAction, make a request with a configurable retry,
 * and handle errors, logging and dispatching all steps.
 *
 * @param modelFetchRequestAction A model fetch request action with the request configuration
 */
export function* modelFetch(
	modelFetchRequestAction: ModelFetchRequestAction | PeriodicModelFetchRequestAction
): SagaIterator {
	let prepareFetchResult: PrepareFetchResult | undefined = undefined
	try {
		prepareFetchResult = prepareFetch(modelFetchRequestAction, endpointMappings)
	} catch (error) {
		// Note: `prepareFetch` already ensures that `error` is an actual `Error`
		logger.error(error)
		errorHandler(error, modelFetchRequestAction)
		return
	}

	// get prepared variables
	const {
		modelPath,
		fetchConfig,
		endpointMapping,
		isCollectionItemCreate,
		isCollectionItemDelete
	} = prepareFetchResult

	// Configure retry
	const tryLimit: number = modelFetchRequestAction.noRetry ? 1 : TRY_LIMIT
	let tryCount = 0
	let didFail = false
	let lastFetchResult: FetchResult | undefined
	let lastError: Error | undefined
	let lastErrorData: FetchErrorData | undefined

	const fetchResultUndefinedErrorMessage = "'fetchResult' is undefined"
	const fetchResultNotOkErrorMessage = "'fetchResult.ok' is false"

	// Run retry loop
	do {
		didFail = false
		tryCount++

		// dispatch that the fetch request started, which updates `_metadata` and `guid`, if provided, at the target redux `modelPath`
		yield put<ModelFetchStartAction>({
			type: modelFetchRequestAction.noStore
				? MODEL_FETCH_START_ACTION_TYPE.TRANSIENT_FETCH_START
				: MODEL_FETCH_START_ACTION_TYPE.FETCH_START,
			modelPath,
			guid: modelFetchRequestAction.guid
		})

		let fetchResult: FetchResult | undefined = undefined
		try {
			// get the OAuth Token, if any, and call `fetch`
			const oauthToken: OAuthTokenOrNull = yield call(tokenAccessFunction, modelFetchRequestAction.modelName)
			if (oauthToken?.access_token) {
				fetchConfig.headers = merge({}, fetchConfig.headers, {
					Authorization: `Bearer ${oauthToken.access_token}`
				})
			}
			fetchResult = yield call(sendFetch, fetchConfig)
			// store `fetchResult` for reference after the do-while loop
			lastFetchResult = fetchResult

			// throw on unsuccessful result to short-circuit and trigger catch+retry
			if (!fetchResult) throw new Error(fetchResultUndefinedErrorMessage)
			if (!fetchResult.ok) throw new Error(fetchResultNotOkErrorMessage)

			// handle success
			const resultActionType = modelFetchRequestAction.noStore
				? MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED
				: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED

			const data = convertCollections(endpointMapping, prepareFetchResult, fetchResult.data)
			// attach guid to result data if it is an object
			if (modelFetchRequestAction.guid && isPlainObject(data)) {
				data.guid = modelFetchRequestAction.guid
			}

			// POST new collection item
			if (isCollectionItemCreate) {
				const modelPathLevels = modelPath.split('.')
				// remove guid
				modelPathLevels.pop()
				// add by new result's id
				yield put<ModelFetchResultAction>({
					type: resultActionType,
					modelPath: `${modelPathLevels.join('.')}.${data.id}`,
					guid: modelFetchRequestAction.guid,
					replaceValue: modelFetchRequestAction.replaceValue,
					data
				})
				// remove temp item under guid key
				yield put<ModelRemoveKeyAction>({
					type: MODEL_REMOVE_KEY_ACTION_TYPE,
					modelPath: modelPath
				})
			}
			// DELETE collection item
			else if (isCollectionItemDelete) {
				yield put<ModelRemoveKeyAction>({
					type: MODEL_REMOVE_KEY_ACTION_TYPE,
					modelPath: modelPath
				})
			} else {
				yield put<ModelFetchResultAction>({
					type: resultActionType,
					modelPath: modelPath,
					guid: modelFetchRequestAction.guid,
					replaceValue: modelFetchRequestAction.replaceValue,
					data
				})
			}
		} catch (caughtError: unknown) {
			// construct `Error` using `caughtError`
			const baseErrorMessage = `'modelFetch' failed for '${modelPath}'`
			const error = constructErrorFromCaughtError(caughtError, 'ModelFetchError', baseErrorMessage)
			// create a default `errorData` object
			const isNetworkOnline = windowService.getIsOnline()
			let errorData: FetchErrorData = isNetworkOnline ? unknownErrorData : networkOfflineErrorData
			// update `errorData` from the `fetchResult.data`
			// for when the fetch got a result, but it was an unsuccessful status
			if (fetchResult?.data && isFetchErrorData(fetchResult.data)) {
				errorData = fetchResult.data
				// update the error's message to include info from the fetch result
				error.message = `${baseErrorMessage}.\n${fetchConfig.method || 'GET'} ${fetchConfig.path} failed.\n${
					errorData.title
				} (${errorData.status})`
				if (errorData.detail) {
					error.message += `: ${errorData.detail}`
				}
			}

			// track `errorData` and `error` for after the while-loop
			lastErrorData = errorData
			lastError = error

			// dispatch that this try failed
			yield put<ModelFetchErrorAction>({
				type: MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED,
				modelPath: modelPath,
				guid: modelFetchRequestAction.guid,
				errorData
			})

			// log to the console
			logger.error(error)

			didFail = true

			// Do not retry if the response is between 400-500 (except 408: request timeout)
			if (
				errorData.status >= HTTP_STATUS_CODE.BAD_REQUEST &&
				errorData.status < HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR &&
				errorData.status !== HTTP_STATUS_CODE.REQUEST_TIMEOUT
			) {
				tryCount = tryLimit
			}

			// if there are tries remaining, perform an exponential backoff
			if (tryCount < tryLimit) {
				yield delay(2 ** (tryCount - 1) * 100) // 100, 200, 400, 800
			}
		}
	} while (tryCount < tryLimit && didFail)

	// if fetch failed completely after all retries
	if (tryCount === tryLimit && didFail) {
		// these will always exist
		const errorData = lastErrorData as FetchErrorData
		const error = lastError as Error

		// dispatch that the fetch failed, which updates `_metadata` (and `guid`, if provided) at the target redux `modelPath`
		yield put<ModelFetchErrorAction>({
			type: modelFetchRequestAction.noStore
				? MODEL_FETCH_ERROR_ACTION_TYPE.TRANSIENT_FETCH_FAILED
				: MODEL_FETCH_ERROR_ACTION_TYPE.FETCH_FAILED,
			modelPath: modelPath,
			guid: modelFetchRequestAction.guid,
			errorData
		})

		// Send error to error handler, except for 401s
		if (errorData.status !== HTTP_STATUS_CODE.UNAUTHORIZED) {
			errorHandler(error, modelFetchRequestAction, fetchConfig, lastFetchResult, errorData)
		}
	}
}

/**
 * The loop saga that makes the request every {action.period} milliseconds until cancelled
 *
 * @param action An action with the request configuration
 */
export function* modelFetchLoop(action: PeriodicModelFetchRequestAction): SagaIterator {
	try {
		let hasFetched = false
		do {
			// call the fetch if this is the first loop iteration,
			// or if the user is not idle
			if (!hasFetched || !isUserIdle()) {
				yield call(modelFetch, action)
				hasFetched = true
			}
			yield delay(action.period)
		} while (true)
	} catch (caughtError: unknown) {
		const error = constructErrorFromCaughtError(caughtError, 'ModelFetchLoopError', 'modelFetchLoop failed')
		logger.error(error)
		// catch and log any unexpected errors not caught inside `fetchData`
		errorHandler(error, action)
	} finally {
		if (yield cancelled()) {
			yield put<PeriodicModelFetchTerminationAction>({
				type: PERIODIC_MODEL_FETCH_TERMINATION_ACTION_TYPE.SUCCEEDED,
				taskId: action.taskId
			})
		}
	}
}

/**
 * Call the fetchData saga every {action.period} milliseconds. This saga requires the 'period' and 'taskId' properties
 * on the action parameter.
 *
 * @param action An action with the request configuration
 */
export function* modelFetchRecurring(action: PeriodicModelFetchRequestAction): SagaIterator {
	if (!action.period) {
		throw new Error("'period' is required")
	}
	if (!action.taskId) {
		throw new Error("'taskId' is required")
	}

	const bgSyncTask = yield fork(modelFetchLoop, action)
	yield take(takeMatchesTerminationAction(action))
	yield cancel(bgSyncTask)
}

/**
 * The main saga for fetching models. Must be initialized with an EndpointMappings object that can be fetched
 * and an API root to prepend to any partial URLs specified in the EndpointMappings object. A logger should normally be provided
 * as well.
 *
 * `EndpointMappings` object require a form as follows (with optional nested models):
 * ```
 * {
 * 	fryModel: {
 * 		path: '/api/Foo'
 * 	},
 * 	groupOfModels: {
 * 		leelaModel: {
 * 			path: '/api/Bar'
 * 		},
 * 		benderModel: {
 * 			path: '/api/Baz'
 * 		}
 * 	}
 * }
 * ```
 * `EndpointMapping` objects are referenced in the actions.DATA_REQUESTED action by path, i.e.
 * `{ type: actions.DATA_REQUESTED, { modelName: 'fryModel' } }`
 * -- or --
 * `{ type: actions.DATA_REQUESTED, { modelName: 'groupOfModels.leelaModel' } }`
 *
 * @export
 * @param endpointMappingsParam A mapping of API endpoints available in the application
 * @param apiRootParam A url to which partial URLs are appended (i.e.) 'https://myapp.com'
 * @param tokenAccessFunctionParam function that returns an optional OAuth token
 * @param errorHandlerParam A function that is called when any fetch exceptions occur
 */
export default function* modelFetchSaga(
	endpointMappingsParam: EndpointMappings,
	apiRootParam?: string,
	tokenAccessFunctionParam: TokenAccessFunction | undefined = defaultTokenAccessFunction,
	errorHandlerParam: ErrorHandler | undefined = defaultErrorHandler
): SagaIterator {
	/* istanbul ignore if */
	if (!endpointMappingsParam) {
		throw new Error("'modelsParam' is required for fetchSaga")
	}
	setApiRoot(apiRootParam)
	setEndpointMappings(endpointMappingsParam)
	errorHandler = errorHandlerParam
	tokenAccessFunction = tokenAccessFunctionParam
	logger = getLogger()

	yield takeEvery(MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelFetch)
	yield takeEvery(MODEL_FETCH_REQUEST_ACTION_TYPE.PERIODIC_FETCH_REQUEST, modelFetchRecurring)
}
