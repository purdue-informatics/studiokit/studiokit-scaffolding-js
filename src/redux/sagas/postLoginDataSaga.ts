import { Saga, SagaIterator } from 'redux-saga'
import { call, take } from 'redux-saga/effects'
import {
	dispatchModelFetchRequest,
	dispatchPeriodicModelFetchRequest,
	dispatchPeriodicModelFetchTerminateAction
} from '../actionCreator'
import { AUTH_ACTION_TYPE, AUTH_TOKEN_SUCCESS_ACTION_TYPE } from '../actions'

//#region Setup

let onPostLoginSaga: Saga

/**
 * Sets or clears a saga that will run after login. It is called inside of `postLoginDataSaga`.
 * * If the saga returns `true`, the parent saga will load "groups" and "externalProviders".
 * * If the saga returns `false`, the parent saga will restart, assuming the child saga has logged out the user.
 * @param value The saga
 */
export const setOnPostLoginSaga = (value: Saga) => {
	onPostLoginSaga = value
}

//#endregion Setup

export default function* postLoginDataSaga(): SagaIterator {
	const externalProvidersTaskId = 'external-provider-refresh'
	while (true) {
		yield take(AUTH_TOKEN_SUCCESS_ACTION_TYPE.GET_TOKEN_SUCCEEDED)
		if (onPostLoginSaga) {
			const result = yield call(onPostLoginSaga)
			if (!result) {
				continue
			}
		}

		// load groups on initial login. refreshing happens elsewhere
		dispatchModelFetchRequest({ modelName: 'groups' })

		// grab external providers once a day (for those tab-leaver-openers)
		dispatchPeriodicModelFetchRequest({
			modelName: 'externalProviders',
			taskId: externalProvidersTaskId,
			period: 1000 * 60 * 60 * 24
		})

		yield take(AUTH_ACTION_TYPE.LOG_OUT_REQUESTED)

		dispatchPeriodicModelFetchTerminateAction({
			taskId: externalProvidersTaskId
		})
	}
}
