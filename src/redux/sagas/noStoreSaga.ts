import { SagaIterator } from '@redux-saga/core'
import { isNil } from 'lodash'
import { AnyAction } from 'redux'
import { race, take, takeEvery } from 'redux-saga/effects'
import { FetchErrorData } from '../../types'
import {
	MODEL_FETCH_ERROR_ACTION_TYPE,
	MODEL_FETCH_REQUEST_ACTION_TYPE,
	MODEL_FETCH_RESULT_ACTION_TYPE,
	ModelFetchErrorAction,
	ModelFetchRequestAction,
	ModelFetchResultAction
} from '../actions'

export type HookFunction<T = any> = (input: T | FetchErrorData | null) => void

//#region Helpers

export const matchesNoStoreAction = (incomingAction: AnyAction): incomingAction is ModelFetchRequestAction => {
	return incomingAction.type === MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST && incomingAction.noStore === true
}

export const matchesFailedNoStoreHookAction = (incomingAction: AnyAction, noStoreAction: ModelFetchRequestAction) => {
	return (
		incomingAction.type === MODEL_FETCH_ERROR_ACTION_TYPE.TRANSIENT_FETCH_FAILED &&
		noStoreAction.noStore === true &&
		incomingAction.guid === noStoreAction.guid
	)
}

export const takeMatchesFailedNoStoreHookAction = (noStoreAction: ModelFetchRequestAction) => (
	incomingAction: AnyAction
) => matchesFailedNoStoreHookAction(incomingAction, noStoreAction)

export const matchesReceivedNoStoreHookAction = (incomingAction: AnyAction, noStoreAction: ModelFetchRequestAction) => {
	return (
		incomingAction.type === MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED &&
		noStoreAction.noStore === true &&
		incomingAction.guid === noStoreAction.guid
	)
}

export const takeMatchesReceivedNoStoreHookAction = (noStoreAction: ModelFetchRequestAction) => (
	incomingAction: AnyAction
) => matchesReceivedNoStoreHookAction(incomingAction, noStoreAction)

//#endregion Helpers

//#region Hooks

const hooks: { [key: string]: HookFunction } = {}

export const registerNoStoreActionHook = <T>(key: string, hook: HookFunction<T>) => {
	hooks[key] = hook
}

export const unregisterNoStoreActionHook = (key: string) => {
	delete hooks[key]
}

export const noStoreHooks = { registerNoStoreActionHook, unregisterNoStoreActionHook }

//#endregion Hooks

export function* handleAction(noStoreAction: ModelFetchRequestAction): SagaIterator {
	const guid = noStoreAction.guid
	if (isNil(guid)) {
		return
	}
	if (isNil(hooks[guid])) {
		return
	}

	const {
		resultAction,
		errorAction
	}: { resultAction?: ModelFetchResultAction; errorAction?: ModelFetchErrorAction } = yield race({
		resultAction: take(takeMatchesReceivedNoStoreHookAction(noStoreAction)),
		errorAction: take(takeMatchesFailedNoStoreHookAction(noStoreAction))
	})

	// check that the hook still exists after the result/error
	const hook = hooks[guid]
	if (isNil(hook)) {
		return
	}

	// return `errorData` on failure. modelFetchSaga will ALWAYS create `errorData`
	if (errorAction) {
		hook(errorAction.errorData)
		return
	}
	// return `data` or `null` on a successful fetch result
	// `data` can technically be `undefined`, e.g. if the server response is a NoContent (204),
	// so the caller will need to determine if `null` is treated as an error or success
	hook(resultAction?.data || null)
}

export default function* noStoreSaga(): SagaIterator {
	/* istanbul ignore next */
	yield takeEvery(matchesNoStoreAction, handleAction)
}
