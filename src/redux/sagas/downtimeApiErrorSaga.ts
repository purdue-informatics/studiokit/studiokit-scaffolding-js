import { push } from 'connected-react-router'
import { AnyAction } from 'redux'
import { put, take } from 'redux-saga/effects'
import { HTTP_STATUS_CODE } from '../../types'
import { isModelTryFetchErrorAction } from '../actions'

export default function* downtimeApiErrorSaga(runIndefinitely = true) {
	do {
		// wait for any downtime error response
		yield take(
			(action: AnyAction) =>
				isModelTryFetchErrorAction(action) &&
				action.errorData?.status === HTTP_STATUS_CODE.FORBIDDEN &&
				!!action.errorData?.title?.includes("We'll be right back!")
		)

		// redirect to downtime route
		yield put(
			push({
				pathname: '/downtime'
			})
		)
	} while (runIndefinitely)
}
