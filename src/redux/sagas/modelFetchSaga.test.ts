/* eslint-disable @typescript-eslint/no-unused-vars */

import { createMockTask } from '@redux-saga/testing-utils'
import MockDate from 'mockdate'
import moment from 'moment-timezone'
import { call, cancel, cancelled, delay, fork, put, takeEvery, takeLatest } from 'redux-saga/effects'
import { v4 as uuidv4 } from 'uuid'
import { networkOfflineErrorData, unknownErrorData } from '../../constants/fetchErrorData'
import {
	forbiddenErrorData,
	internalServerErrorData,
	requestTimedOutErrorData,
	unauthorizedErrorData
} from '../../constants/mockData'
import { sendFetch } from '../../services/fetchService'
import { windowService } from '../../services/windowService'
import { HTTP_STATUS_CODE, OAuthToken, TokenAccessFunction } from '../../types'
import {
	MODEL_FETCH_ERROR_ACTION_TYPE,
	MODEL_FETCH_REQUEST_ACTION_TYPE,
	MODEL_FETCH_RESULT_ACTION_TYPE,
	MODEL_FETCH_START_ACTION_TYPE,
	MODEL_REMOVE_KEY_ACTION_TYPE,
	ModelFetchErrorAction,
	ModelFetchResultAction,
	ModelFetchStartAction,
	ModelRemoveKeyAction,
	PERIODIC_MODEL_FETCH_TERMINATION_ACTION_TYPE,
	PeriodicModelFetchRequestAction,
	PeriodicModelFetchTerminationAction
} from '../actions'
import modelFetchSaga, {
	matchesTerminationAction,
	modelFetch,
	modelFetchLoop,
	modelFetchRecurring,
	takeMatchesTerminationAction,
	TRY_LIMIT
} from './modelFetchSaga'

let lastActivityDate: string | undefined

jest.mock('../../services/dateService', () => {
	return {
		getLastActivityDate: () => lastActivityDate
	}
})

const apiRoot = 'http://www.google.com'
const altApiRoot = 'http://news.ycombinator.com'

const oauthToken: OAuthToken = {
	access_token: 'some-access-token',
	refresh_token: 'some-refresh-token',
	client_id: 'web',
	token_type: 'Bearer',
	expires_in: 3600,
	'.expires': '2019-01-02',
	'.issued': '2019-01-01'
}

/* eslint-disable-next-line require-yield */
const getOAuthToken: TokenAccessFunction = function* () {
	return oauthToken
}

describe('modelFetch', () => {
	let errorName: string | null
	let errorMessage: string | null
	beforeAll(() => {
		const modelFetchSagaGen = modelFetchSaga(
			{
				model: {
					_config: {
						fetch: {
							path: '/api/model'
						}
					}
				},
				entities: {
					_config: {
						fetch: {
							path: 'http://www.google.com/entities'
						},
						isCollection: true
					}
				},
				topLevelEntities: {
					_config: {
						fetch: {
							path: 'http://www.google.com/topLevelEntities'
						},
						isCollection: true
					},
					entityAction: {
						_config: {
							fetch: {
								path: 'entityAction',
								method: 'POST'
							}
						}
					},
					secondLevelEntities: {
						_config: {
							fetch: {
								path: 'secondLevelEntities'
							},
							isCollection: true
						},
						entityAction: {
							_config: {
								fetch: {
									path: 'entityAction'
								}
							}
						}
					}
				},
				topLevelEntitiesNoPath: {
					_config: {
						isCollection: true
					},
					entityAction: {
						_config: {
							fetch: {
								path: 'entityAction',
								method: 'POST'
							}
						}
					},
					secondLevelEntities: {
						_config: {
							isCollection: true
						},
						entityAction: {
							_config: {
								fetch: {
									path: 'entityAction'
								}
							}
						}
					}
				},
				entitiesWithChildEntities: {
					_config: {
						isCollection: true
					},
					childEntities: {
						_config: {
							isCollection: true
						},
						grandchildEntities: {
							_config: {
								isCollection: true
							}
						}
					}
				},
				modelWithChildEntities: {
					_config: {
						fetch: {
							path: '/api/model'
						}
					},
					childEntities: {
						_config: {
							isCollection: true
						},
						grandchildEntities: {
							_config: {
								isCollection: true
							}
						}
					}
				}
			},
			apiRoot,
			getOAuthToken, // this won't get called directly
			error => {
				errorName = error.name
				errorMessage = error.message
			}
		)
		modelFetchSagaGen.next()
	})

	beforeEach(() => {
		errorName = null
		errorMessage = null
		windowService.setIsOnline(true)
	})

	// NOTE: Keep in mind that if you pass a value to gen.next(), that is the value
	// that used to evaluate the previous `yield` call

	describe('before fetch', () => {
		it('should call errorHandler if prepareFetch failed', () => {
			const gen = modelFetch({ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: 'badModel' })
			const startGenerator = gen.next()
			expect(startGenerator.done).toEqual(true)
			expect(errorName).toEqual('PrepareFetchError')
			expect(errorMessage).toEqual("Cannot find 'badModel' in 'endpointMappings'")
		})
		it('should emit FETCH_START', () => {
			const gen = modelFetch({ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: 'model' })
			const startGenerator = gen.next()
			expect(startGenerator.value).toEqual(
				put<ModelFetchStartAction>({ type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START, modelPath: 'model' })
			)
		})
		it('should add oauth token to header if it exists', () => {
			const gen = modelFetch({ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: 'model' })
			const putFetchStartEffect = gen.next()
			const tokenAccessCall = gen.next()
			const fetchEffect = gen.next(oauthToken)
			expect(fetchEffect.value).toEqual(
				call(sendFetch, {
					path: '/api/model',
					headers: { Authorization: 'Bearer some-access-token' },
					queryParams: {}
				})
			)
		})
	})

	describe('successful fetch', () => {
		it('should execute basic fetch w/ object response', () => {
			const gen = modelFetch({ modelName: 'model', type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST })
			const putFetchStartEffect = gen.next()
			expect(putFetchStartEffect.value).toEqual(
				put<ModelFetchStartAction>({ type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START, modelPath: 'model' })
			)
			const tokenAccessCall = gen.next()
			const fetchEffect = gen.next()
			const resultReceivedEffect = gen.next({
				ok: true,
				status: HTTP_STATUS_CODE.OK,
				data: { foo: 'bar' }
			})
			expect(resultReceivedEffect.value).toEqual(
				put<ModelFetchResultAction>({
					type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
					modelPath: 'model',
					data: { foo: 'bar' }
				})
			)
			expect(gen.next().done).toEqual(true)
		})

		it('should execute basic fetch w/ scalar response', () => {
			const gen = modelFetch({ modelName: 'model', type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST })
			const putFetchStartEffect = gen.next()
			const tokenAccessCall = gen.next()
			const fetchEffect = gen.next()
			const resultReceivedEffect = gen.next({
				ok: true,
				status: HTTP_STATUS_CODE.OK,
				data: 'bar'
			})
			expect(resultReceivedEffect.value).toEqual(
				put<ModelFetchResultAction>({
					type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
					modelPath: 'model',
					data: 'bar'
				})
			)
			expect(gen.next().done).toEqual(true)
		})

		it('should execute basic fetch w/ null response', () => {
			const gen = modelFetch({ modelName: 'model', type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST })
			const putFetchStartEffect = gen.next()
			const tokenAccessCall = gen.next()
			const fetchEffect = gen.next()
			const resultReceivedEffect = gen.next({
				ok: true,
				status: HTTP_STATUS_CODE.OK,
				data: null
			})
			expect(resultReceivedEffect.value).toEqual(
				put<ModelFetchResultAction>({
					type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
					modelPath: 'model',
					data: null
				})
			)
			expect(gen.next().done).toEqual(true)
		})

		it('should execute basic fetch with content-type', () => {
			const gen = modelFetch({
				modelName: 'model',
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				contentType: 'text/html; charset=utf-8'
			})
			const putFetchStartEffect = gen.next()
			const tokenAccessCall = gen.next()
			const fetchEffect = gen.next()
			const resultReceivedEffect = gen.next({
				ok: true,
				status: HTTP_STATUS_CODE.OK,
				data: { foo: 'bar' }
			})
			expect(resultReceivedEffect.value).toEqual(
				put<ModelFetchResultAction>({
					type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
					modelPath: 'model',
					data: { foo: 'bar' }
				})
			)
			expect(gen.next().done).toEqual(true)
		})

		it('should execute basic transient fetch', () => {
			const gen = modelFetch({
				modelName: 'model',
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				noStore: true
			})
			const putFetchStartEffect = gen.next()
			const tokenAccessCall = gen.next()
			const fetchEffect = gen.next(oauthToken)
			const resultReceivedEffect = gen.next({
				ok: true,
				status: HTTP_STATUS_CODE.OK,
				data: { foo: 'bar' }
			})
			expect(resultReceivedEffect.value).toEqual(
				put<ModelFetchResultAction>({
					type: MODEL_FETCH_RESULT_ACTION_TYPE.TRANSIENT_FETCH_RESULT_RECEIVED,
					modelPath: 'model',
					data: { foo: 'bar' }
				})
			)
			expect(gen.next().done).toEqual(true)
		})

		it('should return "guid" on fetchResult if passed in "action.guid"', () => {
			const guid = uuidv4()
			const gen = modelFetch({
				modelName: 'model',
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				method: 'POST',
				guid
			})
			const putFetchStartEffect = gen.next()
			const tokenAccessCall = gen.next()
			const fetchEffect = gen.next()
			const resultReceivedEffect = gen.next({
				ok: true,
				status: HTTP_STATUS_CODE.OK,
				data: { foo: 'bar' }
			})
			expect(resultReceivedEffect.value).toEqual(
				put<ModelFetchResultAction>({
					type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
					modelPath: 'model',
					guid,
					data: { foo: 'bar', guid }
				})
			)
			expect(gen.next().done).toEqual(true)
		})
	})

	describe('failed fetch', () => {
		it('should retry on thrown sendFetch error', () => {
			const gen = modelFetch({ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: 'model' })
			const putFetchStartEffect = gen.next()
			const tokenAccessCall = gen.next()
			const fetchEffect = gen.next(oauthToken)
			const sendFetchError = new Error('GET /api/model failed.\nTypeError: Failed to fetch')
			sendFetchError.name = 'SendFetchError'
			const putTryFetchFailedEffect = gen.throw && gen.throw(sendFetchError) // simulate `sendFetch` throwing an exception
			expect(putTryFetchFailedEffect?.value).toEqual(
				put<ModelFetchErrorAction>({
					type: MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED,
					modelPath: 'model',
					errorData: unknownErrorData
				})
			)
			const delayEffect = gen.next()
			const putFetchStartAgainEffect = gen.next()
			expect(putFetchStartAgainEffect.value).toEqual(
				put<ModelFetchStartAction>({ type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START, modelPath: 'model' })
			)
		})

		it('should retry on unsuccessful fetch, with no fetchResult, and create unknown error data', () => {
			const gen = modelFetch({ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: 'model' })
			const putFetchStartEffect = gen.next()
			const tokenAccessCall = gen.next()
			const fetchEffect = gen.next(oauthToken)
			const putTryFetchFailedEffect = gen.next(undefined) // missing fetchResult, should never happen. sendFetch will throw
			expect(putTryFetchFailedEffect.value).toEqual(
				put<ModelFetchErrorAction>({
					type: MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED,
					modelPath: 'model',
					errorData: unknownErrorData
				})
			)
			const delayEffect = gen.next()
			const putFetchStartAgainEffect = gen.next()
			expect(putFetchStartAgainEffect.value).toEqual(
				put<ModelFetchStartAction>({ type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START, modelPath: 'model' })
			)
		})

		it('should retry on unsuccessful fetch, with no error data or status, and create unknown error data', () => {
			const gen = modelFetch({ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: 'model' })
			const putFetchStartEffect = gen.next()
			const tokenAccessCall = gen.next()
			const fetchEffect = gen.next(oauthToken)
			const putTryFetchFailedEffect = gen.next({ ok: false }) // missing data + status (shouldn't ever happen)
			expect(putTryFetchFailedEffect.value).toEqual(
				put<ModelFetchErrorAction>({
					type: MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED,
					modelPath: 'model',
					errorData: unknownErrorData
				})
			)
			const delayEffect = gen.next()
			const putFetchStartAgainEffect = gen.next()
			expect(putFetchStartAgainEffect.value).toEqual(
				put<ModelFetchStartAction>({ type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START, modelPath: 'model' })
			)
		})

		it('should retry on unsuccessful fetch, with no fetchResult, and create no network connection error data', () => {
			const gen = modelFetch({ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: 'model' })
			const putFetchStartEffect = gen.next()
			const tokenAccessCall = gen.next()
			const fetchEffect = gen.next(oauthToken)
			// mock no error connection
			windowService.setIsOnline(false)
			const putTryFetchFailedEffect = gen.next(undefined) // missing fetchResult, should never happen. sendFetch will throw
			expect(putTryFetchFailedEffect.value).toEqual(
				put<ModelFetchErrorAction>({
					type: MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED,
					modelPath: 'model',
					errorData: networkOfflineErrorData
				})
			)
			const delayEffect = gen.next()
			const putFetchStartAgainEffect = gen.next()
			expect(putFetchStartAgainEffect.value).toEqual(
				put<ModelFetchStartAction>({ type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START, modelPath: 'model' })
			)
		})

		it('should retry on unsuccessful fetch, if error status is >= 500', () => {
			const gen = modelFetch({ modelName: 'model', type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST })
			const putFetchStartEffect = gen.next()
			const tokenAccessCall = gen.next()
			const fetchEffect = gen.next(oauthToken)
			const putTryFetchFailedEffect = gen.next({
				ok: false,
				status: HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR,
				data: internalServerErrorData
			})
			expect(putTryFetchFailedEffect.value).toEqual(
				put<ModelFetchErrorAction>({
					type: MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED,
					modelPath: 'model',
					errorData: internalServerErrorData
				})
			)
			const delayEffect = gen.next()
			const putFetchStartAgainEffect = gen.next()
			expect(putFetchStartAgainEffect.value).toEqual(
				put<ModelFetchStartAction>({ type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START, modelPath: 'model' })
			)
		})

		it('should retry on unsuccessful fetch, if error status RequestTimeout (408)', () => {
			const gen = modelFetch({ modelName: 'model', type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST })
			const putFetchStartEffect = gen.next()
			const tokenAccessCall = gen.next()
			const fetchEffect = gen.next(oauthToken)
			const putTryFetchFailedEffect = gen.next({
				ok: false,
				status: HTTP_STATUS_CODE.REQUEST_TIMEOUT,
				data: requestTimedOutErrorData
			})
			expect(putTryFetchFailedEffect.value).toEqual(
				put<ModelFetchErrorAction>({
					type: MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED,
					modelPath: 'model',
					errorData: requestTimedOutErrorData
				})
			)
			const delayEffect = gen.next()
			const putFetchStartAgainEffect = gen.next()
			expect(putFetchStartAgainEffect.value).toEqual(
				put<ModelFetchStartAction>({ type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START, modelPath: 'model' })
			)
		})

		it('should not retry on unsuccessful fetch, if error status is between 400-500 (but not 408), and log to errorHandler', () => {
			const gen = modelFetch({ modelName: 'model', type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST })
			const putFetchStartEffect = gen.next()
			const tokenAccessCall = gen.next()
			const fetchEffect = gen.next(oauthToken)
			const putTryFetchFailedEffect = gen.next({
				ok: false,
				status: HTTP_STATUS_CODE.FORBIDDEN,
				data: forbiddenErrorData
			})
			expect(putTryFetchFailedEffect.value).toEqual(
				put<ModelFetchErrorAction>({
					type: MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED,
					modelPath: 'model',
					errorData: forbiddenErrorData
				})
			)
			// skip delay and exit retry loop
			const putFetchFailedEffect = gen.next()
			expect(putFetchFailedEffect.value).toEqual(
				put<ModelFetchErrorAction>({
					type: MODEL_FETCH_ERROR_ACTION_TYPE.FETCH_FAILED,
					modelPath: 'model',
					errorData: forbiddenErrorData
				})
			)
			const sagaDone = gen.next()
			expect(sagaDone.done).toEqual(true)
			// does log 403s to errorHandler
			expect(errorName).toEqual('ModelFetchError')
			expect(errorMessage).toEqual(
				`'modelFetch' failed for 'model'.\nGET /api/model failed.\nForbidden (403): Your request has been denied. You do not have access.`
			)
		})

		it('should not retry on unsuccessful fetch, if error status is Unauthorized (401), and not log to errorHandler', () => {
			const gen = modelFetch({ modelName: 'model', type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST })
			const putFetchStartEffect = gen.next()
			const tokenAccessCall = gen.next()
			const fetchEffect = gen.next(oauthToken)
			const putTryFetchFailedEffect = gen.next({
				ok: false,
				status: HTTP_STATUS_CODE.UNAUTHORIZED,
				data: unauthorizedErrorData
			})
			expect(putTryFetchFailedEffect.value).toEqual(
				put<ModelFetchErrorAction>({
					type: MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED,
					modelPath: 'model',
					errorData: unauthorizedErrorData
				})
			)
			// skip delay and exit retry loop
			const putFetchFailedEffect = gen.next()
			expect(putFetchFailedEffect.value).toEqual(
				put<ModelFetchErrorAction>({
					type: MODEL_FETCH_ERROR_ACTION_TYPE.FETCH_FAILED,
					modelPath: 'model',
					errorData: unauthorizedErrorData
				})
			)
			const sagaDone = gen.next()
			expect(sagaDone.done).toEqual(true)
			// does not send 401s to errorHandler
			expect(errorName).toEqual(null)
			expect(errorMessage).toEqual(null)
		})

		it('should dispatch FETCH_FAILED when all retries have failed, and log to errorHandler', () => {
			const gen = modelFetch({ modelName: 'model', type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST })
			const putFetchStartEffect = gen.next()
			for (let i = 0; i < TRY_LIMIT; i++) {
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next(oauthToken)
				const putTryFetchFailedEffect = gen.next({
					ok: false,
					status: HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR,
					data: internalServerErrorData
				})
				if (i < TRY_LIMIT - 1) {
					const delayEffect = gen.next()
					const putFetchStartAgainEffect = gen.next()
					expect(putFetchStartAgainEffect.value).toEqual(
						put<ModelFetchStartAction>({
							type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START,
							modelPath: 'model'
						})
					)
				}
			}
			const putFetchFailedEffect = gen.next()
			expect(putFetchFailedEffect.value).toEqual(
				put<ModelFetchErrorAction>({
					type: MODEL_FETCH_ERROR_ACTION_TYPE.FETCH_FAILED,
					modelPath: 'model',
					errorData: internalServerErrorData
				})
			)
			const sagaDone = gen.next()
			expect(sagaDone.done).toEqual(true)
			// does log to errorHandler
			expect(errorName).toEqual('ModelFetchError')
			expect(errorMessage).toEqual(
				`'modelFetch' failed for 'model'.\nGET /api/model failed.\nError (500): An error has occurred.`
			)
		})

		it('should dispatch TRANSIENT_FETCH_FAILED when all retries have failed for `noStore` request', () => {
			const gen = modelFetch({
				modelName: 'model',
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				noStore: true
			})
			const putFetchStartEffect = gen.next()
			for (let i = 0; i < TRY_LIMIT; i++) {
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next(oauthToken)
				const putTryFetchFailedEffect = gen.next({
					ok: false,
					status: HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR,
					data: internalServerErrorData
				})
				if (i < TRY_LIMIT - 1) {
					const delayEffect = gen.next()
					const putFetchStartAgainEffect = gen.next()
					expect(putFetchStartAgainEffect.value).toEqual(
						put<ModelFetchStartAction>({
							type: MODEL_FETCH_START_ACTION_TYPE.TRANSIENT_FETCH_START,
							modelPath: 'model'
						})
					)
				}
			}
			const putFetchFailedEffect = gen.next()
			expect(putFetchFailedEffect.value).toEqual(
				put<ModelFetchErrorAction>({
					type: MODEL_FETCH_ERROR_ACTION_TYPE.TRANSIENT_FETCH_FAILED,
					modelPath: 'model',
					errorData: internalServerErrorData
				})
			)
			const sagaDone = gen.next()
			expect(sagaDone.done).toEqual(true)
			// does log to errorHandler
			expect(errorName).toEqual('ModelFetchError')
			expect(errorMessage).toEqual(
				`'modelFetch' failed for 'model'.\nGET /api/model failed.\nError (500): An error has occurred.`
			)
		})

		it('should not call errorHandler for try fetch error', () => {
			const gen = modelFetch({ modelName: 'model', type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST })
			const putFetchStartEffect = gen.next()
			const tokenAccessCall = gen.next()
			const fetchEffect = gen.next(oauthToken)
			const putTryFetchFailedEffect = gen.next({
				ok: false,
				status: HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR,
				data: internalServerErrorData
			})
			const delayEffect = gen.next()
			const putFetchStartAgainEffect = gen.next()
			expect(errorMessage).toEqual(null)
		})

		it('should return generic unknown error errorData for a thrown generic Error', () => {
			const gen = modelFetch({
				modelName: 'model',
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				noRetry: true
			})
			const putFetchStartEffect = gen.next()
			const tokenAccessCall = gen.next()
			const fetchEffect = gen.next(oauthToken)
			const genericError = new Error('some generic error')
			const throwFetchErrorEffect = gen.throw && gen.throw(genericError)
			expect(throwFetchErrorEffect?.value).toEqual(
				put<ModelFetchErrorAction>({
					type: MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED,
					modelPath: 'model',
					errorData: unknownErrorData
				})
			)
			// no retry = no delay etc.
			const putErrorEffect = gen.next()
			expect(putErrorEffect.value).toEqual(
				put<ModelFetchErrorAction>({
					type: MODEL_FETCH_ERROR_ACTION_TYPE.FETCH_FAILED,
					modelPath: 'model',
					errorData: unknownErrorData
				})
			)
			const sagaDone = gen.next()
			expect(sagaDone.done).toEqual(true)
			expect(errorName).toEqual('ModelFetchError')
			expect(errorMessage).toEqual(`'modelFetch' failed for 'model'.\nsome generic error`)
		})

		it('should return generic unknown error errorData for a thrown SendFetchError', () => {
			const gen = modelFetch({
				modelName: 'model',
				type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
				noRetry: true
			})
			const putFetchStartEffect = gen.next()
			const tokenAccessCall = gen.next()
			const fetchEffect = gen.next(oauthToken)
			const sendFetchError = new Error('No response was received')
			sendFetchError.name = 'SendFetchError'
			const throwFetchErrorEffect = gen.throw && gen.throw(sendFetchError)

			expect(throwFetchErrorEffect?.value).toEqual(
				put<ModelFetchErrorAction>({
					type: MODEL_FETCH_ERROR_ACTION_TYPE.TRY_FETCH_FAILED,
					modelPath: 'model',
					errorData: unknownErrorData
				})
			)
			// no retry = no delay etc.
			const putErrorEffect = gen.next()
			expect(putErrorEffect.value).toEqual(
				put<ModelFetchErrorAction>({
					type: MODEL_FETCH_ERROR_ACTION_TYPE.FETCH_FAILED,
					modelPath: 'model',
					errorData: unknownErrorData
				})
			)
			const sagaDone = gen.next()
			expect(sagaDone.done).toEqual(true)
			expect(errorName).toEqual('ModelFetchError')
			expect(errorMessage).toEqual(`'modelFetch' failed for 'model'.\nSendFetchError: No response was received`)
		})
	})

	describe('collection fetch', () => {
		describe('GET collection', () => {
			it('should return a key-value object by id of nested items, from an api array', () => {
				const fetchedAt = new Date()
				MockDate.set(fetchedAt)

				const gen = modelFetch({
					modelName: 'entities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: [
						{ id: 1, name: 'foo' },
						{ id: 2, name: 'bar' }
					]
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						modelPath: 'entities',
						data: {
							1: {
								id: 1,
								name: 'foo'
							},
							2: {
								id: 2,
								name: 'bar'
							}
						}
					})
				)
				expect(gen.next().done).toEqual(true)
			})

			it('should return a key-value object by id of nested items, from a key-value api object', () => {
				const fetchedAt = new Date()
				MockDate.set(fetchedAt)

				const gen = modelFetch({
					modelName: 'entities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: { 1: { id: 1, name: 'foo' }, 2: { id: 2, name: 'bar' } }
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						modelPath: 'entities',
						data: {
							1: {
								id: 1,
								name: 'foo'
							},
							2: {
								id: 2,
								name: 'bar'
							}
						}
					})
				)
				expect(gen.next().done).toEqual(true)
			})

			it('should succeed with a null response', () => {
				const gen = modelFetch({
					modelName: 'entities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: null
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						modelPath: 'entities',
						data: null
					})
				)
				expect(gen.next().done).toEqual(true)
			})
		})

		describe('GET item', () => {
			it('should add new single entity by id', () => {
				const guid = uuidv4()
				const gen = modelFetch({
					modelName: 'entities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					pathParams: [2],
					guid
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: { id: 2, name: 'bar' }
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						data: { id: 2, name: 'bar', guid },
						guid,
						modelPath: 'entities.2'
					})
				)
				expect(gen.next().done).toEqual(true)
			})

			it('should succeed with null response', () => {
				const guid = uuidv4()
				const gen = modelFetch({
					modelName: 'entities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					pathParams: [2],
					guid
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: null
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						data: null,
						guid,
						modelPath: 'entities.2'
					})
				)
				expect(gen.next().done).toEqual(true)
			})
		})

		describe('POST item', () => {
			it('should store a temp item under "guid" key during request', () => {
				const guid = uuidv4()
				const gen = modelFetch({
					modelName: 'entities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					method: 'POST',
					guid
				})
				const putFetchStartEffect = gen.next()
				expect(putFetchStartEffect.value).toEqual(
					put<ModelFetchStartAction>({
						type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START,
						modelPath: `entities.${guid}`,
						guid
					})
				)
			})

			it('should add new item by "id" key after request', () => {
				const guid = uuidv4()
				const gen = modelFetch({
					modelName: 'entities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					method: 'POST',
					guid,
					body: { name: 'baz' }
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: { id: 3, name: 'baz' }
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						data: { id: 3, name: 'baz', guid },
						guid,
						modelPath: 'entities.3'
					})
				)
			})

			it('should remove temp item by "guid" key after request', () => {
				const guid = uuidv4()
				const gen = modelFetch({
					modelName: 'entities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					method: 'POST',
					guid,
					body: { name: 'baz' }
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: { id: 3, name: 'baz' }
				})
				const resultStoredEffect = gen.next()
				expect(resultStoredEffect.value).toEqual(
					put<ModelRemoveKeyAction>({
						type: MODEL_REMOVE_KEY_ACTION_TYPE,
						modelPath: `entities.${guid}`
					})
				)
			})
		})

		describe('DELETE item', () => {
			it('should remove item by id', () => {
				const guid = uuidv4()
				const gen = modelFetch({
					modelName: 'entities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					method: 'DELETE',
					pathParams: [2],
					guid
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.NO_CONTENT,
					data: undefined
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelRemoveKeyAction>({
						type: MODEL_REMOVE_KEY_ACTION_TYPE,
						modelPath: 'entities.2'
					})
				)
				expect(gen.next().done).toEqual(true)
			})
		})

		describe('GET item action', () => {
			it('should add result on single nested entity by id', () => {
				const guid = uuidv4()
				const gen = modelFetch({
					modelName: 'topLevelEntities.entityAction',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					pathParams: [2],
					guid
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: { foo: 'bar' }
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						data: { foo: 'bar', guid },
						guid,
						modelPath: 'topLevelEntities.2.entityAction'
					})
				)
				expect(gen.next().done).toEqual(true)
			})
		})
	})

	describe('nested collection fetch', () => {
		describe('GET nested collection', () => {
			it('should return a key-value object by id of nested items, from an api array', () => {
				const fetchedAt = new Date()
				MockDate.set(fetchedAt)

				const gen = modelFetch({
					modelName: 'topLevelEntities.secondLevelEntities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					pathParams: [1]
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: [
						{ id: 1, name: 'foo' },
						{ id: 2, name: 'bar' }
					]
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						data: {
							1: {
								id: 1,
								name: 'foo'
							},
							2: {
								id: 2,
								name: 'bar'
							}
						},
						modelPath: 'topLevelEntities.1.secondLevelEntities'
					})
				)
				expect(gen.next().done).toEqual(true)
			})

			it('should return a key-value object by id of nested items, from a key-value api object', () => {
				const fetchedAt = new Date()
				MockDate.set(fetchedAt)

				const gen = modelFetch({
					modelName: 'topLevelEntities.secondLevelEntities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					pathParams: [1]
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: { 1: { id: 1, name: 'foo' }, 2: { id: 2, name: 'bar' } }
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						data: {
							1: {
								id: 1,
								name: 'foo'
							},
							2: {
								id: 2,
								name: 'bar'
							}
						},
						modelPath: 'topLevelEntities.1.secondLevelEntities'
					})
				)
				expect(gen.next().done).toEqual(true)
			})
		})

		describe('GET item', () => {
			it('should add new single nested entity by id', () => {
				const guid = uuidv4()
				const gen = modelFetch({
					modelName: 'topLevelEntities.secondLevelEntities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					pathParams: [2, 999],
					guid
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: { id: 999, name: 'bar' }
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						data: { id: 999, name: 'bar', guid },
						guid,
						modelPath: 'topLevelEntities.2.secondLevelEntities.999'
					})
				)
				expect(gen.next().done).toEqual(true)
			})
		})

		describe('POST item', () => {
			it('should store a temp item under "guid" key when fetch starts', () => {
				const guid = uuidv4()
				const gen = modelFetch({
					modelName: 'topLevelEntities.secondLevelEntities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					pathParams: [1],
					method: 'POST',
					guid
				})
				const putFetchStartEffect = gen.next()
				expect(putFetchStartEffect.value).toEqual(
					put<ModelFetchStartAction>({
						type: MODEL_FETCH_START_ACTION_TYPE.FETCH_START,
						modelPath: `topLevelEntities.1.secondLevelEntities.${guid}`,
						guid
					})
				)
			})

			it('should add new item by "id" key after request', () => {
				const guid = uuidv4()
				const gen = modelFetch({
					modelName: 'topLevelEntities.secondLevelEntities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					pathParams: [1],
					method: 'POST',
					guid,
					body: { name: 'baz' }
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: { id: 3, name: 'baz' }
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						data: { id: 3, name: 'baz', guid },
						guid,
						modelPath: 'topLevelEntities.1.secondLevelEntities.3'
					})
				)
			})

			it('should remove temp item by "guid" key after request', () => {
				const guid = uuidv4()
				const gen = modelFetch({
					modelName: 'topLevelEntities.secondLevelEntities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					pathParams: [1],
					method: 'POST',
					guid,
					body: { name: 'baz' }
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: { id: 3, name: 'baz' }
				})
				const resultStoredEffect = gen.next()
				expect(resultStoredEffect.value).toEqual(
					put<ModelRemoveKeyAction>({
						type: MODEL_REMOVE_KEY_ACTION_TYPE,
						modelPath: `topLevelEntities.1.secondLevelEntities.${guid}`
					})
				)
			})
		})

		describe('DELETE item', () => {
			it('should remove item by id', () => {
				const guid = uuidv4()
				const gen = modelFetch({
					modelName: 'topLevelEntities.secondLevelEntities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					method: 'DELETE',
					pathParams: [1, 2],
					guid
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.NO_CONTENT,
					data: undefined
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelRemoveKeyAction>({
						type: MODEL_REMOVE_KEY_ACTION_TYPE,
						modelPath: 'topLevelEntities.1.secondLevelEntities.2'
					})
				)
				expect(gen.next().done).toEqual(true)
			})
		})

		describe('GET item action', () => {
			it('should add result on single nested entity by id', () => {
				const guid = uuidv4()
				const gen = modelFetch({
					modelName: 'topLevelEntities.secondLevelEntities.entityAction',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					pathParams: [2, 999],
					guid
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: { foo: 'bar' }
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						data: { foo: 'bar', guid },
						guid,
						modelPath: 'topLevelEntities.2.secondLevelEntities.999.entityAction'
					})
				)
				expect(gen.next().done).toEqual(true)
			})
		})
	})

	describe('child collection inside of parent collection fetch', () => {
		describe('GET collection containing child collection', () => {
			it('should return a key-value object by id of items, from an api array, and convert child collection', () => {
				const fetchedAt = new Date()
				MockDate.set(fetchedAt)

				const gen = modelFetch({
					modelName: 'entitiesWithChildEntities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: [
						{
							id: 1,
							foo: 'bar',
							childEntities: [
								{ id: 1, name: 'foo' },
								{ id: 2, name: 'bar' }
							]
						}
					]
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						data: {
							1: {
								id: 1,
								foo: 'bar',
								childEntities: {
									1: {
										id: 1,
										name: 'foo'
									},
									2: {
										id: 2,
										name: 'bar'
									}
								}
							}
						},
						modelPath: 'entitiesWithChildEntities'
					})
				)
				expect(gen.next().done).toEqual(true)
			})

			it('should return a key-value object by id of items, from a key-value api object, and convert child collection', () => {
				const fetchedAt = new Date()
				MockDate.set(fetchedAt)

				const gen = modelFetch({
					modelName: 'entitiesWithChildEntities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: {
						1: {
							id: 1,
							foo: 'bar',
							childEntities: { 1: { id: 1, name: 'foo' }, 2: { id: 2, name: 'bar' } }
						}
					}
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						data: {
							1: {
								id: 1,
								foo: 'bar',
								childEntities: {
									1: {
										id: 1,
										name: 'foo'
									},
									2: {
										id: 2,
										name: 'bar'
									}
								}
							}
						},
						modelPath: 'entitiesWithChildEntities'
					})
				)
				expect(gen.next().done).toEqual(true)
			})

			it('should return a key-value object by id of items, from an api array, and convert child collection at many levels', () => {
				const fetchedAt = new Date()
				MockDate.set(fetchedAt)

				const gen = modelFetch({
					modelName: 'entitiesWithChildEntities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: [
						{
							id: 1,
							foo: 'bar',
							childEntities: [
								{
									id: 1,
									name: 'foo',
									grandchildEntities: [
										{ id: 1, name: 'g-foo' },
										{ id: 2, name: 'g-bar' }
									]
								},
								{
									id: 2,
									name: 'bar',
									grandchildEntities: [
										{ id: 3, name: 'g-foo-2' },
										{ id: 4, name: 'g-bar-2' }
									]
								}
							]
						}
					]
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						data: {
							1: {
								id: 1,
								foo: 'bar',
								childEntities: {
									1: {
										id: 1,
										name: 'foo',
										grandchildEntities: {
											1: { id: 1, name: 'g-foo' },
											2: { id: 2, name: 'g-bar' }
										}
									},
									2: {
										id: 2,
										name: 'bar',
										grandchildEntities: {
											3: { id: 3, name: 'g-foo-2' },
											4: { id: 4, name: 'g-bar-2' }
										}
									}
								}
							}
						},
						modelPath: 'entitiesWithChildEntities'
					})
				)
				expect(gen.next().done).toEqual(true)
			})
		})
	})

	describe('child collection inside of parent collection item fetch', () => {
		describe('GET collection item containing child collection', () => {
			it('should return an object, and convert child collection to a key-value object by id of items, from an api array', () => {
				const fetchedAt = new Date()
				MockDate.set(fetchedAt)

				const gen = modelFetch({
					modelName: 'entitiesWithChildEntities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					pathParams: [1]
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: {
						id: 1,
						foo: 'bar',
						childEntities: [
							{ id: 1, name: 'foo' },
							{ id: 2, name: 'bar' }
						]
					}
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						data: {
							id: 1,
							foo: 'bar',
							childEntities: {
								1: {
									id: 1,
									name: 'foo'
								},
								2: {
									id: 2,
									name: 'bar'
								}
							}
						},
						modelPath: 'entitiesWithChildEntities.1'
					})
				)
				expect(gen.next().done).toEqual(true)
			})

			it('should return an object, and convert child collection to a key-value object by id of items, from a key-value api object', () => {
				const fetchedAt = new Date()
				MockDate.set(fetchedAt)

				const gen = modelFetch({
					modelName: 'entitiesWithChildEntities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					pathParams: [1]
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: { id: 1, foo: 'bar', childEntities: { 1: { id: 1, name: 'foo' }, 2: { id: 2, name: 'bar' } } }
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						data: {
							id: 1,
							foo: 'bar',
							childEntities: {
								1: {
									id: 1,
									name: 'foo'
								},
								2: {
									id: 2,
									name: 'bar'
								}
							}
						},
						modelPath: 'entitiesWithChildEntities.1'
					})
				)
				expect(gen.next().done).toEqual(true)
			})

			it('should return an object, and convert child collections to a key-value object by id of items, from an api array, at many levels', () => {
				const fetchedAt = new Date()
				MockDate.set(fetchedAt)

				const gen = modelFetch({
					modelName: 'entitiesWithChildEntities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					pathParams: [1]
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: {
						id: 1,
						foo: 'bar',
						childEntities: [
							{
								id: 1,
								name: 'foo',
								grandchildEntities: [
									{ id: 1, name: 'g-foo' },
									{ id: 2, name: 'g-bar' }
								]
							},
							{
								id: 2,
								name: 'bar',
								grandchildEntities: null
							}
						]
					}
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						data: {
							id: 1,
							foo: 'bar',
							childEntities: {
								1: {
									id: 1,
									name: 'foo',
									grandchildEntities: {
										1: { id: 1, name: 'g-foo' },
										2: { id: 2, name: 'g-bar' }
									}
								},
								2: {
									id: 2,
									name: 'bar',
									grandchildEntities: null
								}
							}
						},
						modelPath: 'entitiesWithChildEntities.1'
					})
				)
				expect(gen.next().done).toEqual(true)
			})
		})
	})

	describe('child collection inside of non-collection fetch', () => {
		describe('GET containing child collection', () => {
			it('should return an object, and convert child collection to a key-value object by id of items, from an api array', () => {
				const gen = modelFetch({
					modelName: 'modelWithChildEntities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: {
						childEntities: [
							{ id: 1, name: 'foo' },
							{ id: 2, name: 'bar' }
						]
					}
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						data: {
							childEntities: {
								1: {
									id: 1,
									name: 'foo'
								},
								2: {
									id: 2,
									name: 'bar'
								}
							}
						},
						modelPath: 'modelWithChildEntities'
					})
				)
				expect(gen.next().done).toEqual(true)
			})

			it('should return an object, and convert child collection to a key-value object by id of items, from a key-value api object', () => {
				const gen = modelFetch({
					modelName: 'modelWithChildEntities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: {
						childEntities: { 1: { id: 1, name: 'foo' }, 2: { id: 2, name: 'bar' } }
					}
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						data: {
							childEntities: {
								1: {
									id: 1,
									name: 'foo'
								},
								2: {
									id: 2,
									name: 'bar'
								}
							}
						},
						modelPath: 'modelWithChildEntities'
					})
				)
				expect(gen.next().done).toEqual(true)
			})

			it('should return an object, and convert child collections to a key-value object by id of items, from an api array, at many levels', () => {
				const gen = modelFetch({
					modelName: 'modelWithChildEntities',
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST
				})
				const putFetchStartEffect = gen.next()
				const tokenAccessCall = gen.next()
				const fetchEffect = gen.next()
				const resultReceivedEffect = gen.next({
					ok: true,
					status: HTTP_STATUS_CODE.OK,
					data: {
						nonCollectionArray: [
							{
								id: 1,
								name: 'foo'
							},
							{
								id: 2,
								name: 'bar'
							}
						],
						childEntities: [
							{
								id: 1,
								name: 'foo',
								grandchildEntities: [
									{ id: 1, name: 'g-foo' },
									{ id: 2, name: 'g-bar' }
								]
							},
							{
								id: 2,
								name: 'bar',
								grandchildEntities: [
									{ id: 3, name: 'g-foo-2' },
									{ id: 4, name: 'g-bar-2' }
								]
							}
						]
					}
				})
				expect(resultReceivedEffect.value).toEqual(
					put<ModelFetchResultAction>({
						type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
						data: {
							nonCollectionArray: [
								{
									id: 1,
									name: 'foo'
								},
								{
									id: 2,
									name: 'bar'
								}
							],
							childEntities: {
								1: {
									id: 1,
									name: 'foo',
									grandchildEntities: {
										1: { id: 1, name: 'g-foo' },
										2: { id: 2, name: 'g-bar' }
									}
								},
								2: {
									id: 2,
									name: 'bar',
									grandchildEntities: {
										3: { id: 3, name: 'g-foo-2' },
										4: { id: 4, name: 'g-bar-2' }
									}
								}
							}
						},
						modelPath: 'modelWithChildEntities'
					})
				)
				expect(gen.next().done).toEqual(true)
			})
		})
	})
})

describe('modelFetchLoop', () => {
	let action: PeriodicModelFetchRequestAction
	beforeEach(() => {
		action = {
			type: MODEL_FETCH_REQUEST_ACTION_TYPE.PERIODIC_FETCH_REQUEST,
			modelName: 'foo',
			period: 1000,
			taskId: 'foo-task'
		}
	})

	it('should fetch repeatedly until cancelled', () => {
		const gen = modelFetchLoop(action)
		let callFetchDataEffect = gen.next()
		expect(callFetchDataEffect.value).toEqual(call(modelFetch, action))
		let delayEffect = gen.next()
		expect(delayEffect.value).toEqual(delay(1000))

		callFetchDataEffect = gen.next()
		expect(callFetchDataEffect.value).toEqual(call(modelFetch, action))
		delayEffect = gen.next()
		expect(delayEffect.value).toEqual(delay(1000))

		const cancelledSaga = gen.return ? gen.return() : { value: false }
		expect(cancelledSaga.value).toEqual(cancelled())

		const putPeriodicTerminationSucceededEffect = gen.next(true)
		expect(putPeriodicTerminationSucceededEffect.value).toEqual(
			put<PeriodicModelFetchTerminationAction>({
				type: PERIODIC_MODEL_FETCH_TERMINATION_ACTION_TYPE.SUCCEEDED,
				taskId: 'foo-task'
			})
		)

		const sagaDone = gen.next()
		expect(sagaDone.done).toEqual(true)
	})

	it('should fetch on first iteration even if user is idle', () => {
		// set user to be "idle"
		lastActivityDate = moment().add(-15, 'minutes').toISOString()

		const gen = modelFetchLoop(action)
		const callFetchDataEffect = gen.next()
		expect(callFetchDataEffect.value).toEqual(call(modelFetch, action))
		const delayEffect = gen.next()
		expect(delayEffect.value).toEqual(delay(1000))

		lastActivityDate = undefined
	})

	it('should not fetch repeatedly if user is idle', () => {
		// set user to have no tracked activity yet
		lastActivityDate = undefined

		const gen = modelFetchLoop(action)
		let callFetchDataEffect = gen.next()
		expect(callFetchDataEffect.value).toEqual(call(modelFetch, action))
		let delayEffect = gen.next()
		expect(delayEffect.value).toEqual(delay(1000))

		// set user to have recent activity
		lastActivityDate = moment().toISOString()

		callFetchDataEffect = gen.next()
		expect(callFetchDataEffect.value).toEqual(call(modelFetch, action))
		delayEffect = gen.next()
		expect(delayEffect.value).toEqual(delay(1000))

		// set user to be "idle"
		lastActivityDate = moment().add(-15, 'minutes').toISOString()

		delayEffect = gen.next()
		expect(delayEffect.value).toEqual(delay(1000))

		lastActivityDate = undefined
	})

	it('should fetch repeatedly until error is thrown', () => {
		const gen = modelFetchLoop(action)
		let callFetchDataEffect = gen.next()
		expect(callFetchDataEffect.value).toEqual(call(modelFetch, action))
		let delayEffect = gen.next()
		expect(delayEffect.value).toEqual(delay(1000))

		callFetchDataEffect = gen.next()
		expect(callFetchDataEffect.value).toEqual(call(modelFetch, action))
		delayEffect = gen.next()
		expect(delayEffect.value).toEqual(delay(1000))

		const throwEffect = gen.throw ? gen.throw('error') : { value: false }
		expect(throwEffect.value).toEqual(cancelled())

		// send "false" because this was an error not a cancellation
		const putPeriodicTerminationSucceededEffect = gen.next(false)
		const sagaDone = gen.next()
		expect(sagaDone.done).toEqual(true)
	})
})

describe('modelFetchRecurring', () => {
	let action: PeriodicModelFetchRequestAction
	beforeEach(() => {
		action = {
			type: MODEL_FETCH_REQUEST_ACTION_TYPE.PERIODIC_FETCH_REQUEST,
			modelName: 'foo',
			period: 1000,
			taskId: 'foo-task'
		}
	})

	it('should throw without action.period', () => {
		;(action as any).period = undefined
		const gen = modelFetchRecurring(action)
		expect(() => {
			const putFetchStartEffect = gen.next()
		}).toThrow("'period' is required")
	})

	it('should throw without action.taskId', () => {
		action.taskId = ''
		const gen = modelFetchRecurring(action)
		expect(() => {
			const putFetchStartEffect = gen.next()
		}).toThrow("'taskId' is required")
	})

	it('should fork off modelFetchLoop loop if all params are given', () => {
		const gen = modelFetchRecurring(action)
		const forkEffect = gen.next()
		expect(forkEffect.value).toEqual(fork(modelFetchLoop, action))
	})

	it('should call matchesTerminationAction from takeMatchesTerminationAction', () => {
		expect(
			takeMatchesTerminationAction(action)({
				type: PERIODIC_MODEL_FETCH_TERMINATION_ACTION_TYPE.TERMINATE,
				taskId: action.taskId
			})
		).toEqual(true)
	})

	it('should not cancel if action is not a cancel for that task', () => {
		expect(matchesTerminationAction({ type: 'foo' }, action)).toEqual(false)
	})

	it('should not cancel if action is a cancel for another task', () => {
		expect(
			matchesTerminationAction(
				{
					type: PERIODIC_MODEL_FETCH_TERMINATION_ACTION_TYPE.TERMINATE,
					taskId: 'someOtherTask'
				},
				action
			)
		).toEqual(false)
	})

	it('should cancel if action is a cancel for that task', () => {
		expect(
			matchesTerminationAction(
				{
					type: PERIODIC_MODEL_FETCH_TERMINATION_ACTION_TYPE.TERMINATE,
					taskId: action.taskId
				},
				action
			)
		).toEqual(true)
	})

	it('should cancel once take matches action', () => {
		const gen = modelFetchRecurring(action)
		const forkEffect = gen.next()
		const mockTask = createMockTask()
		const takeTerminationEffect = gen.next(mockTask)
		const cancelledTask = gen.next()
		expect(cancelledTask.value).toEqual(cancel(mockTask))
	})
})

describe('modelFetchSaga', () => {
	it('should set up all takes', () => {
		const gen = modelFetchSaga({})
		const takeEveryDataRequestEffect = gen.next()
		expect(takeEveryDataRequestEffect.value).toEqual(
			takeEvery(MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelFetch)
		)
		const takeEveryPeriodicDataRequestEffect = gen.next()
		expect(takeEveryPeriodicDataRequestEffect.value).toEqual(
			takeEvery(MODEL_FETCH_REQUEST_ACTION_TYPE.PERIODIC_FETCH_REQUEST, modelFetchRecurring)
		)
		expect(gen.next().done).toEqual(true)
	})
})
