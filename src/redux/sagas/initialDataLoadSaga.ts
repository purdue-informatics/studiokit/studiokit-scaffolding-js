import { dispatchPeriodicModelFetchRequest } from '../actionCreator'

export default function initialDataLoadSaga() {
	dispatchPeriodicModelFetchRequest({
		modelName: 'configuration',
		period: 1000 * 60 * 60 * 24, // grab configs once a day (for those tab-leaver-openers)
		taskId: 'configuration-refresh'
	})
	dispatchPeriodicModelFetchRequest({
		modelName: 'externalTerms',
		period: 1000 * 60 * 60 * 24, // grab external terms once a day (for those tab-leaver-openers)
		taskId: 'external-term-refresh'
	})
	dispatchPeriodicModelFetchRequest({
		modelName: 'identityProviders',
		period: 1000 * 60 * 60 * 24, // grab identity providers once a day (for those tab-leaver-openers)
		taskId: 'identity-provider-refresh'
	})
	dispatchPeriodicModelFetchRequest({
		modelName: 'client',
		period: 1000 * 60 * 15, // grab client every 15 minutes
		taskId: 'client-refresh'
	})
	dispatchPeriodicModelFetchRequest({
		modelName: 'time',
		period: 1000 * 60 * 15, // grab accurate server time every 15 minutes
		taskId: 'time-sync',
		noStore: true
	})
}
