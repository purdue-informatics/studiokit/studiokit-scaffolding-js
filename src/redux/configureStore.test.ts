import { MODAL_ACTION_TYPE, MODEL_FETCH_REQUEST_ACTION_TYPE, ModelFetchRequestAction } from './actions'
import { actionTransformer, stripState } from './configureStore'

describe('actionTransformer', () => {
	it('should return null for unsupported actions', () => {
		expect(actionTransformer({ type: MODAL_ACTION_TYPE.MODAL_ENTERING })).toEqual(null)
		expect(actionTransformer({ type: MODAL_ACTION_TYPE.MODAL_EXITED })).toEqual(null)
	})
	it('should filter sensitive keys', () => {
		const filteredAction = actionTransformer({
			type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
			body: {
				puid: 'Sensitive',
				employeeNumber: 'Sensitive',
				array: [
					{
						access_token: 'Sensitive',
						subObject: {
							refresh_token: 'Sensitive',
							subArray: [
								{
									password: 'Sensitive'
								}
							]
						}
					}
				]
			}
		})
		expect(filteredAction.body.puid).toEqual('[Filtered]')
		expect(filteredAction.body.employeeNumber).toEqual('[Filtered]')
		expect(filteredAction.body.array[0].access_token).toEqual('[Filtered]')
		expect(filteredAction.body.array[0].subObject.refresh_token).toEqual('[Filtered]')
		expect(filteredAction.body.array[0].subObject.subArray[0].password).toEqual('[Filtered]')
	})
	it('should filter artifact content', () => {
		const modelFetchRequestAction: ModelFetchRequestAction = {
			type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
			modelName: 'problems',
			method: 'PUT',
			pathParams: [11094],
			body: {
				id: 11094,
				name: '1',
				tags: '',
				isDeleted: false,
				statements: [
					{
						id: 11608,
						solutions: [
							{
								id: 15368,
								formula: '1',
								responseType: 'Numeric',
								prefix: '',
								suffix: '',
								tolerance: '2',
								toleranceType: 'Percentage',
								rounding: 2,
								numberFormatType: 'Number',
								choices: []
							}
						],
						contentVariableNames: ['a']
					},
					{
						id: 11609,
						solutions: [
							{
								id: 15369,
								formula: '',
								responseType: 'Numeric',
								prefix: '',
								suffix: '',
								tolerance: '2',
								toleranceType: 'Percentage',
								rounding: 2,
								numberFormatType: 'Number',
								choices: []
							}
						],
						contentVariableNames: []
					}
				],
				variables: [
					{
						id: 30504,
						name: 'a',
						parameters: '[undefined]',
						variableType: 'RichText',
						variableArtifacts: [
							{ id: 628 },
							{
								id: '[undefined]',
								content: 'BIG IMAGE'
							}
						]
					}
				]
			}
		}
		const filteredAction = actionTransformer(modelFetchRequestAction)
		expect(filteredAction.body.variables[0].variableArtifacts[1].content).toEqual('[Filtered]')
	})
})

describe('stripState', () => {
	it('should only keep ids and metadata', () => {
		const state = {
			models: {
				groups: {
					_metadata: {
						isSuccess: true
					},
					1: {
						id: 1,
						name: 'Name',
						array: [
							{
								_metadata: {
									isSuccess: true
								},
								id: 888,
								type: 'Thing'
							},
							{
								_metadata: {
									isSuccess: true
								},
								id: 999,
								type: 'Stuff'
							}
						]
					}
				}
			}
		}
		const expectedState = {
			models: {
				groups: {
					_metadata: {
						isSuccess: true
					},
					1: {
						id: 1
					}
				}
			}
		}
		expect(stripState(state)).toEqual(expectedState)
	})
})
