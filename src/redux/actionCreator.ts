import { Action, Store } from 'redux'
import { v4 as uuidv4 } from 'uuid'
import { Notification } from '../types/Notification'
import {
	MODEL_FETCH_REQUEST_ACTION_TYPE,
	MODEL_REMOVE_KEY_ACTION_TYPE,
	ModelFetchRequestAction,
	ModelRemoveKeyAction,
	PERIODIC_MODEL_FETCH_TERMINATION_ACTION_TYPE,
	PeriodicModelFetchRequestAction,
	PeriodicModelFetchTerminationAction
} from './actions'
import { NOTIFICATION_ACTION_TYPE, NotificationAction } from './actions/NotificationAction'

let store: Store

export const setStore = (storeParam: Store): void => {
	store = storeParam
}

export const dispatchAction = <A extends Action>(action: A): void => {
	store.dispatch(action)
}

export const dispatchModelFetchRequest = (action: Omit<ModelFetchRequestAction, 'type'>) => {
	dispatchAction<ModelFetchRequestAction>({ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, ...action })
}

export const dispatchPeriodicModelFetchRequest = (action: Omit<PeriodicModelFetchRequestAction, 'type'>) => {
	dispatchAction<PeriodicModelFetchRequestAction>({
		type: MODEL_FETCH_REQUEST_ACTION_TYPE.PERIODIC_FETCH_REQUEST,
		...action
	})
}

export const dispatchPeriodicModelFetchTerminateAction = (
	action: Omit<PeriodicModelFetchTerminationAction, 'type'>
) => {
	dispatchAction<PeriodicModelFetchTerminationAction>({
		type: PERIODIC_MODEL_FETCH_TERMINATION_ACTION_TYPE.TERMINATE,
		...action
	})
}

export const dispatchModelRemoveKeyAction = (action: Omit<ModelRemoveKeyAction, 'type'>) => {
	dispatchAction<ModelRemoveKeyAction>({ type: MODEL_REMOVE_KEY_ACTION_TYPE, ...action })
}

export const addNotification = ({ text, type, modalId }: Omit<Notification, 'id'>) => {
	dispatchAction<NotificationAction>({
		type: NOTIFICATION_ACTION_TYPE.ADD_NOTIFICATION,
		notification: {
			id: uuidv4(),
			text,
			type,
			modalId
		}
	})
}
