import { BaseReduxState } from '../types'

export const hasUserData = (state: BaseReduxState) => !!state.models?.user?.userInfo?.id
