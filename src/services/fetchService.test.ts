/* eslint-disable @typescript-eslint/no-unused-vars */

import { badRequestErrorData, badRequestFullErrorData } from '../constants/mockData'
import { HTTP_STATUS_CODE } from '../types'
import { constructPath, getApiRoot, sendFetch, setApiRoot } from './fetchService'

describe('constructPath', () => {
	it('should require config.path', () => {
		expect(() => {
			constructPath({
				path: undefined
			})
		}).toThrow("'config.path' is required")
	})

	it('should not add a question mark to a path without query params', () => {
		const path = constructPath({ path: 'http://abc.xyz/api/foo' })
		expect(path).toEqual('http://abc.xyz/api/foo')
	})

	it('should add a single query param', () => {
		const path = constructPath({
			path: 'http://abc.xyz/api/foo',
			queryParams: {
				bar: 'baz'
			}
		})
		expect(path).toEqual('http://abc.xyz/api/foo?bar=baz')
	})

	it('should add multiple query params', () => {
		const path = constructPath({
			path: 'http://abc.xyz/api/foo',
			queryParams: {
				bar: 'baz',
				quux: 'wawa'
			}
		})
		expect(path).toEqual('http://abc.xyz/api/foo?bar=baz&quux=wawa')
	})

	it('should encode query params', () => {
		const path = constructPath({
			path: 'http://abc.xyz/api/foo',
			queryParams: {
				bar: 'baz',
				$foo: '/bar'
			}
		})
		expect(path).toEqual('http://abc.xyz/api/foo?bar=baz&%24foo=%2Fbar')
	})

	it('should prepend apiRoot for relative path', () => {
		const existingApiRoot = getApiRoot()
		setApiRoot('http://abc.xyz')
		const path = constructPath({ path: '/api/foo' })
		expect(path).toEqual('http://abc.xyz/api/foo')
		setApiRoot(existingApiRoot)
	})
})

/**
 * Note: Results returned from doFetch include diagnostic data including headers, statusText,
 * redirected, etc. These are used for diagnostics when exceptions occur and are extracted
 * from the result.
 */
describe('sendFetch', () => {
	beforeAll(() => {
		setApiRoot('')
	})

	it("should throw without 'config.path'", () => {
		expect(() => {
			const gen = sendFetch({
				path: ''
			})
			const callFetchEffect = gen.next()
		}).toThrowError("'config.path' is required")
	})

	it('should send GET request', () => {
		const gen = sendFetch({ path: '/api/models' })
		const callFetchEffect = gen.next()
		expect(callFetchEffect.value.payload.args).toEqual([
			'/api/models',
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json; charset=utf-8'
				}
			}
		])
	})

	it('should not send body for GET request', () => {
		const gen = sendFetch({ path: '/api/models', body: { somekey: 'somevalue' } })
		const callFetchEffect = gen.next()
		expect(callFetchEffect.value.payload.args).toEqual([
			'/api/models',
			{
				method: 'GET',
				headers: {
					'Content-Type': 'application/json; charset=utf-8'
				}
			}
		])
	})

	it('should send GET request w/ contentType', () => {
		const gen = sendFetch({
			path: '/api/models',
			contentType: 'text/html; charset=utf-8'
		})
		const callFetchEffect = gen.next()
		expect(callFetchEffect.value.payload.args).toEqual([
			'/api/models',
			{
				method: 'GET',
				headers: {
					'Content-Type': 'text/html; charset=utf-8'
				}
			}
		])
	})

	it('should send GET request w/ contentType and other headers', () => {
		const gen = sendFetch({
			path: '/api/models',
			contentType: 'text/html; charset=utf-8',
			headers: { 'some-header': 'some-header-value' }
		})
		const callFetchEffect = gen.next()
		expect(callFetchEffect.value.payload.args).toEqual([
			'/api/models',
			{
				method: 'GET',
				headers: {
					'Content-Type': 'text/html; charset=utf-8',
					'some-header': 'some-header-value'
				}
			}
		])
	})

	it('should send POST request w/ headers', () => {
		const gen = sendFetch({ path: '/api/models', method: 'POST', body: { foo: 'bar' } })
		const callFetchEffect = gen.next()
		expect(callFetchEffect.value.payload.args).toEqual([
			'/api/models',
			{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json; charset=utf-8'
				},
				body: JSON.stringify({ foo: 'bar' })
			}
		])
	})

	it('should send POST request w/ form data', () => {
		const gen = sendFetch({
			path: '/api/models',
			method: 'POST',
			body: new FormData(),
			contentType: 'multipart/form-data'
		})
		const callFetchEffect = gen.next()
		expect(callFetchEffect.value.payload.args[1].method).toEqual('POST')
		expect(callFetchEffect.value.payload.args[1].body).toBeInstanceOf(FormData)
	})

	it('should send GET request w/ headers & form urlencoded', () => {
		const gen = sendFetch({
			path: '/api/models',
			method: 'POST',
			body: 'foo=bar&baz=quux',
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		})
		const callFetchEffect = gen.next()
		expect(callFetchEffect.value.payload.args).toEqual([
			'/api/models',
			{
				method: 'POST',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				body: 'foo=bar&baz=quux'
			}
		])
	})

	it('handles GET request w/ JSON response from server', () => {
		const gen = sendFetch({ path: '/api/models' })
		const callFetchEffect = gen.next()
		const response = {
			ok: true,
			status: HTTP_STATUS_CODE.OK,
			headers: { has: () => true, get: () => 'application/json; charset=utf-8' },
			json: () => ({ foo: 'bar' })
		}
		const responseJsonEffect = gen.next(response)
		expect(responseJsonEffect.value.payload.fn()).toEqual({ foo: 'bar' })
		const sagaDone = gen.next(response.json())
		const { ok, status, data } = sagaDone.value
		expect({ ok, status, data }).toEqual({
			ok: true,
			status: HTTP_STATUS_CODE.OK,
			data: {
				foo: 'bar'
			}
		})
		expect(sagaDone.done).toEqual(true)
	})

	it('handles GET request w/ Text response from server', () => {
		const gen = sendFetch({ path: '/api/models' })
		const callFetchEffect = gen.next()
		const response = {
			ok: true,
			status: HTTP_STATUS_CODE.OK,
			headers: { has: () => true, get: () => 'text/plain' },
			text: () => 'bar'
		}
		const responseTextEffect = gen.next(response)
		expect(responseTextEffect.value.payload.fn()).toEqual('bar')
		const sagaDone = gen.next(response.text())
		const { ok, status, data } = sagaDone.value
		expect({ ok, status, data }).toEqual({
			ok: true,
			status: HTTP_STATUS_CODE.OK,
			data: 'bar'
		})
		expect(sagaDone.done).toEqual(true)
	})

	it('should throw w/ no response from server', () => {
		expect(() => {
			const gen = sendFetch({ path: '/api/models' })
			const callFetchEffect = gen.next()
			// call fetch w/ no response
			const responseJsonEffect = gen.next(undefined)
		}).toThrowError('GET /api/models failed.\nNo response was received')
	})

	it('should throw w/ fetch error', () => {
		expect(() => {
			const gen = sendFetch({ path: '/api/models' })
			const callFetchEffect = gen.next()
			// call fetch w/ no response
			const responseJsonEffect = gen.throw && gen.throw(new TypeError('Failed to fetch'))
		}).toThrowError('GET /api/models failed.\nTypeError: Failed to fetch')
	})

	it('handles empty body on error response and returns default errorData using fetchResult properties (should not happen)', () => {
		const gen = sendFetch({ path: '/api/models' })
		const callFetchEffect = gen.next()
		const response = {
			ok: false,
			status: HTTP_STATUS_CODE.BAD_REQUEST,
			statusText: 'BadRequest',
			headers: { has: () => true, get: () => 'application/json; charset=utf-8' },
			json: () => null
		}
		const responseJsonEffect = gen.next(response)
		expect(responseJsonEffect.value.payload.fn()).toEqual(null)
		const sagaDone = gen.next(response.json())
		const { ok, status, data } = sagaDone.value
		expect({ ok, status, data }).toEqual({
			ok: false,
			status: HTTP_STATUS_CODE.BAD_REQUEST,
			data: {
				title: 'BadRequest',
				detail: null,
				status: HTTP_STATUS_CODE.BAD_REQUEST
			}
		})
		expect(sagaDone.done).toEqual(true)
	})

	it('handles GET request w/ JSON error response from server', () => {
		const gen = sendFetch({ path: '/api/models' })
		const callFetchEffect = gen.next()
		const response = {
			ok: false,
			status: HTTP_STATUS_CODE.BAD_REQUEST,
			statusText: 'BadRequest',
			headers: { has: () => true, get: () => 'application/json; charset=utf-8' },
			json: () => badRequestErrorData
		}
		const responseJsonEffect = gen.next(response)
		expect(responseJsonEffect.value.payload.fn()).toEqual(badRequestErrorData)
		const sagaDone = gen.next(response.json())
		const { ok, status, data } = sagaDone.value
		expect({ ok, status, data }).toEqual({
			ok: false,
			status: HTTP_STATUS_CODE.BAD_REQUEST,
			data: badRequestErrorData
		})
		expect(sagaDone.done).toEqual(true)
	})

	it('handles GET JSON error response from server with custom Content-Type, return all possible errorData values', () => {
		const gen = sendFetch({ path: '/api/models' })
		const callFetchEffect = gen.next()
		const response = {
			ok: false,
			status: HTTP_STATUS_CODE.BAD_REQUEST,
			statusText: 'BadRequest',
			headers: { has: () => true, get: () => 'application/problem+json; charset=utf-8' },
			json: () => badRequestFullErrorData
		}
		const responseJsonEffect = gen.next(response)
		expect(responseJsonEffect.value.payload.fn()).toEqual(badRequestFullErrorData)
		const sagaDone = gen.next(response.json())
		const { ok, status, data } = sagaDone.value
		expect({ ok, status, data }).toEqual({
			ok: false,
			status: HTTP_STATUS_CODE.BAD_REQUEST,
			data: badRequestFullErrorData
		})
		expect(sagaDone.done).toEqual(true)
	})

	it('handles PUT JSON request with NoContent (204) response', () => {
		const putBody = { foo: 'bar', baz: 'quux' }
		const gen = sendFetch({ path: '/api/models', method: 'PUT', body: putBody })
		const callFetchEffect = gen.next()
		expect(callFetchEffect.value.payload.args).toEqual([
			'/api/models',
			{
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json; charset=utf-8'
				},
				body: JSON.stringify(putBody)
			}
		])
		const response = {
			ok: true,
			status: HTTP_STATUS_CODE.NO_CONTENT,
			statusText: 'NoContent'
		}
		const responseJsonEffect = gen.next(response)
		const { ok, status, data } = responseJsonEffect.value
		expect({ ok, status, data }).toEqual({
			ok: true,
			status: HTTP_STATUS_CODE.NO_CONTENT,
			data: putBody
		})
		const sagaDone = gen.next()
		expect(sagaDone.value).toEqual(undefined)
		expect(sagaDone.done).toEqual(true)
	})

	it('handles PUT Text request with NoContent (204) response', () => {
		const putBody = 'something'
		const gen = sendFetch({
			path: '/api/models',
			method: 'PUT',
			body: putBody,
			headers: {
				'Content-Type': 'text/plain'
			}
		})
		const callFetchEffect = gen.next()
		expect(callFetchEffect.value.payload.args).toEqual([
			'/api/models',
			{
				method: 'PUT',
				headers: {
					'Content-Type': 'text/plain'
				},
				body: putBody
			}
		])
		const response = {
			ok: true,
			status: HTTP_STATUS_CODE.NO_CONTENT,
			statusText: 'NoContent'
		}
		const responseJsonEffect = gen.next(response)
		const { ok, status, data } = responseJsonEffect.value
		expect({ ok, status, data }).toEqual({
			ok: true,
			status: HTTP_STATUS_CODE.NO_CONTENT,
			data: undefined
		})
		const sagaDone = gen.next()
		expect(sagaDone.value).toEqual(undefined)
		expect(sagaDone.done).toEqual(true)
	})

	it('handles DELETE request with NoContent (204) response', () => {
		const gen = sendFetch({ path: '/api/models', method: 'DELETE' })
		const callFetchEffect = gen.next()
		expect(callFetchEffect.value.payload.args).toEqual([
			'/api/models',
			{
				method: 'DELETE',
				headers: {
					'Content-Type': 'application/json; charset=utf-8'
				}
			}
		])
		const response = {
			ok: true,
			status: HTTP_STATUS_CODE.NO_CONTENT,
			statusText: 'NoContent'
		}
		const responseJsonEffect = gen.next(response)
		const { ok, status, data } = responseJsonEffect.value
		expect({ ok, status, data }).toEqual({
			ok: true,
			status: HTTP_STATUS_CODE.NO_CONTENT,
			data: undefined
		})
		const sagaDone = gen.next()
		expect(sagaDone.value).toEqual(undefined)
		expect(sagaDone.done).toEqual(true)
	})
})
