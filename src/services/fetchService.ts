import { SagaIterator } from '@redux-saga/core'
import { merge, startsWith } from 'lodash'
import { call } from 'redux-saga/effects'
import { FetchConfig, FetchErrorData, FetchResult, HTTP_STATUS_CODE, HTTPMethod } from '../types'
import { constructErrorFromCaughtError } from '../utils/error'

let apiRoot: string | undefined

/**
 * Set and store the `apiRoot`, the default root path for all relative URLs.
 *
 * @param uri The uri to save and prepend later
 */
export function setApiRoot(uri: string | undefined) {
	apiRoot = uri
}

/**
 * Get the `apiRoot`, the default root path for all relative URLs.
 */
export function getApiRoot() {
	return apiRoot
}

/**
 * Combine the `path` and `queryParams` from a FetchConfig into a final path string.
 *
 * Prepend with `apiRoot` if the path does not start with 'http'.
 *
 * @param config The fetch configuration containing the path and query params
 * @returns A path string containing query params and prepended with the `apiRoot`, if needed
 */
export function constructPath(config: FetchConfig) {
	if (!config.path) {
		throw new Error("'config.path' is required")
	}
	let queryParams
	if (config.queryParams) {
		queryParams = Object.keys(config.queryParams)
			.map(
				key =>
					`${encodeURIComponent(key)}=${!!config.queryParams && encodeURIComponent(config.queryParams[key])}`
			)
			.join('&')
	}

	let path = startsWith(config.path, 'http') ? config.path : `${apiRoot}${config.path}`
	if (queryParams) {
		path = `${path}?${queryParams}`
	}
	return path
}

function isContentTypeJson(contentType: string | null) {
	return !!contentType && /application\/json|.*\+json/.test(contentType)
}

/**
 * Send an HTTP request and return a response, handling errors.
 * * Requests default to using GET method.
 * * Content-Type defaults to 'application/json'.
 * * Body is sent as stringified JSON unless the 'application/x-www-form-urlencoded' Content-Type is detected, in which case
 * it's sent as provided.
 * * If it is a 'multipart/form-data', we are assuming that the data is being sent as a FormData, and we do not set the Content-type
 * (https://stackoverflow.com/questions/39280438/fetch-missing-boundary-in-multipart-form-data-post).
 *
 * @param config The configuration used to construct the fetch request
 * @returns The response, parsed as JSON
 */
export function* sendFetch(config: FetchConfig): SagaIterator {
	const method: HTTPMethod = config.method || 'GET'

	try {
		// construct request
		const path = constructPath(config)
		const headers =
			// setting FormData as the body will set "Content-Type", including "boundary"
			// do not interfere
			config.contentType === 'multipart/form-data'
				? merge({}, config.headers)
				: merge(
						{},
						{
							'Content-Type': config.contentType ? config.contentType : 'application/json; charset=utf-8'
						},
						config.headers
				  )
		const isBodyJson = headers['Content-Type'] && isContentTypeJson(headers['Content-Type'])
		const body = method === 'GET' ? undefined : !isBodyJson ? config.body : JSON.stringify(config.body)

		// call `fetch`
		const response: Response = yield call(fetch, path, {
			method,
			headers,
			body
		})

		if (!response) {
			throw new Error('No response was received')
		}

		// construct a subset of the response object to return
		const result: FetchResult = (({ headers, ok, redirected, status, statusText, type, url, bodyUsed }) => ({
			headers,
			ok,
			redirected,
			status,
			statusText,
			type,
			url,
			bodyUsed,
			data: undefined
		}))(response)

		// If the request was a NoContent (204), use the body (if any) that was PUT in the request as the "result.data",
		// so it gets incorporated correctly into Redux.
		// 200/201 should return a representation of the entity (https://tools.ietf.org/html/rfc7231#section-6.3.1)

		const isResponseJson =
			!!response.headers &&
			response.headers.has('Content-Type') &&
			isContentTypeJson(response.headers.get('Content-Type'))

		if (response.status === HTTP_STATUS_CODE.NO_CONTENT) {
			result.data = isBodyJson ? config.body : undefined
		} else {
			result.data = isResponseJson ? yield call(() => response.json()) : yield call(() => response.text())
		}

		if (!response.ok) {
			const errorData: FetchErrorData = {
				status: result.data?.status || response.status,
				title: result.data?.title || response.statusText,
				detail: result.data?.detail || null,
				type: result.data?.type,
				instance: result.data?.instance,
				traceId: result.data?.traceId,
				errors: result.data?.errors
			}
			result.data = errorData
		}

		return result
	} catch (error: unknown) {
		const sendFetchError = constructErrorFromCaughtError(
			error,
			'SendFetchError',
			config.path ? `${method} ${config.path} failed` : ''
		)
		throw sendFetchError
	}
}
