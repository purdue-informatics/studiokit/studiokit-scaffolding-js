import { parse, stringify } from 'query-string'
import { CodeProviderService } from '../types'
import { windowService } from './windowService'

export const codeProviderService: CodeProviderService = {
	getCode: () => {
		const location = windowService.getLocation()
		return parse(location.search).code as string
	},
	removeCode: () => {
		const location = windowService.getLocation()
		const history = windowService.getHistory()
		const parsedSearch = parse(location.search)
		const { code, ...newSearch } = parsedSearch
		let newSearchString = stringify(newSearch)
		if (newSearchString) {
			newSearchString = `?${newSearchString}`
		}
		history.pushState('', '', `${location.protocol}//${location.host}${location.pathname}${newSearchString}`)
	}
}
