import storage from 'local-storage-fallback'

const lastActivityDateKey = 'studioKit:caliperService:lastActivityDate'

/**
 * Return the `lastActivityDate` set in local storage by "studiokit-caliper-js"
 */
export function getLastActivityDate() {
	const lastActivityDateString = storage.getItem(lastActivityDateKey)
	return lastActivityDateString ? (JSON.parse(lastActivityDateString) as string) : undefined
}
