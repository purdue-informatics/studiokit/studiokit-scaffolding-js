import { OAuthTokenOrNull, TokenPersistenceService } from '../types'

let storedToken: OAuthTokenOrNull = null

export const tokenPersistenceService: TokenPersistenceService = {
	getPersistedToken: () => {
		return storedToken
	},

	persistToken: token => {
		storedToken = token
	}
}
