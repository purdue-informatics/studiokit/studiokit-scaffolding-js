import { SimpleLocation } from '../types'

export interface WindowService {
	setLocation: (value?: SimpleLocation) => void
	getLocation: () => SimpleLocation | Location
	setHistory: (value?: History) => void
	getHistory: () => History
	setIsOnline: (isOnline?: boolean) => void
	getIsOnline: () => boolean
}

let location: SimpleLocation | undefined
let history: History | undefined
let isOnline: boolean | undefined

export const windowService: WindowService = {
	setLocation: (value?: SimpleLocation) => {
		location = value
	},
	getLocation: () => location || window.location,
	setHistory: (value?: History) => {
		history = value
	},
	getHistory: () => history || window.history,
	setIsOnline: (value?: boolean) => {
		isOnline = value
	},
	getIsOnline: () => (typeof isOnline !== 'undefined' ? isOnline : window.navigator.onLine)
}
