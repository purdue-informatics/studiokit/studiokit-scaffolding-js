import storage from 'local-storage-fallback'
import { CaliperToken } from 'studiokit-caliper-js/lib/types'
import { OAuthTokenOrNull, TokenPersistenceService } from '../types'

const tokenKey = '@authStore:oauthToken'
const caliperTokenKey = 'studioKit:caliperService:token'
export const returnUrlKey = 'studioKit:login:returnUrl'
export const identityProviderKey = 'studioKit:auth:identityProvider'
export const isCasGatewayRequestedKey = 'studioKit:auth:isCasGatewayRequested'

const getItem = (key: string) => {
	const itemValue = storage.getItem(key)
	if (!itemValue) {
		return null
	}
	return JSON.parse(itemValue)
}

const setItem = (key: string, item: any) => {
	storage.setItem(key, JSON.stringify(item))
}

const removeItem = (key: string) => {
	return storage.removeItem(key)
}

const getPersistedToken = () => {
	const token: OAuthTokenOrNull = getItem(tokenKey)
	return token
}

const persistToken = (token: OAuthTokenOrNull) => {
	setItem(tokenKey, token)
}

const getPersistedCaliperToken = () => {
	const token: CaliperToken | null = getItem(caliperTokenKey)
	return token
}

export interface PersistenceService extends TokenPersistenceService {
	getItem: (key: string) => any | Promise<any>
	setItem: (key: string, item: any) => void | Promise<void>
	removeItem: (key: string) => void | Promise<void>
	getPersistedCaliperToken: () => CaliperToken | null | Promise<CaliperToken | null>
}

export const persistenceService: PersistenceService = {
	getItem,
	setItem,
	removeItem,
	getPersistedToken,
	persistToken,
	getPersistedCaliperToken
}
