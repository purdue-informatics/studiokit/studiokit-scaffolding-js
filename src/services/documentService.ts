export interface DocumentService {
	setActiveDocument: (value: Element | null) => void
	getActiveDocument: () => Element | null
}

let activeDocument: Element | null = null

/**
 * A service which wraps custom `document` logic.
 * Allows for testing overrides, while otherwise uses the normal functionality.
 */
export const documentService: DocumentService = {
	setActiveDocument: (value: Element | null) => {
		activeDocument = value
		if (value && value instanceof HTMLElement) {
			value.focus()
		}
	},
	getActiveDocument: () => activeDocument || document.activeElement
}
