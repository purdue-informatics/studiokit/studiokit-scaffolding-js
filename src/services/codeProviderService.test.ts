import { defaultLocation, mockHistory } from '../constants/mockData'
import { codeProviderService } from './codeProviderService'
import { windowService } from './windowService'

describe('codeProviderService', () => {
	describe('getCode', () => {
		it('returns code if exists', () => {
			windowService.setLocation({ ...defaultLocation, ...{ search: '?code=foo' } })
			expect(codeProviderService.getCode()).toEqual('foo')
		})
		it('returns nothing if not exists', () => {
			windowService.setLocation(defaultLocation)
			expect(codeProviderService.getCode()).toEqual(undefined)
		})
	})
	describe('removeCode', () => {
		it('removes code from search and calls pushState with new url', () => {
			windowService.setLocation({ ...defaultLocation, ...{ search: '?code=foo', pathname: '/home' } })
			const history = mockHistory()
			windowService.setHistory(history)
			codeProviderService.removeCode()
			const { pushState } = history // eslint-disable-line
			expect(pushState).toHaveBeenCalledWith('', '', `${defaultLocation.href}home`)
		})
	})
})
