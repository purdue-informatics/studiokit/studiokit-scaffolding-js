import { defaultLocation, mockHistory } from '../constants/mockData'
import { ticketProviderService } from './ticketProviderService'
import { windowService } from './windowService'

describe('ticketProviderService', () => {
	describe('getTicket', () => {
		it('returns code if exists', () => {
			windowService.setLocation({ ...defaultLocation, ...{ search: '?ticket=foo' } })
			expect(ticketProviderService.getTicket()).toEqual('foo')
		})
		it('returns nothing if not exists', () => {
			windowService.setLocation(defaultLocation)
			expect(ticketProviderService.getTicket()).toEqual(undefined)
		})
	})
	describe('getAppServiceName', () => {
		it('returns location with a trailing slash', () => {
			windowService.setLocation(defaultLocation)
			expect(ticketProviderService.getAppServiceName()).toEqual('https://purdue.dev.studiokit.org/')
		})
	})
	describe('removeTicket', () => {
		it('removes code from search and calls pushState with new url', () => {
			windowService.setLocation({ ...defaultLocation, ...{ search: '?ticket=foo', pathname: '/home' } })
			const history = mockHistory()
			windowService.setHistory(history)
			ticketProviderService.removeTicket()
			const { pushState } = history // eslint-disable-line
			expect(pushState).toHaveBeenCalledWith('', '', `${defaultLocation.href}home`)
		})
	})
})
