import { parse, stringify } from 'query-string'
import { TicketProviderService } from '../types'
import { windowService } from './windowService'

export const ticketProviderService: TicketProviderService = {
	getTicket: () => {
		const location = windowService.getLocation()
		return parse(location.search).ticket as string
	},
	getAppServiceName: () => {
		const location = windowService.getLocation()
		return `${location.protocol}//${location.host}/`
	},
	removeTicket: () => {
		const location = windowService.getLocation()
		const history = windowService.getHistory()
		const parsedSearch = parse(location.search)
		const { ticket, ...newSearch } = parsedSearch
		let newSearchString = stringify(newSearch)
		if (newSearchString) {
			newSearchString = `?${newSearchString}`
		}
		history.pushState('', '', `${location.protocol}//${location.host}${location.pathname}${newSearchString}`)
	}
}
