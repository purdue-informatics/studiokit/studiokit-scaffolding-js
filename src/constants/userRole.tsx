import React from 'react'
import { getDomainIdentifierTypeString } from '../utils/domainIdentifier'

export const renderAddDescription = (entityName?: string) => {
	return (
		<>
			<h3>Add People</h3>
			<p>
				To add people
				{entityName ? ` to this ${entityName}` : ''}, enter their {getDomainIdentifierTypeString()}, select
				their role
				{entityName ? ` for this ${entityName}` : ''}, and click 'Add.'
			</p>
		</>
	)
}
