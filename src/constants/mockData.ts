import { merge } from 'lodash'
import { EXTERNAL_PROVIDER_TYPE } from '../constants/externalProviderType'
import {
	AppConfiguration,
	ExternalProvider,
	ExternalTerm,
	FetchErrorData,
	Group,
	GroupUserRole,
	HTTP_STATUS_CODE,
	ModelCollection,
	SimpleLocation,
	UserInfo,
	UserRole
} from '../types'
import { BASE_ROLE } from './baseRole'
import { TIER } from './tier'

//#region General

export const defaultAppConfiguration: AppConfiguration = {
	NODE_ENV: 'test',
	APP_NAME: 'StudioKit',
	TIER: TIER.LOCAL,
	API_BASE_URL: '',
	ROOT_DOMAIN: 'studiokit.org',
	CLIENT_ID: 'web',
	CLIENT_SECRET: '****',
	IS_DOWNTIME: false,
	LMS_NAME: 'Brightspace',
	GRADABLE_DESCRIPTOR: 'assessment'
}

export const defaultLocation: SimpleLocation = {
	hash: '',
	host: 'purdue.dev.studiokit.org',
	hostname: 'purdue.dev.studiokit.org',
	href: 'https://purdue.dev.studiokit.org/',
	pathname: '/',
	port: '',
	protocol: 'https:',
	search: ''
}

export function mockHistory(): History {
	return {
		length: 0,
		scrollRestoration: 'auto',
		state: {},
		back: jest.fn(),
		forward: jest.fn(),
		go: jest.fn(),
		pushState: jest.fn(),
		replaceState: jest.fn()
	}
}

//#endregion General

//#region Dates

/** 5/15/18 - 8/14/18 */
export const pastDates = {
	startDate: '2018-05-15T04:00:00.000Z',
	endDate: '2018-08-15T03:59:59.999Z'
}

/** 8/15/18 - 12/31/18: */
export const currentDates = {
	startDate: '2018-08-15T04:00:00.000Z',
	endDate: '2019-01-01T04:59:59.999Z'
}

/**
 * The default date and time for all tests. Used by the 'mockdate' package. One month into `currentDates`.
 */
export const defaultDate = '2018-09-15T04:00:00.000Z'

//#endregion Dates

//#region Users

export const defaultUser = {
	userInfo: {
		id: '1',
		firstName: null,
		lastName: null,
		email: null,
		uid: null,
		activities: [],
		roles: [],
		dateDemoGenerationSucceeded: null,
		dateStored: defaultDate,
		dateLastUpdated: defaultDate,
		isImpersonated: false,
		careerAccountAlias: null,
		employeeNumber: null,
		userName: 'test',
		puid: null,
		lockDownBrowserTestLaunchUrl: null
	} as UserInfo
}

export const defaultNamelessUserInfo = {
	...defaultUser.userInfo,
	...{
		email: 'nicki@minaj.superbass',
		uid: 'nm-nameless'
	}
}

export const defaultNamedUserInfo = {
	...defaultUser.userInfo,
	...{
		firstName: 'Nicki',
		lastName: 'Minaj',
		uid: 'nm-named'
	}
}

export function mockUser(activities: string[] = []): { userInfo: UserInfo } {
	return merge({ userInfo: { activities } }, defaultUser)
}

export const defaultGroupOwnerUserInfo: UserInfo = {
	...defaultUser.userInfo,
	id: '2',
	firstName: 'Jane',
	lastName: 'Doe',
	email: 'jane@gmail.com'
}

export const defaultGroupOwner: GroupUserRole = {
	id: '1',
	userId: '2',
	firstName: 'Jane',
	lastName: 'Doe',
	email: 'jane@gmail.com',
	uid: 'jane',
	entityId: 1,
	role: BASE_ROLE.GROUP_OWNER,
	isExternal: false
}

export const defaultNamelessGroupLearner: GroupUserRole = {
	id: '2',
	userId: '3',
	firstName: null,
	lastName: null,
	email: 'nicki@minaj.superbass',
	uid: 'nm-nameless',
	entityId: 1,
	role: BASE_ROLE.GROUP_LEARNER,
	isExternal: false
}

export const defaultNamedGroupLearner: GroupUserRole = {
	id: '3',
	userId: '4',
	firstName: 'Nicki',
	lastName: 'Minaj',
	email: null,
	uid: 'nm-named',
	entityId: 1,
	role: BASE_ROLE.GROUP_LEARNER,
	isExternal: false
}

export const defaultGroupLearner: GroupUserRole = {
	id: '4',
	userId: '5',
	firstName: 'Joe',
	lastName: 'Schmoe',
	email: 'nope@gmail.com',
	uid: 'nope',
	entityId: 1,
	role: BASE_ROLE.GROUP_LEARNER,
	isExternal: false
}

export const defaultExternalGroupLearner: GroupUserRole = {
	id: '5',
	userId: '6',
	firstName: 'Bob',
	lastName: 'Loblaw',
	email: 'bobloblaw@lawblog.com',
	uid: 'bobloblaw',
	entityId: 1,
	role: BASE_ROLE.GROUP_LEARNER,
	isExternal: true
}

export const defaultGroupGrader: GroupUserRole = {
	id: '6',
	userId: '7',
	firstName: 'Grader',
	lastName: 'McGradeface',
	email: 'grader@gmail.com',
	uid: 'grader',
	entityId: 1,
	role: BASE_ROLE.GROUP_GRADER,
	isExternal: false
}

export const defaultGroupUserRoles = [defaultGroupOwner, defaultGroupLearner, defaultExternalGroupLearner]

export const defaultAdminUserRoles: UserRole[] = [
	{
		id: '1',
		firstName: 'Joe',
		lastName: 'Schmo',
		email: null,
		uid: null,
		role: BASE_ROLE.ADMIN
	},
	{
		id: '2',
		firstName: null,
		lastName: null,
		email: 'carlysimon@clouds.in.coffee',
		uid: 'carlysimon',
		role: BASE_ROLE.ADMIN
	}
]

//#endregion Users

//#region Groups

export const defaultGroup: Group = {
	roles: [],
	activities: [],
	owners: [
		{
			id: '1',
			entityId: 1,
			firstName: 'Instructor',
			lastName: 'McInstructor',
			email: null,
			uid: null
		}
	],
	graders: [],
	id: 1,
	name: 'McGroup',
	externalTermId: null,
	startDate: currentDates.startDate,
	endDate: currentDates.endDate,
	isDeleted: false,
	isRosterSyncEnabled: false,
	externalGroups: []
}

export function mockGroups(activities: string[] = [], roles: string[] = []): ModelCollection<Group> {
	return { '1': merge({ activities, roles }, defaultGroup) }
}

export function mockSyncedGroups(activities: string[] = [], roles: string[] = []): ModelCollection<Group> {
	const rosterSyncEnabledGroup: Group = merge({ activities, roles }, defaultGroup)
	rosterSyncEnabledGroup.isRosterSyncEnabled = true
	return { '1': rosterSyncEnabledGroup }
}

export function mockLtiGroups(activities: string[] = [], roles: string[] = []): ModelCollection<Group> {
	const ltiGroup: Group = merge({ activities, roles }, defaultGroup)
	ltiGroup.isRosterSyncEnabled = true
	ltiGroup.externalGroups = [
		{
			id: 1,
			externalProviderId: 2,
			groupId: 1,
			userId: '1',
			name: 'LTI ExternalGroup',
			description: 'LTI 101',
			gradesUrl: 'www.lti.org',
			rosterUrl: null,
			isAutoGradePushEnabled: false,
			externalId: '1',
			typename: 'ExternalGroup'
		}
	]
	return { '1': ltiGroup }
}

export function mockTermGroups(activities: string[] = [], roles: string[] = []): ModelCollection<Group> {
	const group: Group = merge({ activities, roles }, defaultGroup)
	group.startDate = null
	group.endDate = null
	group.externalTermId = 1
	return { '1': group }
}

export function mockTermAndUniTimeGroups(activities: string[] = [], roles: string[] = []): ModelCollection<Group> {
	const group: Group = merge({ activities, roles }, defaultGroup)
	group.startDate = null
	group.endDate = null
	group.externalTermId = 1
	group.externalGroups = [
		{
			id: 1,
			externalProviderId: 1,
			groupId: 1,
			userId: '1',
			name: 'UniTime ExternalGroup',
			description: 'UniTime 101',
			gradesUrl: null,
			rosterUrl: null,
			isAutoGradePushEnabled: false,
			externalId: '1',
			typename: 'ExternalGroup'
		}
	]
	return { '1': group }
}

export function mockPastGroups(activities: string[] = [], roles: string[] = []): ModelCollection<Group> {
	const pastGroup: Group = merge({ activities, roles }, defaultGroup, pastDates)
	return { '1': pastGroup }
}

export function mockPastAndCurrentGroups(activities: string[] = [], roles: string[] = []): ModelCollection<Group> {
	const pastGroup: Group = merge({ activities, roles }, defaultGroup, pastDates)
	const currentGroup: Group = merge(
		{ activities, roles },
		defaultGroup,
		{
			id: '2'
		},
		currentDates
	)
	return { '1': pastGroup, '2': currentGroup }
}

//#endregion Groups

//#region ExternalTerms + ExternalProviders

export const defaultExternalProviders: ModelCollection<ExternalProvider> = {
	'1': {
		id: 1,
		typename: EXTERNAL_PROVIDER_TYPE.UNITIME,
		termSyncEnabled: true,
		rosterSyncEnabled: true,
		gradePushEnabled: false,
		shouldDisplayCreateNonLtiGroupAlert: false
	},
	'2': {
		id: 2,
		typename: EXTERNAL_PROVIDER_TYPE.LTI,
		termSyncEnabled: false,
		rosterSyncEnabled: false,
		gradePushEnabled: true,
		shouldDisplayCreateNonLtiGroupAlert: true
	}
}

export const defaultExternalTerms: ModelCollection<ExternalTerm> = {
	'1': {
		id: 1,
		externalProviderId: 1,
		externalId: 'foo',
		name: 'UniTime Fall Term',
		startDate: currentDates.startDate,
		endDate: currentDates.endDate
	}
}

//#endregion ExternalTerms + ExternalProviders

//#region FetchErrorData

export const internalServerErrorData: FetchErrorData = {
	status: HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR,
	title: 'Error',
	detail: 'An error has occurred.'
}

export const requestTimedOutErrorData: FetchErrorData = {
	status: HTTP_STATUS_CODE.REQUEST_TIMEOUT,
	title: 'Request Timeout',
	detail: null
}

export const forbiddenErrorData: FetchErrorData = {
	status: HTTP_STATUS_CODE.FORBIDDEN,
	title: 'Forbidden',
	detail: 'Your request has been denied. You do not have access.'
}

export const unauthorizedErrorData: FetchErrorData = {
	status: HTTP_STATUS_CODE.UNAUTHORIZED,
	title: 'Unauthorized',
	detail: 'Your request is unauthorized. You must authenticate.'
}

export const badRequestErrorData: FetchErrorData = {
	status: HTTP_STATUS_CODE.BAD_REQUEST,
	title: 'Bad Request',
	detail: 'Reasons'
}

export const badRequestFullErrorData: FetchErrorData = {
	status: HTTP_STATUS_CODE.BAD_REQUEST,
	title: 'Bad Request',
	detail: 'Reasons',
	// include all optional properties
	type: 'https://tools.ietf.org/html/rfc7231#section-6.5.1',
	instance: 'instance',
	traceId: '00-3914b43b452b94e3ff4c327922880a03-4cfed6fe42afdd8f-00',
	errors: {
		password: ['The Password must be at least 6 characters long.']
	}
}

//#endregion FetchErrorData
