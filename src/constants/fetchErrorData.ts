import { CUSTOM_ERROR_STATUS_CODE, FetchErrorData } from '../types'

export const unknownErrorData: FetchErrorData = {
	status: CUSTOM_ERROR_STATUS_CODE.UNKNOWN,
	title: 'Unknown Error',
	detail: 'An unknown error has occurred.'
}

export const networkOfflineErrorData: FetchErrorData = {
	status: CUSTOM_ERROR_STATUS_CODE.NO_NETWORK_CONNECTION,
	title: 'Network Offline',
	detail: 'There is no network connection.'
}
