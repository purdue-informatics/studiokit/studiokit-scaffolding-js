import { ApplicationInsights } from '@microsoft/applicationinsights-web'
import { AppConfiguration, Configuration, EndpointMappings } from '../types'

let appConfig: AppConfiguration
let shardConfig: Configuration
let endpointMappings: EndpointMappings
let userId: string | null
let appInsights: ApplicationInsights

export const setAppConfig = (value: AppConfiguration) => {
	appConfig = value
}

export const getAppConfig = () => {
	if (!appConfig) {
		throw new Error('`appConfig` is not defined')
	}
	return appConfig
}

export const setShardConfig = (value: Configuration) => {
	shardConfig = value
}

export const getShardConfig = () => {
	if (!shardConfig) {
		throw new Error('`shardConfig` is not defined')
	}
	return shardConfig
}

export const setEndpointMappings = (value: EndpointMappings) => {
	endpointMappings = value
}

export const getEndpointMappings = () => {
	if (!endpointMappings) {
		throw new Error('`endpointMappings` is not defined')
	}
	return endpointMappings
}

export const setUserId = (value: string | null) => {
	userId = value
}

export const getUserId = () => {
	return userId
}

export const setAppInsights = (value: ApplicationInsights) => {
	appInsights = value
}

export const getAppInsights = () => {
	if (!appInsights) {
		throw new Error('`appInsights` is not defined')
	}
	return appInsights
}
