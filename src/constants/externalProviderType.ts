export enum EXTERNAL_PROVIDER_TYPE {
	UNITIME = 'UniTimeExternalProvider',
	LTI = 'LtiExternalProvider'
}
