export enum BASE_ROLE {
	SUPER_ADMIN = 'SuperAdmin',
	ADMIN = 'Admin',
	CREATOR = 'Creator',
	GROUP_OWNER = 'GroupOwner',
	GROUP_LEARNER = 'GroupLearner',
	GROUP_GRADER = 'GroupGrader'
}
