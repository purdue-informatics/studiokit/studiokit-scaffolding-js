export enum OPERATING_SYSTEM {
	WINDOWS = 'Windows',
	MAC = 'macOS',
	IPAD = 'iPad',
	UNKNOWN = 'Unknown'
}
