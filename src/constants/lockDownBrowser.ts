export enum LOCKDOWN_BROWSER_KEY {
	/** Cookie set by LDB to claim it is LDB. */
	CLIENT_IS_LOCK_DOWN_BROWSER = 'rldbci',

	/** Cookie set by LDB containing the client build identifier. */
	BUILD_ID = 'rldbid',

	/** Cookie set by LDB containing the build date, if the client is on windows. */
	BUILD_DATE_WINDOWS = 'rldbbdw',

	/** Cookie set by LDB containing the build date, if the client is on mac. */
	BUILD_DATE_MAC = 'rldbbdm',

	/** Cookie set by LDB containing the build date, if the client is on iPad. */
	BUILD_DATE_IPAD = 'rldbbdi',

	/** Query param to direct LDB to close itself */
	EXIT_BROWSER = 'rldbxb'
}

export const LOCKDOWN_BROWSER_TRUE = '1'

export enum LOCKDOWN_BROWSER_PATH {
	LAUNCH = 'ldb-launch',
	CHECK = 'ldb-check'
}

export const LOCKDOWN_BROWSER_LAUNCH_ERROR =
	'A LockDown Browser session could not be started. Please close the browser and try again.'
