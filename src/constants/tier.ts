export enum TIER {
	LOCAL = 'local',
	DEV = 'development',
	QA = 'qa',
	PROD = 'production'
}
