import { shallow } from 'enzyme'
import React from 'react'
import { SearchControls, SearchControlsProps } from './SearchControls'

function setup(props: Partial<SearchControlsProps>) {
	return shallow(<SearchControls {...(props as SearchControlsProps)} />)
}

describe('SearchControls', () => {
	describe('Required Message Alert', () => {
		it('is visible with text if requiredMessage exists', () => {
			const wrapper = setup({
				requiredMessage: 'You missed something, dummy'
			})
			const alert = wrapper.find({ id: 'requiredMessageAlert' })
			expect(alert.length).toEqual(1)
			expect(alert.childAt(0).text()).toEqual('You missed something, dummy')
		})
		it('is invisible if requiredMessage does not exists', () => {
			const wrapper = setup({})
			expect(wrapper.find({ id: 'requiredMessageAlert' }).length).toEqual(0)
		})
	})
	describe('Invalid Keywords Message', () => {
		it('is visible if invalidKeywords = true', () => {
			const wrapper = setup({
				invalidKeywords: true
			})
			expect(wrapper.find({ id: 'invalidKeywords' }).length).toEqual(1)
		})
		it('is invisible if invalidKeywords = false', () => {
			const wrapper = setup({})
			expect(wrapper.find({ id: 'invalidKeywords' }).length).toEqual(0)
		})
	})
	describe('Date Field', () => {
		it('is visible if handleDateChange exists', () => {
			const wrapper = setup({
				handleDateChange: jest.fn()
			})
			expect(wrapper.find({ id: 'date' }).length).toEqual(1)
		})
		it('is invisible if handleDateChange does not exists', () => {
			const wrapper = setup({})
			expect(wrapper.find({ id: 'date' }).length).toEqual(0)
		})
	})
	describe('Search All Checkbox', () => {
		it('is visible if canReadAnyGlobally = true and handleQueryAllChange exists', () => {
			const wrapper = setup({
				canReadAnyGlobally: true,
				setQueryAll: jest.fn()
			})
			expect(wrapper.find({ id: 'queryAll' }).length).toEqual(1)
		})
		it('is invisible if canReadAnyGlobally = false and handleQueryAllChange exists', () => {
			const wrapper = setup({
				setQueryAll: jest.fn()
			})
			expect(wrapper.find({ id: 'queryAll' }).length).toEqual(0)
		})
		it('is invisible if canReadAnyGlobally = true and handleQueryAllChange does not exists', () => {
			const wrapper = setup({
				canReadAnyGlobally: true
			})
			expect(wrapper.find({ id: 'queryAll' }).length).toEqual(0)
		})
		it('is readOnly if canReadSome = false', () => {
			const wrapper = setup({
				setQueryAll: jest.fn(),
				canReadAnyGlobally: true,
				canReadSome: false
			})
			expect(wrapper.find({ id: 'queryAll' }).props().disabled).toEqual(true)
		})
		it('is not readOnly if canReadSome = true', () => {
			const wrapper = setup({
				setQueryAll: jest.fn(),
				canReadAnyGlobally: true,
				canReadSome: true
			})
			expect(wrapper.find({ id: 'queryAll' }).props().disabled).toEqual(false)
		})
	})
	describe('Submit Button', () => {
		it('is visible if showSubmitButton = true', () => {
			const wrapper = setup({
				showSearchButton: true,
				handleSearchClick: jest.fn()
			})
			expect(wrapper.find('.search-button').length).toEqual(1)
		})
		it('is visible if canReadAnyGlobally = true and queryAll = true', () => {
			const wrapper = setup({
				canReadAnyGlobally: true,
				queryAll: true,
				handleSearchClick: jest.fn()
			})
			expect(wrapper.find('.search-button').length).toEqual(1)
		})
		it('is invisible if canReadAnyGlobally = false', () => {
			const wrapper = setup({
				queryAll: true
			})
			expect(wrapper.find('.search-button').length).toEqual(0)
		})
		it('is invisible if queryAll = false', () => {
			const wrapper = setup({
				canReadAnyGlobally: false
			})
			expect(wrapper.find('.search-button').length).toEqual(0)
		})
		it('is disabled with no keywords', () => {
			const wrapper = setup({
				canReadAnyGlobally: true,
				queryAll: true,
				handleSearchClick: jest.fn()
			})
			const button = wrapper.find('.search-button')
			expect(button.prop('disabled')).toEqual(true)
		})
		it('is disabled with keywords less than 3 chars', () => {
			const wrapper = setup({
				canReadAnyGlobally: true,
				queryAll: true,
				handleSearchClick: jest.fn(),
				keywords: 'x'
			})
			const button = wrapper.find('.search-button')
			expect(button.prop('disabled')).toEqual(true)
		})
		it('is disabled with invalidKeywords', () => {
			const wrapper = setup({
				canReadAnyGlobally: true,
				queryAll: true,
				keywords: 'x',
				invalidKeywords: true,
				handleSearchClick: jest.fn()
			})
			const button = wrapper.find('.search-button')
			expect(button.prop('disabled')).toEqual(true)
		})
		it('is enabled with keywords', () => {
			const wrapper = setup({
				canReadAnyGlobally: true,
				queryAll: true,
				keywords: 'foo',
				handleSearchClick: jest.fn()
			})
			const button = wrapper.find('.search-button')
			expect(button.prop('disabled')).toEqual(false)
		})
	})
})
