import React, { FunctionComponent } from 'react'
import { Modal, ModalProps } from 'react-bootstrap'
import ConnectedModalComponent, { ConnectedModalWrappedProps } from './HOC/ConnectedModalComponent'
import { GuidComponentWrappedProps } from './HOC/GuidComponent'

const ConnectedModal: FunctionComponent<ModalProps & ConnectedModalWrappedProps & GuidComponentWrappedProps> = ({
	// exclude props that shouldn't be passed to the Modal
	guid,
	modals,
	isTopOpenFullscreenModal,
	dispatch,
	...modalProps
}) => {
	return <Modal {...modalProps} />
}

/**
 * Wrapped version of "react-bootstrap"'s `Modal` component. Uses `ConnectedModalComponent` to coordinate modal state in redux.
 */
export default ConnectedModalComponent(ConnectedModal)
