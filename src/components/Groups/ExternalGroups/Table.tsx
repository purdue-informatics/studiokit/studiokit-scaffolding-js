import ToggleButton from '@material-ui/lab/ToggleButton'
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup'
import { isEmpty } from 'lodash'
import React, { FunctionComponent, TableHTMLAttributes } from 'react'
import { Table, Tbody, Td, Th, Thead, Tr } from 'react-super-responsive-table'
import { BASE_ROLE } from '../../../constants'
import { ExternalGroup, Group, GroupUserRole, LtiLaunch, ModelCollection, UniTimeGroup, User } from '../../../types'
import { getModelArray } from '../../../utils/model'
import { displayName } from '../../../utils/user'

export interface ExternalGroupsTableProps extends TableHTMLAttributes<Element> {
	group: Partial<Group>
	externalGroups: Array<ExternalGroup | LtiLaunch | UniTimeGroup>
	hasGradePushEnabled: boolean
	hasAnyLtiExternalGroup: boolean
	canEditExternalGroups: boolean
	groupUserRoles?: ModelCollection<GroupUserRole>
	updateExternalGroup: (externalGroup: ExternalGroup | LtiLaunch | UniTimeGroup) => void
}

const shouldShowAutoGradePushColumn = (
	hasAnyLtiExternalGroup: boolean,
	group: Partial<Group>,
	hasGradePushEnabled: boolean
) => (hasAnyLtiExternalGroup && isEmpty(group)) || hasGradePushEnabled

const renderTableHeaderRow = (hasAnyLtiExternalGroup: boolean, group: Partial<Group>, hasGradePushEnabled: boolean) => {
	const headerCells = ['Course Name', 'Course Description', 'Instructor']
	if (shouldShowAutoGradePushColumn(hasAnyLtiExternalGroup, group, hasGradePushEnabled)) {
		headerCells.push('Grade Push')
	}
	return (
		<Tr>
			{headerCells.map((value, i) => (
				<Th key={i}>{value}</Th>
			))}
		</Tr>
	)
}

const renderTableBodyRow = (
	key: number,
	externalGroup: ExternalGroup | LtiLaunch | UniTimeGroup,
	owner: User | undefined,
	hasGradePushEnabled: boolean,
	hasAnyLtiExternalGroup: boolean,
	canEditExternalGroups: boolean,
	group: Partial<Group>,
	updateExternalGroup: (externalGroup: ExternalGroup | LtiLaunch | UniTimeGroup) => void
) => {
	const cells: Array<string | JSX.Element | null> = [
		externalGroup.name,
		<span style={{ overflowWrap: 'anywhere' }}>{externalGroup.description}</span>,
		owner ? `${displayName(owner)}` : ''
	]
	// brand new LTI group or editing a group that has grade push enabled
	if (shouldShowAutoGradePushColumn(hasAnyLtiExternalGroup, group, hasGradePushEnabled)) {
		if (canEditExternalGroups) {
			cells.push(
				<ToggleButtonGroup
					value={externalGroup.isAutoGradePushEnabled}
					exclusive
					onChange={(_: React.MouseEvent<HTMLElement>, newValue: boolean | null) => {
						// do not allow "turning off" any of the selections
						if (newValue === null) return
						updateExternalGroup({ ...externalGroup, isAutoGradePushEnabled: newValue })
					}}
					aria-label="auto grade push">
					<ToggleButton value={true}>Automatic</ToggleButton>
					<ToggleButton value={false}>Manual</ToggleButton>
				</ToggleButtonGroup>
			)
		} else {
			cells.push(externalGroup.isAutoGradePushEnabled ? 'Automatic' : 'Manual')
		}
	}
	return (
		<Tr key={key}>
			{cells.map((value, i) => (
				<Td key={i}>{value}</Td>
			))}
		</Tr>
	)
}

export const ExternalGroupsTable: FunctionComponent<ExternalGroupsTableProps> = ({
	group,
	externalGroups,
	hasGradePushEnabled,
	hasAnyLtiExternalGroup,
	canEditExternalGroups,
	groupUserRoles,
	updateExternalGroup,
	...other // for passing aria-describedby to tables
}) => (
	<div className="table-container">
		<Table {...other} className="w-100 mb4">
			<Thead>{renderTableHeaderRow(hasAnyLtiExternalGroup, group, hasGradePushEnabled)}</Thead>
			<Tbody>
				{externalGroups.map((externalGroup, i) => {
					const owner =
						isEmpty(group) && !!groupUserRoles
							? (getModelArray(groupUserRoles).find(
									gur => gur.userId === externalGroup.userId && gur.role === BASE_ROLE.GROUP_OWNER
							  ) as User | undefined)
							: (group.owners?.find(o => o.id === externalGroup.userId) as User | undefined)
					return renderTableBodyRow(
						i,
						externalGroup,
						owner,
						hasGradePushEnabled,
						hasAnyLtiExternalGroup,
						canEditExternalGroups,
						group,
						updateExternalGroup
					)
				})}
			</Tbody>
		</Table>
	</div>
)
