import { Dictionary, every, forEach, map, orderBy, remove, set, some } from 'lodash'
import React from 'react'
import { Component } from 'react'
import { FormCheck, FormGroup } from 'react-bootstrap'
import { v4 as uuidv4 } from 'uuid'
import { BASE_ROLE, MODEL_STATUS } from '../../../constants'
import { dispatchModelFetchRequest } from '../../../redux/actionCreator'
import { registerNoStoreActionHook, unregisterNoStoreActionHook } from '../../../redux/sagas/noStoreSaga'
import {
	ExternalGroup,
	FetchErrorData,
	GroupUserRole,
	InstructorSchedule,
	LtiLaunch,
	ModelCollection,
	OwnerSchedule,
	UniTimeGroup,
	UserInfo
} from '../../../types'
import { isFetchErrorData } from '../../../utils/fetch'
import { getModelArray } from '../../../utils/model'
import { isPurdueShard } from '../../../utils/shard'
import { displayName } from '../../../utils/user'
import { AlertWithIcon } from '../../AlertWithIcon'
import { Loading } from '../../Loading'

export interface ExternalGroupsAttachProps {
	userInfo: UserInfo
	canReadExternalGroups: boolean
	canConnectAnyExternalGroups: boolean
	canConnectOwnExternalGroups: boolean
	originalConnectedExternalGroups: Array<ExternalGroup | LtiLaunch>
	selectedExternalGroups: Array<ExternalGroup | LtiLaunch | UniTimeGroup>
	externalTermId: number
	handleSelectedExternalGroups: (externalGroups: Array<ExternalGroup | LtiLaunch | UniTimeGroup>) => void
	groupUserRoles: ModelCollection<GroupUserRole>
	disabled?: boolean
	disabledInfoMessage?: string | JSX.Element
}

interface ExternalGroupsAttachState {
	ownerSchedules?: Dictionary<OwnerSchedule>
	isUniTimeScheduleLoadingDone: boolean
	isShowingErrorAlert: boolean
}

export class ExternalGroupsAttach extends Component<ExternalGroupsAttachProps, ExternalGroupsAttachState> {
	constructor(props: ExternalGroupsAttachProps) {
		super(props)
		this.state = {
			ownerSchedules: undefined,
			isUniTimeScheduleLoadingDone: false,
			isShowingErrorAlert: false
		}
	}

	componentDidMount() {
		const { groupUserRoles, externalTermId, selectedExternalGroups } = this.props
		this.setOwnerSchedules(getModelArray(groupUserRoles), externalTermId, selectedExternalGroups)
	}

	componentWillUnmount() {
		const { ownerSchedules } = this.state
		forEach(ownerSchedules, os => unregisterNoStoreActionHook(os.hookId))
	}

	componentDidUpdate(prevProps: Readonly<ExternalGroupsAttachProps>) {
		const { externalTermId: prevExternalTermId, groupUserRoles: prevGroupUserRoles } = prevProps
		const { externalTermId, groupUserRoles, selectedExternalGroups } = this.props
		// Assumes group user roles are changing one at a time. Can be made fancier if the need arises
		if (
			!!prevGroupUserRoles &&
			!!groupUserRoles &&
			(prevExternalTermId !== externalTermId ||
				Object.keys(prevGroupUserRoles).length !== Object.keys(groupUserRoles).length)
		) {
			this.setOwnerSchedules(getModelArray(groupUserRoles), externalTermId, selectedExternalGroups)
		}
	}

	setOwnerSchedules = (
		groupUserRoles: GroupUserRole[],
		externalTermId: number,
		selectedExternalGroups: Array<ExternalGroup | LtiLaunch | UniTimeGroup>
	) => {
		const { canConnectAnyExternalGroups, canConnectOwnExternalGroups, userInfo } = this.props
		const owners = groupUserRoles.filter(groupUserRole => {
			return groupUserRole.role === BASE_ROLE.GROUP_OWNER
		})

		const ownersInitialScheduleData: Dictionary<OwnerSchedule> = {}
		owners.forEach(owner => {
			set(ownersInitialScheduleData, [owner.userId], {
				hookId: uuidv4(),
				status:
					canConnectAnyExternalGroups || (userInfo.id === owner.id && canConnectOwnExternalGroups)
						? MODEL_STATUS.UNINITIALIZED
						: MODEL_STATUS.READY,
				owner,
				externalGroups: !(
					canConnectAnyExternalGroups ||
					(userInfo.id === owner.id && canConnectOwnExternalGroups)
				)
					? selectedExternalGroups.filter(seg => seg.userId === owner.id)
					: undefined
			})
		})
		this.setState(
			{
				ownerSchedules: ownersInitialScheduleData,
				isUniTimeScheduleLoadingDone: false
			},
			() =>
				forEach(ownersInitialScheduleData, (ownerSchedule, userId) =>
					this.getOwnerUniTimeGroups(ownerSchedule, userId, externalTermId)
				)
		)
	}

	handleSelectExternalGroups = (event: any) => {
		const externalId = event.target.value
		const checked = event.target.checked
		const { originalConnectedExternalGroups, selectedExternalGroups } = this.props

		const allOwnerExternalGroups = map(this.state.ownerSchedules, os => os.externalGroups).reduce((a, b) =>
			a.concat(b)
		)
		const selectedExternalGroupsCopy = selectedExternalGroups.slice()

		if (checked) {
			const originalConnectedExternalGroup = originalConnectedExternalGroups.find(
				oeg => oeg.externalId === externalId
			)
			const selectedExternalGroup = originalConnectedExternalGroup
				? originalConnectedExternalGroup
				: allOwnerExternalGroups?.find(ao => ao.externalId === externalId)

			if (selectedExternalGroup) {
				selectedExternalGroupsCopy.push(selectedExternalGroup)
			}
		} else {
			remove(selectedExternalGroupsCopy, seg => seg.externalId === externalId)
		}

		this.props.handleSelectedExternalGroups(selectedExternalGroupsCopy)
	}

	getOwnerUniTimeGroups = (ownerSchedule: OwnerSchedule, userId: string, externalTermId: number) => {
		if (isPurdueShard() && ownerSchedule.status === MODEL_STATUS.UNINITIALIZED && !ownerSchedule.externalGroups) {
			registerNoStoreActionHook(ownerSchedule.hookId, (data: InstructorSchedule | FetchErrorData | null) => {
				unregisterNoStoreActionHook(ownerSchedule.hookId)
				if (!data || isFetchErrorData(data)) {
					this.setState({ isShowingErrorAlert: true, isUniTimeScheduleLoadingDone: true })
				} else {
					this.setUserUniTimeGroups(userId, data.externalGroups)
				}
			})
			dispatchModelFetchRequest({
				modelName: 'uniTimeInstructorSchedule',
				queryParams: {
					externalTermId,
					userId
				},
				noStore: true,
				guid: ownerSchedule.hookId
			})
		}
	}

	setUserUniTimeGroups = (userId: string, externalGroups: UniTimeGroup[]) => {
		// Setting the user's course sections load status to be ready
		const ownerSchedules = this.state.ownerSchedules || {}
		ownerSchedules[userId].status = MODEL_STATUS.READY
		ownerSchedules[userId].externalGroups = externalGroups

		const isUniTimeScheduleLoadingDone = every(ownerSchedules, os => os.status === MODEL_STATUS.READY)

		this.setState({
			ownerSchedules,
			isUniTimeScheduleLoadingDone
		})
	}

	displayOwnerSchedules = (ownerSchedules: Dictionary<OwnerSchedule>) => {
		return map(ownerSchedules, os => {
			if (!!os.externalGroups && os.externalGroups.length > 0) {
				return (
					<div key={os.hookId}>
						<h4>{displayName(os.owner)}</h4>
						{this.displayUniTimeGroups(os.externalGroups)}
					</div>
				)
			}
		})
	}

	displayUniTimeGroups = (externalGroups: UniTimeGroup[]) => {
		const {
			selectedExternalGroups,
			canConnectAnyExternalGroups,
			canConnectOwnExternalGroups,
			userInfo,
			disabled
		} = this.props

		return orderBy(externalGroups, ['description']).map((eg, index) => {
			const isConnected = selectedExternalGroups.some(
				seg =>
					seg.externalId === eg.externalId &&
					seg.externalProviderId === eg.externalProviderId &&
					seg.userId === eg.userId
			)

			if (
				canConnectAnyExternalGroups ||
				(userInfo.id === eg.userId && canConnectOwnExternalGroups) ||
				isConnected
			) {
				return (
					<FormCheck
						id={`external-group-${eg.id}`}
						type="checkbox"
						key={index}
						onChange={this.handleSelectExternalGroups}
						value={eg.externalId}
						checked={isConnected}
						disabled={(userInfo.id !== eg.userId && !canConnectAnyExternalGroups) || disabled}
						label={`${eg.name} ${eg.description ? `(${eg.description})` : ''}`}
					/>
				)
			}
			return null
		})
	}

	render() {
		const { disabledInfoMessage } = this.props
		const { ownerSchedules, isUniTimeScheduleLoadingDone, isShowingErrorAlert } = this.state
		const hasAnyExternalGroups = some(ownerSchedules, os => !!os.externalGroups && os.externalGroups.length > 0)

		return (
			<div>
				{!isUniTimeScheduleLoadingDone && <Loading />}
				{isUniTimeScheduleLoadingDone && (
					<div>
						{!hasAnyExternalGroups && (
							<AlertWithIcon variant="info">
								<p>No course sections eligible for roster sync.</p>
							</AlertWithIcon>
						)}
						{!!disabledInfoMessage && hasAnyExternalGroups && (
							<AlertWithIcon variant="info">
								<p>{disabledInfoMessage}</p>
							</AlertWithIcon>
						)}
						{isShowingErrorAlert && (
							<AlertWithIcon
								variant="warning"
								onClose={() =>
									this.setState({
										isShowingErrorAlert: false
									})
								}>
								<p className="ma0">Oops! Something went wrong while loading. Please try again.</p>
							</AlertWithIcon>
						)}{' '}
						{hasAnyExternalGroups && !!ownerSchedules && (
							<FormGroup className="ml4 f6">{this.displayOwnerSchedules(ownerSchedules)}</FormGroup>
						)}
					</div>
				)}
			</div>
		)
	}
}
