import { LtiLaunch } from '../../types'

export interface GroupCreateOrEditCommonProps {
	ltiLaunch?: LtiLaunch
	setNewGroupId?: (externalGroupId: number) => void
	onCancel?: () => void
}
