import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import React, { FunctionComponent } from 'react'

export interface GroupCreateEditCopySaveButtonsProps {
	cannotSave: () => boolean
	cannotSaveAndAddOwners: () => boolean
	canAddInstructors: boolean
	save: (shouldEditAfterSaving?: boolean) => void
	cancel: () => void
	disabled?: boolean
}

export const GroupCreateEditCopySaveButtons: FunctionComponent<GroupCreateEditCopySaveButtonsProps> = ({
	disabled,
	cannotSave,
	cannotSaveAndAddOwners,
	canAddInstructors,
	save,
	cancel
}) => {
	return (
		<div>
			{!!disabled && <CircularProgress size={30} className="fr" />}
			<Button
				onClick={() => save()}
				color="primary"
				className={`fr btn-primary${disabled ? ' visually-hidden' : ''}`}
				disabled={cannotSave()}>
				Save
			</Button>

			{canAddInstructors && (
				<Button
					onClick={() => save(true)}
					color="primary"
					className={`fr btn-primary mr1${disabled ? ' visually-hidden' : ''}`}
					disabled={cannotSaveAndAddOwners()}>
					Save and Add Instructors
				</Button>
			)}

			<Button onClick={cancel} color="primary" className={`fr btn-text mr1${disabled ? ' visually-hidden' : ''}`}>
				Cancel
			</Button>
		</div>
	)
}
