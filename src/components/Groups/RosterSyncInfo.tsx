import Button from '@material-ui/core/Button'
import IconHighlight from '@material-ui/icons/Highlight'
import React, { FunctionComponent, useState } from 'react'
import { Modal } from 'react-bootstrap'
import { getAppConfig } from '../../constants/configuration'
import ConnectedModal from '../ConnectedModal'

export interface RosterSyncInfoProps {
	hasLtiRosterSync?: boolean
}

export const RosterSyncInfo: FunctionComponent<RosterSyncInfoProps> = ({ hasLtiRosterSync }) => {
	const [isModalOpen, setIsModalOpen] = useState(false)
	const lmsName = getAppConfig().LMS_NAME
	const title = `${hasLtiRosterSync ? `${lmsName} ` : ''}Roster Sync`
	return (
		<>
			<Button color="primary" className="mb0 mr1-ns pv2 ph2 fr-ns btn-text" onClick={() => setIsModalOpen(true)}>
				<IconHighlight className="v-mid mr1" />
				<span className="mb0 b">{title}</span>
			</Button>
			<ConnectedModal show={isModalOpen} onHide={() => setIsModalOpen(false)}>
				<Modal.Header closeButton>
					<Modal.Title className="f4">{title}</Modal.Title>
				</Modal.Header>

				<Modal.Body>
					<h3>When is the roster synced?</h3>
					<p>
						The roster is synced nightly throughout the duration of the course. People are added and removed
						automatically as the{' '}
						{hasLtiRosterSync ? `roster changes in ${lmsName}` : 'external roster changes'}.
					</p>
				</Modal.Body>

				<Modal.Footer>
					<Button color="primary" className="ma0 btn-primary" onClick={() => setIsModalOpen(false)}>
						Close
					</Button>
				</Modal.Footer>
			</ConnectedModal>
		</>
	)
}
