import * as Sentry from '@sentry/react'
import { Route as NormalRoute } from 'react-router'

const Route = Sentry.withSentryRouting(NormalRoute)
export default Route
