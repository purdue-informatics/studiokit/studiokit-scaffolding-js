import React, { FunctionComponent } from 'react'
import { RefreshIndicator, RefreshIndicatorProps } from './index'

interface BorderedRefreshIndicatorProps extends RefreshIndicatorProps {
	borderClassName?: string
}

export const RefreshIndicatorBordered: FunctionComponent<BorderedRefreshIndicatorProps> = props => {
	const { borderClassName, ...rest } = props
	return (
		<div className={`ba b--dashed bw1 b--custom-blue${borderClassName ? ` ${borderClassName}` : ''}`}>
			<RefreshIndicator {...rest} />
		</div>
	)
}
