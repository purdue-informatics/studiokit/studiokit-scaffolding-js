import CircularProgress, { CircularProgressProps } from '@material-ui/core/CircularProgress'
import React, { FunctionComponent } from 'react'

export const RefreshIndicatorInline: FunctionComponent<CircularProgressProps> = props => {
	const { className, ...rest } = props
	return (
		<CircularProgress
			size={35}
			style={{
				position: 'relative'
			}}
			className={`dib${props.className ? ` ${props.className}` : ''}`}
			{...rest}
		/>
	)
}
