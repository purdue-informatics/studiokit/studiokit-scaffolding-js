import CircularProgress from '@material-ui/core/CircularProgress'
import * as Sentry from '@sentry/react'
import { detect } from 'detect-browser'
import React, { FunctionComponent, useEffect, useState } from 'react'
import { getLockDownBrowserInfo } from '../../utils/lockDownBrowser'
import { getLogger } from '../../utils/logger'

const Shortcut = ({ content }: { content: string | JSX.Element }) => <span className="tag ma0 mb1">{content}</span>

const getRefreshShortcut = () => {
	const browser = detect()
	if (browser?.os === 'Mac OS') {
		return <Shortcut content="command+R" />
	}
	if (browser?.os?.includes('Windows')) {
		return (
			<>
				<Shortcut content="control+R" /> or <Shortcut content="F5" />
			</>
		)
	}
}

const getHardRefreshShortcut = () => {
	const browser = detect()
	if (browser?.os === 'Mac OS') {
		if (browser.name === 'safari') {
			return <Shortcut content="command+option+R" />
		}
		return <Shortcut content="command+option+R" />
	}
	if (browser?.os?.includes('Windows')) {
		return (
			<>
				<Shortcut content="control+shift+R" /> or <Shortcut content="control+F5" />
			</>
		)
	}
}

export interface RefreshIndicatorProps {
	/** An error will still be logged to Sentry after `warnAfterSeconds`, but no warning will be disabled. */
	hideWarning?: boolean
	/** Amount of seconds the component can be rendered before the warning is shown and an error is logged to Sentry. Default is 15 seconds. */
	logAndWarnAfterSeconds?: number
	/** Additional classes to add the the default warning component. Does not apply if `warningMessage` is provided. */
	warningClassName?: string
	/** An alternative message to display after `warnAfterSeconds`. */
	warningMessage?: string | JSX.Element
	/** A label to display under the CircularProgress indicator. */
	label?: string | JSX.Element
	/** Additional classes to add to the `label`'s containing `p`. */
	labelClassName?: string
	/** Optional size for the `CircularProgress` loader. Default is 40. */
	loaderSize?: number
	/** Optional style for the `CircularProgress` loader. */
	loaderStyle?: React.CSSProperties
}

export const RefreshIndicator: FunctionComponent<RefreshIndicatorProps> = ({
	hideWarning,
	logAndWarnAfterSeconds,
	warningClassName,
	warningMessage,
	label,
	labelClassName,
	loaderSize,
	loaderStyle
}) => {
	const [seconds, setSeconds] = useState(0)
	const [hasLogged, setHasLogged] = useState(false)
	const localLogAndWarnAfterSeconds = logAndWarnAfterSeconds !== undefined ? logAndWarnAfterSeconds : 15

	// every time seconds changes, set a 1 sec interval for the next second, a.k.a re-render every second
	useEffect(() => {
		const interval = setInterval(() => {
			setSeconds(seconds => seconds + 1)
		}, 1000)
		return () => clearInterval(interval)
	}, [seconds])

	const shouldLog = seconds >= localLogAndWarnAfterSeconds
	const shouldWarn = shouldLog && !hideWarning

	// log an exception one time, if component has been rendered at least `warnAfterSeconds`
	useEffect(() => {
		if (!hasLogged && shouldLog) {
			const error = new Error(`Loading for more than ${localLogAndWarnAfterSeconds} seconds`)
			const logger = getLogger()
			logger.warn(error)
			Sentry.captureException(error)
			setHasLogged(true)
		}
	}, [hasLogged, localLogAndWarnAfterSeconds, seconds, shouldLog, shouldWarn])

	const refreshShortcut = getRefreshShortcut()
	const hardRefreshShortcut = getHardRefreshShortcut()
	const lockDownBrowserInfo = getLockDownBrowserInfo()

	return (
		<div>
			<CircularProgress
				size={loaderSize ?? 40}
				style={
					loaderStyle ?? {
						display: 'inherit',
						position: 'relative',
						margin: '2rem auto 1rem'
					}
				}
			/>
			{label && <p className={`tc i f4${labelClassName ? ` ${labelClassName}` : ''}`}>{label}</p>}
			{shouldWarn && (
				<div aria-live="polite">
					{warningMessage ?? (
						<div className={`tl center mw7 ph5${warningClassName ? ` ${warningClassName}` : ''}`}>
							<h2>Stuck loading?</h2>
							<p className="mb3">
								If the page does not finish loading soon, please try one of the following:
							</p>
							<ul className="mb3">
								<li className={!refreshShortcut && !!hardRefreshShortcut ? 'mb2' : ''}>
									Refresh the page{refreshShortcut ? <>, {refreshShortcut}</> : ''}
								</li>
								{!lockDownBrowserInfo.isClientLockDownBrowser ? (
									<>
										{hardRefreshShortcut && <li>"Hard Refresh" the page, {hardRefreshShortcut}</li>}
										<li className={!!refreshShortcut || !!hardRefreshShortcut ? 'mb2' : ''}>
											Try a different browser
										</li>
										<li>Clear your browser cache and then refresh</li>
									</>
								) : (
									<>
										<li>Close and re-open LockDown Browser</li>
										<li>Restart your computer and then re-open LockDown Browser</li>
									</>
								)}
							</ul>
							<p className="mb3">
								If you are still experiencing loading issues, please contact us at{' '}
								<a href="mailto:tlt@purdue.edu">tlt@purdue.edu</a>.
							</p>
						</div>
					)}
				</div>
			)}
		</div>
	)
}
