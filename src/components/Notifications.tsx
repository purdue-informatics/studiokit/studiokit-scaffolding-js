import { Button, Snackbar, SnackbarContent } from '@material-ui/core'
import IconClose from '@material-ui/icons/Close'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { v4 as uuidv4 } from 'uuid'
import { NOTIFICATION_TYPE } from '../constants'
import { dispatchAction } from '../redux/actionCreator'
import { NOTIFICATION_ACTION_TYPE, NotificationAction } from '../redux/actions'
import { ModalsState } from '../redux/reducers/modalsReducer'
import { NotificationState } from '../redux/reducers/notificationsReducer'
import { Notification } from '../types'

export interface NotificationsProps {
	/** The queue of notifications, FIFO */
	notifications: Notification[]
	/** The identifiers for all currently open modals, used to correlate with `notification.modalId` */
	openModalIds: string[]
}

interface NotificationsState {
	guid: string
	notification?: Notification
	className: string
	isOpen: boolean
}

export class Notifications extends Component<NotificationsProps, NotificationsState> {
	afterCloseTimeout: any = undefined

	constructor(props: NotificationsProps) {
		super(props)
		this.state = {
			guid: uuidv4(),
			notification: undefined,
			className: '',
			isOpen: false // use to handle animation separate from existence of notification
		}
	}

	componentDidMount() {
		this.updateNotification()
	}

	componentDidUpdate() {
		this.updateNotification()
	}

	componentWillUnmount() {
		const { notifications } = this.props
		const { isOpen } = this.state
		if (isOpen && notifications.length > 0) {
			this.removeNotification()
		}
		this.clearAfterCloseTimeout()
	}

	clearAfterCloseTimeout = () => {
		if (this.afterCloseTimeout) {
			clearTimeout(this.afterCloseTimeout)
			this.afterCloseTimeout = undefined
		}
	}

	updateNotification = () => {
		const { openModalIds, notifications } = this.props
		const { isOpen, notification: currentNotification } = this.state

		// find next notification, if any
		const notification = notifications.length > 0 ? notifications[0] : undefined

		// react to the last notification being removed, and close the alert
		if (isOpen && !notification) {
			this.closeNotification()
			return
		}

		// exit early if there is no notification
		if (!notification) {
			return
		}

		const shouldShowNotification =
			// the notification is different from the current, if any
			notification.id !== currentNotification?.id &&
			// the alert is not open
			!isOpen &&
			// show immediately, ignoring modals
			(notification.modalId === null ||
				// show after all modals are closed
				(notification.modalId === undefined && openModalIds.length === 0) ||
				// show after a specific modal is closed
				(!!notification.modalId && !openModalIds.includes(notification.modalId)))

		if (shouldShowNotification) {
			this.setState({
				notification,
				className: this.classNameForType(notification.type),
				isOpen: true
			})
		}
	}

	closeNotification = () => {
		const { notifications } = this.props
		this.setState(
			{
				isOpen: false
			},
			() => {
				// wait until after Snackbar hide animation to clear the UI
				this.afterCloseTimeout = setTimeout(() => {
					this.removeNotification()
					this.clearAfterCloseTimeout()
					// removing the last notification, clear state
					if (notifications.length <= 1) {
						this.setState({
							notification: undefined,
							className: ''
						})
					}
				}, 300)
			}
		)
	}

	removeNotification = () => {
		const { notification } = this.state
		if (!notification) {
			return
		}
		dispatchAction<NotificationAction>({
			type: NOTIFICATION_ACTION_TYPE.REMOVE_NOTIFICATION,
			notification
		})
	}

	classNameForType = (type: NOTIFICATION_TYPE) => {
		switch (type) {
			case NOTIFICATION_TYPE.INFO:
				return 'alert-info'
			case NOTIFICATION_TYPE.SUCCESS:
				return 'alert-success'
			case NOTIFICATION_TYPE.WARNING:
				return 'alert-warning'
			case NOTIFICATION_TYPE.ERROR:
				return 'alert-danger'
		}
	}

	render() {
		const { guid, notification, className, isOpen } = this.state
		const id = `notifications-${guid}`
		return (
			<div id={id} aria-live="polite">
				<Snackbar
					anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
					open={isOpen}
					autoHideDuration={5000}
					onClose={this.closeNotification}
					className="mh2 alert notification-alert">
					<SnackbarContent
						className={className}
						aria-describedby={`${id}-alert-message`}
						message={
							<span id={`${id}-alert-message`} className="notification-alert-message">
								<h4 className="normal pt2">{!!notification && notification.text}</h4>
							</span>
						}
						action={[
							<Button
								className="notification-close-button"
								aria-label="Close"
								color="inherit"
								key="close"
								onClick={this.removeNotification}>
								<IconClose className="v-mid" />
								Close
							</Button>
						]}
					/>
				</Snackbar>
			</div>
		)
	}
}

export const mapStateToProps = (state: { notifications: NotificationState; modals: ModalsState }) => {
	return {
		notifications: state.notifications.queue,
		openModalIds: Object.keys(state.modals)
	}
}

export default connect(mapStateToProps)(Notifications)
