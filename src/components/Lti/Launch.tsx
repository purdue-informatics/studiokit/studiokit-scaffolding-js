import React, { ComponentType, FunctionComponent, useState } from 'react'
import Helmet from 'react-helmet'
import { useParams } from 'react-router'
import { Loading } from '../../components/Loading'
import { MODEL_STATUS } from '../../constants/modelStatus'
import { useCollectionItem } from '../../hooks/useCollectionItem'
import { LtiLaunch as LtiLaunchModel } from '../../types/LtiLaunch'
import { isBasicLaunch } from '../../utils/lti'
import { GroupCreateOrEditCommonProps } from '../Groups/GroupCreateOrEditCommonProps'
import { LtiLaunchGroup } from './LaunchGroup'

export interface LtiLaunchMatchParams {
	ltiLaunchId: string
}

export interface ChooseGradableProps {
	ltiLaunch: LtiLaunchModel
	/**
	 * We accept this as a parameter even though it's a member of LtiLaunch because in this context
	 * it should never be null - it will either already have existed or it was just created prior to
	 * rendering this component
	 */
	groupId: number
}

export interface LaunchProps {
	GroupCreateOrEdit: ComponentType<GroupCreateOrEditCommonProps>
	ChooseGradable: ComponentType<ChooseGradableProps>
}

export const LtiLaunch: FunctionComponent<LaunchProps> = ({ GroupCreateOrEdit, ChooseGradable }) => {
	const [newGroupId, setNewGroupId] = useState<number | undefined>(undefined)
	const { ltiLaunchId } = useParams<LtiLaunchMatchParams>()

	const { model: ltiLaunch, modelStatus: ltiLaunchStatus } = useCollectionItem<LtiLaunchModel>({
		modelName: 'ltiLaunches',
		pathParams: [ltiLaunchId]
	})

	const launchGroupId = ltiLaunch.groupId ?? newGroupId
	const isBasicLtiLaunch = isBasicLaunch(ltiLaunch)

	return (
		<>
			<Helmet title={`LTI ${isBasicLtiLaunch ? 'Launch' : 'Link'}`} />
			{ltiLaunchStatus !== MODEL_STATUS.READY && <Loading />}
			{ltiLaunchStatus === MODEL_STATUS.READY &&
				(isBasicLtiLaunch || !launchGroupId ? (
					<LtiLaunchGroup
						ltiLaunch={ltiLaunch}
						setNewGroupId={!isBasicLtiLaunch ? setNewGroupId : undefined}
						GroupCreateOrEdit={GroupCreateOrEdit}
					/>
				) : (
					<ChooseGradable ltiLaunch={ltiLaunch} groupId={launchGroupId} />
				))}
		</>
	)
}

// required for use in Routes / AsyncComponent
export default LtiLaunch
