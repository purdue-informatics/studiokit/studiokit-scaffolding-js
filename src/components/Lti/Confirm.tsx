import ArrowBack from '@material-ui/icons/ArrowBack'
import ContentCreate from '@material-ui/icons/Create'
import React, { FunctionComponent } from 'react'
import { Button, Col, Row } from 'react-bootstrap'
import { getAppConfig } from '../../constants/configuration'
import { LtiLaunch } from '../../types/LtiLaunch'
import { isInsideIFrame } from '../../utils/lti'

export interface ConfirmProps {
	ltiLaunch: LtiLaunch
	onConfirm: () => void
	onCancel: () => void
}

export const Confirm: FunctionComponent<ConfirmProps> = ({ ltiLaunch, onConfirm, onCancel }) => {
	const isWithinIFrame = isInsideIFrame()
	return (
		<div className={`tc ${isWithinIFrame ? `mb4` : `mb5`}`}>
			<Row>
				<Col xs={12}>
					<h2 className={`f3 tc ${isWithinIFrame ? `mv4` : `mb4`}`}>
						Would you like to set up <strong>{ltiLaunch.name}</strong> in {getAppConfig().APP_NAME}?
					</h2>
				</Col>
			</Row>
			<Row>
				{!isWithinIFrame && (
					<Col xs={12} md={6}>
						<Button
							className="ml0 mb3 mt3 br3 ws-normal button-large button-fat"
							variant="light"
							onClick={onCancel}>
							<ArrowBack color="primary" className="icon-xl" />
							<h3 className="black">Not now, thanks</h3>
							<p className="f6 mb0">No problem! You can always come back and create your course later.</p>
						</Button>
					</Col>
				)}
				<Col xs={12} md={isWithinIFrame ? 12 : 6}>
					<Button
						className="ml0 mb3 mt3 br3 ws-normal button-large button-fat"
						variant="light"
						onClick={onConfirm}>
						<ContentCreate color="primary" className="icon-xl" />
						<h3 className="black">Sure!</h3>
						<p className="f6 mb0">You’ll be guided through creating your course. Let’s get started!</p>
					</Button>
				</Col>
			</Row>
		</div>
	)
}
