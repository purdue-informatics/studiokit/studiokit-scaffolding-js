import { History } from 'history'
import React, { ComponentType, FunctionComponent, useState } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { useHistory } from 'react-router'
import { LtiLaunch } from '../../types/LtiLaunch'
import { isInsideIFrame } from '../../utils/lti'
import { GroupCreateOrEditCommonProps } from '../Groups/GroupCreateOrEditCommonProps'
import { Confirm } from './Confirm'

export interface LtiLaunchGroupProps {
	ltiLaunch: LtiLaunch
	setNewGroupId?: (externalGroupId: number) => void
	GroupCreateOrEdit: ComponentType<GroupCreateOrEditCommonProps>
}

const onCancel = (ltiLaunch: LtiLaunch, history: History<any>) => {
	ltiLaunch.returnUrl ? (window.location.href = ltiLaunch.returnUrl) : history.push('/')
}

export const LtiLaunchGroup: FunctionComponent<LtiLaunchGroupProps> = ({
	ltiLaunch,
	setNewGroupId,
	GroupCreateOrEdit
}) => {
	const [didConfirm, setDidConfirm] = useState(false)
	const history = useHistory()
	const isWithinIFrame = isInsideIFrame()

	return (
		<>
			{!didConfirm && (
				<div className={`${isWithinIFrame ? '' : 'pv5 '}nb4`}>
					<Container className="mw7">
						{!isWithinIFrame && (
							<Row>
								<Col xs={12}>
									<h1 className="tc">Course Setup</h1>
								</Col>
							</Row>
						)}
						<Confirm
							ltiLaunch={ltiLaunch}
							onConfirm={() => setDidConfirm(true)}
							onCancel={() => onCancel(ltiLaunch, history)}
						/>
					</Container>
				</div>
			)}
			{didConfirm && (
				<GroupCreateOrEdit
					ltiLaunch={ltiLaunch}
					setNewGroupId={setNewGroupId}
					onCancel={() => setDidConfirm(false)}
				/>
			)}
		</>
	)
}
