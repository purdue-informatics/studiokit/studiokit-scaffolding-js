import React, { FunctionComponent } from 'react'
import { connect } from 'react-redux'
import { getAppConfig } from '../../constants'
import { EXTERNAL_PROVIDER_TYPE } from '../../constants/externalProviderType'
import { BaseReduxState, ExternalProvider, ModelCollection } from '../../types'
import { getModelArray } from '../../utils/model'
import { AlertDialog, AlertDialogProps } from '../AlertDialog'

export interface CreateNonLtiGroupAlertDialogReduxProps {
	externalProviders: ModelCollection<ExternalProvider>
}

export interface CreateNonLtiGroupAlertDialogProps
	extends Omit<AlertDialogProps, 'title'>,
		CreateNonLtiGroupAlertDialogReduxProps {
	externalHelpResourcePageUrl?: string
}

export const CreateNonLtiGroupAlertDialog: FunctionComponent<CreateNonLtiGroupAlertDialogProps> = ({
	externalProviders,
	externalHelpResourcePageUrl,
	...otherProps
}) => {
	const isLtiRosterSyncEnabled = getModelArray(externalProviders)
		.filter(ep => ep.typename === EXTERNAL_PROVIDER_TYPE.LTI)
		.some(ep => ep.rosterSyncEnabled)

	const isLtiGradePushEnabled = getModelArray(externalProviders)
		.filter(ep => ep.typename === EXTERNAL_PROVIDER_TYPE.LTI)
		.some(ep => ep.gradePushEnabled)

	const isNonLtiRosterSyncEnabled = getModelArray(externalProviders)
		.filter(ep => ep.typename !== EXTERNAL_PROVIDER_TYPE.LTI)
		.some(ep => ep.rosterSyncEnabled)

	const config = getAppConfig()

	return (
		<AlertDialog
			{...otherProps}
			title={`Add a Course without ${config.LMS_NAME} Integration`}
			description={
				<>
					<p className="mb4">
						You are about to manually add a course outside of {config.LMS_NAME}.
						{((isLtiRosterSyncEnabled && !isNonLtiRosterSyncEnabled) || isLtiGradePushEnabled) && (
							<span className="b">
								{' '}
								{!isNonLtiRosterSyncEnabled && 'Roster Sync'}
								{isLtiRosterSyncEnabled &&
									!isNonLtiRosterSyncEnabled &&
									isLtiGradePushEnabled &&
									' and '}
								{isLtiGradePushEnabled && 'Grade Push'} will not be available in this course.
							</span>
						)}
					</p>
					{externalHelpResourcePageUrl && (
						<p>
							For help on linking to your {config.LMS_NAME} course, please{' '}
							<a href={externalHelpResourcePageUrl} target="_blank" rel="noopener noreferrer">
								check out our resources page
							</a>
							.
						</p>
					)}
					<p className="mb0">Are you sure you want to add a course outside of {config.LMS_NAME}?</p>
				</>
			}
			cancelText={'No, I changed my mind'}
			proceedText={'Yes, continue'}
		/>
	)
}

export const mapStateToProps = (state: BaseReduxState): CreateNonLtiGroupAlertDialogReduxProps => {
	if (!state.models.externalProviders) {
		throw new Error('No externalProviders stored in redux')
	}
	return {
		externalProviders: state.models.externalProviders
	}
}

export default connect(mapStateToProps)(CreateNonLtiGroupAlertDialog)
