import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { User } from '../types/User'
import { displayName } from '../utils/user'

export interface EntityOwnerListProps {
	owners: User[]
	canReadAnyUserGlobally: boolean
	displayAsInstructors?: boolean
}

export class EntityOwnerList extends Component<EntityOwnerListProps> {
	renderOwner = (owner: User, className: string) => {
		const { canReadAnyUserGlobally } = this.props
		const name = canReadAnyUserGlobally ? (
			<Link to={`/users/${owner.id}`} className={`ownerLink ${className}`}>
				<strong>{displayName(owner)}</strong>
			</Link>
		) : (
			<strong className={className}>{displayName(owner)}</strong>
		)
		return (
			<>
				{name} (<a href={`mailto:${owner.email}`}>{owner.email}</a>)
			</>
		)
	}

	render() {
		const { owners, displayAsInstructors } = this.props

		return (
			<div className="mb4">
				<h3>{displayAsInstructors ? 'Instructors' : 'Owners'}</h3>
				{owners.length > 1 ? (
					<ul>
						{owners.map((owner, i) => (
							<li key={i}>{this.renderOwner(owner, 'owner')}</li>
						))}
					</ul>
				) : (
					this.renderOwner(owners[0], 'ml3 owner')
				)}
			</div>
		)
	}
}
