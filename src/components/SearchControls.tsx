import Button from '@material-ui/core/Button'
import IconReset from '@material-ui/icons/Autorenew'
import IconSearch from '@material-ui/icons/Search'
import React, { FunctionComponent } from 'react'
import { FormCheck, FormControl, FormGroup, FormLabel, FormText } from 'react-bootstrap'
import { Search } from '../types'
import { AlertWithIcon } from './AlertWithIcon'
import { DateField } from './Forms/DateField'
import { SearchPersistorMethods } from './HOC/SearchPersistorComponent'

interface SearchControlsMethods
	extends Pick<SearchPersistorMethods, 'handleSearchClick' | 'setKeywords' | 'handleKeywordsKeyDown' | 'resetSearch'>,
		Partial<Pick<SearchPersistorMethods, 'setQueryAll'>> {}

export interface SearchControlsProps extends Search, SearchControlsMethods {
	keywordPlaceholder: string
	queryAllText?: string
	handleDateChange?: (dateString: string, date?: Date) => void
	canReadAnyGlobally?: boolean
	canReadSome?: boolean
	/** force Search button to be visible. otherwise only shows when `queryAll` is `true`. */
	showSearchButton?: boolean
}

export const SearchControls: FunctionComponent<SearchControlsProps> = ({
	keywords,
	invalidKeywords,
	dateString,
	queryAll,
	requiredMessage,
	setKeywords,
	handleKeywordsKeyDown,
	handleSearchClick,
	resetSearch,
	setQueryAll,
	keywordPlaceholder,
	handleDateChange,
	queryAllText,
	canReadAnyGlobally,
	canReadSome,
	showSearchButton
}) => {
	return (
		<>
			{!!requiredMessage && (
				<AlertWithIcon variant="danger" className="mb0" id="requiredMessageAlert">
					<p>{requiredMessage}</p>
				</AlertWithIcon>
			)}
			<FormGroup controlId="keywords" className="fl-ns mw5-ns mr3-ns mb3 mb0-ns w-100-lt-xs">
				<FormLabel className="f7 b">Keywords</FormLabel>
				<FormControl
					type="text"
					placeholder={keywordPlaceholder}
					name="keywords"
					onChange={setKeywords}
					onKeyDown={handleKeywordsKeyDown}
					value={keywords}
					isInvalid={invalidKeywords}
				/>
				{invalidKeywords && (
					<FormText className="i f6" id="invalidKeywords" muted>
						Please enter at least 3 characters.
					</FormText>
				)}
			</FormGroup>
			{!!handleDateChange && (
				<>
					<span id="and-or-label" className="i mw3-ns mr3-ns mt3-ns pv3-ns fl-ns tc w-100-lt-xs date-align">
						and / or
					</span>
					<DateField
						id="date"
						onChange={handleDateChange}
						label="Date"
						aria-label="Active date"
						className="fl-ns mw5-ns mr3-ns mb3 mb0-ns w-100-lt-xs date-align"
						value={dateString}
					/>
				</>
			)}
			<div className="fl-ns mt3-ns pt3-ns w-100-lt-xs">
				{canReadAnyGlobally && !!setQueryAll && (
					<FormCheck
						name="queryAll"
						id="queryAll"
						checked={queryAll}
						disabled={canReadAnyGlobally && !canReadSome}
						onChange={setQueryAll}
						className="fl-ns f6 mr2-ns mb3 mt2 w-100-lt-xs"
						label={<span className="b black">{queryAllText || 'Search All'}</span>}
					/>
				)}
				{(showSearchButton || (canReadAnyGlobally && queryAll)) && (
					<Button
						className="btn-primary fr-ns ml2-ns mb3 w-100-lt-xs search-button"
						onClick={handleSearchClick}
						color="primary"
						disabled={(!keywords || keywords.length < 3 || !!invalidKeywords) && !dateString}>
						<IconSearch />
						Search
					</Button>
				)}
				<Button className="btn-text color-pink fr-ns mb3 w-100-lt-xs" onClick={resetSearch}>
					<IconReset />
					Reset
				</Button>
			</div>
		</>
	)
}
