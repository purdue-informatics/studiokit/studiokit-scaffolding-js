import React, { FunctionComponent } from 'react'
import { UserInfo } from '../../types/User'
import { displayName } from '../../utils/user'
import { IconImpersonation } from '../Icons/IconImpersonation'

import './UserDetail.css'

export interface ImpersonationUserDetailProps {
	/** UserInfo of the current user who is being impersonated. */
	userInfo: UserInfo
	/**
	 * (Optional) className string to append additional classes to the <span> around the icon span and message.
	 */
	className?: string
	/** (Optional) icon to override the default "mustache" icon. */
	impersonationIcon?: JSX.Element
	/**
	 * (Optional) className string to append additional classes to the <span> around the icon
	 * e.g. to set the bg color and text color to be app specific.
	 */
	impersonationIconWrapperClassName?: string
}

/**
 * Renders the user's name along with an "Impersonating" icon and message.
 */
export const ImpersonationUserDetail: FunctionComponent<ImpersonationUserDetailProps> = ({
	userInfo,
	className,
	impersonationIcon,
	impersonationIconWrapperClassName
}) => {
	const Icon = impersonationIcon ?? <IconImpersonation />

	return (
		<span className={`ImpersonationUserDetail${className ? ` ${className}` : ''}`}>
			<span
				className={`ImpersonationUserDetail-IconWrapper${
					impersonationIconWrapperClassName ? ` ${impersonationIconWrapperClassName}` : ''
				}`}>
				{Icon} Impersonating
			</span>
			{displayName(userInfo)}
		</span>
	)
}
