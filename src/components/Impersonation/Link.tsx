import React, { FunctionComponent, useState } from 'react'
import { Link, LinkProps } from 'react-router-dom'
import { v4 as uuidv4 } from 'uuid'
import { startImpersonation } from '../../utils/user'
import { IconImpersonation } from '../Icons/IconImpersonation'

export interface ImpersonationLinkProps extends Omit<LinkProps, 'to'> {
	userId: string
	icon?: JSX.Element
}

export const ImpersonationLink: FunctionComponent<ImpersonationLinkProps> = ({ userId, icon, ...linkProps }) => {
	const [guid] = useState(uuidv4())

	const Icon = icon ?? <IconImpersonation className="mr2" />

	return (
		<Link to="" onClick={() => startImpersonation(guid, userId)} {...linkProps}>
			{Icon}
			Impersonate
		</Link>
	)
}
