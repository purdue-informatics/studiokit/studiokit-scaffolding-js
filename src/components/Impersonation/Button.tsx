import Button, { ButtonProps } from '@material-ui/core/Button'
import React, { FunctionComponent, useState } from 'react'
import { v4 as uuidv4 } from 'uuid'
import { startImpersonation } from '../../utils/user'
import { IconImpersonation } from '../Icons/IconImpersonation'

export interface ImpersonationButtonProps extends ButtonProps {
	userId: string
	icon?: JSX.Element
}

export const ImpersonationButton: FunctionComponent<ImpersonationButtonProps> = ({ userId, icon, ...buttonProps }) => {
	const [guid] = useState(uuidv4())

	const Icon = icon ?? <IconImpersonation />

	return (
		<Button className="btn-text" color="primary" onClick={() => startImpersonation(guid, userId)} {...buttonProps}>
			{Icon}
			Impersonate
		</Button>
	)
}
