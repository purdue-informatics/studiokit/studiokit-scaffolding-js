import Lock from '@material-ui/icons/Lock'
import React, { FunctionComponent, ReactNode, useState } from 'react'
import { NavDropdown } from 'react-bootstrap'
import { v4 as uuidv4 } from 'uuid'
import { dispatchAction } from '../../redux/actionCreator'
import { AUTH_ACTION_TYPE, AuthAction } from '../../redux/actions'
import { displayFirstName, stopImpersonation } from '../../utils/user'
import { IconStopImpersonating } from '../Icons/IconStopImpersonating'
import { ImpersonationUserDetail, ImpersonationUserDetailProps } from '../Impersonation/UserDetail'
import { ManagedNavDropdown } from './ManagedNavDropdown'

export interface UserDropdownProps extends ImpersonationUserDetailProps {
	children?: ReactNode
}

const doLogOut = () => {
	dispatchAction<AuthAction>({ type: AUTH_ACTION_TYPE.LOG_OUT_REQUESTED })
}

export const UserDropdown: FunctionComponent<UserDropdownProps> = ({ userInfo, children, ...props }) => {
	const [guid] = useState(uuidv4())

	const dropdownTitle: JSX.Element = userInfo.isImpersonated ? (
		<ImpersonationUserDetail userInfo={userInfo} {...props} />
	) : (
		<>Hi, {displayFirstName(userInfo)}</>
	)

	return (
		<ManagedNavDropdown
			id="user-menu"
			className={`user-menu${userInfo.isImpersonated ? ' user-menu-impersonated' : ''}`}
			title={dropdownTitle}>
			{children}
			{userInfo.isImpersonated && (
				<NavDropdown.Item onSelect={() => stopImpersonation(guid)}>
					<IconStopImpersonating className="menu-icon" />
					Stop Impersonating
				</NavDropdown.Item>
			)}
			<NavDropdown.Item onSelect={doLogOut}>
				<Lock className="menu-icon" />
				Log Out
			</NavDropdown.Item>
		</ManagedNavDropdown>
	)
}
