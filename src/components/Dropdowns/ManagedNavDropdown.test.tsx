import { shallow } from 'enzyme'
import React from 'react'
import { ManagedNavDropdown } from './ManagedNavDropdown'

const setup = (didToggle?: () => void) => {
	return shallow<ManagedNavDropdown>(<ManagedNavDropdown id="mock" title="mock" didToggle={didToggle} />)
}

describe('ManagedNavDropdown', () => {
	it("doesn't pass didToggle prop to child", () => {
		const wrapper = setup(jest.fn())
		expect(wrapper.props().didToggle).toBe(undefined)
	})
	it('Calls the didToggle prop when menu is toggled', () => {
		const didToggle = jest.fn()
		const wrapper = setup(didToggle)
		wrapper.instance().onToggle(true, {}, {})
		expect(didToggle).toHaveBeenCalledWith(true)
		wrapper.instance().onToggle(false, {}, {})
		expect(didToggle).toHaveBeenCalledWith(false)
	})
})
