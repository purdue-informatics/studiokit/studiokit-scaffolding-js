import React, { Component } from 'react'
import { NavDropdown, NavDropdownProps } from 'react-bootstrap'

enum EVENT_SOURCE {
	MOUSE_ENTER = 'mouseEnter',
	MOUSE_LEAVE = 'mouseLeave'
}

export interface ManagedNavDropdownProps {
	didToggle?: (isOpen: boolean) => void
	id?: string
	className?: string
}

interface State {
	isOpen: boolean
	lastSource: EVENT_SOURCE | Record<string, unknown> | undefined
	areMouseEventsEnabled: boolean
}

/**
 * @remarks
 * A Wrapper around NavDropdown that manages its own `open` status, adding mouse enter and mouse leave triggers, as well as a `didToggle` callback.
 */
export class ManagedNavDropdown extends Component<ManagedNavDropdownProps & NavDropdownProps, State> {
	state: State = {
		isOpen: false,
		lastSource: undefined,
		areMouseEventsEnabled: true
	}

	componentDidMount() {
		this.updateWindowDimensions()
		window.addEventListener('resize', this.updateWindowDimensions)
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateWindowDimensions)
	}

	updateWindowDimensions = () => {
		this.setState({ areMouseEventsEnabled: window.innerWidth >= 815 })
	}

	onMouseEnter = () => {
		if (!this.state.areMouseEventsEnabled) {
			return
		}
		this.setState({ isOpen: true, lastSource: EVENT_SOURCE.MOUSE_ENTER }, this.didToggle)
	}

	onMouseLeave = () => {
		if (!this.state.areMouseEventsEnabled) {
			return
		}
		this.setState({ isOpen: false, lastSource: EVENT_SOURCE.MOUSE_LEAVE }, this.didToggle)
	}

	// Source is an object with a source property, but we set it to be a string in our methods. If it's an object we know
	// the last change wasn't from our mouse hover event
	onToggle = (nextIsOpen: boolean, event: any, source: Record<string, unknown>) => {
		const { isOpen, lastSource } = this.state
		this.setState(
			{
				// do not toggle if we just opened the dropdown from a mouse event
				isOpen: lastSource === EVENT_SOURCE.MOUSE_ENTER && isOpen ? isOpen : nextIsOpen,
				lastSource: source
			},
			this.didToggle
		)
	}

	didToggle = () => {
		const { didToggle } = this.props
		const { isOpen } = this.state
		if (didToggle) {
			didToggle(isOpen)
		}
	}

	render() {
		const { isOpen } = this.state

		// prevent custom props from being passed to child
		const newProps: ManagedNavDropdownProps & NavDropdownProps = { ...this.props }
		delete newProps.didToggle

		return (
			<NavDropdown
				onToggle={this.onToggle}
				onMouseEnter={this.onMouseEnter}
				onMouseLeave={this.onMouseLeave}
				show={isOpen}
				alignRight
				{...newProps}
			/>
		)
	}
}
