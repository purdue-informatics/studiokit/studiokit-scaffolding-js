import Button from '@material-ui/core/Button'
import BugReport from '@material-ui/icons/BugReport'
import Refresh from '@material-ui/icons/Refresh'
import * as Sentry from '@sentry/react'
import React, { FunctionComponent, useCallback } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { useSelector } from 'react-redux'
import { BaseReduxState } from '../types'
import { displayName } from '../utils/user'

export interface ShowReportDialogButtonProps {
	eventId: string
	className?: string
}

export const ShowReportDialogButton: FunctionComponent<ShowReportDialogButtonProps> = ({ className, eventId }) => {
	const userInfo = useSelector((state: BaseReduxState) => {
		return state.models.user?.userInfo
	})

	const showDialog = useCallback(() => {
		const user = userInfo
			? { name: displayName(userInfo) || undefined, email: userInfo.email || undefined }
			: undefined
		Sentry.showReportDialog({
			eventId,
			user
		})
	}, [eventId, userInfo])

	return (
		<Button className={className} aria-label="Send Crash Report" onClick={showDialog}>
			<BugReport /> Send Crash Report
		</Button>
	)
}

export const ErrorBoundary: FunctionComponent<Sentry.ErrorBoundaryProps> = ({ children, ...props }) => (
	<Sentry.ErrorBoundary
		// set default props
		fallback={({ eventId, resetError }) => (
			<Container className="mt3">
				<Row>
					<Col md={8}>
						<h2>Oops! Something went wrong while processing your request.</h2>
						<p className="mb2">Please try reloading or refresh the page to try again.</p>
						<p className="mb0">
							<ShowReportDialogButton className="btn-primary" eventId={eventId} />
							<Button className="ml2 btn-primary" aria-label="Reload" onClick={resetError}>
								<Refresh /> Reload
							</Button>
						</p>
						<p>
							If the problem persists please contact us at{' '}
							<a href="mailto:tlt@purdue.edu">tlt@purdue.edu</a>.
						</p>
					</Col>
				</Row>
			</Container>
		)}
		// still allow caller to override all props
		{...props}>
		{children}
	</Sentry.ErrorBoundary>
)
