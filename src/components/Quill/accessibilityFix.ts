import { forEach, includes, isPlainObject, isString } from 'lodash'
import { Quill } from 'react-quill'
import { TABLE_ACTION } from './TableModule/constants'

/**
 * Apply aria-labels to the icons from the quill text editor
 * Imports the icons from Quill, recursively go through each icon and get the <svg /> html.
 * Then it sets a <label /> between the <svg />
 * Using css, it hides the label content.
 *
 * @param {*} icons The object that includes all the icons from Quill
 */
export function applyAccessibilityToIcons(icons: any) {
	forEach(icons, (value, key) => {
		// if the value is an object, recursively call this function
		if (isPlainObject(value)) {
			applyAccessibilityToIcons(value)
		}
		// if the value is a string, i.e, "<svg></svg>", then it adds the label
		if (isString(value)) {
			const regex = /^(.*) <\/svg>/g
			const match = regex.exec(value)
			if (!match) {
				return
			}
			let iconName
			if (key) {
				iconName = key
			} else {
				iconName = 'left'
			}
			const label = `<label class="hidden" aria-label="${iconName}">${iconName}</label>`
			icons[key] = match[1] + label + '</svg>'
		}
	})
}

/**
 * Grabs the DOM from the toolbar reference and loop through each button, picker, and dropdowns adding accessibility values
 * Help from: https://github.com/quilljs/quill/issues/1173
 *
 * @param toolbarRef: reference to the div containing our custom Quill toolbar
 * @param editor: reference to the Quill editor
 */
export function applyAccessibilityHacks(toolbarRef: React.RefObject<HTMLDivElement>, editor: Quill) {
	// Get ref to the toolbar, its not available through the quill api ughh
	if (!toolbarRef.current) {
		return
	}
	const toolbar = toolbarRef.current
	// apply aria labels to base buttons
	const buttons = toolbar.getElementsByTagName('button')

	for (const button of Array.from(buttons)) {
		const className = button.classList.value
			.split(' ')
			.filter(v => v.includes('ql-'))
			.map(v => v.replace('ql-', ''))[0]
		switch (className) {
			case 'bold':
				button.setAttribute('aria-label', 'Toggle bold text')
				break
			case 'italic':
				button.setAttribute('aria-label', 'Toggle italic text')
				break
			case 'underline':
				button.setAttribute('aria-label', 'Toggle underline text')
				break
			case 'script':
				if (button.value === 'sub') {
					button.setAttribute('aria-label', 'Toggle subscript')
				}
				if (button.value === 'super') {
					button.setAttribute('aria-label', 'Toggle superscript')
				}
				break
			case 'blockquote':
				button.setAttribute('aria-label', 'Toggle blockquote text')
				break
			case 'list':
				if (button.value === 'ordered') {
					button.setAttribute('aria-label', 'Toggle ordered list')
				}
				if (button.value === 'bullet') {
					button.setAttribute('aria-label', 'Toggle bulleted list')
				}
				break
			case 'header':
				if (button.value === '1') {
					button.setAttribute('aria-label', 'Toggle heading 1')
				}
				if (button.value === '2') {
					button.setAttribute('aria-label', 'Toggle heading 2')
				}
				break
			case 'strike':
				button.setAttribute('aria-label', 'Toggle strike text')
				break
			case 'code-block':
				button.setAttribute('aria-label', 'Toggle code block text')
				break
			case 'indent':
				if (button.value === '-1') {
					button.setAttribute('aria-label', 'Indent left')
				}
				if (button.value === '+1') {
					button.setAttribute('aria-label', 'Indent right')
				}
				break
			case 'link':
				button.setAttribute('aria-label', 'Insert link')
				break
			case 'image':
				button.setAttribute('aria-label', 'Insert image')
				break
			case 'video':
				button.setAttribute('aria-label', 'Insert video')
				break
			case 'formula':
				button.setAttribute('aria-label', 'Insert formula')
				break
			case 'clean':
				button.setAttribute('aria-label', 'Clear formatting')
				break
			case 'table':
				switch (button.value) {
					case TABLE_ACTION.INSERT_ROW:
						button.setAttribute('aria-label', 'Insert row')
						break
					case TABLE_ACTION.DELETE_ROW:
						button.setAttribute('aria-label', 'Delete row')
						break
					case TABLE_ACTION.INSERT_COL:
						button.setAttribute('aria-label', 'Insert column')
						break
					case TABLE_ACTION.DELETE_COL:
						button.setAttribute('aria-label', 'Delete column')
						break
				}
				break
			default:
				break
		}
		const ariaLabel = button.getAttribute('aria-label')
		if (ariaLabel) {
			button.setAttribute('title', ariaLabel)
		}

		// Put title tags to the dropdowns
		const dropdowns = toolbar.getElementsByTagName('select')
		for (const dropdown of Array.from(dropdowns)) {
			const classNameAttribute = dropdown.getAttribute('class')
			if (classNameAttribute) {
				dropdown.setAttribute('title', classNameAttribute.toLowerCase())
			}
		}

		// Make pickers work with keyboard and apply aria labels
		const pickers = toolbar.getElementsByClassName('ql-picker')
		for (const picker of Array.from(pickers)) {
			const label = picker.getElementsByClassName('ql-picker-label')[0]
			const optionsContainer = picker.getElementsByClassName('ql-picker-options')[0]
			const options = optionsContainer.getElementsByClassName('ql-picker-item')
			label.setAttribute('role', 'button')
			label.setAttribute('aria-haspopup', 'true')
			label.setAttribute('tabindex', '0')
			if (!!label.parentElement && label.parentElement.classList.contains('ql-font')) {
				label.setAttribute('title', 'Font face')
			}
			if (!!label.parentElement && label.parentElement.classList.contains('ql-size')) {
				label.setAttribute('title', 'Font size')
			}
			if (!!label.parentElement && label.parentElement.classList.contains('ql-align')) {
				label.setAttribute('title', 'Alignment')
			}
			if (!!label.parentElement && label.parentElement.classList.contains('ql-table')) {
				label.setAttribute('title', 'Insert table')
			}
			optionsContainer.setAttribute('aria-hidden', 'true')
			optionsContainer.setAttribute('aria-label', 'submenu')

			for (const item of Array.from(options)) {
				item.setAttribute('tabindex', '0')
				item.setAttribute('role', 'button')

				if (!includes(picker.classList, 'ql-align')) {
					// Read the css 'content' values and generate aria labels for font face/size
					const content = window.getComputedStyle(item, ':before').content
					if (content) {
						const contentLabels = content.split('"').join('')
						item.setAttribute('aria-label', contentLabels)
					}
				} else {
					// Add the title labels to the justification icons
					const iconLabels = item.getElementsByTagName('label')
					if (iconLabels.length === 1) {
						const iconLabel = iconLabels[0].getAttribute('aria-label')
						if (iconLabel) {
							const iconTitle = iconLabel.charAt(0).toUpperCase() + iconLabel.slice(1)
							item.setAttribute('title', iconTitle)
							item.setAttribute('aria-label', iconLabel)
						}
					}
				}

				item.addEventListener('keyup', (e: any) => {
					if (e.keyCode === 13) {
						;(item as HTMLElement).click()
						optionsContainer.setAttribute('aria-hidden', 'true')
						picker.classList.remove('ql-expanded')
						editor.focus()
					}
				})
			}

			label.addEventListener('keyup', (e: any) => {
				if (e.keyCode === 13) {
					;(label as HTMLElement).click()
					picker.classList.add('ql-expanded')
					optionsContainer.setAttribute('aria-hidden', 'false')
				}
			})
		}
	}

	const tooltips: HTMLCollectionOf<Element> = (editor as any).container.getElementsByClassName('ql-tooltip')
	for (const tooltip of Array.from(tooltips)) {
		tooltip.setAttribute('tabindex', '-1')
		const embedUrls = tooltip.getElementsByTagName('input')
		for (const embedUrl of Array.from(embedUrls)) {
			embedUrl.setAttribute('aria-label', 'Embed Url')
		}
		const previews = tooltip.getElementsByClassName('ql-preview')
		for (const preview of Array.from(previews)) {
			preview.setAttribute('aria-label', 'Preview')
			preview.setAttribute('tabindex', '0')
		}
		const actions = tooltip.getElementsByClassName('ql-action')
		const remove = tooltip.getElementsByClassName('ql-remove')
		const allActions = Array.from(actions).concat(Array.from(remove))
		for (const action of allActions) {
			action.setAttribute('tabindex', '0')
			action.setAttribute('role', 'button')
			action.addEventListener('keyup', (e: any) => {
				if (e.keyCode === 13) {
					;(action as HTMLElement).click()
				}
			})
		}
	}
}
