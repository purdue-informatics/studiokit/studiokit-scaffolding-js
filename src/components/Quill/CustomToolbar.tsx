import IconAddBox from '@material-ui/icons/AddBox'
import IconMoreHoriz from '@material-ui/icons/MoreHoriz'
import { isUndefined } from 'lodash'
import React, { Fragment, FunctionComponent, useEffect, useMemo, useState } from 'react'
import ReactQuill from 'react-quill'
import { IconAlphaList } from '../Icons/IconAlphaList'
import { mdiTable } from '../Icons/IconTable'
import { IconTableDeleteCol } from '../Icons/IconTableDeleteCol'
import { IconTableDeleteRow } from '../Icons/IconTableDeleteRow'
import { IconTableInsertCol } from '../Icons/IconTableInsertCol'
import { IconTableInsertRow } from '../Icons/IconTableInsertRow'
import { applyAccessibilityHacks } from './accessibilityFix'
import { TABLE_ACTION } from './TableModule/constants'

/** Options for Quill, used for defining options in the CustomToolbar, and for enabling formats in Quill */
export enum QUILL_OPTION {
	HEADER = 1 << 1,
	FONT = 1 << 2,
	SIZE = 1 << 3,
	BOLD = 1 << 4,
	ITALIC = 1 << 5,
	UNDERLINE = 1 << 6,
	/** NOTE: there is only a single "script" format, but there are separate options for the toolbar buttons */
	SCRIPT_SUB = 1 << 7,
	/** NOTE: there is only a single "script" format, but there are separate options for the toolbar buttons */
	SCRIPT_SUPER = 1 << 8,
	STRIKE_THROUGH = 1 << 9,
	BLOCKQUOTE = 1 << 10,
	CODE_BLOCK = 1 << 11,
	ALIGN = 1 << 12,
	/** NOTE: there is only a single "list" format, but there are separate options for the toolbar buttons */
	LIST_ORDERED = 1 << 13,
	/** NOTE: there is only a single "list" format, but there are separate options for the toolbar buttons */
	LIST_BULLET = 1 << 14,
	/** NOTE: there is only a single "list" format, but there are separate options for the toolbar buttons */
	LIST_ALPHA = 1 << 15,
	INDENT = 1 << 16,
	LINK = 1 << 17,
	IMAGE = 1 << 18,
	VIDEO = 1 << 19,
	FORMULA = 1 << 20,
	TABLE = 1 << 21,
	/** NOTE: variable support is not currently in scaffolding, just the toolbar option */
	VARIABLE = 1 << 22,
	/** NOTE: there is no "clean" format, this is only used to display the toolbar button */
	CLEAN = 1 << 23
}

const getNameForOption = (option: QUILL_OPTION): string => {
	switch (option) {
		case QUILL_OPTION.HEADER:
			return 'header'
		case QUILL_OPTION.FONT:
			return 'font'
		case QUILL_OPTION.SIZE:
			return 'size'
		case QUILL_OPTION.BOLD:
			return 'bold'
		case QUILL_OPTION.ITALIC:
			return 'italic'
		case QUILL_OPTION.UNDERLINE:
			return 'underline'
		case QUILL_OPTION.STRIKE_THROUGH:
			return 'strike'
		case QUILL_OPTION.BLOCKQUOTE:
			return 'blockquote'
		case QUILL_OPTION.CODE_BLOCK:
			return 'code-block'
		case QUILL_OPTION.ALIGN:
			return 'align'
		case QUILL_OPTION.INDENT:
			return 'indent'
		case QUILL_OPTION.LINK:
			return 'link'
		case QUILL_OPTION.IMAGE:
			return 'image'
		case QUILL_OPTION.VIDEO:
			return 'video'
		case QUILL_OPTION.FORMULA:
			return 'formula'
		case QUILL_OPTION.VARIABLE:
			return 'variable'
		case QUILL_OPTION.LIST_ORDERED:
			return 'ordered'
		case QUILL_OPTION.LIST_BULLET:
			return 'bullet'
		case QUILL_OPTION.LIST_ALPHA:
			return 'alpha'
		case QUILL_OPTION.SCRIPT_SUB:
			return 'sub'
		case QUILL_OPTION.SCRIPT_SUPER:
			return 'super'
		case QUILL_OPTION.TABLE:
			return 'table'
		case QUILL_OPTION.CLEAN:
			return 'clean'
		default:
			throw new Error(`Unsupported option: ${option}`)
	}
}

/** Returns the Quill format for a given option, which enables that given feature in Quill */
const getFormatsForOption = (option: QUILL_OPTION): string[] => {
	switch (option) {
		case QUILL_OPTION.HEADER:
		case QUILL_OPTION.FONT:
		case QUILL_OPTION.SIZE:
		case QUILL_OPTION.BOLD:
		case QUILL_OPTION.ITALIC:
		case QUILL_OPTION.UNDERLINE:
		case QUILL_OPTION.STRIKE_THROUGH:
		case QUILL_OPTION.BLOCKQUOTE:
		case QUILL_OPTION.CODE_BLOCK:
		case QUILL_OPTION.ALIGN:
		case QUILL_OPTION.INDENT:
		case QUILL_OPTION.LINK:
		case QUILL_OPTION.IMAGE:
		case QUILL_OPTION.VIDEO:
		case QUILL_OPTION.FORMULA:
		case QUILL_OPTION.VARIABLE:
			return [getNameForOption(option)]
		case QUILL_OPTION.LIST_ORDERED:
		case QUILL_OPTION.LIST_BULLET:
		case QUILL_OPTION.LIST_ALPHA:
			return ['list']
		case QUILL_OPTION.SCRIPT_SUB:
		case QUILL_OPTION.SCRIPT_SUPER:
			return ['script']
		case QUILL_OPTION.TABLE:
			return ['table-container', 'table', 'tbody', 'tr', 'td']
		case QUILL_OPTION.CLEAN:
			return []
		default:
			throw new Error(`Unsupported option: ${option}`)
	}
}

/** Flatten the nested section options for the toolbar into a flat array of options. */
export const getFormatsForOptions = (sectionOptionsArray: QUILL_OPTION[][]): string[] => {
	// flatten section options to array of options
	// map options to formats and flatten those formats
	// get distinct values using Set, then convert back to Array
	return Array.from(new Set(sectionOptionsArray.flat(2).flatMap(option => getFormatsForOption(option))))
}

export type ToolbarSection = (disabled?: boolean) => JSX.Element

const getToolbarSectionForOption = (option: QUILL_OPTION): ToolbarSection => {
	const optionName = getNameForOption(option)
	switch (option) {
		case QUILL_OPTION.HEADER:
			return (disabled?: boolean) => (
				<>
					<button className="ql-header" value="1" disabled={disabled} />
					<button className="ql-header" value="2" disabled={disabled} />
				</>
			)
		case QUILL_OPTION.FONT:
		case QUILL_OPTION.SIZE:
		case QUILL_OPTION.ALIGN:
			return (disabled?: boolean) => <select className={`ql-${optionName}`} disabled={disabled} />
		case QUILL_OPTION.BOLD:
		case QUILL_OPTION.ITALIC:
		case QUILL_OPTION.UNDERLINE:
		case QUILL_OPTION.STRIKE_THROUGH:
		case QUILL_OPTION.BLOCKQUOTE:
		case QUILL_OPTION.CODE_BLOCK:
		case QUILL_OPTION.LINK:
		case QUILL_OPTION.IMAGE:
		case QUILL_OPTION.VIDEO:
		case QUILL_OPTION.FORMULA:
		case QUILL_OPTION.CLEAN:
			return (disabled?: boolean) => <button className={`ql-${optionName}`} disabled={disabled} />
		case QUILL_OPTION.SCRIPT_SUB:
		case QUILL_OPTION.SCRIPT_SUPER:
			return (disabled?: boolean) => <button className="ql-script" value={optionName} disabled={disabled} />
		case QUILL_OPTION.LIST_ORDERED:
		case QUILL_OPTION.LIST_BULLET:
			return (disabled?: boolean) => <button className="ql-list" value={optionName} disabled={disabled} />
		case QUILL_OPTION.LIST_ALPHA:
			return (disabled?: boolean) => (
				<button
					className="ql-list"
					value={optionName}
					disabled={disabled}
					title="Toggle alpha list"
					aria-label="Toggle alpha list">
					<IconAlphaList style={{ width: 'auto' }} />
				</button>
			)
		case QUILL_OPTION.INDENT:
			return (disabled?: boolean) => (
				<>
					<button className="ql-indent" value="-1" disabled={disabled} />
					<button className="ql-indent" value="+1" disabled={disabled} />
				</>
			)
		case QUILL_OPTION.TABLE:
			return (disabled?: boolean) => (
				<>
					<select className="ql-table" disabled={disabled}>
						{Array.from(Array(5).keys()).flatMap(row => {
							return Array.from(Array(5).keys()).map(col => {
								return (
									<option
										key={`${row}-${col}`}
										value={`${TABLE_ACTION.NEW_TABLE}${row + 1}_${col + 1}`}
									/>
								)
							})
						})}
					</select>
					<button className="ql-table" value="insert-row" disabled={disabled}>
						<IconTableInsertRow style={{ width: 'auto' }} />
					</button>
					<button className="ql-table" value="delete-row" disabled={disabled}>
						<IconTableDeleteRow style={{ width: 'auto' }} />
					</button>
					<button className="ql-table" value="insert-col" disabled={disabled}>
						<IconTableInsertCol style={{ width: 'auto' }} />
					</button>
					<button className="ql-table" value="delete-col" disabled={disabled}>
						<IconTableDeleteCol style={{ width: 'auto' }} />
					</button>
				</>
			)
		case QUILL_OPTION.VARIABLE:
			return (disabled?: boolean) => (
				<button
					className={`ql-createVariable f6 fw5${disabled ? '' : ' color-primary'}`}
					style={{ width: '100%' }}
					disabled={disabled}
					type="button"
					aria-label="Insert variable"
					title="Insert variable">
					<IconAddBox className="ma0" />
					Variable
				</button>
			)
		default:
			throw new Error(`Unsupported option: ${option}`)
	}
}

const getToolbarSectionsForOptions = (sectionOptionsArray: QUILL_OPTION[][]): ToolbarSection[] => {
	return sectionOptionsArray.map((sectionOptions, sectionIndex) => {
		return (disabled?: boolean) => (
			<>
				{sectionOptions.map((sectionOption, optionIndex) => (
					<Fragment key={`${sectionIndex}-${optionIndex}`}>
						{getToolbarSectionForOption(sectionOption)(disabled)}
					</Fragment>
				))}
			</>
		)
	})
}

export interface CustomToolbarProps {
	guid: string
	/** An array of toolbar option arrays. Each top level array will equate to a toolbar section, with spacing between each section. */
	sectionOptions: QUILL_OPTION[][]
	/** (Optional) An array of toolbar option arrays. When provided, a "more options" button is provided and a secondary toolbar can be expanded and collapsed. */
	moreSectionOptions?: QUILL_OPTION[][]
	className?: string
	disabled?: boolean
	reactQuillRef: ReactQuill | null
	isVisibleWhenDisabled?: boolean
}

// Following this example: https://codepen.io/alexkrolick/pen/gmroPj?editors=0010
// All the options available here: https://github.com/quilljs/quill/blob/develop/docs/_includes/standalone/full.html
export const CustomToolbar: FunctionComponent<CustomToolbarProps> = props => {
	const {
		guid,
		sectionOptions,
		moreSectionOptions,
		className,
		disabled,
		reactQuillRef,
		isVisibleWhenDisabled
	} = props

	const [isInitialized, setIsInitialized] = useState(false)
	const [isShowingMoreOptions, setIsShowingMoreOptions] = useState(false)
	const ref = React.createRef<HTMLDivElement>()

	useEffect(() => {
		// initialize the toolbar after reactQuill exists and the buttons are displayed
		if (!isInitialized && reactQuillRef && (!disabled || isVisibleWhenDisabled)) {
			applyAccessibilityHacks(ref, reactQuillRef.getEditor())

			// manually modify the table picker select svg icon
			const label = ref.current?.querySelector('.ql-table .ql-picker-label')
			if (label) {
				const svg = label.childNodes[0] as SVGElement
				const polygons = svg.querySelectorAll('polygon')
				polygons.forEach(polygon => {
					polygon.remove()
				})
				svg.setAttribute('viewBox', '0 0 24 24')
				svg.classList.add('ql-fill')
				const path = svg.querySelector('path')
				if (!path) {
					const path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
					path.setAttribute('d', mdiTable)
					svg.appendChild(path)
				}
			}

			setIsInitialized(true)
		}
	}, [disabled, isInitialized, isVisibleWhenDisabled, reactQuillRef, ref])

	const toolbarSections = useMemo(() => getToolbarSectionsForOptions(sectionOptions), [sectionOptions])
	const hasMoreOptions = !isUndefined(moreSectionOptions)
	const moreToolbarSections = useMemo(
		() => (!isUndefined(moreSectionOptions) ? getToolbarSectionsForOptions(moreSectionOptions) : []),
		[moreSectionOptions]
	)

	return (
		<div
			id={`quill-toolbar-${guid}`}
			className={`${className ? ` ${className}` : ''}${disabled ? ' disabled' : ''}${
				!isVisibleWhenDisabled && disabled ? ' disabled-invisible' : ''
			}`}
			ref={ref}>
			{(!disabled || isVisibleWhenDisabled) && (
				<>
					{toolbarSections.map((section, i) => (
						<span className="ql-formats" key={i}>
							{section(disabled)}
						</span>
					))}
					{hasMoreOptions && (
						<button
							className={`more-options f6 fw5${isShowingMoreOptions ? ' expanded color-primary' : ''}`}
							type="button"
							title="More options"
							aria-expanded={isShowingMoreOptions}
							onClick={() => setIsShowingMoreOptions(!isShowingMoreOptions)}>
							<IconMoreHoriz className="ma0" />
						</button>
					)}
					{
						// This is a sub-div so it's contained in the div with the id/guid for the forward ref.
						// CSS handles appearance so it renders as if it were a sibling div.
						hasMoreOptions && (
							<div
								className={`more-options-toolbar ql-snow${
									isShowingMoreOptions ? ' expanded' : ' invisible'
								}`}
								aria-hidden={!isShowingMoreOptions}>
								{moreToolbarSections.map((section, i) => (
									<span className="ql-formats" key={i}>
										{section(disabled)}
									</span>
								))}
							</div>
						)
					}
				</>
			)}
		</div>
	)
}
