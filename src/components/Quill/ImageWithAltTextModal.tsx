import { Button, SvgIcon } from '@material-ui/core'
import IconHelp from '@material-ui/icons/Help'
import IconSwap from '@material-ui/icons/SwapVert'
import { isEmpty } from 'lodash'
import { DeltaStatic, RangeStatic } from 'quill'
import React, { FunctionComponent, useEffect, useState } from 'react'
import { FormControl, FormGroup, FormLabel, Modal, OverlayTrigger, Popover, Row } from 'react-bootstrap'
import ReactQuill from 'react-quill'
import { Quill } from 'react-quill'
import ConnectedModal from '../ConnectedModal'
import { ErrorMessage } from '../ErrorMessage'
import { ImageValue } from './Formats/Image'
import { ImageWarning } from './ImageWarning'

const Delta = Quill.import('delta')

const mdiUpload = 'M9,16V10H5L12,3L19,10H15V16H9M5,20V18H19V20H5Z'
const IconUpload = (
	<SvgIcon role="presentation">
		<path d={mdiUpload} />
	</SvgIcon>
)

interface ImageWithAltTextProps {
	isOpen: boolean
	imageToEdit?: ImageValue
	supportedImageExtensions: string[]
	onClose: () => void
	fileSelector: HTMLInputElement
	reactQuillRef: ReactQuill | null
	range: RangeStatic | undefined
}

export const ImageWithAltTextModal: FunctionComponent<ImageWithAltTextProps> = ({
	isOpen,
	onClose,
	imageToEdit,
	supportedImageExtensions,
	fileSelector,
	reactQuillRef,
	range
}) => {
	// setup state that is editable in the modal, based on the targeted image, if any
	const [alt, setAlt] = useState<string | undefined>(imageToEdit?.alt)
	const [src, setSrc] = useState<string | undefined>(imageToEdit?.src)
	const [nonPublic, setNonPublic] = useState<boolean | undefined>(imageToEdit?.nonPublic)
	const [isSelectedFileInvalid, setIsSelectedFileInvalid] = useState<boolean>(false)

	// update local state based on the targeted image, if any
	useEffect(() => {
		if (imageToEdit) {
			setAlt(imageToEdit.alt)
			setSrc(imageToEdit.src)
			setNonPublic(imageToEdit.nonPublic)
		}
	}, [imageToEdit])

	const onTryClose = () => {
		setAlt(undefined)
		setSrc(undefined)
		setNonPublic(undefined)
		// programmatically reset file selector
		fileSelector.value = ''
		onClose()
	}

	/** Use file selector to get an image */
	const onClickImageUpload = () => {
		// programmatically click to open the file upload dialog
		fileSelector.click()
		// when a file is selected
		fileSelector.onchange = function () {
			if (fileSelector.files && fileSelector.files?.length > 0) {
				const file = fileSelector.files[0]
				// convert filename to lowercase to make file extension check not case sensitive
				setIsSelectedFileInvalid(!supportedImageExtensions.some(e => file.name.toLocaleLowerCase().endsWith(e)))
				const reader = new FileReader()
				reader.onload = function (e) {
					if (e.target?.DONE) {
						// result is image in base64
						setSrc(e.target.result as string)
						if (!alt) setAlt(file.name)
					}
				}
				// get the file as a dataURI in base64
				reader.readAsDataURL(file)
				setNonPublic(false)
			}
		}
	}

	/** insert Image into quill */
	const insertImage = () => {
		if (reactQuillRef) {
			const quill = reactQuillRef.getEditor()
			const imageToInsert = { src, alt, nonPublic } as Partial<ImageValue>
			if (imageToEdit) {
				imageToInsert.height = imageToEdit.height
				imageToInsert.width = imageToEdit.width
			}

			const index = range ? range.index : 0
			const diff: DeltaStatic = new Delta()
			// retain up until the selected range
			diff.retain(index)
			// remove the existing image, if any
			if (imageToEdit) {
				diff.delete(1)
			}
			// insert the new image
			diff.insert({ image: imageToInsert })
			quill.updateContents(diff, 'user')
		}
		onTryClose()
	}

	return (
		<ConnectedModal show={isOpen} onHide={onTryClose}>
			<Modal.Header closeButton>
				<Modal.Title className="f4">{imageToEdit ? `Edit Image` : `Image Upload`}</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<p className="mb2 f7">* Indicates required field</p>
				{src && !isSelectedFileInvalid && (
					<Row>
						<img className="mb2 h5" src={src} alt={alt} />
					</Row>
				)}
				<div aria-live="polite" className="f7 mb2">
					{isSelectedFileInvalid && (
						<ErrorMessage message="The chosen file type is not supported. Please use one of the image formats listed below." />
					)}
				</div>
				{imageToEdit && !imageToEdit?.alt && !alt && (
					<ImageWarning text="This image is missing alt text. Please provide it below." />
				)}
				{imageToEdit?.nonPublic && nonPublic && (
					<ImageWarning
						isError
						text={
							<>
								<span>This image is not publicly available</span>
								<div className="mv2">
									Due to browser security issues, you'll need to upload this from your computer
									instead of linking from its original source. To fix this:
								</div>
								<ol className="mb1">
									<li>Right click on the image and save to your computer.</li>
									<li>Then use the 'Change Image' button below to upload the saved file.</li>
								</ol>
							</>
						}
					/>
				)}
				{(!imageToEdit || (imageToEdit.nonPublic && nonPublic)) && (
					<>
						<Button className="w-100-lt-xs pa2 mv1 btn-primary" onClick={onClickImageUpload}>
							{src ? <IconSwap /> : IconUpload}
							<span className="ml1">{src ? 'Change' : 'Upload'} Image</span>
						</Button>
						<p className="f7 mb3">Supported formats: .jpg, .png, .gif, .bmp</p>
					</>
				)}
				<FormGroup>
					<Row className="mb2">
						<h3 className="mv0">
							<FormLabel htmlFor="img-alt-text" className="mb0">
								Alt Text *
								<OverlayTrigger
									placement="top"
									trigger={['hover', 'focus']}
									overlay={
										<Popover id="alt-text-explanation">
											<p className="mb0">
												Used by screen readers to describe the content of an image.
											</p>
										</Popover>
									}>
									<button type="button" className="dib help-button bg-transparent">
										<IconHelp color="primary" titleAccess="Alt text explanation" />
									</button>
								</OverlayTrigger>
							</FormLabel>
						</h3>
					</Row>
					<FormControl
						id="img-alt-text"
						aria-label="Image Alt Text"
						as="textarea"
						onChange={(event: any) => {
							setAlt(event.target.value)
						}}
						value={alt}
						spellCheck={false}
						autoComplete="off"
						autoCorrect="off"
					/>
				</FormGroup>
			</Modal.Body>
			<Modal.Footer>
				<Button className="mr1 btn-text color-primary" onClick={onTryClose}>
					Cancel
				</Button>
				<Button
					className="btn-primary mb1"
					id="save-image-button"
					onClick={insertImage}
					disabled={isEmpty(alt?.trim()) || isEmpty(src) || isSelectedFileInvalid}>
					Save
				</Button>
			</Modal.Footer>
		</ConnectedModal>
	)
}
