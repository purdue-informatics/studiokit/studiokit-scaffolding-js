import BlotFormatter from 'quill-blot-formatter'
import { Quill } from 'react-quill'
import { applyAccessibilityToIcons } from './accessibilityFix'
import { Image } from './Formats/Image'
import { List, ListItem } from './Formats/List'
import { Video } from './Formats/Video'
import { ImageDropModule } from './ImageDropModule'
import { TableModule } from './TableModule'

export const registerQuill = () => {
	Quill.register('modules/blotFormatter', BlotFormatter)
	// replace image format
	Quill.register('formats/image', Image, true)
	// replace list format with new extended version
	Quill.register('formats/list/item', ListItem, true)
	Quill.register('formats/list', List, true)
	// replace image format
	Quill.register('formats/video', Video, true)
	Quill.register('modules/table', TableModule)
	Quill.register('modules/imageDrop', ImageDropModule)

	// apply the accessibility label issues to each icons
	applyAccessibilityToIcons(Quill.import('ui/icons'))
}

export const supportedFileExtensions = ['.png', '.jpeg', '.jpg', '.gif', '.bmp']

// Get the file dialog upload programmatically
export const buildFileSelector = () => {
	const fileSelector = document.createElement('input')
	fileSelector.setAttribute('type', 'file')
	fileSelector.setAttribute('accept', supportedFileExtensions.join(', '))
	return fileSelector
}
