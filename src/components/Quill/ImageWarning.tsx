import IconError from '@material-ui/icons/Error'
import IconWarning from '@material-ui/icons/Warning'
import React, { FunctionComponent, useMemo } from 'react'

export interface ImageWarningProps {
	text: string | JSX.Element
	className?: string
	isError?: boolean
}

export const ImageWarning: FunctionComponent<ImageWarningProps> = ({ text, className, isError }) =>
	useMemo(() => {
		return (
			<div className={`small pv1 mv1${className ? ` ${className}` : ''}`}>
				{isError ? <IconError className="fill-red nt1 mr1" /> : <IconWarning className="fill-orange nt1 mr1" />}
				{text}
			</div>
		)
	}, [className, text, isError])
