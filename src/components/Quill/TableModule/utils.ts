import { Blot } from 'parchment/dist/src/blot/abstract/blot'
import { RangeStatic } from 'quill'
import { Quill } from 'react-quill'
import { getLogger } from '../../../utils/logger'
import { TableCellBlot } from './Blots/TableCellBlot'
import { TABLE_BLOT_NAME } from './constants'

export const isInstanceOfBlot = (blot: Blot, blotName: string) => (blot as any).statics.blotName === blotName

export const isLineInTableCell = (line: Blot) => line.parent && isInstanceOfBlot(line.parent, TABLE_BLOT_NAME.TD)

export const isLineBeforeTable = (line: Blot) =>
	line.next && isInstanceOfBlot(line.next, TABLE_BLOT_NAME.TABLE_CONTAINER)

export const isLineAfterTable = (line: Blot) =>
	line.prev && isInstanceOfBlot(line.prev, TABLE_BLOT_NAME.TABLE_CONTAINER)

export const rangeRemovalWillDeleteCellWithoutRow = (quill: Quill, range: RangeStatic) => {
	const lines = quill.getLines(range)
	const willDeleteCellWithoutRow = lines.some((line: Blot) => {
		if (isLineInTableCell(line)) {
			const cell = line.parent as TableCellBlot
			const cellOffset = cell.offset(quill.scroll)
			const cellLength = cell.length()
			const willDeleteCell =
				// starts in cell and goes out of cell, or is exactly the cell
				(range.index >= cellOffset && range.index + range.length >= cellOffset + cellLength) ||
				// starts before cell and stops in cell
				(range.index < cellOffset && range.index + range.length < cellOffset + cellLength)
			const row = cell.row()
			if (!row) return false
			const rowOffset = row.offset(quill.scroll)
			const rowLength = row.length()
			const willDeleteRow = range.index <= rowOffset && range.index + range.length >= rowOffset + rowLength
			getLogger().debug('rangeRemovalWillDeleteCellWithoutRow line', {
				range,
				line,
				cellOffset,
				cellLength,
				willDeleteCell,
				willDeleteRow
			})
			return willDeleteCell && !willDeleteRow
		}
		return false
	}, {})
	return willDeleteCellWithoutRow
}

export function newId(prefix: string) {
	return `${prefix}-${Math.random().toString(36).slice(2, 6)}`
}
