export enum TABLE_BLOT_NAME {
	TABLE_CONTAINER = 'table-container',
	TABLE = 'table',
	TBODY = 'tbody',
	TR = 'tr',
	TD = 'td'
}

export enum TAG_NAME {
	DIV = 'DIV',
	TABLE = 'TABLE',
	TBODY = 'TBODY',
	TR = 'TR',
	TD = 'TD',
	P = 'P',
	BR = 'BR'
}

export enum TABLE_ACTION {
	NEW_TABLE = 'newtable_',
	INSERT_COL = 'insert-col',
	INSERT_ROW = 'insert-row',
	DELETE_COL = 'delete-col',
	DELETE_ROW = 'delete-row'
}

export enum ATTRIBUTE {
	ID = 'id',
	DATA_VALUE = 'data-value',
	TABLE = 'data-table',
	ROW = 'data-row',
	CELL = 'data-cell'
}

export enum KEY {
	BACKSPACE = 8,
	DELETE = 46
}
