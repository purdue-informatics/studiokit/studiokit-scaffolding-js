import { merge } from 'lodash'
import ParchmentType from 'parchment'
import { Blot } from 'parchment/dist/src/blot/abstract/blot'
import { ClipboardStatic, DeltaStatic, RangeStatic } from 'quill'
import { Quill } from 'react-quill'
import { getLogger } from '../../../utils/logger'
import { BaseTableBlot } from './Blots/BaseTableBlot'
import { TableBlot } from './Blots/TableBlot'
import { TableBodyBlot } from './Blots/TableBodyBlot'
import { TableCellBlot } from './Blots/TableCellBlot'
import { TableContainerBlot } from './Blots/TableContainer'
import { TableRowBlot } from './Blots/TableRowBlot'
import { ATTRIBUTE, KEY, TABLE_ACTION, TAG_NAME } from './constants'
import { isLineAfterTable, isLineBeforeTable, isLineInTableCell, rangeRemovalWillDeleteCellWithoutRow } from './utils'

const Delta = Quill.import('delta')
const Scroll = Quill.import('blots/scroll') as typeof ParchmentType.Scroll

export class TableModule {
	static register() {
		Quill.register(BaseTableBlot)
		Quill.register(TableCellBlot)
		Quill.register(TableRowBlot)
		Quill.register(TableBodyBlot)
		Quill.register(TableBlot)
		Quill.register(TableContainerBlot)
	}

	quill: Quill
	options: any
	tableToolbarControls: HTMLElement[] = []

	constructor(quill: Quill, options: any) {
		this.quill = quill
		this.options = options

		// Define Toolbar Handler
		const toolbar = quill.getModule('toolbar')
		if (toolbar) {
			toolbar.addHandler('table', (action: string) => {
				return this.tableHandler(action)
			})

			// by default, hide all table controls
			toolbar.controls.forEach((control: [string, HTMLElement]) => {
				const [name, domNode] = control
				if (name === 'table' && domNode.tagName === 'BUTTON') {
					this.tableToolbarControls.push(domNode)
					domNode.classList.add('dn')
				}
			})
		}

		this.quill.on('editor-change', this.showOrHideToolbarControls)
		this.quill.on('selection-change', this.showOrHideToolbarControls)

		// Define Clipboard Matchers
		// When HTML is loaded or pasted into Quill, the matchers
		// will find the given HTML Tag and allow the delta for the element's
		// content to be modified.
		const clipboard = quill.getModule('clipboard') as ClipboardStatic
		if (clipboard) {
			clipboard.addMatcher(TAG_NAME.TD, function (node: HTMLTableCellElement, delta: DeltaStatic) {
				getLogger().debug(`${TAG_NAME.TD} matched`, { node, delta })

				// load the table cell attribute to use as the delta operation attributes
				const dataValue = node.getAttribute(ATTRIBUTE.DATA_VALUE)
				const defaultAttributes = {
					td: dataValue
				}

				// handle empty cells, insert a single block / paragraph
				if (delta.length() === 0) {
					const updatedDelta = (new Delta() as DeltaStatic).insert('\n', defaultAttributes)
					getLogger().debug(`${TAG_NAME.TD} matched - final delta (empty cell)`, updatedDelta)
					return updatedDelta
				}

				// for each operation inside the cell's delta, add the table cell attributes
				const updatedDelta = delta.reduce((newDelta, op) => {
					newDelta.insert(op.insert, merge({}, op.attributes, defaultAttributes))
					return newDelta
				}, new Delta() as DeltaStatic)

				getLogger().debug(`${TAG_NAME.TD} matched - final delta`, updatedDelta)

				return updatedDelta
			})
		}

		// Override "backspace" and "delete" actions that would delete or combine a table cell
		const keyboard = quill.getModule('keyboard')
		if (keyboard) {
			keyboard.addBinding(
				{ key: KEY.BACKSPACE, altKey: null, ctrlKey: null, metaKey: null, shiftKey: null },
				{ collapsed: true, offset: 0 },
				this.handleBackspace
			)
			// move binding to first position
			const backspaceOtherBinding = keyboard.bindings[KEY.BACKSPACE].pop()
			keyboard.bindings[KEY.BACKSPACE].splice(0, 0, backspaceOtherBinding)

			keyboard.addBinding({ key: KEY.BACKSPACE }, { collapsed: false }, this.handleDeleteRange)
			// move binding to first position
			const backspaceRangeBinding = keyboard.bindings[KEY.BACKSPACE].pop()
			keyboard.bindings[KEY.BACKSPACE].splice(0, 0, backspaceRangeBinding)

			keyboard.addBinding({ key: KEY.BACKSPACE }, { collapsed: true }, this.handleBackspace)
			// move binding to first position
			const backspaceBinding = keyboard.bindings[KEY.BACKSPACE].pop()
			keyboard.bindings[KEY.BACKSPACE].splice(0, 0, backspaceBinding)

			keyboard.addBinding({ key: KEY.DELETE }, { collapsed: false }, this.handleDeleteRange)
			// move binding to first position
			const deleteRangeBinding = keyboard.bindings[KEY.DELETE].pop()
			keyboard.bindings[KEY.DELETE].splice(0, 0, deleteRangeBinding)

			keyboard.addBinding({ key: KEY.DELETE }, { collapsed: true, altKey: true }, this.handleDelete)
			// move binding to first position
			const deleteWithAltBinding = keyboard.bindings[KEY.DELETE].pop()
			keyboard.bindings[KEY.DELETE].splice(0, 0, deleteWithAltBinding)

			keyboard.addBinding({ key: KEY.DELETE }, { collapsed: true }, this.handleDelete)
			// move binding to first position
			const deleteBinding = keyboard.bindings[KEY.DELETE].pop()
			keyboard.bindings[KEY.DELETE].splice(0, 0, deleteBinding)
		}
	}

	//#region Helpers

	showOrHideToolbarControls = () => {
		const selectedCell = this.getSelectedCell()
		if (selectedCell) {
			this.tableToolbarControls.forEach(domNode => {
				domNode.classList.remove('dn')
			})
		} else {
			this.tableToolbarControls.forEach(domNode => {
				domNode.classList.add('dn')
			})
		}
	}

	handleBackspace = (range: RangeStatic, context: any) => {
		if (range.index === 0 || this.quill.getLength() <= 1) return true
		const [line]: [Blot, number] = this.quill.getLine(range.index)
		if (context.offset === 0 && isLineAfterTable(line)) {
			getLogger().debug('prevent backspace after table', { range, context, line })
			return false
		}
		// if inside a TableCellBlot and at the start of the cell
		// prevent "backspace" from removing previous cell
		// or items before the table if in the first cell
		if (
			// in a cell
			isLineInTableCell(line) &&
			// at the start of the cell
			context.offset === 0 &&
			!line.prev &&
			// there is a cell before this one in the row
			((line.parent.prev && line.parent.prev instanceof TableCellBlot) ||
				// or the cell is at the start of a row
				!line.parent.prev)
		) {
			getLogger().debug('prevent cell backspace', { range, context, line })
			return false
		}
		return true
	}

	handleDelete = (range: RangeStatic, context: any) => {
		const length = /^[\uD800-\uDBFF][\uDC00-\uDFFF]/.test(context.suffix) ? 2 : 1
		if (range.index >= this.quill.getLength() - length) return true
		const [line]: [Blot, number] = this.quill.getLine(range.index)
		if (context.offset === line.length() - 1 && isLineBeforeTable(line)) {
			getLogger().debug('prevent delete before table', { range, context, line })
			return false
		}
		// if inside a TableCellBlot and at the end of the cell
		// prevent "delete" from removing the next cell
		// or items after the table if in the last cell
		if (
			// in a cell
			isLineInTableCell(line) &&
			// at the end of the cell
			!line.next &&
			context.offset === line.length() - 1 &&
			// there is a cell after this one in the row
			((line.parent.next && line.parent.next instanceof TableCellBlot) ||
				// or the cell is at the start of a row
				!line.parent.next)
		) {
			getLogger().debug('prevent cell delete', { range, context, line })
			return false
		}
		return true
	}

	handleDeleteRange = (range: RangeStatic) => {
		const lines = this.quill.getLines(range)
		getLogger().debug('handleDeleteRange', { range, lines })
		const willDeleteCellWithoutRow = rangeRemovalWillDeleteCellWithoutRow(this.quill, range)
		if (willDeleteCellWithoutRow) {
			getLogger().debug('prevent delete cell out of row')
		}
		return !willDeleteCellWithoutRow
	}

	getSelectedCell() {
		const selection = this.quill.getSelection()
		if (!selection) {
			return null
		}
		const leaf = this.quill.getLeaf(selection.index)
		let blot: Blot | null = leaf[0]
		while (blot && !(blot instanceof TableCellBlot) && blot.parent) {
			blot = blot.parent
		}
		if (blot instanceof TableCellBlot) {
			return blot
		}
		return null
	}

	getSelectedTable() {
		return this.getSelectedCell()?.table()
	}

	//#endregion Helpers

	//#region Table Handler

	tableHandler(action: string) {
		getLogger().debug('tableHandler', action)

		// require a selection range
		const range = this.quill.getSelection()
		if (!range) return

		if (action.includes(TABLE_ACTION.NEW_TABLE)) {
			this.addNewTable(action, range)
			return
		}

		// require a selected cell
		const selectedCell = this.getSelectedCell()
		if (!selectedCell) return

		switch (action) {
			case TABLE_ACTION.INSERT_COL:
				this.insertCol(selectedCell, range)
				break
			case TABLE_ACTION.INSERT_ROW:
				this.insertRow(selectedCell, range)
				break
			case TABLE_ACTION.DELETE_COL:
				this.deleteCol(selectedCell)
				break
			case TABLE_ACTION.DELETE_ROW:
				this.deleteRow(selectedCell)
				break
		}
	}

	addNewTable(action: string, range: RangeStatic) {
		const { index } = range

		// e.g. "new-table-2-2"
		const sizes = action.replace(TABLE_ACTION.NEW_TABLE, '').split('_')
		const rows = parseInt(sizes[0])
		const cols = parseInt(sizes[1])
		const tableContainer = TableContainerBlot.createWithChildren(rows, cols)

		// insert the table into the top level scroll element
		// before the current leaf's top level branch
		// PREVENT NESTED TABLES
		let topBranch = null
		const leaf = this.quill.getLeaf(index)
		let blot: Blot | null = leaf[0]
		while (blot && blot.parent && !(blot instanceof Scroll)) {
			topBranch = blot
			blot = blot.parent
		}

		if (!blot || !(blot instanceof Scroll)) return

		blot.insertBefore(tableContainer, topBranch || undefined)
		this.quill.setSelection(tableContainer.offset(this.quill.scroll), 0, 'user')
	}

	insertCol(selectedCell: TableCellBlot, range: RangeStatic) {
		const selectedCellColumn = selectedCell.column()
		const tableId = selectedCell.tableId()
		const tableBody = selectedCell.row()?.tableBody()
		if (!tableBody) return
		tableBody.children.forEach(child => {
			const tableRow = child as TableRowBlot
			const rowId = tableRow.id()
			const tableCell = TableCellBlot.createWithChildren(tableId, rowId)
			const cells = tableRow.children.map(c => c)
			const nextColumnCell = cells.length > selectedCellColumn - 1 ? cells[selectedCellColumn] : undefined
			if (nextColumnCell) {
				tableRow.insertBefore(tableCell, nextColumnCell)
			} else {
				tableRow.appendChild(tableCell)
			}
		})
	}

	insertRow(selectedCell: TableCellBlot, range: RangeStatic) {
		const currentRow = selectedCell.row()
		if (!currentRow) return
		const nextRow = currentRow.next
		const tableBody = currentRow.tableBody()
		if (!tableBody) return
		const newRow = TableRowBlot.createFromReference(currentRow)
		if (nextRow) {
			tableBody.insertBefore(newRow, nextRow)
		} else {
			tableBody.appendChild(newRow)
		}
	}

	deleteCol(selectedCell: TableCellBlot) {
		const selectedCellColumn = selectedCell.column()
		const tableBody = selectedCell.row()?.tableBody()
		if (!tableBody) return
		tableBody.children.forEach(c => {
			const tableRow = c as TableRowBlot
			const cells = tableRow.children.map(c => c)
			const cell = cells[selectedCellColumn - 1]
			cell?.remove()
		})
	}

	deleteRow(selectedCell: TableCellBlot) {
		const selectedRow = selectedCell.row()
		if (!selectedRow) return
		const cellOffset = selectedCell.offset() // offset inside row
		const tableBody = selectedRow.tableBody()
		const rowToSelectAfter = selectedRow.next || selectedRow.prev
		// delete the row
		selectedRow.remove()
		// focus the next row or prev row if possible
		if (tableBody && tableBody.children.length > 0 && rowToSelectAfter) {
			const index = rowToSelectAfter.offset(this.quill.scroll)
			this.quill.setSelection(index + cellOffset, 0)
		}
	}

	//#endregion Table Handler
}
