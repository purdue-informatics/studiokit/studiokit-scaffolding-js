import ParchmentType from 'parchment'
import { Blot, Parent } from 'parchment/dist/src/blot/abstract/blot'
import BlockBlot from 'parchment/dist/src/blot/block'
import { Quill } from 'react-quill'
import { getLogger } from '../../../../utils/logger'
import { ATTRIBUTE, TABLE_BLOT_NAME, TAG_NAME } from '../constants'
import { isInstanceOfBlot, newId } from '../utils'
import { BaseTableBlot } from './BaseTableBlot'
import { TableRowBlot } from './TableRowBlot'

const Parchment = Quill.import('parchment') as typeof ParchmentType
const Container = Quill.import('blots/container') as typeof ParchmentType.Container
const Block = Quill.import('blots/block') as typeof ParchmentType.Block
const BlockEmbed = Quill.import('blots/block/embed') as typeof ParchmentType.Embed
const Break = Quill.import('blots/break') as typeof ParchmentType.Embed

export class TableCellBlot extends BaseTableBlot {
	static blotName = TABLE_BLOT_NAME.TD
	static tagName = TAG_NAME.TD
	static scope = Parchment.Scope.BLOCK_BLOT
	static requiredParentName: string | null = TABLE_BLOT_NAME.TR
	static defaultChildName = Block.blotName
	/** use normal `allowedChildren` instead of `allowedChildrenNames` */
	static allowedChildren = [Block, BlockEmbed, Container]

	static create(value: string) {
		const node = super.createWithValue(TAG_NAME.TD, value)

		// split data-value into separate attributes for easier access
		const values = value.split('|')
		node.setAttribute(ATTRIBUTE.TABLE, values[0])
		node.setAttribute(ATTRIBUTE.ROW, values[1])
		node.setAttribute(ATTRIBUTE.CELL, values[2])
		node.setAttribute(ATTRIBUTE.ID, values[2])

		return node
	}

	static createWithChildren(tableId: string, rowId: string) {
		const id = newId(this.blotName)
		const tableCell = Parchment.create(this.blotName, `${tableId}|${rowId}|${id}`) as TableCellBlot
		const p = Parchment.create(Block.blotName) as Parent
		tableCell.appendChild(p)
		const br = Parchment.create(Break.blotName)
		p.appendChild(br)
		return tableCell
	}

	formats() {
		// We don't inherit from FormatBlot
		return {
			[this.statics.blotName]: `${this.tableId()}|${this.rowId()}|${this.id()}`
		}
	}

	format(_name: string, _value: any) {
		// don't do anything
		// our "td" value does not do anything beyond making the <td> tag
	}

	optimize(context: { [key: string]: any }) {
		super.optimize(context)

		const didRemove = this.optimizeRemoveIfEmpty()
		if (didRemove) return

		this.optimizeMergeWithNext()

		let firstChild = this.children.head
		while (firstChild instanceof TableCellBlot) {
			getLogger().debug(
				`${this.statics.tagName} optimize: head child is TD. insert into parent before this, then optimize`
			)
			this.parent.insertBefore(firstChild, this)
			firstChild.optimize({})
			firstChild = this.children.head
		}

		let lastChild = this.children.tail
		while (lastChild instanceof TableCellBlot) {
			getLogger().debug(
				`${this.statics.tagName} optimize: tail child is TD. insert into parent after this, then optimize`
			)
			this.parent.insertBefore(lastChild, this.next)
			lastChild.optimize({})
			lastChild = this.children.tail
		}

		if (this.parent && !isInstanceOfBlot(this.parent, this.statics.requiredParentName)) {
			getLogger().debug(`${this.statics.tagName} optimize: parent is not ${this.statics.requiredParentName}`, {
				this: this,
				context,
				parent: this.parent,
				prev: this.prev,
				next: this.next
			})

			if (this.parent instanceof TableCellBlot && this.parent.dataValue() === this.dataValue()) {
				getLogger().debug(`${this.statics.tagName} optimize: parent is matching TD. merge this into parent.`)
				this.moveChildren(this.parent, this.next)
				this.remove()
			} else if (this.parent instanceof TableCellBlot) {
				getLogger().debug(`${this.statics.tagName} optimize: parent is TD. optimize parent TD.`)
				this.parent.optimize(context)
			} else {
				const newRow = Parchment.create(
					this.statics.requiredParentName,
					`${this.tableId()}|${this.rowId()}`
				) as TableRowBlot

				getLogger().debug(`${this.statics.tagName} optimize: wrap in new TR.`, {
					this: this,
					newRow,
					before: this.next
				})

				this.parent.insertBefore(newRow, this.next)
				this.parent.removeChild(this)
				newRow.appendChild(this)
			}
		}
	}

	insertBefore(child: Blot, refNode?: Blot) {
		// does not inherit from super, uses allowedChildren instead of allowedChildrenNames

		getLogger().debug(`${this.statics.tagName} insertBefore: ${(child as any).statics.tagName}`, {
			this: this,
			child,
			ref: refNode
		})

		if (child instanceof TableCellBlot) {
			getLogger().debug(`${this.statics.tagName} insertBefore: inserting cells, copy contents`, {
				this: this,
				child,
				ref: refNode
			})
			child.moveChildren(this, refNode)
			child.remove()
			return
		}

		if (
			this.statics.allowedChildren != null &&
			!this.statics.allowedChildren.some(function (allowedChild: any) {
				return child instanceof allowedChild
			})
		) {
			const newChild = this.createDefaultChild()
			getLogger().debug(`${this.statics.tagName} insertBefore: createDefaultChild and append blot`, {
				this: this,
				child,
				newChild,
				ref: refNode
			})
			newChild.appendChild(child)
			newChild.insertInto(this, refNode)
		} else {
			child.insertInto(this, refNode)
		}
	}

	createDefaultChild() {
		return Parchment.create(this.statics.defaultChildName) as BlockBlot
	}

	replace(target: Blot): void {
		getLogger().debug(`${this.statics.tagName} replace: ${(target as any).statics.tagName}`, {
			this: this,
			target
		})
		if (target instanceof Block) {
			const newChild = target.clone() as BlockBlot
			target.moveChildren(newChild)
			this.appendChild(newChild)
		}
		super.replace(target)
	}

	formatAt(index: number, length: number, name: string, value: any): void {
		getLogger().debug(`TD formatAt`, {
			this: this,
			childrenLength: this.children.length,
			dataValue: this.dataValue(),
			index,
			length,
			name,
			value
		})

		// make a local copy of the "children.forEachAt" params, incase the cell is "unwrapped"
		const childFormatAtParams: [Blot, number, number][] = []
		this.children.forEachAt(index, length, (child, offset, length) => {
			childFormatAtParams.push([child, offset, length])
		})

		if (name === this.statics.blotName && (!value || value !== this.dataValue())) {
			const tableContainer = this.table()?.tableContainer()
			if (tableContainer) {
				getLogger().debug(`TD formatAt: cols or rows added were removed, deconstruct table to flatten cells`)
				tableContainer.unwrap()
			}

			if (value && index === 0 && length >= this.children.length) {
				getLogger().debug(`TD formatAt: replace with new cell, do not format children`)
				this.replaceWith(name, value) as TableCellBlot
				return
			}

			if (!value && this.statics.containsOnlyEmptyBlock(this)) {
				getLogger().debug(`TD formatAt: no value, unwrap, format children`)
				this.unwrap()
			}
		}

		childFormatAtParams.forEach(params => {
			const [child, offset, length] = params
			getLogger().debug(`TD formatAt child`, {
				this: this,
				child,
				offset,
				length,
				name,
				value
			})
			child.formatAt(offset, length, name, value)
		})
	}

	updateDataValue() {
		this.domNode.setAttribute(ATTRIBUTE.DATA_VALUE, this.formats()[this.statics.blotName])
	}

	id() {
		return this.domNode.getAttribute(ATTRIBUTE.ID) as string
	}

	tableId() {
		return this.domNode.getAttribute(ATTRIBUTE.TABLE) as string
	}

	rowId() {
		return this.domNode.getAttribute(ATTRIBUTE.ROW) as string
	}

	row() {
		// td -> tr
		return this.parent instanceof TableRowBlot ? this.parent : undefined
	}

	table() {
		return this.row()?.table()
	}

	column() {
		const row = this.row()
		if (!row) {
			throw new Error('row is required to know column')
		}
		const index = row.children.map(c => c).indexOf(this)
		return index + 1
	}
}
