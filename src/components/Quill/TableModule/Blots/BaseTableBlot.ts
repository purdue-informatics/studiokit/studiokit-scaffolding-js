import ParchmentType from 'parchment'
import { Blot, Parent } from 'parchment/dist/src/blot/abstract/blot'
import { Quill } from 'react-quill'
import { getLogger } from '../../../../utils/logger'
import { ATTRIBUTE, TAG_NAME } from '../constants'
import { isInstanceOfBlot } from '../utils'

const Parchment = Quill.import('parchment') as typeof ParchmentType
const Container = Quill.import('blots/container') as typeof ParchmentType.Container

/**
 * A base class for all Table module blots to inherit.
 *
 * Created with a `value` that is stored on the DOM Node as the `data-value` attribute.
 * This is used for optimization (merging related blots).
 */
export class BaseTableBlot extends Container {
	static scope = Parchment.Scope.BLOCK_BLOT
	static requiredParentName: string | null = null
	/** alternate to `allowedChildren`, to avoid circular class refs */
	static allowedChildrenNames: string[] = []

	static createWithValue(tagName: string, value: string) {
		const node = super.create(tagName) as HTMLElement
		node.setAttribute(ATTRIBUTE.DATA_VALUE, value)
		getLogger().debug(`${tagName} created`, { value, node })
		return node
	}

	/**
	 * Check if the blot only contains `<p><br></p>`
	 */
	static containsOnlyEmptyBlock(blot: BaseTableBlot) {
		return (
			blot.length() === 1 &&
			blot.children.head &&
			(blot.children.head.domNode as HTMLElement).tagName === TAG_NAME.P &&
			blot.children.head.length() === 1 &&
			((blot.children.head as Parent).children.head?.domNode as HTMLElement).tagName === TAG_NAME.BR
		)
	}

	insertBefore(childBlot: Blot, refBlot?: Blot): void {
		// override super.insertBefore to change `allowedChildren` to use blotName strings, instead of class constructors
		if (
			this.statics.allowedChildrenNames != null &&
			!this.statics.allowedChildrenNames.some(function (allowedBlotName: string) {
				return (childBlot as any).statics.blotName === allowedBlotName
			})
		) {
			throw new Error(`Cannot insert ${(childBlot as any).statics.blotName} into ${this.statics.blotName}`)
		}
		childBlot.insertInto(this, refBlot)
	}

	optimize(_context: { [key: string]: any }): void {
		// @ts-ignore: copied from Parchment.ShadowBlot
		if (this.domNode['__blot'] != null) {
			// @ts-ignore: copied from Parchment.ShadowBlot
			delete this.domNode['__blot'].mutations
		}

		// do not inherit from Container, allow each child to determine optimization
	}

	unwrap(): void {
		super.unwrap()
		getLogger().debug(`${this.statics.tagName} unwrap`)
	}

	optimizeRemoveIfEmpty() {
		if (this.children.length === 0) {
			this.remove()
			return true
		}
		return false
	}

	optimizeMergeWithNext() {
		const next = this.next as Parent
		if (
			next != null &&
			next.prev === this &&
			isInstanceOfBlot(next, this.statics.blotName) &&
			next.domNode.tagName === this.domNode.tagName &&
			next.domNode.getAttribute(ATTRIBUTE.DATA_VALUE) === this.dataValue()
		) {
			getLogger().debug(`${this.statics.tagName} optimize: next is matching blot. merge next into this.`, {
				this: this,
				parent: this.parent,
				prev: this.prev,
				next: this.next
			})
			next.moveChildren(this)
			next.remove()
			return true
		}
		return false
	}

	optimizeWrapInRequiredParent(value: string) {
		if (
			this.parent &&
			this.statics.requiredParentName !== null &&
			!isInstanceOfBlot(this.parent, this.statics.requiredParentName)
		) {
			getLogger().debug(
				`${this.statics.tagName} optimize: parent not ${this.statics.requiredParentName}. wrap in new ${this.statics.requiredParentName}.`,
				{
					this: this,
					parent: this.parent,
					prev: this.prev,
					next: this.next
				}
			)
			const newParent = Parchment.create(this.statics.requiredParentName, value) as Parent
			this.parent.removeChild(this)
			this.parent.insertBefore(newParent, this.next)
			newParent.appendChild(this)
			return true
		}
		return false
	}

	dataValue() {
		return this.domNode.getAttribute(ATTRIBUTE.DATA_VALUE) as string
	}
}
