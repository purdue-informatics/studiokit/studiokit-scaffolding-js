import ParchmentType from 'parchment'
import { Quill } from 'react-quill'
import { ATTRIBUTE, TABLE_BLOT_NAME, TAG_NAME } from '../constants'
import { BaseTableBlot } from './BaseTableBlot'
import { TableBlot } from './TableBlot'
import { TableRowBlot } from './TableRowBlot'

const Parchment = Quill.import('parchment') as typeof ParchmentType

export class TableBodyBlot extends BaseTableBlot {
	static blotName = TABLE_BLOT_NAME.TBODY
	static tagName = TAG_NAME.TBODY
	static scope = Parchment.Scope.BLOCK_BLOT
	static requiredParentName: string | null = TABLE_BLOT_NAME.TABLE
	static allowedChildrenNames: string[] = [TABLE_BLOT_NAME.TR]

	static create(value: string) {
		const node = super.createWithValue(TAG_NAME.TBODY, value)
		return node
	}

	static createWithChildren(id: string, rows: number, cols: number) {
		const tableBody = Parchment.create(this.blotName, id) as TableBodyBlot
		for (let row = 0; row < rows; row++) {
			const tableRow = TableRowBlot.createWithChildren(id, cols)
			tableBody.appendChild(tableRow)
		}
		return tableBody
	}

	optimize(context: { [key: string]: any }): void {
		super.optimize(context)

		const didRemove = this.optimizeRemoveIfEmpty()
		if (didRemove) return

		this.optimizeMergeWithNext()
		this.optimizeWrapInRequiredParent(this.tableId())
	}

	unwrap(): void {
		// keep a reference to the rows, since `super.unwrap()` will break the ref
		const rows = this.children.map(r => r)
		super.unwrap()
		rows.forEach(c => {
			const row = c as TableRowBlot
			row.unwrap()
		})
	}

	tableId() {
		return this.domNode.getAttribute(ATTRIBUTE.DATA_VALUE) as string
	}

	table() {
		// tbody -> table
		return this.parent instanceof TableBlot ? this.parent : undefined
	}
}
