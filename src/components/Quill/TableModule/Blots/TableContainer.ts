import ParchmentType from 'parchment'
import { Quill } from 'react-quill'
import { getLogger } from '../../../../utils/logger'
import { TABLE_BLOT_NAME, TAG_NAME } from '../constants'
import { newId } from '../utils'
import { BaseTableBlot } from './BaseTableBlot'
import { TableBlot } from './TableBlot'
import { TableBodyBlot } from './TableBodyBlot'
import { TableCellBlot } from './TableCellBlot'
import { TableRowBlot } from './TableRowBlot'

const Parchment = Quill.import('parchment') as typeof ParchmentType
const Block = Quill.import('blots/block') as typeof ParchmentType.Block

export class TableContainerBlot extends BaseTableBlot {
	static blotName = TABLE_BLOT_NAME.TABLE_CONTAINER
	static tagName = TAG_NAME.DIV
	static scope = Parchment.Scope.BLOCK_BLOT
	static allowedChildrenNames: string[] = [TABLE_BLOT_NAME.TABLE]

	static create(value: string) {
		const node = super.createWithValue(TAG_NAME.DIV, value) as HTMLDivElement
		node.classList.add('table-container')
		return node
	}

	static createWithChildren(rows: number, cols: number) {
		const tableId = newId(TABLE_BLOT_NAME.TABLE)
		const tableContainer = Parchment.create(this.blotName, tableId) as TableContainerBlot
		const table = TableBlot.createWithChildren(tableId, rows, cols)
		tableContainer.appendChild(table)
		return tableContainer
	}

	optimize(context: { [key: string]: any }): void {
		super.optimize(context)

		const didRemove = this.optimizeRemoveIfEmpty()
		if (didRemove) return

		this.optimizeMergeWithNext()

		if (
			!this.prev ||
			// check if prev is already a block
			(!(this.prev instanceof Block) &&
				// check if prev will be merged into the same table
				!(this.prev instanceof TableContainerBlot && this.prev.dataValue() === this.dataValue()) &&
				!(this.prev instanceof TableBlot && this.prev.id() === this.dataValue()) &&
				!(this.prev instanceof TableBodyBlot && this.prev.tableId() === this.dataValue()) &&
				!(this.prev instanceof TableRowBlot && this.prev.tableId() === this.dataValue()) &&
				!(this.prev instanceof TableCellBlot && this.prev.tableId() === this.dataValue()))
		) {
			getLogger().debug(
				`${this.statics.tagName} optimize: no block before table container, inserting new block`,
				{
					this: this,
					parent: this.parent,
					prev: this.prev,
					next: this.next
				}
			)
			const block = Parchment.create(Block.blotName)
			this.parent.insertBefore(block, this)
		}

		if (
			!this.next ||
			// check if next is already a block
			(!(this.next instanceof Block) &&
				// check if next will be merged into the same table
				!(this.next instanceof TableContainerBlot && this.next.dataValue() === this.dataValue()) &&
				!(this.next instanceof TableBlot && this.next.id() === this.dataValue()) &&
				!(this.next instanceof TableBodyBlot && this.next.tableId() === this.dataValue()) &&
				!(this.next instanceof TableRowBlot && this.next.tableId() === this.dataValue()) &&
				!(this.next instanceof TableCellBlot && this.next.tableId() === this.dataValue()))
		) {
			getLogger().debug(`${this.statics.tagName} optimize: no block after table container, inserting new block`, {
				this: this,
				parent: this.parent,
				prev: this.prev,
				next: this.next
			})
			const block = Parchment.create(Block.blotName)
			this.parent.insertBefore(block, this.next)
		}
	}

	unwrap(): void {
		// keep a reference to the body, since `super.unwrap()` will break the ref
		const table = this.table()
		super.unwrap()
		table.unwrap()
	}

	table() {
		return this.children.head as TableBlot
	}
}
