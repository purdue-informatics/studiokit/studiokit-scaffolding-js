import ParchmentType from 'parchment'
import { Quill } from 'react-quill'
import { ATTRIBUTE, TABLE_BLOT_NAME, TAG_NAME } from '../constants'
import { BaseTableBlot } from './BaseTableBlot'
import { TableBodyBlot } from './TableBodyBlot'
import { TableContainerBlot } from './TableContainer'

const Parchment = Quill.import('parchment') as typeof ParchmentType

export class TableBlot extends BaseTableBlot {
	static blotName = TABLE_BLOT_NAME.TABLE
	static tagName = TAG_NAME.TABLE
	static scope = Parchment.Scope.BLOCK_BLOT
	static requiredParentName: string | null = TABLE_BLOT_NAME.TABLE_CONTAINER
	static allowedChildrenNames: string[] = [TABLE_BLOT_NAME.TBODY]

	static create(value: string) {
		const node = super.createWithValue(TAG_NAME.TABLE, value) as HTMLTableElement
		node.setAttribute(ATTRIBUTE.ID, value)
		return node
	}

	static createWithChildren(id: string, rows: number, cols: number) {
		const table = Parchment.create(this.blotName, id) as TableBlot
		const tableBody = TableBodyBlot.createWithChildren(id, rows, cols)
		table.appendChild(tableBody)
		return table
	}

	optimize(context: { [key: string]: any }): void {
		super.optimize(context)

		const didRemove = this.optimizeRemoveIfEmpty()
		if (didRemove) return

		this.optimizeMergeWithNext()
		this.optimizeWrapInRequiredParent(this.id())
	}

	unwrap(): void {
		// keep a reference to the body, since `super.unwrap()` will break the ref
		const tbody = this.tableBody()
		super.unwrap()
		tbody.unwrap()
	}

	id() {
		return this.domNode.getAttribute(ATTRIBUTE.ID) as string
	}

	tableContainer() {
		return this.parent instanceof TableContainerBlot ? this.parent : undefined
	}

	tableBody() {
		return this.children.head as TableBodyBlot
	}
}
