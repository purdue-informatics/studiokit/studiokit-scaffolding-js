import ParchmentType from 'parchment'
import { Quill } from 'react-quill'
import { ATTRIBUTE, TABLE_BLOT_NAME, TAG_NAME } from '../constants'
import { newId } from '../utils'
import { BaseTableBlot } from './BaseTableBlot'
import { TableBodyBlot } from './TableBodyBlot'
import { TableCellBlot } from './TableCellBlot'

const Parchment = Quill.import('parchment') as typeof ParchmentType

export class TableRowBlot extends BaseTableBlot {
	static blotName = TABLE_BLOT_NAME.TR
	static tagName = TAG_NAME.TR
	static scope = Parchment.Scope.BLOCK_BLOT
	static requiredParentName: string | null = TABLE_BLOT_NAME.TBODY
	static allowedChildrenNames: string[] = [TABLE_BLOT_NAME.TD]

	static create(value: string) {
		const node = super.createWithValue(TAG_NAME.TR, value)

		// split data-value into separate attributes for easier access
		const values = value.split('|')
		node.setAttribute(ATTRIBUTE.TABLE, values[0])
		node.setAttribute(ATTRIBUTE.ROW, values[1])
		node.setAttribute(ATTRIBUTE.ID, values[1])

		return node
	}

	static createWithChildren(tableId: string, cols: number) {
		const id = newId(this.blotName)
		const tableRow = Parchment.create(this.blotName, `${tableId}|${id}`) as TableRowBlot
		for (let col = 0; col < cols; col++) {
			const tableCell = TableCellBlot.createWithChildren(tableId, id)
			tableRow.appendChild(tableCell)
		}
		return tableRow
	}

	static createFromReference(reference: TableRowBlot) {
		const tableId = reference.tableId()
		const tableRow = this.createWithChildren(tableId, 0)
		reference.children.forEach(c => {
			const newTableCell = TableCellBlot.createWithChildren(tableId, tableRow.id())
			tableRow.appendChild(newTableCell)
		})
		return tableRow
	}

	optimize(context: { [key: string]: any }): void {
		super.optimize(context)

		const didRemove = this.optimizeRemoveIfEmpty()
		if (didRemove) return

		this.optimizeMergeWithNext()
		this.optimizeWrapInRequiredParent(this.tableId())
	}

	id() {
		return this.domNode.getAttribute(ATTRIBUTE.ID) as string
	}

	tableId() {
		return this.domNode.getAttribute(ATTRIBUTE.TABLE) as string
	}

	tableBody() {
		// tr -> tbody
		return this.parent instanceof TableBodyBlot ? this.parent : undefined
	}

	prevRow() {
		return this.prev ? (this.prev as TableRowBlot) : null
	}

	nextRow() {
		return this.next ? (this.next as TableRowBlot) : null
	}

	table() {
		return this.tableBody()?.table()
	}
}
