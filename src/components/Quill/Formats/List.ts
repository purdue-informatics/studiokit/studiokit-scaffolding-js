import ParchmentType from 'parchment'
import { Quill } from 'react-quill'
import { getLogger } from '../../../utils/logger'
import { TABLE_BLOT_NAME } from '../TableModule/constants'

const QuillList = Quill.import('formats/list') as typeof ParchmentType.Container
const QuillListItem = Quill.import('formats/list/item') as typeof ParchmentType.Block

const ALPHA = 'alpha'

export class ListItem extends QuillListItem {
	format(name: string, value: any) {
		if (name === TABLE_BLOT_NAME.TD && this.parent) {
			getLogger().debug('ListItem format: wrap parent in TD')
			this.parent.wrap(name, value)
		} else {
			super.format(name, value)
		}
	}
}

/**
 * Extend `List` to add new `alpha` type.
 */
export class List extends QuillList {
	static allowedChildren = [ListItem]

	static create(value: string) {
		const node = super.create(value === ALPHA ? 'ordered' : value) as HTMLElement
		if (value === ALPHA) {
			node.setAttribute('data-value', ALPHA)
		}
		return node
	}

	static formats(domNode: HTMLElement) {
		if (
			domNode.tagName === 'OL' &&
			domNode.hasAttribute('data-value') &&
			domNode.getAttribute('data-value') === ALPHA
		) {
			return ALPHA
		}
		// @ts-ignore: parent List class _does_ implement `formats`
		return super.formats(domNode)
	}

	optimize(context: any) {
		const next = this.next
		if (
			next != null &&
			next.prev === this &&
			next instanceof QuillList &&
			next.domNode.tagName === this.domNode.tagName &&
			next.domNode.getAttribute('data-checked') === this.domNode.getAttribute('data-checked') &&
			// add data-value comparison, avoid combining "ordered" and "alpha" lists
			next.domNode.getAttribute('data-value') === this.domNode.getAttribute('data-value')
		) {
			super.optimize(context)
		}
	}
}
