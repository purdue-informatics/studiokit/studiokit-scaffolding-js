import { Dictionary } from 'lodash'
import ParchmentType from 'parchment'
import { Quill } from 'react-quill'
import { getFirstParentElementWithClassName } from '../../../utils/dom'
import { getLogger } from '../../../utils/logger'

const Parchment = Quill.import('parchment') as typeof ParchmentType

export interface ImageValue {
	src: string
	alt?: string
	width: string
	height: string
	nonPublic?: boolean
}

type ImageBlotOnClick = (img: ImageValue, blot: Image) => void

let clickHandlersEnabled = true
const onClickHandlers: Dictionary<ImageBlotOnClick | undefined> = {}

export function setImageClickHandlersEnabled(value: boolean) {
	clickHandlersEnabled = value
}

export function setOnImageBlotClick(guid: string, value: ImageBlotOnClick | undefined) {
	if (value) {
		onClickHandlers[guid] = value
	} else {
		delete onClickHandlers[guid]
	}
}

/**
 * Defines a new implementation of `Image`, to allow setting alt text, width, and height.
 *
 * https://github.com/zenoamaro/react-quill/issues/169
 */
export class Image extends Parchment.Embed {
	static blotName = 'image'
	static tagName = 'IMG'

	/** Creates the DOM Node from a value */
	static create(value: ImageValue) {
		const node = super.create(this.tagName) as HTMLImageElement
		if (value.alt?.trim()) {
			node.setAttribute('alt', value.alt)
		} else {
			node.className = 'alt-text-missing'
		}
		// Note: an image that's not public error "level" supercedes an image missing alt text
		if (value.nonPublic) {
			node.className = 'non-public-image'
		}
		node.setAttribute('src', value.src)
		// Setting up the height and width to be reactive when an image is getting resize
		node.setAttribute('height', value.height ?? 'auto')
		node.setAttribute('width', value.width ?? 'auto')
		return node
	}

	/** Returns the value for a DOM Node */
	static value(node: HTMLImageElement): ImageValue {
		return {
			width: node.getAttribute('width') || 'auto',
			height: node.getAttribute('height') || 'auto',
			alt: node.getAttribute('alt') || undefined,
			src: node.getAttribute('src') as string,
			nonPublic: node.classList.contains('non-public-image')
		}
	}

	/** Create the Blot using the DOM Node from static `create` */
	constructor(domNode: HTMLImageElement) {
		super(domNode)

		// right click listener to open modal to edit alt text
		domNode.addEventListener('contextmenu', (ev: Event) => {
			getLogger().debug('image right clicked', { this: this, ev })
			const quillEdit = getFirstParentElementWithClassName(domNode, 'QuillEdit')
			if (!quillEdit) {
				return
			}
			const onClick = onClickHandlers[quillEdit.id]
			if (onClick && clickHandlersEnabled) {
				ev.preventDefault()
				onClick(Image.value(domNode), this)
			}
		})
	}
}
