import ParchmentType from 'parchment'
import { Quill } from 'react-quill'
import { TableCellBlot } from '../TableModule/Blots/TableCellBlot'
import { TABLE_BLOT_NAME } from '../TableModule/constants'

const QuillVideo = Quill.import('formats/video') as typeof ParchmentType.Embed

/**
 * Extend `Video` to allow use inside of a `TableCellBlot`
 */
export class Video extends QuillVideo {
	formats(): { [index: string]: any } {
		const formats = super.formats()
		// allow video that is wrapped in a table cell to return this "format"
		// when the delta op for this blot is created
		if (this.parent instanceof TableCellBlot) {
			formats[TABLE_BLOT_NAME.TD] = this.parent.dataValue()
		}
		return formats
	}

	formatAt(index: number, length: number, name: string, value: any): void {
		// `BlockEmbed` ignores all formatting sent to `formatAt`
		// allow the video iframe to be wrapped in a table cell, e.g. "formatted as a table cell"
		if (name === TABLE_BLOT_NAME.TD) {
			this.wrap(name, value)
		} else {
			super.formatAt(index, length, name, value)
		}
	}
}
