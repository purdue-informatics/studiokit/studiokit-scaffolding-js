import { DeltaStatic } from 'quill'
import { Quill } from 'react-quill'
import { getLogger } from '../../utils/logger'

const Delta = Quill.import('delta')

/**
 * Custom module for quilljs to allow user to drag images from their file system into the editor
 * and paste images from clipboard (Works on Chrome, Firefox, Edge, not on Safari)
 * @see https://quilljs.com/blog/building-a-custom-module/
 *
 * Roughly forked (and TypeScripted) from https://github.com/kensnyder/quill-image-drop-module/blob/master/src/ImageDrop.js
 */
export class ImageDropModule {
	quill: Quill
	options: any

	/**
	 * Instantiate the module given a quill instance and any options
	 */
	constructor(quill: Quill, options: any) {
		this.quill = quill
		this.handleDrop = this.handleDrop.bind(this)
		this.handlePaste = this.handlePaste.bind(this)
		this.quill.root.addEventListener('drop', this.handleDrop, false)
		this.quill.root.addEventListener('paste', this.handlePaste, false)
	}

	/**
	 * Handler for drop event to read dropped files from evt.dataTransfer
	 */
	handleDrop = (evt: DragEvent) => {
		evt.preventDefault()
		if (evt.dataTransfer && evt.dataTransfer.files && evt.dataTransfer.files.length) {
			const selection = document.getSelection()
			let range: Range | null = null
			if (document.caretRangeFromPoint) {
				range = document.caretRangeFromPoint(evt.clientX, evt.clientY)
			} else if (document.caretPositionFromPoint) {
				const pos = document.caretPositionFromPoint(evt.clientX, evt.clientY)
				range = document.createRange()
				range.setStart(pos?.offsetNode || document.body, pos?.offset || 0)
				range.collapse(true)
			}
			if (selection && range) {
				selection.setBaseAndExtent(
					range.startContainer,
					range.startOffset,
					range.startContainer,
					range.startOffset
				)
			}
			this.readFiles(evt.dataTransfer.files, this.insert.bind(this))
		}
	}

	/**
	 * Handler for paste event to read pasted files from evt.clipboardData
	 */
	handlePaste = (evt: ClipboardEvent) => {
		// Get alt text from clipboard data and add it to the image we create with base64 data
		const text = evt.clipboardData?.getData('text/html')
		let alt = ''
		if (text) {
			const regExp = /alt="(.*?)"/
			alt = regExp.exec(text)?.[1] || ''
		}

		if (evt.clipboardData && evt.clipboardData.items && evt.clipboardData.items.length) {
			this.readFiles(evt.clipboardData.items, base64DataUrl => {
				// wait until after the default paste action finishes
				setTimeout(() => this.insert(base64DataUrl, alt), 50)
			})
		}
	}

	/**
	 * Insert the image into the document at the current cursor position
	 */
	insert = (base64DataUrl: string | ArrayBuffer | null | undefined, alt = ''): void => {
		const logger = getLogger()
		const index = (this.quill.getSelection() || {}).index || this.quill.getLength()
		const diff: DeltaStatic = new Delta()
		diff.ops = []

		if (index > 0) {
			diff.ops.push({ retain: index })
		}

		const indexBefore = Math.max(0, index - 1)
		const contentsBeforeIndex = this.quill.getContents(indexBefore, 1)
		logger.debug('insert - find contents before index', { index: indexBefore, contentsBeforeIndex })

		// when copy-pasting an image from the web/browser, the web url image is immediately inserted.
		// check if a web image exists at the previous index, and replace it.
		// when copy-pasting an image file from the computer, the `base64DataUrl` image is immediately inserted,but is missing some blot info.
		// check if the same image data already exists at the previous index, and replace it.
		if (
			contentsBeforeIndex?.ops?.[0]?.insert?.image?.src?.startsWith('http') ||
			contentsBeforeIndex?.ops?.[0]?.insert?.image?.src === base64DataUrl
		) {
			logger.debug('insert - remove image before index')
			diff.ops = []
			if (indexBefore > 0) {
				diff.ops.push({ retain: indexBefore })
			}
			diff.ops.push({ delete: 1 })
		}

		// insert the image at the index
		diff.ops.push({ insert: { image: { src: base64DataUrl, alt } } })

		if (diff.ops.length > 0) {
			logger.debug('insert - quill.updateContents', { diff })
			this.quill.updateContents(diff, 'user')
		}
	}

	/**
	 * Extract base64 image URIs from a list of files from evt.dataTransfer or evt.clipboardData
	 */
	readFiles = (
		files: DataTransferItemList | FileList,
		callback: (dataUrl: string | ArrayBuffer | null | undefined) => void
	): void => {
		const regExp = RegExp(/^image\/(gif|jpe?g|a?png|svg|webp|bmp|vnd\.microsoft\.icon)/i)
		// check each file for an image
		;[].forEach.call(files, (file: DataTransferItem) => {
			if (!regExp.test(file.type)) {
				// file is not an image
				// Note that some file formats such as psd start with image/* but are not readable
				return
			}
			const reader = new FileReader()
			reader.onload = evt => {
				callback(evt.target?.result)
			}
			const blob = file.getAsFile ? file.getAsFile() : file
			if (blob instanceof Blob) {
				reader.readAsDataURL(blob)
			}
		})
	}
}
