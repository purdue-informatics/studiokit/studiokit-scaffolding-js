import { DeleteAction, IframeVideoSpec, ResizeAction } from 'quill-blot-formatter'

/**
 * This is a custom Spec for the Blot Formatter.
 * https://github.com/Fandom-OSS/quill-blot-formatter
 * Uses the default actions, align, resize, and delete
 * Additionally, the overlay is repositioned correctly after scroll or resize events.
 * Unfortunately the alignment formatting is not supported by quill/react quill.
 * Also this class uses an 'imageProxy' which will prevent the embedded
 * video from being played in editing mode
 */
export class CustomVideoSpec extends IframeVideoSpec {
	init() {
		super.init()
		this.formatter.quill.root.addEventListener('scroll', this.reposition)
		window.addEventListener('resize', this.reposition)
	}

	getActions() {
		return [ResizeAction, DeleteAction]
	}

	reposition = (e: any) => {
		if (this.proxyImage) {
			this.formatter.repositionOverlay()
		}
	}
}
