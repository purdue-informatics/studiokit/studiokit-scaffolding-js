import { DeleteAction, ImageSpec, ResizeAction } from 'quill-blot-formatter'

/**
 * This is a custom Spec for the Blot Formatter.
 * https://github.com/Fandom-OSS/quill-blot-formatter
 * Uses the default actions, align, resize, and delete
 * Additionally, the overlay is repositioned correctly after scroll or resize events.
 * Unfortunately the alignment formatting is not supported by quill/react quill.
 */
export class CustomImageSpec extends ImageSpec {
	init() {
		super.init()

		// listen for scroll and resize, to reposition the overlay
		this.formatter.quill.root.addEventListener('scroll', this.reposition)
		window.addEventListener('resize', this.reposition)

		// listen for right-click of the overlay, to pass through to the img
		this.formatter.overlay.addEventListener('contextmenu', this.onRightClick)
	}

	getActions() {
		return [ResizeAction, DeleteAction]
	}

	onRightClick = (e: Event) => {
		e.preventDefault()
		if (this.img) {
			this.img.dispatchEvent(new CustomEvent('contextmenu'))
		}
	}

	reposition = (_e: Event) => {
		if (this.img) {
			this.formatter.repositionOverlay()
		}
	}
}
