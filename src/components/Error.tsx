import { parse } from 'query-string'
import React, { FunctionComponent } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import Helmet from 'react-helmet'
import { RouteComponentProps } from 'react-router'
import { getLockDownBrowserInfo } from '../utils/lockDownBrowser'
import { LockDownBrowserExitButton } from './LockDownBrowser/ExitButton'

const Error: FunctionComponent<RouteComponentProps> = props => {
	const { message } = parse(props.location.search)
	const lockDownBrowserInfo = getLockDownBrowserInfo()
	return (
		<>
			<Helmet title="Error" />

			<Container>
				<Row>
					<Col md={8}>
						<h1>Uh oh!</h1>
						{message && <blockquote>{message}</blockquote>}
						<p> Sorry, but it looks like something went wrong while processing your request.</p>
						<p>
							If you have any questions or concerns please contact us at{' '}
							<a href="mailto:tlt@purdue.edu">tlt@purdue.edu</a>
						</p>
						{lockDownBrowserInfo.isClientLockDownBrowser && <LockDownBrowserExitButton />}
					</Col>
				</Row>
			</Container>
		</>
	)
}

// required for use in Routes / AsyncComponent
export default Error
