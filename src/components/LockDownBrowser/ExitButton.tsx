import Button from '@material-ui/core/Button'
import React, { FunctionComponent } from 'react'
import { useLocation } from 'react-router'
import { LOCKDOWN_BROWSER_KEY, LOCKDOWN_BROWSER_TRUE } from '../../constants'

export const LockDownBrowserExitButton: FunctionComponent = () => {
	const location = useLocation()
	return (
		<Button
			className="btn-primary mb4"
			onClick={() =>
				(window.location.href =
					location.pathname + `?${LOCKDOWN_BROWSER_KEY.EXIT_BROWSER}=${LOCKDOWN_BROWSER_TRUE}`)
			}>
			Close LockDown Browser
		</Button>
	)
}
