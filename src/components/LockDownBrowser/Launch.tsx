import * as Sentry from '@sentry/react'
import { parse, stringify } from 'query-string'
import React from 'react'
import { FunctionComponent, useEffect } from 'react'
import { Helmet } from 'react-helmet'
import { useHistory, useLocation } from 'react-router'
import { LOCKDOWN_BROWSER_LAUNCH_ERROR } from '../../constants'
import { getLockDownBrowserInfo } from '../../utils/lockDownBrowser'
import { parseLocation } from '../../utils/url'
import { Loading } from '../Loading'

export const LockDownBrowserLaunch: FunctionComponent = () => {
	const history = useHistory()
	const location = useLocation()
	const search = parse(location.search)
	const redirectPath = search.redirectPath
	let reloadCount = search.reloadCount && typeof search.reloadCount === 'string' ? parseInt(search.reloadCount) : 0

	// this effect will run once after this component renders
	// allow LockDown Browser to set its cookies
	useEffect(() => {
		const lockDownBrowserInfo = getLockDownBrowserInfo()
		// redirect path is required and must be a single string
		if (!redirectPath || typeof redirectPath !== 'string') {
			Sentry.captureException('redirectPath is undefined')
			history.push({
				pathname: '/error',
				search: stringify({ message: LOCKDOWN_BROWSER_LAUNCH_ERROR })
			})
			return
		}
		// if LDB cookies are set, redirect to the final path
		if (lockDownBrowserInfo.isClientLockDownBrowser) {
			const redirectPathLocation = parseLocation(redirectPath)
			history.push(redirectPathLocation)
			return
		}
		// increment reloadCount and reload the current url fully (not using in-app router)
		// try 3 times before going to the error route
		if (reloadCount < 2) {
			Sentry.captureMessage('could not determine if client is LockDown Browser, reloading')
			reloadCount++
			search.reloadCount = reloadCount.toString()
			window.location.href =
				window.location.protocol +
				'//' +
				window.location.host +
				window.location.pathname +
				'?' +
				stringify(search)
			return
		}
		Sentry.captureException('client is not LockDown Browser')
		history.push({
			pathname: '/error',
			search: stringify({ message: LOCKDOWN_BROWSER_LAUNCH_ERROR })
		})
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])

	return (
		<>
			<Helmet title="LockDown Browser Launch" />
			<Loading />
		</>
	)
}

// required for use in Routes / AsyncComponent
export default LockDownBrowserLaunch
