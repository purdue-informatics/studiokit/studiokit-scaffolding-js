import IconCancel from '@material-ui/icons/Cancel'
import IconCheckCircle from '@material-ui/icons/CheckCircle'
import React, { FunctionComponent } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import { OPERATING_SYSTEM } from '../../constants/operatingSystem'
import { getLockDownBrowserInfo } from '../../utils/lockDownBrowser'
import { LockDownBrowserExitButton } from './ExitButton'

export const LockDownBrowserCheck: FunctionComponent = () => {
	const lockDownBrowserInfo = getLockDownBrowserInfo()
	const platform = lockDownBrowserInfo.buildDateWindows
		? OPERATING_SYSTEM.WINDOWS
		: lockDownBrowserInfo.buildDateMac
		? OPERATING_SYSTEM.MAC
		: lockDownBrowserInfo.buildDateIpad
		? OPERATING_SYSTEM.IPAD
		: undefined
	const buildDate =
		lockDownBrowserInfo.buildDateWindows || lockDownBrowserInfo.buildDateMac || lockDownBrowserInfo.buildDateIpad
	return (
		<>
			<Helmet title="LockDown Browser Check" />

			<Container className="main-content">
				<Row>
					<Col xs={12}>
						<h1>
							{lockDownBrowserInfo.isClientLockDownBrowser ? (
								<>
									<IconCheckCircle className="fill-green f1 mr2" /> Success! You are using LockDown
									Browser.
								</>
							) : (
								<>
									<IconCancel className="fill-red f1 mr2" />
									Oops! You are not using LockDown Browser.
								</>
							)}
						</h1>
						{lockDownBrowserInfo.isClientLockDownBrowser && (
							<>
								<p>
									You’re all set to use LockDown Browser for your assessments. You can close LockDown
									Browser when you’re ready.
								</p>
								{(lockDownBrowserInfo.buildId || platform) && (
									<dl className="f6">
										{lockDownBrowserInfo.buildId && (
											<>
												<dt>Build Id</dt>
												<dd>{lockDownBrowserInfo.buildId}</dd>
											</>
										)}
										{platform && (
											<>
												<dt>Platform</dt>
												<dd>{platform}</dd>
												<dt>Build Date</dt>
												<dd>{buildDate}</dd>
											</>
										)}
									</dl>
								)}
								<LockDownBrowserExitButton />
							</>
						)}
					</Col>
				</Row>
			</Container>
		</>
	)
}

// required for use in Routes / AsyncComponent
export default LockDownBrowserCheck
