import { shallow } from 'enzyme'
import React, { ComponentType } from 'react'
import { Omit } from 'react-router'
import { setEndpointMappings } from '../../constants/configuration'
import { MODEL_STATUS } from '../../constants/modelStatus'
import { BaseReduxState, CollectionReduxResponse, Model, ModelCollection } from '../../types'
import {
	CollectionComponentProps,
	CollectionComponentWrappedProps,
	configureCollectionComponent,
	configureMapStateToProps
} from './CollectionComponent'

jest.mock('../../redux/actionCreator')

interface TestModel extends Model {
	id: number
	name: string
}

interface TestComponentProps
	extends Omit<CollectionComponentProps<TestModel>, keyof CollectionReduxResponse<TestModel>> {
	foo: string
}

const TestLoader = () => <div id="testLoader" />

const TestComponent = (props: TestComponentProps & CollectionComponentWrappedProps<TestModel>) => (
	<div id="testComponent">
		{props.foo}
		{props.model._metadata}
	</div>
)

const setup = (testModels: ModelCollection<TestModel> = {}, extraProps?: any, LoadingComponent?: ComponentType) => {
	const CollectionComponent = configureCollectionComponent<TestModel, TestComponentProps>(
		TestComponent,
		LoadingComponent
	)
	const mapStateToProps = configureMapStateToProps<TestModel, TestComponentProps>('testModels')
	const state: Partial<BaseReduxState> = {
		models: {
			testModels
		}
	}
	const ownProps = {
		...{
			foo: 'bar',
			guid: '12345',
			location: {
				pathname: '/'
			},
			match: {
				params: {}
			}
		},
		...extraProps
	}
	const props: TestComponentProps & CollectionComponentProps<TestModel> = {
		...ownProps,
		...mapStateToProps(state as BaseReduxState, ownProps)
	}

	// define the interface of the wrapper using the generic types
	return shallow(<CollectionComponent {...props} />)
}

beforeAll(() => {
	setEndpointMappings({
		testModels: {
			_config: {
				isCollection: true
			}
		}
	})
})

describe('CollectionComponent', () => {
	it('should render <Loading> component on first render', () => {
		const wrapper = setup()
		expect(wrapper.find('Loading').length).toEqual(1)
	})
	it('should render <TestLoader> component if provided', () => {
		const wrapper = setup({}, {}, TestLoader)
		expect(wrapper.find('TestLoader').length).toEqual(1)
	})
	it('should render <TestComponent> immediately if `disableAutoLoad = true`', () => {
		const wrapper = setup({}, { disableAutoLoad: true })
		expect(wrapper.find('TestComponent').length).toEqual(1)
	})
	describe('functions', () => {
		describe('componentDidMount', () => {
			it('should not load() if disableAutoLoad = true', () => {
				const wrapper = setup(undefined, { disableAutoLoad: true })
				const instance = wrapper.instance() as any
				const loadSpy = jest.spyOn(instance, 'load')
				instance.componentDidMount()
				expect(loadSpy).toHaveBeenCalledTimes(0)
			})
			it('should load() if disableAutoLoad = falsy and model has no metadata', () => {
				const wrapper = setup()
				const instance = wrapper.instance() as any
				const loadSpy = jest.spyOn(instance, 'load')
				instance.componentDidMount()
				expect(loadSpy).toHaveBeenCalledTimes(1)
			})
			it('should load() if disableAutoLoad = falsy and model is not fetching', () => {
				const wrapper = setup({
					_metadata: {
						isFetching: false,
						hasError: false
					}
				})
				const instance = wrapper.instance() as any
				const loadSpy = jest.spyOn(instance, 'load')
				instance.componentDidMount()
				expect(loadSpy).toHaveBeenCalledTimes(1)
			})
		})
		describe('componentDidUpdate', () => {
			it('should load() when params change if disableAutoLoadOnParamsChange = falsy', () => {
				const wrapper = setup()
				const instance = wrapper.instance() as any
				const loadSpy = jest.spyOn(instance, 'load')
				instance.componentDidMount()
				expect(loadSpy).toHaveBeenCalledTimes(1)
				wrapper.setProps({
					queryParams: { foo: 'bar' }
				})
				expect(loadSpy).toHaveBeenCalledTimes(2)
			})
			it('should not load() when params change if disableAutoLoadOnParamsChange = true', () => {
				const wrapper = setup(undefined, { disableAutoLoadOnParamsChange: true })
				const instance = wrapper.instance() as any
				const loadSpy = jest.spyOn(instance, 'load')
				instance.componentDidMount()
				expect(loadSpy).toHaveBeenCalledTimes(1)
				wrapper.setProps({
					queryParams: { foo: 'bar' }
				})
				expect(loadSpy).toHaveBeenCalledTimes(1)
			})
		})
	})
	it('should track previousModelStatus', () => {
		const wrapper = setup({}, { disableAutoLoad: true })
		const instance = wrapper.instance() as any
		expect(wrapper.state('modelStatus')).toEqual(MODEL_STATUS.READY)
		expect(wrapper.state('previousModelStatus')).toEqual(MODEL_STATUS.UNINITIALIZED)
		instance.load()
		expect(wrapper.state('modelStatus')).toEqual(MODEL_STATUS.LOADING)
		expect(wrapper.state('previousModelStatus')).toEqual(MODEL_STATUS.READY)
	})
})
