import { shallow } from 'enzyme'
import React from 'react'
import { MODEL_STATUS } from '../../constants/modelStatus'
import { HTTP_STATUS_CODE, Model } from '../../types'
import { CollectionItemComponentWrappedProps } from './CollectionItemComponent'
import ModelErrorRedirectComponent from './ModelErrorRedirectComponent'

interface TestComponentProps extends CollectionItemComponentWrappedProps<Model> {
	foo: string
}

const TestComponent = (props: TestComponentProps) => <div id="testComponent">{props.foo}</div>

const setup = (previousModelStatus: MODEL_STATUS, modelStatus: MODEL_STATUS, hasError: boolean, status?: number) => {
	const lastFetchErrorData = hasError ? { status } : undefined
	const props: Partial<TestComponentProps> = {
		foo: 'bar',
		previousModelStatus,
		modelStatus,
		model: {
			_metadata: {
				isFetching: false,
				hasError,
				lastFetchErrorData
			}
		} as Partial<Model>
	}
	const ModelErrorRedirect = ModelErrorRedirectComponent(TestComponent)
	return shallow(
		<ModelErrorRedirect {...(props as TestComponentProps & CollectionItemComponentWrappedProps<Model>)} />
	)
}

describe('ModelErrorRedirect', () => {
	it('should render WrappedComponent for good route', () => {
		const wrapper = setup(MODEL_STATUS.LOADING, MODEL_STATUS.READY, false)
		expect(wrapper.find('TestComponent').length).toEqual(1)
	})
	it('should render Redirect if errorRoute is true', () => {
		const wrapper = setup(MODEL_STATUS.UNINITIALIZED, MODEL_STATUS.ERROR, true, HTTP_STATUS_CODE.NOT_FOUND)
		expect(wrapper.find('Redirect').length).toEqual(1)
	})
	it('should render Redirect if notFoundRoute is true', () => {
		const wrapper = setup(
			MODEL_STATUS.UNINITIALIZED,
			MODEL_STATUS.ERROR,
			true,
			HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR
		)
		expect(wrapper.find('Redirect').length).toEqual(1)
	})
})
