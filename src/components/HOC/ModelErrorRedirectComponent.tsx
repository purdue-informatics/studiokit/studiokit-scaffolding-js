import React, { Component, ComponentType } from 'react'
import { Redirect } from 'react-router-dom'
import { MODEL_STATUS } from '../../constants/modelStatus'
import { HTTP_STATUS_CODE, Model } from '../../types'
import { CollectionComponentWrappedProps } from './CollectionComponent'
import { CollectionItemComponentWrappedProps } from './CollectionItemComponent'

/**
 * This component exists as a child to CollectionItemComponent to
 * redirect the user to a 404/500 page if errors occur while fetching data.
 *
 * Nested CollectionItemComponent instances should not
 * use this - they’ll want to handle errors according to the need for
 * alerting the user in the parent.
 *
 * @export
 * @param WrappedComponent Wrapped component that depends on the model. (Usually AsyncComponent)
 * @returns A wrapped component or redirects
 */
export default function modelErrorRedirectComponent<
	TOwnProps extends CollectionItemComponentWrappedProps<Model> | CollectionComponentWrappedProps<Model>
>(WrappedComponent: ComponentType<TOwnProps>) {
	return class ModelErrorRedirectComponent extends Component<TOwnProps> {
		render() {
			const { model, modelStatus, previousModelStatus } = this.props

			const notFoundRoute =
				previousModelStatus === MODEL_STATUS.UNINITIALIZED &&
				modelStatus === MODEL_STATUS.ERROR &&
				model._metadata &&
				model._metadata.hasError &&
				model._metadata.lastFetchErrorData &&
				model._metadata.lastFetchErrorData.status === HTTP_STATUS_CODE.NOT_FOUND

			const errorRoute =
				previousModelStatus === MODEL_STATUS.UNINITIALIZED &&
				modelStatus === MODEL_STATUS.ERROR &&
				model._metadata &&
				model._metadata.hasError &&
				model._metadata.lastFetchErrorData &&
				model._metadata.lastFetchErrorData.status === HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR

			if (notFoundRoute) {
				const pathname = window.location.pathname
				return (
					<Redirect
						to={{
							pathname: '/not-found',
							search: `?pathname=${pathname}`
						}}
					/>
				)
			}
			if (errorRoute) {
				return (
					<Redirect
						to={{
							pathname: '/error'
						}}
					/>
				)
			}
			return <WrappedComponent {...this.props} />
		}
	}
}
