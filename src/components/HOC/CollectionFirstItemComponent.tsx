import memoizeOne from 'memoize-one'
import React, { Component, ComponentClass, ComponentType } from 'react'
import { CollectionItemDerivedProps, CollectionItemReduxResponse, Model, ModelCollection } from '../../types'
import { getModelArray, getModelMinusRelations } from '../../utils/model'
import { CollectionComponentWrappedProps } from './CollectionComponent'

/** The props passed into `WrappedComponent`. */
export interface CollectionFirstItemComponentWrappedProps<TModel extends Model>
	extends Omit<CollectionComponentWrappedProps<TModel>, 'model'>,
		CollectionItemReduxResponse<TModel>,
		CollectionItemDerivedProps<TModel> {}

/**
 * HOC meant to pass the first collection item to the wrapped component as its model.
 * Should be wrapped in `CollectionComponent`.
 */
export function configureCollectionFirstItemComponent<TModel extends Model, TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & CollectionFirstItemComponentWrappedProps<TModel>>
) {
	return class CollectionFirstItemComponent extends Component<TOwnProps & CollectionComponentWrappedProps<TModel>> {
		getModelArray = memoizeOne((model: ModelCollection<TModel>, guid?: string) => getModelArray(model, guid))

		getModelMinusRelations = memoizeOne((model: TModel) => getModelMinusRelations(model))

		getFirstItem = () => {
			const { model, guid } = this.props
			const modelArray = this.getModelArray(model, guid)
			const singleItem =
				modelArray.length > 0
					? modelArray[0]
					: model[guid]
					? ((model[guid] as unknown) as TModel)
					: ({} as TModel)
			return singleItem
		}

		render() {
			const { pathParams } = this.props
			const firstItem = this.getFirstItem()
			const modelMinusRelations = this.getModelMinusRelations(firstItem)
			const p: Array<string | number> = [...pathParams]
			if (firstItem.id) {
				p.push(firstItem.id)
			}
			return (
				<WrappedComponent
					{...this.props}
					pathParams={p}
					model={firstItem}
					modelMinusRelations={modelMinusRelations}
				/>
			)
		}
	}
}

export default function collectionFirstItemComponent<
	TModel extends Model,
	TOwnProps extends CollectionFirstItemComponentWrappedProps<TModel>
>(
	wrappedComponent: ComponentType<TOwnProps>
): ComponentClass<
	Omit<TOwnProps, keyof CollectionFirstItemComponentWrappedProps<TModel>> & CollectionComponentWrappedProps<TModel>
> {
	// @ts-ignore: Could not match inferred type from HOC
	return configureCollectionFirstItemComponent(wrappedComponent)
}
