import React, { Component, ComponentClass, ComponentType } from 'react'
import { connect } from 'react-redux'
import { Redirect, RouteComponentProps, withRouter } from 'react-router-dom'
import { persistenceService, returnUrlKey } from '../../services/persistenceService'
import { AuthState, BaseReduxState } from '../../types'

export type AuthenticatedComponentStateProps = Pick<AuthState, 'isAuthenticating' | 'isAuthenticated'>

export interface AuthenticatedComponentProps extends AuthenticatedComponentStateProps, RouteComponentProps {}

export const configureAuthenticatedComponent = <TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps>
): ComponentType<TOwnProps & AuthenticatedComponentProps> => {
	return class AuthenticatedComponent extends Component<
		TOwnProps & AuthenticatedComponentStateProps & RouteComponentProps
	> {
		isNotAuthenticated() {
			const { isAuthenticating, isAuthenticated } = this.props
			return !isAuthenticating && !isAuthenticated
		}

		persistReturnUrlIfNeeded() {
			const { pathname } = this.props.location
			const returnUrl = persistenceService.getItem(returnUrlKey)
			if (!returnUrl) {
				void persistenceService.setItem(returnUrlKey, pathname)
			}
		}

		render() {
			const { isAuthenticating, isAuthenticated, history, location, match, staticContext, ...rest } = this.props
			if (this.isNotAuthenticated()) {
				this.persistReturnUrlIfNeeded()
				return <Redirect to="/login" />
			}
			return <WrappedComponent {...(rest as TOwnProps)} />
		}
	}
}

export const configureMapStateToProps = () => (state: BaseReduxState): AuthenticatedComponentStateProps => {
	return {
		isAuthenticating: !state.auth.isAuthenticated && state.auth.isAuthenticating,
		isAuthenticated: state.auth.isAuthenticated
	}
}

export default function authenticatedComponent<TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps>
): ComponentClass<TOwnProps> {
	const AuthenticatedComponent = configureAuthenticatedComponent(WrappedComponent)
	const mapStateToProps = configureMapStateToProps()
	// @ts-ignore: could not match inferred type from the `connect` HOC
	return withRouter(connect(mapStateToProps)(AuthenticatedComponent))
}
