import { shallow } from 'enzyme'
import React from 'react'
import { guidComponent, GuidComponentWrappedProps } from './GuidComponent'

interface TestComponentOwnProps {
	foo: string
}

type TestComponentProps = TestComponentOwnProps & GuidComponentWrappedProps

const TestComponent = (props: TestComponentProps) => (
	<div id="testComponent">
		{props.foo}
		{props.guid}
	</div>
)

const setup = () => {
	const TestComponentWithGuid = guidComponent(TestComponent)
	return shallow(<TestComponentWithGuid foo="bar" />)
}

describe('GuidComponent', () => {
	it('should render with a `guid`', () => {
		const wrapper = setup()
		expect(wrapper.prop('guid')).toBeTruthy()
	})
})
