import { shallow } from 'enzyme'
import React from 'react'
import * as actionCreator from '../../redux/actionCreator'
import { MODAL_ACTION_TYPE } from '../../redux/actions'
import { ModalsState } from '../../redux/reducers/modalsReducer'
import { configureConnectedModalComponent, ConnectedModalWrappedProps } from './ConnectedModalComponent'
import { GuidComponentWrappedProps } from './GuidComponent'
import SpyInstance = jest.SpyInstance

interface TestComponentOwnProps {
	foo: string
}

type TestComponentProps = TestComponentOwnProps & ConnectedModalWrappedProps & GuidComponentWrappedProps

const TestComponent = (props: TestComponentProps) => (
	<div id="testComponent">
		{props.foo}
		{props.guid}
		{!!props.onEntering}
	</div>
)

const setup = (modals?: ModalsState, guid?: string) => {
	const Component = configureConnectedModalComponent(TestComponent)
	return shallow<TestComponentProps>(<Component foo="bar" guid={guid ?? '1'} modals={modals ?? {}} />)
}

jest.mock('../../redux/actionCreator')

describe('ConnectedModalComponent', () => {
	let spy: SpyInstance
	beforeAll(() => {
		spy = jest.spyOn(actionCreator, 'dispatchAction')
	})
	afterEach(() => {
		spy.mockClear()
	})
	afterAll(() => {
		spy.mockRestore()
	})
	it('dispatches modal opening action after calling onEntering', () => {
		const wrapper = setup()
		const wrappedProps = wrapper.props()
		wrappedProps.onEntering()
		expect(actionCreator.dispatchAction).toHaveBeenCalledWith({
			type: MODAL_ACTION_TYPE.MODAL_ENTERING,
			guid: '1',
			index: 0,
			isFullscreen: false
		})
	})
	it('dispatches modal exited action if isModalOpen is true after calling onExited', () => {
		const wrapper = setup()
		wrapper.setState({
			isModalOpen: true
		})
		const wrappedProps = wrapper.props()
		wrappedProps.onExited()
		const guid = '1'
		expect(actionCreator.dispatchAction).toHaveBeenCalledWith({ type: MODAL_ACTION_TYPE.MODAL_EXITED, guid })
	})
	it('does not dispatch modal exited action if isModalOpen is false after calling onExited', () => {
		const wrapper = setup()
		wrapper.setState({
			isModalOpen: false
		})
		const wrappedProps = wrapper.props()
		wrappedProps.onExited()
		expect(actionCreator.dispatchAction).toHaveBeenCalledTimes(0)
	})
	it('sets isTopOpenFullscreenModal to false when it is the top modal but not fullscreen', () => {
		const wrapper = setup({ guid1: { index: 0, isFullscreen: false } }, 'guid1')
		const wrappedProps = wrapper.props()
		expect(wrappedProps.isTopOpenFullscreenModal).toBe(false)
	})
	it('sets isTopOpenFullscreenModal to true when it is the top modal and is fullscreen', () => {
		const wrapper = setup({ guid1: { index: 1, isFullscreen: true } }, 'guid1')
		const wrappedProps = wrapper.props()
		expect(wrappedProps.isTopOpenFullscreenModal).toBe(true)
	})
	it('sets isTopOpenFullscreenModal to false when it is not top modal even if fullscreen', () => {
		const wrapper = setup(
			{ guid1: { index: 1, isFullscreen: true }, guid2: { index: 2, isFullscreen: true } },
			'guid1'
		)
		const wrappedProps = wrapper.props()
		expect(wrappedProps.isTopOpenFullscreenModal).toBe(false)
	})
	it('sets isTopOpenFullscreenModal to false when it is not top modal and not fullscreen', () => {
		const wrapper = setup(
			{ guid1: { index: 1, isFullscreen: false }, guid2: { index: 2, isFullscreen: true } },
			'guid1'
		)
		const wrappedProps = wrapper.props()
		expect(wrappedProps.isTopOpenFullscreenModal).toBe(false)
	})
	it('sets isTopOpenFullscreenModal to true when it is not top modal and is fullscreen, one above is not ', () => {
		const wrapper = setup(
			{ guid1: { index: 1, isFullscreen: true }, guid2: { index: 2, isFullscreen: false } },
			'guid1'
		)
		const wrappedProps = wrapper.props()
		expect(wrappedProps.isTopOpenFullscreenModal).toBe(true)
	})
})
