import React, { Component, ComponentClass, ComponentType } from 'react'
import { v4 as uuidv4 } from 'uuid'

export interface GuidComponentWrappedProps {
	guid: string
}

export interface GuidComponentState {
	guid: string
}

/**
 * HOC that provides a `guid` prop to the `WrappedComponent`.
 *
 * @param WrappedComponent  The component to wrap.
 */
export function guidComponent<TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & GuidComponentWrappedProps> // intersect type separates the guid props
): ComponentClass<TOwnProps> {
	return class GuidComponent extends Component<TOwnProps, GuidComponentState> {
		constructor(props: TOwnProps) {
			super(props)
			this.state = {
				guid: uuidv4()
			}
		}

		render() {
			return <WrappedComponent {...this.props} guid={this.state.guid} />
		}
	}
}

export default guidComponent
