import memoizeOne from 'memoize-one'
import React, { Component, ComponentClass, ComponentType } from 'react'
import { connect } from 'react-redux'
import { RouteComponentProps, withRouter } from 'react-router'
import { MODEL_STATUS } from '../../constants/modelStatus'
import {
	BaseReduxState,
	CollectionCommonProps,
	CollectionCommonState,
	CollectionCreateParams,
	CollectionItemDeleteParams,
	CollectionItemDerivedProps,
	CollectionItemLoadParams,
	CollectionItemMethods,
	CollectionItemReduxResponse,
	CollectionItemUpdateParams,
	CollectionMethodConfiguration,
	Model
} from '../../types'
import {
	createCollectionItem,
	deleteCollectionItem,
	handleCollectionItemParamsChange,
	initializeCollectionItem,
	loadCollectionItem,
	selectCollectionItemFromState,
	stopCollectionPeriodicLoad,
	updateCollectionItem
} from '../../utils/collection'
import { getModelMinusRelations, handleModelFetchFinish } from '../../utils/model'
import { Loading } from '../Loading'
import guidComponent, { GuidComponentWrappedProps } from './GuidComponent'

/** The props passed into `CollectionItemComponent` from the user and other HOCs. */
export interface CollectionItemComponentProps<TModel extends Model>
	extends CollectionCommonProps,
		GuidComponentWrappedProps,
		RouteComponentProps,
		CollectionItemReduxResponse<TModel> {}

/** The props passed down to the `WrappedComponent`. */
export interface CollectionItemComponentWrappedProps<TModel extends Model>
	extends Omit<CollectionItemComponentProps<TModel>, keyof RouteComponentProps>,
		CollectionItemDerivedProps<TModel>,
		CollectionItemMethods,
		CollectionCommonState {}

export function configureCollectionItemComponent<TModel extends Model, TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & CollectionItemComponentWrappedProps<TModel>>,
	LoaderComponent: ComponentType = Loading
) {
	type HocProps = TOwnProps & CollectionItemComponentProps<TModel>

	return class CollectionItemComponent
		extends Component<HocProps, CollectionCommonState>
		implements CollectionItemMethods {
		constructor(props: HocProps) {
			super(props)
			this.state = {
				// initializing until model is loaded, or if no model
				modelStatus: MODEL_STATUS.UNINITIALIZED,
				previousModelStatus: MODEL_STATUS.UNINITIALIZED
			}
		}

		componentDidMount() {
			const { model } = this.props
			initializeCollectionItem(model, this.load, this.getCollectionMethodConfig())
		}

		componentDidUpdate(prevProps: HocProps) {
			const {
				model: prevModel,
				modelName: prevModelName,
				pathParams: prevPathParams,
				queryParams: prevQueryParams
			} = prevProps
			const { modelName, model, pathParams, queryParams, disableAutoLoadOnParamsChange } = this.props
			handleModelFetchFinish(model, prevModel, undefined, this.setModelStatus)
			if (!disableAutoLoadOnParamsChange) {
				handleCollectionItemParamsChange(
					modelName,
					prevModelName,
					pathParams,
					prevPathParams,
					queryParams,
					prevQueryParams,
					this.load
				)
			}
		}

		setModelStatus = (newModelStatus: MODEL_STATUS) => {
			this.setState(prevState => ({
				previousModelStatus: prevState.modelStatus,
				modelStatus: newModelStatus
			}))
		}

		getCollectionMethodConfig = () => {
			const { guid, modelName, pathParams, queryParams, disableAutoLoad } = this.props
			const { modelStatus } = this.state
			const collectionMethodConfig: CollectionMethodConfiguration = {
				modelName,
				pathParams,
				queryParams,
				disableAutoLoad,
				guid,
				isInitialized: modelStatus !== MODEL_STATUS.UNINITIALIZED,
				changeModelStatus: this.setModelStatus
			}
			return collectionMethodConfig
		}

		load = (params: CollectionItemLoadParams = {}) => {
			loadCollectionItem(this.getCollectionMethodConfig(), params)
		}

		stopPeriodicLoad = (taskId: string) => {
			stopCollectionPeriodicLoad(taskId)
		}

		create = (params: CollectionCreateParams) => {
			const { model } = this.props
			createCollectionItem(model, this.getCollectionMethodConfig(), params)
		}

		update = (params: CollectionItemUpdateParams) => {
			const { model } = this.props
			updateCollectionItem(model, this.getCollectionMethodConfig(), params)
		}

		delete = (params: CollectionItemDeleteParams = {}) => {
			const { model } = this.props
			deleteCollectionItem(model, this.getCollectionMethodConfig(), params)
		}

		getModelMinusRelations = memoizeOne((model: TModel) => getModelMinusRelations(model))

		render() {
			const { history, match, location, staticContext, ...otherProps } = this.props
			const { modelStatus, previousModelStatus } = this.state

			if (modelStatus === MODEL_STATUS.UNINITIALIZED) {
				return <LoaderComponent />
			}

			return (
				<WrappedComponent
					{...(otherProps as TOwnProps &
						Omit<CollectionItemComponentProps<TModel>, keyof RouteComponentProps>)}
					model={this.props.model}
					modelMinusRelations={this.getModelMinusRelations(this.props.model)}
					modelStatus={modelStatus}
					previousModelStatus={previousModelStatus}
					load={this.load}
					stopPeriodicLoad={this.stopPeriodicLoad}
					create={this.create}
					update={this.update}
					delete={this.delete}
				/>
			)
		}
	}
}

export const configureMapStateToProps = <
	TModel extends Model,
	TOwnProps extends Omit<CollectionItemComponentProps<TModel>, keyof CollectionItemReduxResponse<TModel>>
>(
	modelName: string
) => (state: BaseReduxState, ownProps: TOwnProps) => {
	return selectCollectionItemFromState({
		guid: ownProps.guid,
		modelName: ownProps.modelName || modelName,
		pathParams: ownProps.pathParams,
		routeMatchParams: ownProps.match.params,
		state
	})
}

/**
 * HOC that provides "collection" related functionality for a single item in a collection, using redux `connect()`, react-router-dom `withRouter()`, and `GuidComponent`.
 *
 * @template T The type of model.
 *
 * @param WrappedComponent The component to wrap.
 * @param modelName The generic path (no Ids) to where the collection item is stored in redux.
 * A path relating to an item in defined in `constants/configuration/getEndpointMappings()` (levels separated by a '.').
 * Can override at render time, e.g. `<C modelName="otherModel" />`.
 * @param LoaderComponent Component to use as the Loader. Defaults to `<Loading />`.
 */
export default function collectionItemComponent<
	TModel extends Model,
	TOwnProps extends CollectionItemComponentWrappedProps<TModel>
>(
	WrappedComponent: ComponentType<TOwnProps>,
	modelName: string,
	LoaderComponent?: ComponentType
): ComponentClass<Omit<TOwnProps, keyof CollectionItemComponentWrappedProps<TModel>> & Partial<CollectionCommonProps>> {
	const CollectionItemComponent = configureCollectionItemComponent<TModel, TOwnProps>(
		WrappedComponent,
		LoaderComponent
	)
	const mapStateToProps = configureMapStateToProps(modelName)
	// @ts-ignore: could not match inferred type from the `connect` HOC
	return withRouter(guidComponent(connect(mapStateToProps)(CollectionItemComponent)))
}
