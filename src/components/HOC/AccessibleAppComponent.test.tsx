import { mount } from 'enzyme'
import React from 'react'
import { RouteComponentProps } from 'react-router'
import { DeepPartial } from 'redux'
import { setAppConfig } from '../../constants/configuration'
import { defaultAppConfiguration } from '../../constants/mockData'
import { documentService } from '../../services/documentService'
import { setupAccessibleComponent } from './AccessibleAppComponent'

interface TestComponentProps {
	foo: string
}

const TestComponent = (props: TestComponentProps & RouteComponentProps) => (
	<div id="testComponent">
		{props.foo}
		{props.location.pathname}
	</div>
)

const mockMutationObserveFunc = jest.fn()

const setup = (initialPath?: string) => {
	const routeProps = {
		location: {
			pathname: initialPath || '/'
		}
	} as RouteComponentProps
	const ownProps: TestComponentProps = {
		foo: 'bar'
	}
	const props = {
		...ownProps,
		...routeProps
	}
	const AccessibleAppComponent = setupAccessibleComponent(TestComponent)
	return mount(<AccessibleAppComponent {...props} />)
}

describe('AccessibleAppComponent', () => {
	beforeAll(() => {
		setAppConfig(defaultAppConfiguration)
	})

	let titleCallback: (mutation: MutationRecord[]) => {}
	beforeEach(() => {
		documentService.setActiveDocument(document.body)
		mockMutationObserveFunc.mockClear()
		// To avoid adding an interface onto global for a test
		;(global as any).MutationObserver = jest.fn().mockImplementation(callback => {
			titleCallback = callback
			return {
				disconnect: () => {
					/* no-op */
				},
				observe: mockMutationObserveFunc
			}
		})
	})
	it('should update the title span when the document title changes', () => {
		const wrapper = setup()
		const updatedTitle = `${defaultAppConfiguration.APP_NAME} - test`
		const mutations: Array<DeepPartial<MutationRecord>> = [{ target: { textContent: updatedTitle } }]

		expect(mockMutationObserveFunc).not.toHaveBeenCalled()
		expect(wrapper.children('.accessible-title').text()).toEqual('')

		titleCallback(mutations as MutationRecord[])
		expect(wrapper.children('.accessible-title').text()).toEqual(updatedTitle)
	})
	it('should not update the title span when the title is the default APP_NAME', () => {
		const wrapper = setup()
		const defaultTitle = `${defaultAppConfiguration.APP_NAME} - test`

		expect(mockMutationObserveFunc).not.toHaveBeenCalled()

		let mutations: Array<DeepPartial<MutationRecord>> = [{ target: { textContent: defaultTitle } }]
		titleCallback(mutations as MutationRecord[])
		expect(wrapper.children('.accessible-title').text()).toEqual(defaultTitle)

		mutations = [{ target: { textContent: defaultAppConfiguration.APP_NAME } }]
		titleCallback(mutations as MutationRecord[])
		expect(wrapper.children('.accessible-title').text()).toEqual(defaultTitle)
	})
	it('should reset focus to the root on url path changes', () => {
		const wrapper = setup()
		wrapper.setProps({
			location: {
				pathname: '/testing'
			}
		})
		expect(documentService.getActiveDocument()).toEqual(wrapper.children('.app-root').instance())
	})
	it('should not reset focus when the path does not change', () => {
		const wrapper = setup()
		wrapper.setProps({
			location: {
				pathname: '/'
			}
		})
		expect(documentService.getActiveDocument()).toEqual(document.body)
	})
	it('should not reset focus when the path is part of a route with multiple paths', () => {
		const wrapper = setup('/help/scoring-docs/test-1')
		wrapper.setProps({
			location: {
				pathname: '/help/scoring-docs/test-2'
			}
		})
		expect(documentService.getActiveDocument()).toEqual(document.body)
	})
})
