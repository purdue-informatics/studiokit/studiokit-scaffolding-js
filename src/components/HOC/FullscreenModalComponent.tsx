import React, { Component, ComponentClass, ComponentType } from 'react'
import ReactModal from 'react-modal'
import { connectedModalComponent, ConnectedModalWrappedProps } from './ConnectedModalComponent'
import { GuidComponentWrappedProps } from './GuidComponent'

/**
 * The props that are provided to the HOC component directly from the caller, not other wrapping HOCs
 * NOTE: if more flexibility is needed, add more props from `ReactModal.Props`
 */
export interface FullscreenModalOwnProps {
	/* Boolean describing if the modal should be shown or not. If not provided, the modal manages itself, defaulting to open. */
	isOpen?: boolean

	/**
	 * Function that will be called when the modal should be closed, and the parent should set `props.isOpen` to `false`.
	 * If not provided, the modal manages itself.
	 */
	closeModal?: () => void

	/* String indicating how the content container should be announced to screen readers. */
	contentLabel?: string

	/* Optional value indicating the zIndex the modal overlay should have */
	zIndex?: number
}

/** The props that are provided to the HOC component, either directly or from other wrapping HOCs */
export type FullscreenModalComponentProps = FullscreenModalOwnProps &
	ConnectedModalWrappedProps &
	GuidComponentWrappedProps

/** The props that are provide to the wrapped component. */
export interface FullscreenModalWrappedProps {
	/** GUID of the fullscreen modal, used in state.modals. */
	guid: string
	/** Function that will close the modal. */
	closeModal: () => void
}

interface FullscreenModalState {
	isOpen: boolean
}

/**
 * HOC that manages a `ReactModal`, adds inline css, and
 * coordinates modal state in redux using.
 *
 * NOTE: Meant for testing. Should be wrapped in `connectedModalComponent` normally.
 *
 * @param WrappedComponent The component to wrap.
 */
export function configureFullscreenModalComponent<TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & FullscreenModalWrappedProps>
) {
	type HocProps = TOwnProps & FullscreenModalComponentProps
	return class FullscreenModalComponent extends Component<HocProps, FullscreenModalState> {
		constructor(props: HocProps) {
			super(props)

			this.state = {
				isOpen: props.isOpen === undefined ? true : props.isOpen
			}

			// https://github.com/reactjs/react-modal#app-element
			// Accessibility fixed when reading the content in the modal
			if (document.getElementById('root')) {
				ReactModal.setAppElement('#root')
			}
		}

		componentDidMount() {
			const { isOpen } = this.state
			if (isOpen) {
				this.onOpen()
			}
		}

		componentWillUnmount() {
			document.documentElement.removeEventListener('touchmove', this.disableScroll)
		}

		componentDidUpdate(prevProps: HocProps, prevState: FullscreenModalState) {
			let isOpen: boolean | undefined

			if (this.props.isOpen !== undefined && this.props.isOpen !== this.state.isOpen) {
				// using props to handle isOpen, update state
				isOpen = this.props.isOpen
			} else if (this.props.isOpen === undefined && prevState.isOpen !== this.state.isOpen) {
				// using state only to handle isOpen
				isOpen = this.state.isOpen
			}

			if (isOpen === undefined) {
				return
			}

			if (isOpen) {
				this.onOpen()
			}
			this.setState({
				isOpen
			})
		}

		disableScroll = (e: Event) => {
			document.documentElement.scrollTop = 0
			e.preventDefault()
			return false
		}

		onOpen = () => {
			document.documentElement.addEventListener('touchmove', this.disableScroll)
			this.props.onEntering()
		}

		closeModal = () => {
			document.documentElement.removeEventListener('touchmove', this.disableScroll)
			if (this.props.closeModal) {
				this.props.closeModal()
				return
			}
			this.setState({
				isOpen: false
			})
		}

		render() {
			const {
				contentLabel,
				isTopOpenFullscreenModal,
				onEntering,
				onExited,
				isOpen: _,
				closeModal,
				zIndex,
				...remainingProps
			} = this.props
			// do not pass ConnectedModalWrappedProps to wrapped component
			const wrappedProps = {
				...remainingProps,
				closeModal: this.closeModal
			} as TOwnProps & FullscreenModalWrappedProps
			const { isOpen } = this.state
			return (
				<ReactModal
					isOpen={isOpen}
					contentLabel={contentLabel}
					style={{
						content: {
							position: 'absolute',
							top: 0,
							left: 0,
							right: 0,
							bottom: 0,
							overflow: 'hidden',
							padding: 0,
							border: 'none',
							backgroundColor: 'white'
						},
						overlay: {
							position: 'absolute',
							top: 0,
							left: 0,
							right: 0,
							bottom: 0,
							zIndex: zIndex ?? 2000
						}
					}}
					// hide the rendered portal if this is not the top open fullscreen modal
					portalClassName={`ReactModalPortal${!isTopOpenFullscreenModal ? ' dn' : ''}`}
					shouldCloseOnOverlayClick={false}>
					<WrappedComponent {...wrappedProps} />
				</ReactModal>
			)
		}
	}
}

/**
 * HOC that contains a `ReactModal` which can be auto managed or managed with props,
 * adds fullscreen styles, and coordinates modal state in redux.
 *
 * Uses `connectedModalComponent`.
 *
 * NOTE: Please add the following css
 *
 * .ReactModal__Body--open {
 * 	overflow: hidden;
 * }
 *
 * @param WrappedComponent The component to wrap.
 */
export function fullscreenModalComponent<TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & FullscreenModalWrappedProps>
): ComponentClass<TOwnProps & FullscreenModalOwnProps> {
	const component = configureFullscreenModalComponent<TOwnProps>(WrappedComponent)
	return connectedModalComponent(component, true)
}

export default fullscreenModalComponent
