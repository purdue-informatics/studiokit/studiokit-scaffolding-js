import React, { Component, ComponentClass, ComponentType } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { BaseReduxState, Model, OptionalRecord } from '../../types'
import { ActivityOptions, defaultOptions } from '../../utils/baseActivity'

export interface ActivityRequiredStateProps {
	hasAccess: boolean
	redirectPath: string
}

export const configureActivityRequiredComponent = <TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps>
) => {
	return class ActivityRequiredComponent extends Component<TOwnProps & ActivityRequiredStateProps> {
		public render() {
			const { hasAccess, redirectPath, ...rest } = this.props
			if (!hasAccess) {
				return <Redirect to={redirectPath} />
			}
			return <WrappedComponent {...(rest as TOwnProps)} />
		}
	}
}

/**
 * Return `mapStateToProps` function. Add a `hasAccess` boolean property to the component's props
 * checking whether the user has the passed `requiredActivity`.
 *
 * @param accessPredicate A predicate accepting a required activity and an optional entity and/or userInfo object
 * @param requiredActivity The required activity which is passed to the predicate for evaluation
 * @param modelsProperty (Optional) The property used to locate the entity or entities needed for entity-level activity grants
 * @param redirectPath (Optional) A string or function that provides the redirect path for when `accessPredicate` is false. Defaults to '/'.
 */
export const configureMapStateToProps = <TOwnProps extends {}>(
	accessPredicate: (requiredActivity: string, options: ActivityOptions) => boolean,
	requiredActivity: string,
	modelsProperty?: string,
	redirectPath?: string | ((options: ActivityOptions) => string | undefined) | undefined
) => (state: BaseReduxState, ownProps?: TOwnProps): ActivityRequiredStateProps => {
	const options = defaultOptions(state, ownProps, modelsProperty)
	return {
		hasAccess: accessPredicate(requiredActivity, options),
		redirectPath: (typeof redirectPath === 'function' ? redirectPath(options) : redirectPath) || '/'
	}
}

/**
 * This HOC ensures that the wrapped component is only rendered if the predicate provided is satisfied.
 *
 * Typically this component is used by passing one of the functions in utils/activities as the
 * `accessPredicate` and a constant from "constants/activities" as the `requiredActivity`.
 *
 * If a lambda is passed as the predicate, it is passed (but may or may not opt to utilize) the `requiredActivity`
 * parameter. It can be ignored if not needed by the lambda.
 *
 * @param WrappedComponent The component which requires activity/activities in order to render
 * @param accessPredicate A predicate accepting a required activity and an optional entity and/or userInfo object
 * @param requiredActivity The required activity which is passed to the predicate for evaluation
 * @param redirectPath (Optional) A string or function that provides the redirect path for when `accessPredicate` is false. Defaults to '/'.
 */
export default function activityRequiredComponent<TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps>,
	accessPredicate: (requiredActivity: string, options: ActivityOptions) => boolean,
	requiredActivity: string,
	redirectPath?: string | ((options: ActivityOptions) => string | undefined) | undefined
): ComponentClass<TOwnProps>
/**
 * This HOC ensures that the wrapped component is rendered if the predicate provided is satisfied.
 *
 * Typically this component is used by passing one of the functions in utils/activities as the
 * `accessPredicate` and a constant from "constants/activities" as the `requiredActivity`.
 *
 * If a lambda is passed as the predicate, it is passed (but may or may not opt to utilize) the `requiredActivity`
 * and `modelsProperty` parameters. They can be ignored if not needed by the lambda.
 *
 * @param WrappedComponent The component which requires activity/activities in order to render
 * @param accessPredicate A predicate accepting a required activity and an optional entity and/or userInfo object
 * @param requiredActivity The required activity which is passed to the predicate for evaluation
 * @param modelsProperty The property used to locate the entity or entities needed for entity-level activity grants
 * @param redirectPath (Optional) A string or function that provides the redirect path for when `accessPredicate` is false. Defaults to '/'.
 */
export default function activityRequiredComponent<TOwnProps extends {}, TPropName extends string>(
	WrappedComponent: ComponentType<TOwnProps>,
	accessPredicate: (requiredActivity: string, options: ActivityOptions) => boolean,
	requiredActivity: string,
	modelsProperty: TPropName,
	redirectPath?: string | ((options: ActivityOptions) => string | undefined) | undefined
): ComponentClass<TOwnProps & OptionalRecord<TPropName, Model>>
/**
 * This HOC ensures that the wrapped component is rendered if the predicate provided is satisfied.
 *
 * Typically this component is used by passing one of the functions in utils/activities as the
 * `accessPredicate` and a constant from "constants/activities" as the `requiredActivity`.
 *
 * If a lambda is passed as the predicate, it is passed (but may or may not opt to utilize) the `requiredActivity`
 * and `modelsProperty` parameters. They can be ignored if not needed by the lambda.
 *
 * @param WrappedComponent The component which requires activity/activities in order to render
 * @param accessPredicate A predicate accepting a required activity and an optional entity and/or userInfo object
 * @param requiredActivity The required activity which is passed to the predicate for evaluation
 * @param modelsProperty The property used to locate the entity or entities needed for entity-level activity grants
 * @param redirectPath (Optional) A string or function that provides the redirect path for when `accessPredicate` is false. Defaults to '/'.
 */
export default function activityRequiredComponent<TOwnProps extends {}, TPropName extends string>(
	WrappedComponent: ComponentType<TOwnProps>,
	accessPredicate: (requiredActivity: string, options: ActivityOptions) => boolean,
	requiredActivity: string,
	modelsProperty?: TPropName,
	redirectPath?: string | ((options: ActivityOptions) => string | undefined) | undefined
): ComponentClass<TOwnProps & OptionalRecord<TPropName, Model>> {
	const ActivityRequiredComponent = configureActivityRequiredComponent(WrappedComponent)
	const mapStateToProps = configureMapStateToProps(accessPredicate, requiredActivity, modelsProperty, redirectPath)
	// @ts-ignore: could not match inferred type from the `connect` HOC
	return connect(mapStateToProps)(ActivityRequiredComponent)
}
