import React, { Component, ComponentClass, ComponentType } from 'react'
import { connect } from 'react-redux'
import { Redirect, RouteComponentProps, withRouter } from 'react-router-dom'
import { BaseReduxState, DeletableModel, Model, ModelCollection } from '../../types'
import { CollectionCommonProps } from '../../types/Collection'
import { canPerformActivityGlobally, defaultOptions } from '../../utils/baseActivity'
import { getModelIdFromRouteMatchParams } from '../../utils/route'
import CollectionItemComponent, { CollectionItemComponentWrappedProps } from './CollectionItemComponent'
import ModelErrorRedirectComponent from './ModelErrorRedirectComponent'

export interface EntityComponentProps<T extends DeletableModel>
	extends CollectionItemComponentWrappedProps<T>,
		RouteComponentProps {
	model: T
}

export type EntityComponentWrappedHeaderProps<T extends DeletableModel, TProp extends keyof any> = { [P in TProp]: T }

export type EntityComponentWrappedProps<
	T extends DeletableModel,
	TProp extends keyof any
> = EntityComponentWrappedHeaderProps<T, TProp> & {
	modelName: string
}

export const configureEntityComponent = <T extends DeletableModel, TPropName extends keyof any>(
	WrappedComponent: ComponentType<EntityComponentWrappedProps<T, TPropName>>,
	WrappedHeader: ComponentType<EntityComponentWrappedHeaderProps<T, TPropName>> | undefined,
	propName: TPropName,
	entityName: string
) => {
	return class EntityComponent extends Component<EntityComponentProps<T>> {
		render() {
			const { model, modelName }: { model: T; modelName: string } = this.props
			const newProps = {
				[propName]: model
			} as EntityComponentWrappedHeaderProps<T, TPropName>
			return (
				<div>
					{!!WrappedHeader && <WrappedHeader {...newProps} />}
					{model.isDeleted ? (
						<Redirect to={`/${entityName === 'groups' ? 'courses' : entityName}`} />
					) : (
						<WrappedComponent {...newProps} modelName={modelName} />
					)}
				</div>
			)
		}
	}
}

export const configureMapStateToProps = <T extends DeletableModel>(readActivity: string, modelName: string) => (
	state: BaseReduxState,
	ownProps: EntityComponentProps<T>
) => {
	const canReadGlobally = canPerformActivityGlobally(readActivity, defaultOptions(state))

	// override modelName if admin does not have a role in the given entity
	let newModelName = modelName
	const modelId = getModelIdFromRouteMatchParams(ownProps.match.params, newModelName)
	let hasAccess
	if (modelId === undefined) {
		hasAccess = false
	} else {
		hasAccess =
			!!state.models[modelName] &&
			!!(state.models[modelName] as ModelCollection)[modelId] &&
			!!((state.models[modelName] as ModelCollection)[modelId] as Model).id
	}
	if (canReadGlobally && !hasAccess) {
		newModelName = `search.${newModelName}`
	}

	return {
		modelName: newModelName
	}
}

/**
 * A wrapper using CollectionItemComponent that passes down the `model`, to allow nesting of collection components.
 * Uses `CollectionItemComponent`, `ModelErrorRedirectComponent`, `withRouter`, and `connect`.
 *
 * This HOC does NOT pass extra props through to children
 *
 * @param WrappedComponent The wrapped component
 * @param WrappedHeader The wrapped header
 * @param readActivity Read activity that will be checked for global access
 * @param modelName Model name passed to wrapped component and `CollectionItemComponent`. May be modified when only global read is available
 * @param propName The name for the prop with which the `model` will be passed to the wrapped component
 */
export default function entityComponent<T extends DeletableModel, TPropName extends keyof any>(
	WrappedComponent: ComponentType<EntityComponentWrappedProps<T, TPropName>>,
	WrappedHeader: ComponentType<EntityComponentWrappedHeaderProps<T, TPropName>> | undefined,
	readActivity: string,
	modelName: string,
	propName: TPropName
): ComponentClass<Partial<CollectionCommonProps>> {
	const EntityComponent = configureEntityComponent<T, TPropName>(WrappedComponent, WrappedHeader, propName, modelName)
	const mapStateToProps = configureMapStateToProps(readActivity, modelName)
	// @ts-ignore: could not match inferred type from the `connect` HOC
	return withRouter(
		// @ts-ignore: could not match inferred type from the `connect` HOC
		connect(mapStateToProps)(CollectionItemComponent(ModelErrorRedirectComponent(EntityComponent), modelName))
	)
}
