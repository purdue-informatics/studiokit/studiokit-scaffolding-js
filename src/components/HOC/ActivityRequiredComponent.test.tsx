import { shallow } from 'enzyme'
import React from 'react'
import { BASE_ACTIVITY } from '../../constants'
import { mockGroups, mockUser } from '../../constants/mockData'
import { BaseReduxState, Group, UserInfo } from '../../types'
import {
	ActivityOptions,
	canPerformActivityGlobally,
	canPerformActivityOnEntity,
	canPerformActivityOnSomeEntities
} from '../../utils/baseActivity'
import {
	ActivityRequiredStateProps,
	configureActivityRequiredComponent,
	configureMapStateToProps
} from './ActivityRequiredComponent'

interface TestComponentProps {
	group: Group
	foo: string
}

const TestComponent = (props: TestComponentProps) => <div id="testComponent">{props.foo}</div>

const setup = (
	user: { userInfo: UserInfo },
	ownProps: Partial<TestComponentProps>,
	accessPredicate: (requiredActivity: string, options: ActivityOptions) => boolean,
	requiredActivity: string,
	modelsProperty?: string,
	redirectPath?: string | ((options: ActivityOptions) => string | undefined) | undefined,
	extraModelsState: any = {}
) => {
	const ActivityRequiredComponent = configureActivityRequiredComponent(TestComponent)
	const mapStateToProps = configureMapStateToProps(accessPredicate, requiredActivity, modelsProperty, redirectPath)
	const state = {
		models: {
			user,
			...extraModelsState
		}
	}
	const props = {
		...ownProps,
		...mapStateToProps(state as BaseReduxState, ownProps)
	} as TestComponentProps & ActivityRequiredStateProps
	return shallow<TestComponentProps>(<ActivityRequiredComponent {...props} />)
}

describe('ActivityRequiredComponent', () => {
	it('should render Redirect if `accessPredicate` returns false', () => {
		const wrapper = setup(mockUser(), {}, () => false, '')
		expect(wrapper.find('Redirect').length).toEqual(1)
		expect(wrapper.find('Redirect').prop('to')).toEqual('/')
	})
	it('should render Redirect to root path when `redirectPath` is undefined if `accessPredicate` returns false', () => {
		const wrapper = setup(mockUser(), {}, () => false, '', undefined, undefined)
		expect(wrapper.find('Redirect').length).toEqual(1)
		expect(wrapper.find('Redirect').prop('to')).toEqual('/')
	})
	it('should render Redirect with custom `redirectPath` string if `accessPredicate` returns false', () => {
		const wrapper = setup(mockUser(), {}, () => false, '', undefined, '/foo')
		expect(wrapper.find('Redirect').length).toEqual(1)
		expect(wrapper.find('Redirect').prop('to')).toEqual('/foo')
	})
	it('should render Redirect with custom path returned from `redirectPath` function if `accessPredicate` returns false', () => {
		const wrapper = setup(
			mockUser(),
			{},
			() => false,
			'',
			undefined,
			(_options: ActivityOptions) => '/foo'
		)
		expect(wrapper.find('Redirect').length).toEqual(1)
		expect(wrapper.find('Redirect').prop('to')).toEqual('/foo')
	})
	it('should render Redirect with undefined returned from `redirectPath` function if `accessPredicate` returns false', () => {
		const wrapper = setup(
			mockUser(),
			{},
			() => false,
			'',
			undefined,
			(_options: ActivityOptions) => undefined
		)
		expect(wrapper.find('Redirect').length).toEqual(1)
		expect(wrapper.find('Redirect').prop('to')).toEqual('/')
	})
	it('should render WrappedComponent if accessPredicate returns true', () => {
		const wrapper = setup(mockUser(), {}, () => true, '')
		expect(wrapper.find('TestComponent').length).toEqual(1)
	})
	it('should pass ownProps to WrappedComponent when rendered', () => {
		const wrapper = setup(
			mockUser(),
			{
				foo: 'bar'
			},
			() => true,
			''
		)
		expect(wrapper.props().foo).toEqual('bar')
	})
	it('should render WrappedComponent if user has requiredActivity globally', () => {
		const wrapper = setup(
			mockUser([BASE_ACTIVITY.GROUP_CREATE]),
			{},
			canPerformActivityGlobally,
			BASE_ACTIVITY.GROUP_CREATE
		)
		expect(wrapper.find('TestComponent').length).toEqual(1)
	})
	it('should render WrappedComponent if user has requiredActivity on entity, using modelsProperty prop lookup', () => {
		const wrapper = setup(
			mockUser(),
			{
				group: mockGroups([BASE_ACTIVITY.GROUP_CREATE])['1'] as Group
			},
			canPerformActivityOnEntity,
			BASE_ACTIVITY.GROUP_CREATE,
			'group'
		)
		expect(wrapper.find('TestComponent').length).toEqual(1)
	})
	it('should render WrappedComponent if user has requiredActivity on some entities, using modelsProperty for state lookup', () => {
		const wrapper = setup(
			mockUser(),
			{},
			canPerformActivityOnSomeEntities,
			BASE_ACTIVITY.GROUP_CREATE,
			'groups',
			undefined,
			{
				groups: mockGroups([BASE_ACTIVITY.GROUP_CREATE])
			}
		)
		expect(wrapper.find('TestComponent').length).toEqual(1)
	})
})
