/* eslint-disable @typescript-eslint/no-non-null-assertion */

import { shallow } from 'enzyme'
import each from 'jest-each'
import _ from 'lodash'
import React, { Component, ComponentType } from 'react'
import { BaseReduxState } from '../../types'
import {
	configureMapDispatchToProps,
	configureMapStateToProps,
	configureSearchPersistorComponent,
	SearchPersistorWrappedProps
} from './SearchPersistorComponent'

const requiredTestMessage = 'Required test message'

interface TestComponentProps {
	foo: string
}

class TestComponent extends Component<TestComponentProps & SearchPersistorWrappedProps> {
	componentDidMount() {
		this.props.setSearchDefaults(() => this.doSearch(), {
			keywords: 'initial',
			date: null,
			dateString: ''
		})
	}
	// eslint-disable-next-line
	doSearch = () => {}
	// note: doSearch is passed as a lambda so `spyOn` works correctly
	render() {
		return (
			<div>
				{this.props.foo}
				<button
					id="updateAndPersistSearchButton"
					onClick={() =>
						this.props.updateAndPersistSearch(
							{
								keywords: 'foobar',
								requiredMessage: requiredTestMessage
							},
							this.doSearch
						)
					}
				/>
				<button id="searchButton" onClick={() => this.props.handleSearchClick()} />
				<button id="selectedTabButton" onClick={() => this.props.setSelectedTab(2)} />
				<button id="noKeywordChangeButton" onClick={() => this.props.setKeywords({ target: { value: '' } })} />
				<button
					id="shortKeywordChangeButton"
					onClick={() => this.props.setKeywords({ target: { value: 'f' } })}
				/>
				<button
					id="longKeywordChangeButton"
					onClick={() => this.props.setKeywords({ target: { value: 'foobar' } })}
				/>
				<button
					id="numericKeywordChangeButton"
					onClick={() => this.props.setKeywords({ target: { value: '12' } })}
				/>
				<button id="queryAllChecked" onClick={() => this.props.setQueryAll({ target: { checked: true } })} />
				<button id="queryAllUnchecked" onClick={() => this.props.setQueryAll({ target: { checked: false } })} />
				<button id="pressEnterKeyButton" onClick={() => this.props.handleKeywordsKeyDown({ key: 'Enter' })} />
				<button id="pressSomeOtherKeyButton" onClick={() => this.props.handleKeywordsKeyDown({ key: 'x' })} />

				<button
					id="setDateStringButton"
					onClick={() =>
						this.props.updateAndPersistSearch(
							{
								dateString: '2020-01-17',
								date: new Date('2020-01-17')
							},
							this.doSearch
						)
					}
				/>

				<button id="resetSearchButton" onClick={() => this.props.resetSearch()} />

				<button
					id="setSortingRulesButton"
					onClick={() => this.props.setSortingRules([{ id: 'date', desc: true }])}
				/>
				<button id="setPageSizeButton" onClick={() => this.props.setPageSize(100, 4)} />
				<button id="setPageButton" onClick={() => this.props.setPage(2)} />
			</div>
		)
	}
}

const mockDispatch = {
	dispatch: jest.fn()
}

function setup(
	wrappedComponent: ComponentType<TestComponentProps & SearchPersistorWrappedProps>,
	usePersistedSearch = true,
	additionalState = {},
	disableLocalSearch = false
) {
	const key = 'UsersManage'
	const mockState: Partial<BaseReduxState> = {
		search: {
			UsersManage: usePersistedSearch ? _.merge({}, { keywords: '' }, additionalState) : undefined
		}
	}

	const mockProps = {
		foo: 'bar'
	}
	const mapStateToProps = configureMapStateToProps(key)
	const mapDispatchToProps = configureMapDispatchToProps(key)
	const PersistorComponent = configureSearchPersistorComponent(wrappedComponent, disableLocalSearch)
	const props = {
		...mockProps,
		...mapStateToProps(mockState as BaseReduxState),
		...mapDispatchToProps(mockDispatch.dispatch)
	}
	return shallow<SearchPersistorWrappedProps>(<PersistorComponent {...props} />)
}

describe('SearchPersistorComponent', () => {
	// Apparently jest does not parallelize tests. So this should be safe in the absence of any
	// other context in which to stick it
	let dispatchSpy: any
	beforeEach(() => {
		jest.useFakeTimers()
		dispatchSpy = jest.spyOn(mockDispatch, 'dispatch')
	})

	afterEach(() => {
		expect(dispatchSpy).toHaveBeenCalled()
	})

	each([true, false]).describe('shouldUsePersistedSearch: %s', shouldUsePersistedSearch => {
		it('should update child search props and call callback (i.e. doSearch) when keywords change', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#updateAndPersistSearchButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.keywords).toEqual('foobar')
			expect(callbackSpy).toHaveBeenCalled()
		})

		it('should set search props invalidKeywords = true when keywords are too short and trying to search', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			child.find('#shortKeywordChangeButton').simulate('click')
			child.find('#searchButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.invalidKeywords).toBeTruthy()
		})

		it('should set search props invalidKeywords = false when keywords are one or two numeric digits trying to search', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			child.find('#numericKeywordChangeButton').simulate('click')
			child.find('#searchButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.invalidKeywords).toBeFalsy()
		})

		it('should set search props invalidKeywords = false, hasSearch = true, and call doSearch when keywords not too short and trying to search', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#longKeywordChangeButton').simulate('click')
			child.find('#searchButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.invalidKeywords).toBeFalsy()
			expect(wrapper.props().search!.hasSearched).toBeTruthy()
			expect(callbackSpy).toHaveBeenCalled()
		})

		it('should set search props invalidKeywords = false, hasSearch = true, and call doSearch with no keywords', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#noKeywordChangeButton').simulate('click')
			child.find('#searchButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.invalidKeywords).toBeFalsy()
			expect(wrapper.props().search!.hasSearched).toBeTruthy()
			expect(callbackSpy).toHaveBeenCalled()
		})

		it('should update the selected tab and pageByTab prop when user changes tabs', () => {
			const wrapper = setup(
				TestComponent,
				!!shouldUsePersistedSearch,
				shouldUsePersistedSearch ? { pageByTab: { 2: 5 } } : {}
			)
			const child = wrapper.dive()
			child.find('#selectedTabButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.selectedTab).toEqual(2)
			expect(wrapper.props().search!.pageByTab).toEqual({
				2: shouldUsePersistedSearch ? 5 : 0
			})
		})

		it('should update the keywords search prop when keyword is changed', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			jest.runOnlyPendingTimers()
			child.find('#shortKeywordChangeButton').simulate('click')
			expect(wrapper.props().search!.keywords).toEqual('f')
		})

		it('should call doSearch and set requiredMessage when queryAll is checked', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#updateAndPersistSearchButton').simulate('click')
			expect(callbackSpy).toHaveBeenCalledTimes(1)
			child.find('#queryAllChecked').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.queryAll).toBeTruthy()
			expect(wrapper.props().search!.requiredMessage).toEqual(requiredTestMessage)
			expect(callbackSpy).toHaveBeenCalledTimes(2)
		})

		it('should call doSearch and clear requiredMessage when queryAll is unchecked', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#updateAndPersistSearchButton').simulate('click')
			expect(callbackSpy).toHaveBeenCalledTimes(1)
			child.find('#queryAllUnchecked').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.queryAll).toBeFalsy()
			expect(wrapper.props().search!.requiredMessage).toBeNull()
			expect(callbackSpy).toHaveBeenCalledTimes(2)
		})

		it('should not call doSearch and not set requiredMessage when queryAll is checked, and disableLocalSearch is true', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch, {}, true)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#updateAndPersistSearchButton').simulate('click')
			expect(callbackSpy).toHaveBeenCalledTimes(1)
			child.find('#queryAllChecked').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.queryAll).toBeTruthy()
			expect(wrapper.props().search!.requiredMessage).toEqual(requiredTestMessage)
			expect(callbackSpy).toHaveBeenCalledTimes(1)
		})

		it('should not call doSearch and not clear requiredMessage when queryAll is unchecked, and disableLocalSearch is true', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch, {}, true)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#updateAndPersistSearchButton').simulate('click')
			expect(callbackSpy).toHaveBeenCalledTimes(1)
			child.find('#queryAllUnchecked').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.queryAll).toBeFalsy()
			expect(wrapper.props().search!.requiredMessage).toEqual(requiredTestMessage)
			expect(callbackSpy).toHaveBeenCalledTimes(1)
		})

		it('should search when the enter key is pressed with queryAll true', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#queryAllChecked').simulate('click')
			child.find('#longKeywordChangeButton').simulate('click')
			child.find('#pressEnterKeyButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(callbackSpy).toHaveBeenCalled()
		})

		it('should not search when the enter key is pressed with queryAll false', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#longKeywordChangeButton').simulate('click')
			child.find('#pressEnterKeyButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(callbackSpy).not.toHaveBeenCalled()
		})

		it('should search when the enter key is pressed and disableLocalSearch is true', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch, {}, true)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#longKeywordChangeButton').simulate('click')
			child.find('#pressEnterKeyButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(callbackSpy).toHaveBeenCalled()
		})

		it('should not search when some other key besides is pressed', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#longKeywordChangeButton').simulate('click')
			child.find('#pressSomeOtherKeyButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(callbackSpy).not.toHaveBeenCalled()
		})

		it('should not call doSearch after reset if queryAll is not checked', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#resetSearchButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.keywords).toEqual('initial')
			expect(callbackSpy).not.toHaveBeenCalled()
		})

		it('should call doSearch after reset if queryAll is checked', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#queryAllChecked').simulate('click')
			child.find('#resetSearchButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.keywords).toEqual('initial')
			expect(callbackSpy).toHaveBeenCalled()
		})

		it('should call doSearch after reset if disableLocalSearch is true', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch, {}, true)
			const child = wrapper.dive()
			const callbackSpy = jest.spyOn(child.instance() as TestComponent, 'doSearch')
			child.find('#queryAllChecked').simulate('click')
			child.find('#resetSearchButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.keywords).toEqual('initial')
			expect(callbackSpy).toHaveBeenCalled()
		})

		it('should set and clear date/dateString correctly', () => {
			const wrapper = setup(TestComponent, !!shouldUsePersistedSearch)
			const child = wrapper.dive()

			child.find('#setDateStringButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.date).toEqual(new Date('2020-01-17'))
			expect(wrapper.props().search!.dateString).toEqual('2020-01-17')

			child.find('#resetSearchButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.date).toBeNull()
			expect(wrapper.props().search!.dateString).toEqual('')
		})

		it('should set sorting rules', () => {
			const wrapper = setup(
				TestComponent,
				!!shouldUsePersistedSearch,
				shouldUsePersistedSearch ? { sortingRules: [] } : {}
			)
			const child = wrapper.dive()
			child.find('#setSortingRulesButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.sortingRules).toEqual([{ id: 'date', desc: true }])
		})

		it('should set page size', () => {
			const wrapper = setup(
				TestComponent,
				!!shouldUsePersistedSearch,
				shouldUsePersistedSearch ? { pageSize: 50, selectedTab: 1, pageByTab: { 1: 3 } } : {}
			)
			const child = wrapper.dive()
			child.find('#setPageSizeButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.pageSize).toEqual(100)
			expect(wrapper.props().search!.pageByTab).toEqual({ 1: 4 })
		})

		it('should set page', () => {
			const wrapper = setup(
				TestComponent,
				!!shouldUsePersistedSearch,
				shouldUsePersistedSearch ? { selectedTab: 1, pageByTab: { 1: 3 } } : {}
			)
			const child = wrapper.dive()
			child.find('#setPageButton').simulate('click')
			jest.runOnlyPendingTimers()
			expect(wrapper.props().search!.pageByTab).toEqual({ 1: 2 })
		})
	})

	describe('persisted date', () => {
		it('should rehydrate dates correctly from redux', () => {
			const dateString = '2019-01-01T00:00:00.000Z'
			const date = new Date(Date.UTC(2019, 0, 1, 0, 0, 0, 0))
			const wrapper = setup(TestComponent, true, { date, dateString })
			expect(wrapper.props().search!.date).toEqual(date)
			expect(wrapper.props().search!.dateString).toEqual(dateString)
		})
	})
})
