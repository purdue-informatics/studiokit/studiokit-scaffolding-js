import { maxBy } from 'lodash'
import React, { Component, ComponentClass, ComponentType } from 'react'
import { connect } from 'react-redux'
import { dispatchAction } from '../../redux/actionCreator'
import { MODAL_ACTION_TYPE, ModalEnterAction, ModalExitAction } from '../../redux/actions'
import { ModalsState } from '../../redux/reducers/modalsReducer'
import { BaseReduxState } from '../../types'
import guidComponent, { GuidComponentWrappedProps } from '../HOC/GuidComponent'

export type ConnectedModalReduxProps = {
	modals: ModalsState
}

export interface ConnectedModalWrappedProps {
	onEntering: () => void
	onExited: () => void
	isTopOpenFullscreenModal?: boolean
}

export interface ConnectedModalState {
	isModalOpen: boolean
}

export const configureMapStateToProps = () => (state: BaseReduxState): ConnectedModalReduxProps => {
	return {
		modals: state.modals
	}
}

/**
 * HOC that provides modal lifecycle event methods to coordinate modal state in redux.
 *
 * NOTE: Meant for testing. Should be wrapped in `guidComponent` to provide the `guid` prop normally.
 *
 * @param {*} WrappedComponent The component to wrap.
 * @param {*} isFullscreen Are we rendering a fullscreen modal?
 */
export function configureConnectedModalComponent<TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & ConnectedModalWrappedProps & GuidComponentWrappedProps>,
	isFullscreen = false
) {
	type ConnectedModalProps = TOwnProps & ConnectedModalReduxProps & GuidComponentWrappedProps
	return class ConnectedModalComponent extends Component<ConnectedModalProps, ConnectedModalState> {
		constructor(props: ConnectedModalProps) {
			super(props)
			this.state = {
				isModalOpen: false
			}
		}

		componentWillUnmount() {
			this.onExited()
		}

		onEntering = () => {
			const { guid, modals } = this.props

			// Store each modal's GUID in redux with a zero-based index to determine the order in which they were opened
			const maxModal = maxBy(Object.values(modals), modal => modal.index)
			dispatchAction<ModalEnterAction>({
				type: MODAL_ACTION_TYPE.MODAL_ENTERING,
				guid,
				index: (maxModal?.index ?? -1) + 1,
				isFullscreen
			})
			this.setState({
				isModalOpen: true
			})
		}

		onExited = () => {
			const { guid } = this.props
			const { isModalOpen } = this.state
			if (isModalOpen) {
				dispatchAction<ModalExitAction>({ type: MODAL_ACTION_TYPE.MODAL_EXITED, guid })
			}
			this.setState({
				isModalOpen: false
			})
		}

		render() {
			const { modals, guid, ...ownProps } = this.props

			const topOpenFullscreenModal = maxBy(
				Object.values(modals).filter(m => m.isFullscreen),
				modal => modal.index
			)
			const thisModalIndex = modals[guid]?.index ?? null
			const isTopOpenFullscreenModal = topOpenFullscreenModal?.index === thisModalIndex

			return (
				<WrappedComponent
					{...(ownProps as TOwnProps)}
					guid={guid}
					onEntering={this.onEntering}
					onExited={this.onExited}
					isTopOpenFullscreenModal={isTopOpenFullscreenModal}
				/>
			)
		}
	}
}

/**
 * HOC that provides modal lifecycle event methods to coordinate modal state in redux.
 *
 * Uses `guidComponent`.
 *
 * @param {*} WrappedComponent The component to wrap.
 * @param {*} isFullscreen Are we rendering a fullscreen modal?
 */
export function connectedModalComponent<TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & ConnectedModalWrappedProps & GuidComponentWrappedProps>,
	isFullscreen = false
): ComponentClass<TOwnProps> {
	const component = configureConnectedModalComponent(WrappedComponent, isFullscreen)
	const mapStateToProps = configureMapStateToProps()
	// @ts-ignore: could not match inferred type from the `connect` HOC
	return connect(mapStateToProps)(guidComponent(component))
}

export default connectedModalComponent
