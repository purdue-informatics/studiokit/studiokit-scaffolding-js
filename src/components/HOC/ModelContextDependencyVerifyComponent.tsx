import { Dictionary } from 'lodash'
import React from 'react'
import { Component, ComponentType } from 'react'
import { RouteComponentProps } from 'react-router'
import { Redirect, withRouter } from 'react-router-dom'
import { Model } from '../../types'

export interface ModelContextDependencyVerifyComponentProps {
	model: Model
}

export function setupModelContextDependencyVerifyComponent<
	TOwnProps extends ModelContextDependencyVerifyComponentProps
>(WrappedComponent: ComponentType<TOwnProps>, modelName: string, dependentModelName: string) {
	return class ModelContextVerifyComponent extends Component<TOwnProps & RouteComponentProps> {
		render() {
			const {
				match: { params },
				model
			} = this.props
			const routeParams: Dictionary<string> = params

			const modelId = routeParams[`${modelName}Id`] ? parseInt(routeParams[`${modelName}Id`], 10) : undefined
			const dependentModelId = routeParams[`${dependentModelName}Id`]
				? parseInt(routeParams[`${dependentModelName}Id`], 10)
				: undefined
			if (
				!!modelId &&
				!!dependentModelId &&
				model.id === modelId &&
				(model as any)[`${dependentModelName}Id`] === dependentModelId
			) {
				return <WrappedComponent {...this.props} />
			}
			return (
				<Redirect
					to={{
						pathname: '/error'
					}}
				/>
			)
		}
	}
}

/**
 * HOC that checks if the depender component is related to the dependee component.
 *
 * @param {*} WrappedComponent The component to wrap.
 * @param {string} modelName The modelName of the dependent component itself.
 * @param {string} dependentModelName The modelName that the dependent component depends on.
 */

export default function modelContextDependencyVerifyComponent<
	TOwnProps extends ModelContextDependencyVerifyComponentProps
>(WrappedComponent: ComponentType<TOwnProps>, modelName: string, dependentModelName: string) {
	const component = setupModelContextDependencyVerifyComponent(WrappedComponent, modelName, dependentModelName)
	return withRouter(component)
}
