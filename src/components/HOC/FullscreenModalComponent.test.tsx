import { shallow } from 'enzyme'
import React, { FunctionComponent } from 'react'
import { ConnectedModalWrappedProps } from './ConnectedModalComponent'
import {
	configureFullscreenModalComponent,
	FullscreenModalOwnProps,
	FullscreenModalWrappedProps
} from './FullscreenModalComponent'
import { GuidComponentWrappedProps } from './GuidComponent'

interface TestComponentOwnProps {
	foo: string
}

type TestComponentProps = TestComponentOwnProps & FullscreenModalWrappedProps

const TestComponent: FunctionComponent<TestComponentProps> = props => <div id="testComponent">{props.foo}</div>

// mock all "wrapped" props to simulate being wrapped in guidComponent and connectedModalComponent
const guidComponentProps: GuidComponentWrappedProps = {
	guid: '1'
}
const onEntering = jest.fn()
const onExited = jest.fn()
const connectedModalProps: ConnectedModalWrappedProps = {
	onEntering,
	onExited
}

const setup = (props: TestComponentOwnProps & FullscreenModalOwnProps) => {
	const WrappedComponent = configureFullscreenModalComponent(TestComponent)
	// instead of actually wrapping in `connectedModalComponent`, mock the props for testing
	const allProps: TestComponentOwnProps &
		FullscreenModalOwnProps &
		ConnectedModalWrappedProps &
		GuidComponentWrappedProps = {
		...connectedModalProps,
		...guidComponentProps,
		...props
	}
	return shallow(<WrappedComponent {...allProps} />)
}

describe('FullscreenModalComponent', () => {
	const props: TestComponentOwnProps & FullscreenModalOwnProps = {
		foo: 'bar'
	}
	beforeEach(() => {
		onEntering.mockReset()
		onExited.mockReset()
	})
	describe('componentDidMount', () => {
		it('should be open by default if `props.isOpen` is undefined', () => {
			const wrapper = setup(props)
			expect(wrapper.state('isOpen')).toEqual(true)
		})
		it('should call `props.onEntering` if `props.isOpen` is undefined', () => {
			setup(props)
			expect(onEntering.mock.calls.length).toEqual(1)
		})
		it('should call `props.onEntering` if `props.isOpen` is true', () => {
			setup({ ...props, ...{ isOpen: true } })
			expect(onEntering.mock.calls.length).toEqual(1)
		})
		it('should not call `props.onEntering` if `props.isOpen` is false', () => {
			setup({ ...props, ...{ isOpen: false } })
			expect(onEntering.mock.calls.length).toEqual(0)
		})
	})
	describe('componentDidUpdate', () => {
		it('should call `props.onEntering` if `props.isOpen` is defined, and changes to true', () => {
			const wrapper = setup({ ...props, ...{ isOpen: false } })
			expect(onEntering.mock.calls.length).toEqual(0)
			wrapper.setProps({
				isOpen: true
			})
			expect(onEntering.mock.calls.length).toEqual(1)
		})
		it('should call `props.onEntering` if `props.isOpen` is undefined and `state.isOpen` changes to true', () => {
			const wrapper = setup(props)
			expect(onEntering.mock.calls.length).toEqual(1)
			wrapper.setState({
				isOpen: false
			})
			wrapper.setState({
				isOpen: true
			})
			expect(onEntering.mock.calls.length).toEqual(2)
		})
	})
})
