import { merge } from 'lodash'
import React, { Component, ComponentClass, ComponentType } from 'react'
import { connect } from 'react-redux'
import { SortingRule } from 'react-table'
import { Dispatch } from 'redux'
import { SEARCH_ACTION_TYPE, SearchAction } from '../../redux/actions'
import { BaseReduxState, Search } from '../../types'

export interface SearchPersistorProps<T extends Search = Search> {
	persistSearch: (search?: T) => void
	persistedSearch: T
}

interface SearchPersistorState<T extends Search = Search> {
	search: T | undefined
	defaultSearch: T | undefined
}

export interface SearchPersistorMethods<T extends Search = Search> {
	setSearchDefaults: (doSearch: () => void, search: T) => void
	updateAndPersistSearch: (search?: T, callback?: () => void) => void
	resetSearch: () => void
	handleSearchClick: () => void
	setKeywords: (event: any) => void
	handleKeywordsKeyDown: (event: any) => void
	setQueryAll: (event: any) => void
	setSelectedTab: (selectedTab: number) => void
	setSortingRules: (newSortingRules: SortingRule[]) => void
	setPageSize: (newPageSize: number, newPage: number) => void
	setPage: (newPage: number) => void
}

export interface SearchPersistorWrappedProps<T extends Search = Search>
	extends SearchPersistorProps<T>,
		SearchPersistorMethods<T> {
	search?: T
}

export function configureSearchPersistorComponent<TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & SearchPersistorWrappedProps>,
	disableLocalSearch?: boolean
): ComponentType<TOwnProps & SearchPersistorProps>
export function configureSearchPersistorComponent<TOwnProps extends {}, TSearchType extends Search>(
	WrappedComponent: ComponentType<TOwnProps & SearchPersistorWrappedProps<TSearchType>>,
	disableLocalSearch?: boolean
): ComponentType<TOwnProps & SearchPersistorProps<TSearchType>>
export function configureSearchPersistorComponent<TOwnProps extends {}, TSearchType extends Search>(
	WrappedComponent: ComponentType<TOwnProps & SearchPersistorWrappedProps<TSearchType>>,
	disableLocalSearch?: boolean
) {
	return class SearchPersistorComponent
		extends Component<TOwnProps & SearchPersistorProps<TSearchType>, SearchPersistorState<TSearchType>>
		implements SearchPersistorMethods<TSearchType> {
		constructor(props: TOwnProps & SearchPersistorProps<TSearchType>) {
			super(props)
			this.state = {
				search: undefined,
				defaultSearch: undefined
			}
		}

		// eslint-disable-next-line
		doSearch: () => void = () => {}

		componentDidMount() {
			const { persistedSearch } = this.props
			if (persistedSearch) {
				this.setState({ search: persistedSearch })
			}
		}

		//#region init and state updates

		setSearchDefaults = (doSearch: () => void, search: TSearchType) => {
			this.doSearch = doSearch

			if (this.state.search === undefined) {
				this.setState({ search, defaultSearch: search }, doSearch)
			} else {
				this.setState(
					{
						defaultSearch: search
					},
					doSearch
				)
			}
		}

		updateAndPersistSearch = (search?: Search | TSearchType, callback?: () => void) => {
			this.setState(
				{
					search: Object.assign({}, this.state.search, search)
				},
				() => {
					this.props.persistSearch(this.state.search)
					if (callback) {
						callback()
					}
				}
			)
		}

		//#endregion init and state updates

		//#region handlers

		handleSearchClick = () => {
			const { search } = this.state
			const allNumeric = /^[0-9]+$/
			// keywords should be more than 2 characters unless they are numeric
			if (!!search && !!search.keywords && search.keywords.length < 3 && !allNumeric.exec(search.keywords)) {
				this.updateAndPersistSearch({
					invalidKeywords: true
				})
				return
			}

			this.updateAndPersistSearch(
				{
					hasSearched: true,
					invalidKeywords: false
				},
				this.doSearch
			)
		}

		resetSearch = () => {
			const search = this.state.search ?? ({} as TSearchType)
			this.updateAndPersistSearch(
				// preserve items related to filtering results
				merge({}, this.state.defaultSearch, {
					selectedTab: search.selectedTab,
					sortingRules: search.sortingRules,
					pageSize: search.pageSize,
					pageByTab: search.pageByTab
				}),
				// do not trigger a re-load unless queryAll is true, or local search is disabled
				disableLocalSearch || search.queryAll ? this.doSearch : undefined
			)
		}

		setSelectedTab = (selectedTab: number) => {
			const pageByTab = { ...(this.state.search?.pageByTab ?? {}) }
			// if not set, default page to zero when switching tabs
			if (!pageByTab[selectedTab]) pageByTab[selectedTab] = 0
			this.updateAndPersistSearch({
				selectedTab,
				pageByTab
			})
		}

		setKeywords = (event: any) => {
			this.updateAndPersistSearch({
				keywords: event.target.value
			})
		}

		handleKeywordsKeyDown = (event: any) => {
			if (event.key === 'Enter' && (disableLocalSearch || this.state.search?.queryAll)) {
				this.handleSearchClick()
			}
		}

		setQueryAll = (event: any) => {
			const queryAll = event.target.checked
			this.updateAndPersistSearch(
				{
					queryAll,
					// clear the `requiredMessage` when turning off `queryAll` and local search is enabled
					requiredMessage: !queryAll && !disableLocalSearch ? null : this.state.search?.requiredMessage
				},
				// only automatically trigger re-load when local search is enabled
				!disableLocalSearch ? this.doSearch : undefined
			)
		}

		setSortingRules = (newSortingRules: SortingRule[]) => {
			this.updateAndPersistSearch({
				sortingRules: newSortingRules
			})
		}

		setPageSize = (newPageSize: number, newPage: number) => {
			const pageByTab = { ...(this.state.search?.pageByTab ?? {}) }
			const selectedTab = this.state.search?.selectedTab ?? 1
			pageByTab[selectedTab] = newPage
			this.updateAndPersistSearch({
				pageSize: newPageSize,
				pageByTab
			})
		}

		setPage = (newPage: number) => {
			const pageByTab = { ...(this.state.search?.pageByTab ?? {}) }
			const selectedTab = this.state.search?.selectedTab ?? 1
			pageByTab[selectedTab] = newPage
			this.updateAndPersistSearch({
				pageByTab
			})
		}

		//#endregion handlers

		render() {
			return (
				<WrappedComponent
					search={this.state.search}
					setSearchDefaults={this.setSearchDefaults}
					updateAndPersistSearch={this.updateAndPersistSearch}
					resetSearch={this.resetSearch}
					handleSearchClick={this.handleSearchClick}
					setKeywords={this.setKeywords}
					handleKeywordsKeyDown={this.handleKeywordsKeyDown}
					setQueryAll={this.setQueryAll}
					setSelectedTab={this.setSelectedTab}
					setSortingRules={this.setSortingRules}
					setPageSize={this.setPageSize}
					setPage={this.setPage}
					{...this.props}
				/>
			)
		}
	}
}

export const configureMapStateToProps = <TSearchType extends Search>(key: string) => (
	state: BaseReduxState
): SearchPersistorProps<TSearchType> => {
	const search = state.search[key]
	if (!!search && !!search.date) {
		search.date = new Date(Date.parse(search.date))
	}
	return {
		persistedSearch: search
	} as SearchPersistorProps<TSearchType>
}

export const configureMapDispatchToProps = <TSearchType extends Search>(key: string) => (
	dispatch: Dispatch
): SearchPersistorProps<TSearchType> => {
	let searchDebounce: any
	const dispatchPersistSearch = (search: TSearchType) => {
		dispatch<SearchAction>({
			type: SEARCH_ACTION_TYPE.PERSIST_SEARCH,
			data: {
				[`${key}`]: search
			}
		})
	}
	const persistSearchDebounce = (search: TSearchType) => {
		if (searchDebounce) {
			clearTimeout(searchDebounce)
		}
		searchDebounce = setTimeout(() => dispatchPersistSearch(search), 1000)
	}

	return {
		persistSearch: persistSearchDebounce
	} as SearchPersistorProps<TSearchType>
}

/**
 * HOC to handle persistence and restoration of "Manage" components search parameters.
 * Note: if you store a search date in a child component, make sure it’s called `date`.
 * Rehydration of dates doesn’t work as expected out of the box with redux-persist.
 * Something more generic might be in order in the future.
 *
 * Note: this also uses the `configureX` pattern of declaration for testability.
 *
 * API for child components:
 *  - Call `setSearchDefaults(this.doSearch, this.getDefaultSearch())` in child’s `componentDidMount`
 *  - Implement `getDefaultSearch`
 *  - Call this HOC’s handlers as needed
 *  - Implement any additional child-specific handlers and call this HOC’s `updateAndPersistSearch` in them
 *  - Make sure your `doSearch` and `render` handle undefined search props
 *  - Avoid intoxicating substances and wash your hands regularly
 *
 * @param WrappedComponent The component to wrap
 * @param key The key under "search" in redux where the component’s search params will be stored
 * @param disableLocalSearch By default, `doSearch` is only called at initialization and when `queryAll` is `true`, and searching happens locally (see ManageTable).
 * If `disableLocalSearch` is `true` then the legacy logic is used, where `doSearch` is always called for all searches.
 * @returns The wrapped component, passing down `search` and `persistSearch` as props
 */
export default function searchPersistorComponent<TOwnProps extends SearchPersistorWrappedProps>(
	WrappedComponent: ComponentType<TOwnProps>,
	key: string,
	disableLocalSearch?: boolean
): ComponentClass<Omit<TOwnProps, keyof SearchPersistorWrappedProps>>

/**
 * HOC to handle persistence and restoration of "Manage" components search parameters.
 * Note: if you store a search date in a child component, make sure it’s called `date`.
 * Rehydration of dates doesn’t work as expected out of the box with redux-persist.
 * Something more generic might be in order in the future.
 *
 * Note: this also uses the `configureX` pattern of declaration for testability.
 *
 * API for child components:
 *  - Call `setSearchDefaults(this.doSearch, this.getDefaultSearch())` in child’s `componentDidMount`
 *  - Implement `getDefaultSearch`
 *  - Call this HOC’s handlers as needed
 *  - Implement any additional child-specific handlers and call this HOC’s `updateAndPersistSearch` in them
 *  - Make sure your `doSearch` and `render` handle undefined search props
 *  - Avoid intoxicating substances and wash your hands regularly
 *
 * @param WrappedComponent The component to wrap
 * @param key The key under "search" in redux where the component’s search params will be stored
 * @param disableLocalSearch By default, `doSearch` is only called at initialization and when `queryAll` is `true`, and searching happens locally (see ManageTable).
 * If `disableLocalSearch` is `true` then the legacy logic is used, where `doSearch` is always called for all searches.
 * @returns The wrapped component, passing down `search` and `persistSearch` as props
 */
export default function searchPersistorComponent<
	TOwnProps extends SearchPersistorWrappedProps<TSearchType>,
	TSearchType extends Search
>(
	WrappedComponent: ComponentType<TOwnProps>,
	key: string,
	disableLocalSearch?: boolean
): ComponentClass<Omit<TOwnProps, keyof SearchPersistorWrappedProps<TSearchType>>>
export default function searchPersistorComponent<
	TOwnProps extends SearchPersistorWrappedProps<TSearchType>,
	TSearchType extends Search
>(
	WrappedComponent: ComponentType<TOwnProps>,
	key: string,
	disableLocalSearch?: boolean
): ComponentClass<Omit<TOwnProps, keyof SearchPersistorWrappedProps<TSearchType>>> {
	const SearchPersistorComponent = configureSearchPersistorComponent(WrappedComponent, disableLocalSearch)
	const mapStateToProps = configureMapStateToProps(key)
	const mapDispatchToProps = configureMapDispatchToProps(key)
	// @ts-ignore: could not match inferred type from the `connect` HOC
	return connect(mapStateToProps, mapDispatchToProps)(SearchPersistorComponent)
}
