import { shallow } from 'enzyme'
import React from 'react'
import { BASE_ACTIVITY } from '../../constants'
import { mockGroups, mockUser } from '../../constants/mockData'
import { BaseReduxState, Group, Model, ModelCollection, UserInfo } from '../../types'
import { ActivityOptions, canPerformActivityGlobally, canPerformActivityOnEntity } from '../../utils/baseActivity'
import { ActivityRequiredStateProps, configureActivityRequiredComponent } from './ActivityRequiredComponent'
import { CollectionItemComponentWrappedProps } from './CollectionItemComponent'
import { configureMapStateToProps, GroupRelatedEntity } from './GroupActivityRequiredComponent'

interface Cat extends Model {
	id: number
	groupId: number
	name: string
}

const cats: ModelCollection<Cat> = {
	'1': {
		id: 1,
		groupId: 1,
		name: 'Kitkat'
	},
	'2': {
		id: 2,
		groupId: 2,
		name: 'Chonker'
	}
}

interface TestComponentProps extends CollectionItemComponentWrappedProps<GroupRelatedEntity> {
	foo: string
}

const TestComponent = (props: TestComponentProps) => <div id="testComponent">{props.foo}</div>

const setup = (
	user: { userInfo: UserInfo },
	groups: ModelCollection<Group>,
	ownProps: Partial<TestComponentProps>,
	accessPredicate: (requiredActivity: string, options: ActivityOptions) => boolean,
	requiredActivity: string,
	redirectPath?: string | ((options: ActivityOptions) => string | undefined) | undefined
) => {
	const GroupActivityRequiredComponent = configureActivityRequiredComponent(TestComponent)
	const mapStateToProps = configureMapStateToProps(accessPredicate, requiredActivity, redirectPath)
	const state = {
		models: {
			user,
			groups,
			cats
		}
	}
	const props = {
		...ownProps,
		...mapStateToProps(
			state as BaseReduxState & { models: { cats: ModelCollection<Cat> } },
			ownProps as TestComponentProps
		)
	} as TestComponentProps & ActivityRequiredStateProps
	return shallow<TestComponentProps>(<GroupActivityRequiredComponent {...props} />)
}

describe('GroupActivityRequiredComponent', () => {
	it('should render Redirect if `accessPredicate` returns false', () => {
		const wrapper = setup(mockUser(), mockGroups(), {}, () => false, '')
		expect(wrapper.find('Redirect').length).toEqual(1)
		expect(wrapper.find('Redirect').prop('to')).toEqual('/')
	})
	it('should render Redirect to root path when `redirectPath` is undefined if `accessPredicate` returns false', () => {
		const wrapper = setup(mockUser(), mockGroups(), {}, () => false, '', undefined)
		expect(wrapper.find('Redirect').length).toEqual(1)
		expect(wrapper.find('Redirect').prop('to')).toEqual('/')
	})
	it('should render Redirect with custom `redirectPath` string if `accessPredicate` returns false', () => {
		const wrapper = setup(mockUser(), mockGroups(), {}, () => false, '', '/foo')
		expect(wrapper.find('Redirect').length).toEqual(1)
		expect(wrapper.find('Redirect').prop('to')).toEqual('/foo')
	})
	it('should render Redirect with custom path returned from `redirectPath` function if `accessPredicate` returns false', () => {
		const wrapper = setup(
			mockUser(),
			mockGroups(),
			{},
			() => false,
			'',
			(_options: ActivityOptions) => '/foo'
		)

		expect(wrapper.find('Redirect').length).toEqual(1)
		expect(wrapper.find('Redirect').prop('to')).toEqual('/foo')
	})
	it('should render Redirect with undefined returned from `redirectPath` function if `accessPredicate` returns false', () => {
		const wrapper = setup(
			mockUser(),
			mockGroups(),
			{},
			() => false,
			'',
			(_options: ActivityOptions) => undefined
		)

		expect(wrapper.find('Redirect').length).toEqual(1)
		expect(wrapper.find('Redirect').prop('to')).toEqual('/')
	})
	it('should render WrappedComponent if accessPredicate returns true', () => {
		const wrapper = setup(mockUser(), mockGroups(), {}, () => true, '')
		expect(wrapper.find('TestComponent').length).toEqual(1)
	})
	it('should pass ownProps to WrappedComponent when rendered', () => {
		const wrapper = setup(
			mockUser(),
			mockGroups(),
			{
				foo: 'bar'
			},
			() => true,
			''
		)
		expect(wrapper.props().foo).toEqual('bar')
	})
	it('should render WrappedComponent if user has requiredActivity globally', () => {
		const wrapper = setup(
			mockUser([BASE_ACTIVITY.GROUP_CREATE]),
			mockGroups(),
			{},
			canPerformActivityGlobally,
			BASE_ACTIVITY.GROUP_CREATE
		)
		expect(wrapper.find('TestComponent').length).toEqual(1)
	})
	it('should render WrappedComponent if user has requiredActivity on the group related to the model prop', () => {
		const wrapper = setup(
			mockUser(),
			mockGroups([BASE_ACTIVITY.GROUP_CREATE]),
			{
				model: cats['1'] as GroupRelatedEntity
			},
			canPerformActivityOnEntity,
			BASE_ACTIVITY.GROUP_CREATE
		)
		expect(wrapper.find('TestComponent').length).toEqual(1)
	})
})
