import memoizeOne from 'memoize-one'
import React, { Component, ComponentClass, ComponentType } from 'react'
import { connect } from 'react-redux'
import { RouteComponentProps, withRouter } from 'react-router'
import { MODEL_STATUS } from '../../constants/modelStatus'
import { BaseReduxState, Model, ModelCollection } from '../../types'
import {
	CollectionCommonProps,
	CollectionCommonState,
	CollectionCreateParams,
	CollectionDeleteParams,
	CollectionDerivedProps,
	CollectionLoadParams,
	CollectionMethodConfiguration,
	CollectionMethods,
	CollectionReduxResponse,
	CollectionUpdateParams
} from '../../types/Collection'
import {
	cleanupCollectionGuidKey,
	createItemInCollection,
	deleteItemFromCollection,
	handleCollectionParamsChange,
	initializeCollection,
	loadCollection,
	selectCollectionFromState,
	stopCollectionPeriodicLoad,
	updateItemInCollection
} from '../../utils/collection'
import { getModelArray, handleModelFetchFinish } from '../../utils/model'
import { Loading } from '../Loading'
import guidComponent, { GuidComponentWrappedProps } from './GuidComponent'

/** The props passed into `CollectionComponent` from the user and other HOCs. */
export interface CollectionComponentProps<TModel extends Model>
	extends CollectionCommonProps,
		GuidComponentWrappedProps,
		RouteComponentProps,
		CollectionReduxResponse<TModel> {}

/** The props passed down to the `WrappedComponent`. */
export interface CollectionComponentWrappedProps<TModel extends Model>
	extends Omit<CollectionComponentProps<TModel>, keyof RouteComponentProps>,
		CollectionDerivedProps<TModel>,
		CollectionMethods,
		CollectionCommonState {}

export function configureCollectionComponent<TModel extends Model, TOwnProps extends {}>(
	WrappedComponent: ComponentType<TOwnProps & CollectionComponentWrappedProps<TModel>>,
	LoaderComponent: ComponentType = Loading
) {
	type HocProps = TOwnProps & CollectionComponentProps<TModel>

	return class CollectionComponent extends Component<HocProps, CollectionCommonState> implements CollectionMethods {
		constructor(props: HocProps) {
			super(props)
			this.state = {
				// initializing until model is loaded
				modelStatus: MODEL_STATUS.UNINITIALIZED,
				previousModelStatus: MODEL_STATUS.UNINITIALIZED,
				fetchingId: undefined
			}
		}

		componentDidMount() {
			const { model } = this.props
			initializeCollection(model, this.load, this.getCollectionMethodConfig())
		}

		componentDidUpdate(prevProps: HocProps) {
			const {
				model: prevModel,
				modelName: prevModelName,
				pathParams: prevPathParams,
				queryParams: prevQueryParams
			} = prevProps
			const { model, modelName, pathParams, queryParams, disableAutoLoadOnParamsChange } = this.props
			const { fetchingId } = this.state
			handleModelFetchFinish(model, prevModel, fetchingId, this.changeModelStatus)
			if (!disableAutoLoadOnParamsChange) {
				handleCollectionParamsChange(
					modelName,
					prevModelName,
					pathParams,
					prevPathParams,
					queryParams,
					prevQueryParams,
					this.load
				)
			}
		}

		componentWillUnmount() {
			const { model } = this.props
			cleanupCollectionGuidKey(model, this.getCollectionMethodConfig())
		}

		changeModelStatus = (newModelStatus: MODEL_STATUS, fetchingId?: string | number) => {
			this.setState(prevState => ({
				fetchingId,
				previousModelStatus: prevState.modelStatus,
				modelStatus: newModelStatus
			}))
		}

		getCollectionMethodConfig = () => {
			const { guid, modelName, pathParams, queryParams, disableAutoLoad } = this.props
			const { modelStatus } = this.state
			const collectionMethodConfig: CollectionMethodConfiguration = {
				modelName,
				pathParams,
				queryParams,
				disableAutoLoad,
				guid,
				isInitialized: modelStatus !== MODEL_STATUS.UNINITIALIZED,
				changeModelStatus: this.changeModelStatus
			}
			return collectionMethodConfig
		}

		load = (params: CollectionLoadParams = {}) => {
			loadCollection(this.getCollectionMethodConfig(), params)
		}

		stopPeriodicLoad = (taskId: string) => {
			stopCollectionPeriodicLoad(taskId)
		}

		create = (params: CollectionCreateParams) => {
			createItemInCollection(this.getCollectionMethodConfig(), params)
		}

		update = (params: CollectionUpdateParams) => {
			updateItemInCollection(this.getCollectionMethodConfig(), params)
		}

		delete = (params: CollectionDeleteParams) => {
			deleteItemFromCollection(this.getCollectionMethodConfig(), params)
		}

		getModelArray = memoizeOne((model: ModelCollection<TModel>, guid?: string) => getModelArray(model, guid))

		render() {
			const { history, match, location, staticContext, ...otherProps } = this.props
			const { modelStatus, previousModelStatus, fetchingId } = this.state

			if (modelStatus === MODEL_STATUS.UNINITIALIZED) {
				return <LoaderComponent />
			}

			return (
				<WrappedComponent
					{...(otherProps as TOwnProps & Omit<CollectionComponentProps<TModel>, keyof RouteComponentProps>)}
					modelArray={this.getModelArray(this.props.model, this.props.guid)}
					modelStatus={modelStatus}
					previousModelStatus={previousModelStatus}
					fetchingId={fetchingId}
					load={this.load}
					stopPeriodicLoad={this.stopPeriodicLoad}
					create={this.create}
					update={this.update}
					delete={this.delete}
				/>
			)
		}
	}
}

export const configureMapStateToProps = <
	TModel extends Model,
	TOwnProps extends Omit<CollectionComponentProps<TModel>, keyof CollectionReduxResponse<TModel>>
>(
	modelName: string
) => (state: BaseReduxState, ownProps: TOwnProps & Partial<CollectionComponentProps<TModel>>) => {
	return selectCollectionFromState({
		guid: ownProps.guid,
		modelName: ownProps.modelName || modelName,
		pathParams: ownProps.pathParams,
		routeMatchParams: ownProps.match.params,
		state
	})
}

/**
 * HOC that provides "collection" related functionality, using redux `connect()`, react-router-dom `withRouter()`, and `GuidComponent`.
 *
 * @template T The type of model that is in this collection.
 *
 * @param WrappedComponent The component to wrap.
 * @param modelName The generic path (no Ids) to where the collection is stored in redux.
 * A path relating to an item in defined in `constants/configuration/getEndpointMappings()` (levels separated by a '.').
 * Can override at render time, e.g. `<C modelName="otherModel" />`.
 * @param LoaderComponent Component to use as the Loader. Defaults to `<Loading />`.
 */
export default function collectionComponent<
	TModel extends Model,
	TOwnProps extends CollectionComponentWrappedProps<TModel>
>(
	WrappedComponent: ComponentType<TOwnProps>,
	modelName: string,
	LoaderComponent?: ComponentType
): ComponentClass<Omit<TOwnProps, keyof CollectionComponentWrappedProps<TModel>> & Partial<CollectionCommonProps>> {
	const CollectionComponent = configureCollectionComponent(WrappedComponent, LoaderComponent)
	const mapStateToProps = configureMapStateToProps(modelName)
	// @ts-ignore: could not match inferred type from the `connect` HOC
	return withRouter(guidComponent(connect(mapStateToProps)(CollectionComponent)))
}
