import { shallow } from 'enzyme'
import React from 'react'
import { RouteComponentProps } from 'react-router'
import { Model } from '../../types'
import {
	ModelContextDependencyVerifyComponentProps,
	setupModelContextDependencyVerifyComponent
} from './ModelContextDependencyVerifyComponent'

const defaultModel = {
	id: 1,
	groupId: 2,
	assignmentId: 3
}
const WrappedComponent = (props: ModelContextDependencyVerifyComponentProps) => <div id="wrappedComponent" />

const setup = (model: Model, modelName: string, dependentModelName: string, params = {}) => {
	const mockProps = {
		// collection item
		model,
		// router
		match: { params }
	} as ModelContextDependencyVerifyComponentProps & RouteComponentProps

	const ModelContextVerifyComponent = setupModelContextDependencyVerifyComponent(
		WrappedComponent,
		modelName,
		dependentModelName
	)
	return shallow(<ModelContextVerifyComponent {...mockProps} />)
}

describe('ModelContextDependencyVerifyComponent', () => {
	it('should pass the defaultModel and return the wrappedComponent with correct groupAssignment/group modelNames', () => {
		const model = Object.assign({}, defaultModel)
		// ids are string because it is coming from the match.params in the url
		const params = {
			groupId: '2',
			groupAssignmentId: '1'
		}
		const wrapper = setup(model, 'groupAssignment', 'group', params)
		expect(wrapper.find('WrappedComponent').length).toEqual(1)
		expect(wrapper.find(WrappedComponent).props().model).toEqual(model)
		expect(wrapper.props().match.params).toEqual(params)
	})
	it('should pass the defaultModel and return the wrappedComponent with correct groupAssignment/assignment modelNames', () => {
		const model = Object.assign({}, defaultModel)
		// ids are string because it is coming from the match.params in the url
		const params = {
			assignmentId: '3',
			groupAssignmentId: '1'
		}
		const wrapper = setup(model, 'groupAssignment', 'assignment', params)
		expect(wrapper.find('WrappedComponent').length).toEqual(1)
		expect(wrapper.find(WrappedComponent).props().model).toEqual(model)
		expect(wrapper.props().match.params).toEqual(params)
	})
	it('should not pass if match params is empty', () => {
		const model = Object.assign({}, defaultModel)
		const wrapper = setup(model, 'groupAssignment', 'group')
		expect(wrapper.find('WrappedComponent').length).toEqual(0)
	})
	it('should not pass with no matching dependentModelName id', () => {
		const model = Object.assign({}, defaultModel)
		const params = {
			groupId: '4',
			groupAssignmentId: '1'
		}
		const wrapper = setup(model, 'groupAssignment', 'group', params)
		expect(wrapper.find('WrappedComponent').length).toEqual(0)
	})
	it('should not pass with no matching modelName id', () => {
		const model = Object.assign({}, defaultModel)
		const params = {
			assignmentId: '2',
			groupAssignmentId: '2'
		}
		const wrapper = setup(model, 'groupAssignment', 'group', params)
		expect(wrapper.find('WrappedComponent').length).toEqual(0)
	})
})
