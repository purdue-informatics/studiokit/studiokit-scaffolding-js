import { shallow } from 'enzyme'
import React from 'react'
import { DeepPartial } from 'redux'
import { BASE_ACTIVITY } from '../../constants/baseActivity'
import { mockUser } from '../../constants/mockData'
import { BaseReduxState, Model, ModelCollection, UserInfo } from '../../types'
import {
	configureEntityComponent,
	configureMapStateToProps,
	EntityComponentProps,
	EntityComponentWrappedProps
} from './EntityComponent'

interface EntityComponentTestModel extends Model {
	isDeleted: boolean
}
interface WrappedProps {
	group: EntityComponentTestModel
}

const defaultModel = {
	id: 1,
	isDeleted: false
}
const WrappedComponent = (props: WrappedProps) => <div id="wrappedComponent" />
const WrappedHeader = (props: WrappedProps) => <div id="wrappedHeader" />

const setup = (
	readActivity: string,
	modelName: string,
	propName: keyof WrappedProps,
	user: { userInfo: UserInfo },
	model: EntityComponentTestModel = { isDeleted: false },
	params = {}
) => {
	const mockState = {
		models: {
			user,
			[modelName]: {}
		}
	}

	if (model.id) {
		const stateModels = mockState.models[modelName] as ModelCollection<EntityComponentTestModel>
		stateModels[model.id] = model
	}

	const mockProps = {
		// collection item
		model,
		// router
		match: { params },
		// extra
		someString: 'someValue'
	}

	const EntityComponent = configureEntityComponent(WrappedComponent, WrappedHeader, propName, 'groups')
	const mapStateToProps = configureMapStateToProps(readActivity, modelName)
	const props: DeepPartial<EntityComponentProps<EntityComponentTestModel>> = Object.assign(
		{},
		mockProps,
		mapStateToProps(mockState as BaseReduxState, mockProps as any)
	)
	return shallow(<EntityComponent {...(props as EntityComponentProps<EntityComponentTestModel>)} />)
}

describe('EntityComponent', () => {
	it('should pass the model and modelName to the wrapped components', () => {
		const model = Object.assign({}, defaultModel)
		const wrapper = setup(BASE_ACTIVITY.GROUP_READ, 'groups', 'group', mockUser(), model, {
			groupId: 1
		})
		expect(wrapper.find('WrappedHeader').length).toEqual(1)
		const headerProps = wrapper.find('WrappedHeader').props() as Record<'group', Model>
		expect(headerProps.group).toEqual(model)
		expect(wrapper.find('WrappedComponent').length).toEqual(1)
		const componentProps = wrapper.find('WrappedComponent').props() as EntityComponentWrappedProps<
			EntityComponentTestModel,
			'group'
		>
		expect(componentProps.group).toEqual(model)
		expect(componentProps.modelName).toEqual('groups')

		// Does not pass extra props to wrapped component
		expect((wrapper.find('WrappedComponent').props() as any).someString).toBe(undefined)
	})
	it('should modify the modelName when user has global permission but no entity', () => {
		const wrapper = setup(
			BASE_ACTIVITY.GROUP_READ,
			'groups',
			'group',
			mockUser([BASE_ACTIVITY.GROUP_READ]),
			{ isDeleted: false },
			{ groupId: 1 }
		)
		expect(wrapper.find('WrappedComponent').length).toEqual(1)
		const componentProps = wrapper.find('WrappedComponent').props() as EntityComponentWrappedProps<
			EntityComponentTestModel,
			'group'
		>
		expect(componentProps.modelName).toEqual('search.groups')
	})
	it('should render <Redirect /> if model is deleted', () => {
		const wrapper = setup(
			BASE_ACTIVITY.GROUP_READ,
			'groups',
			'group',
			mockUser([BASE_ACTIVITY.GROUP_READ]),
			{ id: 1, isDeleted: true },
			{ groupId: 1 }
		)
		expect(wrapper.find('WrappedComponent').length).toEqual(0)
		expect(wrapper.find('Redirect').length).toEqual(1)
	})
})
