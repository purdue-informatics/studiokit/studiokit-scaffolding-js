import { ComponentClass, ComponentType } from 'react'
import { connect } from 'react-redux'
import { BaseReduxState, Model } from '../../types'
import { ActivityOptions, defaultOptions } from '../../utils/baseActivity'
import { ActivityRequiredStateProps, configureActivityRequiredComponent } from './ActivityRequiredComponent'
import { CollectionItemComponentWrappedProps } from './CollectionItemComponent'

/**
 * An entity that has a `groupId`
 */
export interface GroupRelatedEntity extends Model {
	groupId: number
}

/**
 * Return `mapStateToProps` function. Add a `hasAccess` boolean property to the component's props
 * checking whether the user has the passed `requiredActivity` for the group referenced by
 * `ownProps.model.groupId`, if any.
 *
 * @param accessPredicate A predicate accepting a required activity and an optional entity and/or userInfo object
 * @param requiredActivity The required activity which is passed to the predicate for evaluation
 * @param redirectPath (Optional) A string or function that provides the redirect path for when `accessPredicate` is false. Defaults to '/'.
 */
export const configureMapStateToProps = <TOwnProps extends CollectionItemComponentWrappedProps<GroupRelatedEntity>>(
	accessPredicate: (requiredActivity: string, options: ActivityOptions) => boolean,
	requiredActivity: string,
	redirectPath?: string | undefined | ((options: ActivityOptions) => string | undefined)
) => (state: BaseReduxState, ownProps?: TOwnProps): ActivityRequiredStateProps => {
	const entity =
		ownProps && ownProps.model && state.models && state.models.groups
			? state.models.groups[ownProps.model.groupId]
			: undefined
	const options = { ...defaultOptions(state, ownProps), ...{ entity } } as ActivityOptions
	return {
		hasAccess: accessPredicate(requiredActivity, options),
		redirectPath: (typeof redirectPath === 'function' ? redirectPath(options) : redirectPath) || '/'
	}
}

/**
 * This HOC ensures that the wrapped component is only rendered if the group referenced by
 * `props.model.groupId`, if any, satisfies the `accessPredicate` for the `requiredActivity`.
 *
 * @param WrappedComponent The component which requires activity/activities in order to render
 * @param accessPredicate A predicate accepting a required activity and an optional entity and/or userInfo object
 * @param requiredActivity The required activity which is passed to the predicate for evaluation
 * @param redirectPath (Optional) A string or function that provides the redirect path for when `accessPredicate` is false. Defaults to '/'.
 */
export default function groupActivityRequiredComponent<
	TOwnProps extends CollectionItemComponentWrappedProps<GroupRelatedEntity>
>(
	WrappedComponent: ComponentType<TOwnProps>,
	accessPredicate: (requiredActivity: string, options: any) => boolean,
	requiredActivity: string,
	redirectPath?: string | undefined | ((options: ActivityOptions) => string | undefined)
): ComponentClass<TOwnProps> {
	const ActivityRequiredComponent = configureActivityRequiredComponent(WrappedComponent)
	const mapStateToProps = configureMapStateToProps(accessPredicate, requiredActivity, redirectPath)
	// @ts-ignore: could not match inferred type from the `connect` HOC
	return connect(mapStateToProps)(ActivityRequiredComponent)
}
