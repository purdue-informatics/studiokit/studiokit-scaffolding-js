import { shallow } from 'enzyme'
import React from 'react'
import { Model, ModelCollection } from '../../types'
import { getModelArray } from '../../utils/model'
import { CollectionComponentWrappedProps } from './CollectionComponent'
import CollectionFirstItemComponent, { CollectionFirstItemComponentWrappedProps } from './CollectionFirstItemComponent'

interface TestModel extends Model {
	id: number
	name: string
}

interface TestComponentProps extends CollectionFirstItemComponentWrappedProps<TestModel> {
	foo: string
}

const TestComponent = (props: TestComponentProps) => (
	<div id="testComponent">
		{props.model._metadata}
		{props.model.id}
		{props.foo}
	</div>
)

const testModels: ModelCollection<TestModel> = {
	'1': {
		id: 1,
		name: 'First'
	},
	'2': {
		id: 2,
		name: 'Second'
	}
}

const setup = () => {
	type EmittedType = Omit<TestComponentProps, keyof CollectionFirstItemComponentWrappedProps<TestModel>> &
		CollectionComponentWrappedProps<TestModel>
	const props: Partial<EmittedType> = {
		foo: 'bar',
		guid: '12345',
		pathParams: [],
		model: testModels,
		modelArray: getModelArray(testModels)
	}
	const NewComponent = CollectionFirstItemComponent(TestComponent)
	// define the interface of the wrapper using the generic types
	return shallow<EmittedType>(<NewComponent {...(props as EmittedType)} />)
}

describe('CollectionFirstItemComponent', () => {
	it('should render with first item as the model', () => {
		const wrapper = setup()
		const target = wrapper.find('TestComponent')
		expect(target.length).toEqual(1)
		expect(target.prop('model')).toEqual(testModels['1'])
	})
})
