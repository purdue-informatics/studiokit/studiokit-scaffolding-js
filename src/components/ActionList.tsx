import {
	Button,
	ClickAwayListener,
	Divider,
	Grow,
	IconButton,
	MenuItem,
	MenuList,
	Paper,
	Popper
} from '@material-ui/core'
import IconMoreVert from '@material-ui/icons/MoreVert'
import React, { FunctionComponent, useState } from 'react'
import { Model } from '../types'

export interface ActionListProps {
	actions?: ListAction[]
	model: Model | null
	className?: string
	/** Even when there is a single ListAction, show the kebab menu */
	alwaysShowKebab?: boolean
}

export interface ListAction {
	predicate: boolean
	action: () => void
	name: string
	icon: JSX.Element
	className: string
	id: string
	isDefault?: boolean
	disabled?: boolean
}

export const ActionList: FunctionComponent<ActionListProps> = ({
	actions = [],
	model = null,
	className,
	alwaysShowKebab
}) => {
	const anchorRef = React.useRef<HTMLButtonElement>(null)
	const [infoMenuAnchorElement, setInfoMenuAnchorElement] = useState<HTMLButtonElement | undefined>(undefined)

	const onActionItemClick = (action?: () => void) => () => {
		setInfoMenuAnchorElement(undefined)
		action?.()
	}

	const handleListKeyDown = (event: React.KeyboardEvent) => {
		if (event.key === 'Tab' || event.key === 'Escape' || event.key === 'Esc') {
			event.preventDefault()
			setInfoMenuAnchorElement(undefined)
			anchorRef.current?.focus()
		}
	}

	let allowedActions = actions.filter((a: ListAction) => a.predicate)
	const actionCount = allowedActions.length
	const defaultAction =
		actionCount === 1 && !alwaysShowKebab ? allowedActions[0] : allowedActions.find(a => !!a.isDefault)

	if (defaultAction) {
		allowedActions = allowedActions.filter(a => a !== defaultAction)
	}

	return (
		<div className={!!defaultAction && !!model && (actionCount > 1 || alwaysShowKebab) ? 'inline-flex' : 'di'}>
			{!!defaultAction && (
				<Button
					color="primary"
					onClick={defaultAction.action}
					id={defaultAction.id}
					className={defaultAction.className || ''}
					disabled={defaultAction.disabled}>
					{defaultAction.icon}
					{defaultAction.name}
				</Button>
			)}
			{!!model && (actionCount > 1 || alwaysShowKebab) && (
				<>
					<Button
						ref={anchorRef}
						onClick={event => setInfoMenuAnchorElement(event.currentTarget)}
						aria-label="Actions"
						aria-haspopup="true"
						aria-controls={infoMenuAnchorElement ? `info-menu-${model.id}` : undefined}
						className={`action-list-text-button${defaultAction ? ' with-default-action' : ''}`}
						color="primary">
						Actions
						<IconMoreVert color="primary" />
					</Button>

					<IconButton
						ref={anchorRef}
						onClick={event => setInfoMenuAnchorElement(event.currentTarget)}
						aria-label="Actions"
						aria-haspopup="true"
						aria-controls={infoMenuAnchorElement ? `info-menu-${model.id}` : undefined}
						className={`action-list-icon-button${defaultAction ? ' with-default-action' : ''}`}>
						<IconMoreVert color="primary" />
					</IconButton>

					<Popper
						open={!!infoMenuAnchorElement}
						anchorEl={infoMenuAnchorElement}
						placement="bottom-end"
						className={`z-1${className ? ` ${className}` : ''}`}
						transition>
						{({ TransitionProps, placement }) => (
							<Grow
								{...TransitionProps}
								style={{ transformOrigin: placement === 'bottom-end' ? 'top right' : 'bottom right' }}>
								<Paper>
									<ClickAwayListener onClickAway={onActionItemClick()}>
										<MenuList
											id={`info-menu-${model.id}`}
											autoFocusItem={!!infoMenuAnchorElement}
											onKeyDown={handleListKeyDown}
											className="action-list shadow-large">
											{allowedActions.map((a, index) =>
												a.predicate
													? [
															...(index > 0 ? [<Divider />] : []),
															<MenuItem
																key={a.id}
																className={`action-list-menu mv1 pv2 ${a.className}`}
																onClick={onActionItemClick(a.action)}
																color="primary"
																disabled={a.disabled}>
																{a.icon}
																{a.name}
															</MenuItem>
													  ]
													: undefined
											)}
										</MenuList>
									</ClickAwayListener>
								</Paper>
							</Grow>
						)}
					</Popper>
				</>
			)}
		</div>
	)
}
