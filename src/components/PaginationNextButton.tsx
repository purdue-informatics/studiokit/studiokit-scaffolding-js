import { Button, ButtonProps } from '@material-ui/core'
import IconKeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight'
import React, { FunctionComponent } from 'react'

export const PaginationNextButton: FunctionComponent<ButtonProps> = props => (
	<Button className="btn-text color-primary w-100" {...props}>
		Next <IconKeyboardArrowRight />
	</Button>
)
