import IconSearch from '@material-ui/icons/Search'
import React from 'react'
import { FormControl, FormGroup } from 'react-bootstrap'
import { Filter } from 'react-table'

export const TextFilter = ({ filter, onChange }: { filter: Filter; onChange: any }) => (
	<FormGroup className="flex table-search-container mb0">
		<IconSearch className="ma1 icon-size-1 v-mid fill-black" />
		<FormControl
			className="table-search-input"
			type="search"
			aria-label="search"
			placeholder="search"
			onChange={(event: any) => {
				onChange(event.target.value)
			}}
			value={filter ? filter.value : ''}
		/>
	</FormGroup>
)

export const textFilterMethod = (filter: Filter, row: any) => {
	const text = row[filter.id]
	if (!text && filter.value) {
		return false
	}
	return text.toLowerCase().includes(filter.value.toLowerCase())
}
