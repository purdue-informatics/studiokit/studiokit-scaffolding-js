import React from 'react'
import { FormControl } from 'react-bootstrap'
import { Filter } from 'react-table'
import { UserRole, UserWithRoles } from '../../types'
import { textForBaseRole } from '../../utils/baseRole'
import { isExternal } from '../../utils/userRole'

export enum ROLE_FILTER {
	ALL = 'ALL',
	MANUAL = 'MANUAL',
	EXTERNAL = 'EXTERNAL'
}

export const RoleFilter = (roles: string[], textForRole?: (role: string) => string, hasExternal?: boolean) => ({
	filter,
	onChange
}: {
	filter: Filter
	onChange: any
}) => {
	// we have nothing to filter if there is only one role and no external userRoles
	if (roles.length === 1 && !hasExternal) {
		return <></>
	}
	return (
		<FormControl
			className="w-100"
			aria-label="Filter column"
			as="select"
			onChange={(event: any) => {
				onChange(event.target.value)
			}}
			value={filter ? filter.value : ROLE_FILTER.ALL}>
			<option value={ROLE_FILTER.ALL}>Show All</option>
			{roles.length > 1 && (
				<>
					<option disabled>──────────</option>
					{roles.map(role => (
						<option key={role} value={role}>
							{textForRole ? textForRole(role) : textForBaseRole(role)}
						</option>
					))}
				</>
			)}
			{hasExternal && (
				<>
					<option disabled>──────────</option>
					<option value={ROLE_FILTER.MANUAL}>Manually Added</option>
					<option value={ROLE_FILTER.EXTERNAL}>Roster Synced</option>
				</>
			)}
		</FormControl>
	)
}

export const roleUserRoleFilterMethod = (filter: Filter, userRole: UserRole) => {
	switch (filter.value) {
		case ROLE_FILTER.ALL:
			return true
		case ROLE_FILTER.MANUAL:
			return !isExternal(userRole)
		case ROLE_FILTER.EXTERNAL:
			return isExternal(userRole)
		default:
			return userRole.role === filter.value
	}
}

export const roleFilterMethod = (filter: Filter, row: any) => {
	const user: UserWithRoles = row.role
	return user.roles.some(r => roleUserRoleFilterMethod(filter, r))
}
