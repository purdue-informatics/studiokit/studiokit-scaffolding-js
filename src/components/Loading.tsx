import React, { FunctionComponent } from 'react'
import { RefreshIndicator, RefreshIndicatorProps } from './RefreshIndicator'

export const Loading: FunctionComponent<RefreshIndicatorProps> = props => {
	return (
		<div className="loading loading-screen">
			<div className="section text-center main-content">
				<RefreshIndicator {...props} />
			</div>
		</div>
	)
}
