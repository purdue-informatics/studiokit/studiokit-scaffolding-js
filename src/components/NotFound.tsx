import { parse } from 'query-string'
import React, { FunctionComponent } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import Helmet from 'react-helmet'
import { useLocation } from 'react-router'

const NotFound: FunctionComponent = () => {
	const { search } = useLocation()
	const parsedSearch = parse(search)
	return (
		<>
			<Helmet title="Page Not Found" />
			<Container>
				<Row>
					<Col md={8}>
						<h1>Not Found</h1>
						<p>
							We could not find the requested page
							{parsedSearch.pathname && ` at "${parsedSearch.pathname}"`}.
						</p>
						<p>
							If you have any questions or concerns, please contact us at{' '}
							<a href="mailto:tlt@purdue.edu">tlt@purdue.edu</a>.
						</p>
					</Col>
				</Row>
			</Container>
		</>
	)
}

// required for use in Routes / AsyncComponent
export default NotFound
