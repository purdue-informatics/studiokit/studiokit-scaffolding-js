import { Button, ButtonProps } from '@material-ui/core'
import IconKeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft'
import React, { FunctionComponent } from 'react'

export const PaginationPreviousButton: FunctionComponent<ButtonProps> = props => (
	<Button className="btn-text color-primary w-100" {...props}>
		<IconKeyboardArrowLeft /> Previous
	</Button>
)
