import IconCheckCircle from '@material-ui/icons/CheckCircle'
import IconError from '@material-ui/icons/Error'
import IconInfo from '@material-ui/icons/Info'
import IconWarning from '@material-ui/icons/Warning'
import React, { FunctionComponent } from 'react'
import { Alert, AlertProps } from 'react-bootstrap'

function getIconForVariant(variant: string, iconClassName?: string) {
	const combinedIconClassName = `self-start mr2 ${iconClassName ? ` ${iconClassName}` : ''}`
	switch (variant) {
		case 'success':
			return <IconCheckCircle className={combinedIconClassName} />
		case 'info':
			return <IconInfo className={combinedIconClassName} />
		case 'warning':
			return <IconWarning className={combinedIconClassName} />
		case 'danger':
			return <IconError className={combinedIconClassName} />
		default:
			return null
	}
}

export interface AlertWithIconProps extends AlertProps {
	iconClassName?: string
}

export const AlertWithIcon: FunctionComponent<AlertWithIconProps> = props => {
	const { iconClassName, className, children, variant, ...otherProps } = props
	const combinedClassName = `alert-with-icon${className ? ` ${className}` : ''}`
	const Icon = getIconForVariant(variant as string, iconClassName)
	return (
		<Alert className={combinedClassName} variant={variant} {...otherProps}>
			{Icon}
			{props.children}
		</Alert>
	)
}
