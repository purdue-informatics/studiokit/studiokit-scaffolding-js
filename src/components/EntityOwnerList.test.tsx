import { shallow } from 'enzyme'
import React from 'react'
import { BASE_ROLE } from '../constants/baseRole'
import { defaultAdminUserRoles } from '../constants/mockData'
import { EntityOwnerList } from './EntityOwnerList'

function setup(canReadAnyUserGlobally = false) {
	const mockProps = {
		owners: defaultAdminUserRoles.map(a => Object.assign({}, a, { roles: [BASE_ROLE.GROUP_OWNER] })),
		canReadAnyUserGlobally
	}
	return shallow(<EntityOwnerList {...mockProps} />)
}

describe('Entity Owner List', () => {
	it('displays multiple owners', () => {
		const wrapper = setup(true)
		const owners = wrapper.find('.owner')
		expect(owners.length).toEqual(2)

		expect(owners.at(0).find('strong').text()).toBe('Joe Schmo')
		expect(owners.at(1).find('strong').text()).toBe('carlysimon@clouds.in.coffee')
	})
	it('display as links with global UserReadAny', () => {
		const wrapper = setup(true)
		expect(wrapper.find('.ownerLink').length).toEqual(2)
	})
	it('do not display as links without UserReadAny', () => {
		const wrapper = setup(false)
		expect(wrapper.find('.ownerLink').length).toEqual(0)
	})
})
