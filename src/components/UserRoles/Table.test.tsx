import { shallow } from 'enzyme'
import React from 'react'
import { BASE_ROLE } from '../../constants/baseRole'
import { defaultGroupUserRoles } from '../../constants/mockData'
import { GroupUserRole } from '../../types'
import { convertToUserWithRoles } from '../../utils/userRole'
import { UserRolesContextType } from './Context'
import { UserRolesTable, UserRolesTableProps } from './Table'

const setup = (props?: Partial<UserRolesTableProps>, contextValue?: Partial<UserRolesContextType>) => {
	const mockProps: UserRolesTableProps = Object.assign(
		{
			users: defaultGroupUserRoles.map(gur => convertToUserWithRoles<GroupUserRole>(gur))
		},
		props
	)

	const mockContextValue: UserRolesContextType = Object.assign(
		{
			roles: [BASE_ROLE.GROUP_OWNER, BASE_ROLE.GROUP_GRADER, BASE_ROLE.GROUP_LEARNER],
			requiredRole: BASE_ROLE.GROUP_OWNER,
			isUpdating: false,
			updateUserRole: jest.fn(),
			isRemoving: false,
			removeUserRole: jest.fn()
		},
		contextValue
	)

	jest.spyOn(React, 'useContext').mockImplementation(() => mockContextValue)

	return shallow(<UserRolesTable {...mockProps} />)
}

describe('UserRolesTable', () => {
	it('renders', () => {
		const wrapper = setup()
		const table = wrapper.find('ReactTable')
		expect(table.length).toEqual(1)
	})
	it('hides Role col without roles', () => {
		const wrapper = setup(undefined, {
			roles: []
		})
		const columns: any = wrapper.find('ReactTable').prop('columns')
		expect(columns.length).toEqual(3)
	})
	it('shows Role col with roles', () => {
		const wrapper = setup()
		const columns: any = wrapper.find('ReactTable').prop('columns')
		expect(columns.length).toEqual(4)
	})
})
