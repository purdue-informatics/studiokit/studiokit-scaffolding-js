import { IconButton } from '@material-ui/core'
import IconDelete from '@material-ui/icons/Delete'
import React, { FunctionComponent, useContext } from 'react'
import { OverlayTrigger, Popover } from 'react-bootstrap'
import { useSelector } from 'react-redux'
import { BaseReduxState } from '../../types/BaseReduxState'
import { UserWithRoles } from '../../types/UserRole'
import { getRoleText, getUserId, isExternal, sortByRole } from '../../utils/userRole'
import { IconExternalUser } from '../Icons/IconExternalUser'
import { RefreshIndicatorInline } from '../RefreshIndicator/Inline'
import { UserRolesContext } from './Context'
import { RoleSelectControl } from './Select'

export interface RoleCellProps {
	user: UserWithRoles
	requiredRoleCount?: number
}

export const RoleCell: FunctionComponent<RoleCellProps> = ({ user, requiredRoleCount }) => {
	const {
		isUpdateDisabled,
		isDeleteDisabled,
		canModify,
		canDeleteSelf,
		allowMultipleRoles,
		roles,
		requiredRole,
		textForRole,
		userRoleToUpdate,
		isUpdating,
		updateUserRole,
		userRoleToRemove,
		isRemoving,
		removeUserRole
	} = useContext(UserRolesContext)

	const currentUserId = useSelector((state: BaseReduxState) => {
		if (!state.models.user || !state.models.user.userInfo) {
			throw new Error('Current user id is not stored in redux')
		}
		return state.models.user.userInfo.id
	})

	return (
		<ul className="mb0 list pa0 tr">
			{user.roles.sort(sortByRole(roles)).map(userRole => {
				const roleText = getRoleText(textForRole)(userRole)
				const isUserRoleExternal = isExternal(userRole)
				const userId = getUserId(userRole)

				const canUpdate =
					// current user has modify access
					canModify &&
					// in single role mode (multiple roles are not allowed)
					!allowMultipleRoles &&
					// the userRole is not external (roster synced)
					!isUserRoleExternal &&
					// update is not disabled
					!isUpdateDisabled &&
					// there is more than one possible role
					roles.length > 1 &&
					// the user role is not the last remaining required role user
					(!requiredRole || userRole.role !== requiredRole || !requiredRoleCount || requiredRoleCount > 1)

				const canDelete =
					// the userRole is not external (roster synced)
					!isUserRoleExternal &&
					// delete is not disabled, e.g. course is ended
					!isDeleteDisabled &&
					// the role is not the last required role
					(!requiredRole || userRole.role !== requiredRole || !requiredRoleCount || requiredRoleCount > 1) &&
					// the current user can delete the role for another user, or the current user can delete themselves
					((canModify && user.id !== currentUserId) || (canDeleteSelf && user.id === currentUserId))

				const shouldDisplayRightAligned =
					// in multiple role mode
					allowMultipleRoles ||
					// only one role option
					roles.length === 1 ||
					// no access to update
					!canModify ||
					// update is disabled
					isUpdateDisabled

				const isExternalPopover = (
					<Popover id={`is-external-popover-${userId}-${userRole.role}`}>
						<h3>Added via Roster Sync</h3>
						<p className="mb2">
							This person was added automatically via roster sync and cannot be manually removed.
						</p>
					</Popover>
				)

				const isUserRoleUpdating = isUpdating && userRoleToUpdate?.id === userRole.id
				const isUserRoleRemoving = isRemoving && userRoleToRemove?.id === userRole.id

				return (
					<li
						key={`${user.id}-${roleText}`}
						className={`nowrap flex items-center${shouldDisplayRightAligned ? ' justify-end' : ''}`}>
						{isUserRoleUpdating ? (
							<div className="flex justify-start flex-grow-1 pl3">
								<RefreshIndicatorInline size={20} />
							</div>
						) : canUpdate ? (
							<RoleSelectControl
								options={roles}
								value={userRole.role}
								onChange={(role: string) => {
									updateUserRole(userRole, role)
								}}
								textForRole={textForRole}
								disabled={isUpdating || isRemoving}
							/>
						) : (
							<span className={shouldDisplayRightAligned ? '' : 'pl3'}>{roleText}</span>
						)}

						{isUserRoleRemoving ? (
							<div className="flex items-center">
								<RefreshIndicatorInline size={20} className="ma3" />
							</div>
						) : canDelete ? (
							<IconButton
								className="remove-user-button"
								aria-label={`Remove ${roleText}`}
								onClick={() => {
									removeUserRole(userRole)
								}}
								disabled={isUpdating || isRemoving}>
								<IconDelete color="error" />
							</IconButton>
						) : isUserRoleExternal ? (
							<OverlayTrigger
								placement="auto"
								trigger={['click', 'hover', 'focus']}
								overlay={isExternalPopover}>
								<IconExternalUser
									tabIndex={0}
									style={{ margin: '12px', opacity: 0.5 }}
									className="external-icon"
								/>
							</OverlayTrigger>
						) : (
							// dummy spacer to keep items aligned
							<span
								style={{
									margin: '12px',
									width: '24px',
									height: '24px',
									display: 'inline-block',
									verticalAlign: 'middle'
								}}></span>
						)}
					</li>
				)
			})}
		</ul>
	)
}
