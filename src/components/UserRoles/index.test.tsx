import { shallow, ShallowWrapper } from 'enzyme'
import React from 'react'
import { BASE_ACTIVITY } from '../../constants/baseActivity'
import { BASE_ROLE } from '../../constants/baseRole'
import {
	defaultAdminUserRoles,
	defaultGroupLearner,
	defaultGroupOwner,
	defaultGroupUserRoles,
	defaultNamedGroupLearner,
	defaultNamelessGroupLearner,
	mockGroups,
	mockUser
} from '../../constants/mockData'
import { MODEL_STATUS } from '../../constants/modelStatus'
import { setEndpointMappings } from '../../redux/sagas/modelFetchSaga'
import { noStoreHooks } from '../../redux/sagas/noStoreSaga'
import {
	BaseReduxState,
	Model,
	ModelCollection,
	RoleDescriptions,
	UserInfo,
	UserRole,
	UserWithRoles
} from '../../types'
import { filterManualGroupUsers } from '../../utils/groupRoles'
import { filterUsersByRole } from '../../utils/user'
import { convertToUserWithRoles } from '../../utils/userRole'
import { CollectionComponentWrappedProps } from '../HOC/CollectionComponent'
import { mapStateToProps, UserRoles, UserRolesOwnProps, UserRolesProps, UserRolesReduxProps } from './index'

jest.mock('../../redux/actionCreator')

noStoreHooks.registerNoStoreActionHook = jest.fn((_key, hook) => {
	hook(null)
})

jest.mock('../../utils/shard', () => {
	return {
		isPurdueShard: jest.fn().mockImplementation(() => true)
	}
})

const onAdd = jest.fn()
const onUpdate = jest.fn()
const onRemove = jest.fn()

type SetupFunction = (
	userInfo: UserInfo,
	entities: Model[],
	role: string,
	userRoles?: UserRole[]
) => ShallowWrapper<UserRolesProps>

const setupAdmins: SetupFunction = (
	userInfo: UserInfo,
	_entities: Model[],
	role: string,
	adminUserRoles = defaultAdminUserRoles
) => {
	return setup(
		userInfo,
		'userRoles',
		adminUserRoles,
		BASE_ACTIVITY.USER_ROLE_MODIFY_ANY,
		BASE_ACTIVITY.USER_ROLE_MODIFY_ANY,
		role,
		{
			[role]: 'some description'
		},
		undefined,
		undefined,
		filterUsersByRole(role)
	)
}
const setupGroup: SetupFunction = (
	userInfo: UserInfo,
	groups: Model[],
	role: string,
	groupUserRoles = defaultGroupUserRoles as UserRole[]
) => {
	return setup(
		userInfo,
		'groups.groupUserRoles',
		groupUserRoles,
		BASE_ACTIVITY.GROUP_USER_ROLE_MODIFY,
		BASE_ACTIVITY.GROUP_USER_ROLE_MODIFY,
		role,
		{
			[BASE_ROLE.GROUP_GRADER]: 'something',
			[BASE_ROLE.GROUP_LEARNER]: 'something',
			[BASE_ROLE.GROUP_OWNER]: 'something'
		},
		groups,
		'course',
		filterManualGroupUsers
	)
}

function setup(
	userInfo: UserInfo,
	modelName: string,
	userRoles: UserRole[],
	modifyUserRoleActivityName: string,
	deleteOwnUserRoleActivityName: string,
	role: string,
	roleDescriptions: RoleDescriptions,
	entities?: Model[],
	entityName?: string,
	filterUsers?: (users: UserWithRoles[]) => UserWithRoles[]
) {
	setEndpointMappings({
		userRoles: {
			_config: {
				isCollection: true
			}
		},
		groups: {
			_config: {
				isCollection: true
			},
			groupUserRoles: {
				_config: {
					isCollection: true
				}
			}
		}
	})
	const mockState = {
		models: {
			user: {
				userInfo
			}
		}
	} as Partial<BaseReduxState>
	const wrappedProps = {
		load: jest.fn(),
		delete: jest.fn(),
		create: jest.fn(),
		update: jest.fn(),
		model: userRoles.reduce((retval: ModelCollection<UserRole>, userRole) => {
			retval[userRole.id] = userRole
			return retval
		}, {}),
		modelArray: userRoles,
		modelName,
		modelStatus: MODEL_STATUS.READY,
		pathParams: entityName ? [1] : undefined
	} as Partial<CollectionComponentWrappedProps<UserRole>>

	const mockProps = {
		canModify: true,
		modifyUserRoleActivityName,
		deleteOwnUserRoleActivityName,
		defaultRole: role,
		requiredRole: role,
		roleDescriptions,
		entity: !!entityName && !!entities ? entities[1] : undefined,
		entityName,
		filterUsers
	} as UserRolesProps

	const ownProps = Object.assign(
		{
			onAdd,
			onUpdate,
			onRemove
		},
		mockProps,
		wrappedProps
	) as UserRolesOwnProps

	const props = Object.assign(
		{},
		ownProps,
		mapStateToProps(
			mockState as BaseReduxState,
			ownProps as UserRolesProps & CollectionComponentWrappedProps<UserWithRoles>
		)
	) as UserRolesProps & UserRolesReduxProps & CollectionComponentWrappedProps<UserWithRoles>

	return shallow<UserRolesProps>(<UserRoles {...props} />)
}

function runTests(
	setupFunc: SetupFunction,
	mockEntities: any,
	modifyUserRoleActivityName: string,
	deleteOwnUserRoleActivityName: string,
	entityName: string | undefined,
	defaultRole: string
) {
	describe('Redux Props', () => {
		describe('canModify', () => {
			it(`is true for user with global ${modifyUserRoleActivityName}`, () => {
				const wrapper = setupFunc(
					mockUser([modifyUserRoleActivityName]).userInfo,
					mockEntities ? mockEntities() : undefined,
					defaultRole
				)
				expect((wrapper.instance().props as UserRolesProps).canModify).toEqual(true)
			})
			if (mockEntities) {
				it(`is true for user with entity ${modifyUserRoleActivityName}`, () => {
					const wrapper = setupFunc(
						mockUser().userInfo,
						mockEntities([modifyUserRoleActivityName]),
						defaultRole
					)
					expect((wrapper.instance().props as UserRolesProps).canModify).toEqual(true)
				})
			}
			it(`is false for user without global or entity ${modifyUserRoleActivityName}`, () => {
				const wrapper = setupFunc(mockUser().userInfo, mockEntities ? mockEntities() : undefined, defaultRole)
				expect((wrapper.instance().props as UserRolesProps).canModify).toEqual(false)
			})
		})
		describe('canDeleteSelf', () => {
			it(`is true for user with global ${modifyUserRoleActivityName}`, () => {
				const wrapper = setupFunc(
					mockUser([modifyUserRoleActivityName]).userInfo,
					mockEntities ? mockEntities() : undefined,
					defaultRole
				)
				expect((wrapper.instance().props as UserRolesProps).canDeleteSelf).toEqual(true)
			})
			if (mockEntities) {
				it(`is true for user with entity ${modifyUserRoleActivityName}`, () => {
					const wrapper = setupFunc(
						mockUser().userInfo,
						mockEntities([modifyUserRoleActivityName]),
						defaultRole
					)
					expect((wrapper.instance().props as UserRolesProps).canDeleteSelf).toEqual(true)
				})
				it(`is true for user with entity ${deleteOwnUserRoleActivityName}`, () => {
					const wrapper = setupFunc(
						mockUser().userInfo,
						mockEntities([deleteOwnUserRoleActivityName]),
						defaultRole
					)
					expect((wrapper.instance().props as UserRolesProps).canDeleteSelf).toEqual(true)
				})
			}
			it(`is false for user without global or entity ${modifyUserRoleActivityName} or ${deleteOwnUserRoleActivityName}`, () => {
				const wrapper = setupFunc(mockUser().userInfo, mockEntities ? mockEntities() : undefined, defaultRole)
				expect((wrapper.instance().props as UserRolesProps).canDeleteSelf).toEqual(false)
			})
		})
	})
	describe('UserRolesAdd', () => {
		it(`is visible for user with global ${modifyUserRoleActivityName}`, () => {
			const wrapper = setupFunc(
				mockUser([modifyUserRoleActivityName]).userInfo,
				mockEntities ? mockEntities() : undefined,
				defaultRole
			)
			expect(wrapper.find({ id: 'entityUserRolesAdd' }).length).toEqual(1)
		})
		if (mockEntities) {
			it(`is visible for user with ${modifyUserRoleActivityName} on ${entityName}`, () => {
				const wrapper = setupFunc(mockUser().userInfo, mockEntities([modifyUserRoleActivityName]), defaultRole)
				expect(wrapper.find({ id: 'entityUserRolesAdd' }).length).toEqual(1)
			})
		}
		it(`is invisible for user without ${modifyUserRoleActivityName}`, () => {
			const wrapper = setupFunc(mockUser().userInfo, mockEntities ? mockEntities() : undefined, defaultRole)
			expect(wrapper.find({ id: 'entityUserRolesAdd' }).length).toEqual(0)
		})
	})
	describe('UserRolesTable', () => {
		it('is not displayed without users', () => {
			const wrapper = setupFunc(
				mockUser([modifyUserRoleActivityName]).userInfo,
				mockEntities ? mockEntities() : undefined,
				defaultRole,
				[]
			)
			expect(wrapper.find({ id: 'entityUserRolesTable' }).length).toEqual(0)
		})
	})
	describe('functions', () => {
		let wrapper: any
		let instance: UserRoles
		beforeEach(() => {
			wrapper = setupFunc(
				mockUser([modifyUserRoleActivityName]).userInfo,
				mockEntities ? mockEntities() : undefined,
				defaultRole
			)
			instance = wrapper.instance()
		})
		describe('componentDidUpdate', () => {
			it('should update state if props.modelArray changes', () => {
				const spy = jest.spyOn(instance, 'setStateFromProps')
				wrapper.setProps({
					modelArray: [defaultGroupLearner, defaultGroupOwner]
				})
				expect(spy).toHaveBeenCalledTimes(1)
				if (defaultRole === BASE_ROLE.ADMIN) {
					// Note: In the admin users case, we filter out anyone who is not an admin from the list
					expect(wrapper.state('sortedUsers')).toEqual([])
				} else {
					expect(wrapper.state('sortedUsers')).toEqual([
						convertToUserWithRoles(defaultGroupOwner),
						convertToUserWithRoles(defaultGroupLearner)
					])
				}
			})

			it('should set failMessage on load error', () => {
				wrapper.setProps({
					modelStatus: MODEL_STATUS.LOADING
				})
				wrapper.setProps({
					modelStatus: MODEL_STATUS.ERROR
				})
				expect(wrapper.state('failMessage')).toEqual(
					`Oops! There was an error loading${
						entityName ? ` the people for your ${entityName}` : ''
					}.\r\nPlease try again.`
				)
			})
			it('should call didRemove after delete success', () => {
				const spy = jest.spyOn(instance, 'didRemove')
				instance.alertRemoveUserRole(defaultGroupLearner)
				instance.removeUserRole(true)
				wrapper.setProps({
					modelStatus: MODEL_STATUS.DELETING
				})
				wrapper.setProps({
					modelStatus: MODEL_STATUS.READY
				})
				expect(spy).toHaveBeenCalledWith(true)
				expect(wrapper.state('userRoleToRemove')).toBeUndefined()
			})
			it('should call didRemove after delete fail', () => {
				const spy = jest.spyOn(instance, 'didRemove')
				instance.alertRemoveUserRole(defaultGroupLearner)
				instance.removeUserRole(true)
				wrapper.setProps({
					modelStatus: MODEL_STATUS.DELETING
				})
				wrapper.setProps({
					modelStatus: MODEL_STATUS.ERROR
				})
				expect(spy).toHaveBeenCalledWith(false)
				expect(wrapper.state('userRoleToRemove')).toBeUndefined()
			})
		})
		describe('addUserRoles', () => {
			it('should call didAdd from hook', () => {
				const spy = jest.spyOn(instance, 'didAdd')
				instance.addUserRoles(['wgrauvog'], BASE_ROLE.GROUP_OWNER)
				expect(spy).toHaveBeenCalled()
			})
		})
		describe('didAdd', () => {
			it('should throw if no `roleForAdd` in state', () => {
				expect(() => {
					instance.didAdd(null)
				}).toThrowError('didAdd was called in the incorrect state')
				expect(onAdd).toHaveBeenCalledTimes(0)
			})
			it('should throw if no `identifierToAdd` in state', () => {
				expect(() => {
					wrapper.setState({
						roleForAdd: BASE_ROLE.GROUP_LEARNER
					})
					instance.didAdd(null)
				}).toThrowError('didAdd was called in the incorrect state')
				expect(onAdd).toHaveBeenCalledTimes(0)
			})
			it('sets failMessage if no data, and shows alert', () => {
				wrapper.setState({
					roleForAdd: BASE_ROLE.GROUP_OWNER,
					identifiersToAdd: ['wgrauvog']
				})
				instance.didAdd(null)
				expect(wrapper.state('failMessage')).toEqual(
					`The following identifiers were not added${entityName ? ` to your ${entityName}` : ''}:\r\nwgrauvog`
				)
				expect(wrapper.find({ id: 'failMessageAlert' }).length).toEqual(1)
				expect(onAdd).toHaveBeenCalledTimes(0)
			})
			it('sets success message, shows alert', () => {
				wrapper.setState({
					roleForAdd: BASE_ROLE.GROUP_OWNER,
					identifiersToAdd: [defaultGroupOwner.uid]
				})
				instance.didAdd({
					addedUserRoles: [defaultGroupOwner],
					existingUserRoles: [],
					blockedUserRoles: [],
					invalidIdentifiers: [],
					allowedDomains: 'domain',
					invalidDomainIdentifiers: []
				})
				expect(wrapper.state('successMessage')).toEqual(
					`The following instructor was successfully added${entityName ? ` to your ${entityName}` : ''}:\r\n${
						defaultGroupOwner.firstName
					} ${defaultGroupOwner.lastName} (${defaultGroupOwner.uid})`
				)
				expect(wrapper.find({ id: 'successMessageAlert' }).length).toEqual(1)
				expect(onAdd).toHaveBeenCalledWith([defaultGroupOwner])
				expect(onAdd).toHaveBeenCalledTimes(1)
			})
			it('sets success message for multiple, shows alert', () => {
				wrapper.setState({
					roleForAdd: BASE_ROLE.GROUP_OWNER,
					identifiersToAdd: [defaultNamelessGroupLearner.uid, defaultNamedGroupLearner.uid]
				})
				instance.didAdd({
					addedUserRoles: [defaultNamelessGroupLearner, defaultNamedGroupLearner], // these aren't actually GroupOwners, but it works
					existingUserRoles: [],
					blockedUserRoles: [],
					invalidIdentifiers: [],
					allowedDomains: 'domain',
					invalidDomainIdentifiers: []
				})
				expect(wrapper.state('successMessage')).toEqual(
					`The following instructors were successfully added${
						entityName ? ` to your ${entityName}` : ''
					}:\r\n${defaultNamelessGroupLearner.email} (${defaultNamelessGroupLearner.uid})\r\n${
						defaultNamedGroupLearner.firstName
					} ${defaultNamedGroupLearner.lastName} (${defaultNamedGroupLearner.uid})`
				)
				expect(wrapper.find({ id: 'successMessageAlert' }).length).toEqual(1)
				expect(onAdd).toHaveBeenCalledWith([defaultNamelessGroupLearner, defaultNamedGroupLearner])
				expect(onAdd).toHaveBeenCalledTimes(1)
			})
			it('sets existing message, shows alert', () => {
				wrapper.setState({
					roleForAdd: BASE_ROLE.GROUP_OWNER,
					identifiersToAdd: [defaultNamedGroupLearner.uid, 'x']
				})
				instance.didAdd({
					addedUserRoles: [],
					existingUserRoles: [defaultNamedGroupLearner], // these aren't actually GroupOwners, but it works
					blockedUserRoles: [],
					invalidIdentifiers: ['x'],
					allowedDomains: 'domain',
					invalidDomainIdentifiers: []
				})
				expect(wrapper.state('existingMessage')).toEqual(
					`The following person was already ${
						entityName ? `in your ${entityName} as an ` : 'an '
					}instructor:\r\n${defaultNamedGroupLearner.firstName} ${defaultNamedGroupLearner.lastName} (${
						defaultNamedGroupLearner.uid
					})`
				)
				expect(wrapper.find({ id: 'existingMessageAlert' }).length).toEqual(1)
				expect(onAdd).toHaveBeenCalledTimes(0)
			})
			it('sets existing message for multiple, shows alert', () => {
				wrapper.setState({
					roleForAdd: BASE_ROLE.GROUP_OWNER,
					identifiersToAdd: [defaultNamedGroupLearner.uid, defaultNamelessGroupLearner.uid, 'x']
				})
				instance.didAdd({
					addedUserRoles: [],
					existingUserRoles: [defaultNamedGroupLearner, defaultNamelessGroupLearner], // these aren't actually GroupOwners, but it works
					blockedUserRoles: [],
					invalidIdentifiers: ['x'],
					allowedDomains: 'domain',
					invalidDomainIdentifiers: []
				})
				expect(wrapper.state('existingMessage')).toEqual(
					`The following people were already${
						entityName ? ` in your ${entityName} as` : ''
					} instructors:\r\n${defaultNamedGroupLearner.firstName} ${defaultNamedGroupLearner.lastName} (${
						defaultNamedGroupLearner.uid
					})\r\n${defaultNamelessGroupLearner.email} (${defaultNamelessGroupLearner.uid})`
				)
				expect(wrapper.find({ id: 'existingMessageAlert' }).length).toEqual(1)
				expect(onAdd).toHaveBeenCalledTimes(0)
			})
			it('sets existing message, shows alert without uid', () => {
				wrapper.setState({
					roleForAdd: BASE_ROLE.GROUP_LEARNER,
					identifiersToAdd: ['jase@purdue.edu', 'x']
				})
				instance.didAdd({
					addedUserRoles: [],
					existingUserRoles: [
						{
							id: '2',
							firstName: null,
							lastName: null,
							email: 'jase@purdue.edu',
							uid: null,
							role: BASE_ROLE.GROUP_LEARNER
						}
					],
					blockedUserRoles: [],
					invalidIdentifiers: ['x'],
					allowedDomains: 'domain',
					invalidDomainIdentifiers: []
				})
				expect(wrapper.state('existingMessage')).toEqual(
					`The following person was already ${
						entityName ? `in your ${entityName} as a ` : 'a '
					}student:\r\njase@purdue.edu`
				)
				expect(wrapper.find({ id: 'existingMessageAlert' }).length).toEqual(1)
				expect(onAdd).toHaveBeenCalledTimes(0)
			})
			it('sets blocked message, shows alert', () => {
				wrapper.setState({
					roleForAdd: BASE_ROLE.GROUP_OWNER,
					identifiersToAdd: [defaultGroupOwner.uid]
				})
				instance.didAdd({
					addedUserRoles: [],
					existingUserRoles: [],
					blockedUserRoles: [defaultGroupOwner],
					invalidIdentifiers: [],
					allowedDomains: 'domain',
					invalidDomainIdentifiers: []
				})
				expect(wrapper.state('blockedMessage')).toEqual(
					`The following person could not be added ${
						entityName ? `to your ${entityName}` : ''
					} as an instructor because they already have a role:\r\n${defaultGroupOwner.firstName} ${
						defaultGroupOwner.lastName
					} (${defaultGroupOwner.uid})`
				)
				expect(wrapper.find({ id: 'blockedMessageAlert' }).length).toEqual(1)
				expect(onAdd).toHaveBeenCalledTimes(0)
			})
			it('sets blocked message for multiple, shows alert', () => {
				wrapper.setState({
					roleForAdd: BASE_ROLE.GROUP_LEARNER,
					identifiersToAdd: [defaultNamedGroupLearner.uid, defaultNamelessGroupLearner.uid]
				})
				instance.didAdd({
					addedUserRoles: [],
					existingUserRoles: [],
					blockedUserRoles: [defaultNamedGroupLearner, defaultNamelessGroupLearner],
					invalidIdentifiers: [],
					allowedDomains: 'domain',
					invalidDomainIdentifiers: []
				})
				expect(wrapper.state('blockedMessage')).toEqual(
					`The following people could not be added ${
						entityName ? `to your ${entityName}` : ''
					} as students because they already have a role:\r\n${defaultNamedGroupLearner.firstName} ${
						defaultNamedGroupLearner.lastName
					} (${defaultNamedGroupLearner.uid})\r\n${defaultNamelessGroupLearner.email} (${
						defaultNamelessGroupLearner.uid
					})`
				)
				expect(wrapper.find({ id: 'blockedMessageAlert' }).length).toEqual(1)
				expect(onAdd).toHaveBeenCalledTimes(0)
			})
			it('sets invalid identifiers message, shows alert', () => {
				wrapper.setState({
					roleForAdd: BASE_ROLE.GROUP_OWNER,
					identifiersToAdd: ['x']
				})
				instance.didAdd({
					addedUserRoles: [],
					existingUserRoles: [],
					blockedUserRoles: [],
					invalidIdentifiers: ['x'],
					allowedDomains: 'domain',
					invalidDomainIdentifiers: []
				})
				expect(wrapper.state('failMessage')).toEqual(
					'The following Purdue career account aliases or PUIDs are invalid:\r\nx'
				)
				expect(wrapper.find({ id: 'failMessageAlert' }).length).toEqual(1)
				expect(onAdd).toHaveBeenCalledTimes(0)
			})
			it('sets invalid identifiers domain message, shows alert', () => {
				wrapper.setState({
					roleForAdd: BASE_ROLE.GROUP_OWNER,
					identifiersToAdd: ['x']
				})
				instance.didAdd({
					addedUserRoles: [],
					existingUserRoles: [],
					blockedUserRoles: [],
					invalidIdentifiers: [],
					allowedDomains: 'gmail.com,purdue.edu',
					invalidDomainIdentifiers: ['x']
				})
				expect(wrapper.state('failMessage')).toEqual(
					'The following Purdue career account aliases or PUIDs do not match the allowed domains of gmail.com, purdue.edu:\r\nx'
				)
				expect(wrapper.find({ id: 'failMessageAlert' }).length).toEqual(1)
				expect(onAdd).toHaveBeenCalledTimes(0)
			})
			it('combines invalid identifiers and invalid identifiers domain message, shows alert', () => {
				wrapper.setState({
					roleForAdd: BASE_ROLE.GROUP_OWNER,
					identifiersToAdd: ['x', 'y']
				})
				instance.didAdd({
					addedUserRoles: [],
					existingUserRoles: [],
					blockedUserRoles: [],
					invalidIdentifiers: ['x'],
					allowedDomains: 'gmail.com,purdue.edu',
					invalidDomainIdentifiers: ['y']
				})
				expect(wrapper.state('failMessage')).toEqual(
					'The following Purdue career account aliases or PUIDs are invalid:\r\nx\r\nThe following Purdue career account aliases or PUIDs do not match the allowed domains of gmail.com, purdue.edu:\r\ny'
				)
				expect(wrapper.find({ id: 'failMessageAlert' }).length).toEqual(1)
				expect(onAdd).toHaveBeenCalledTimes(0)
			})
		})
		describe('updateUserRole', () => {
			it('should set state and call props.update', () => {
				const updateFn = jest.fn()
				wrapper.setProps({
					update: updateFn
				})
				instance.updateUserRole(defaultGroupLearner, BASE_ROLE.GROUP_GRADER)
				expect(wrapper.state('userRoleToUpdate')).toEqual(defaultGroupLearner)
				expect(wrapper.state('roleForUpdate')).toEqual(BASE_ROLE.GROUP_GRADER)
				expect(updateFn.mock.calls.length).toEqual(1)
			})
		})
		describe('didUpdate', () => {
			it('should throw if no `userRoleToUpdate` in state', () => {
				expect(() => {
					instance.didUpdate(true)
				}).toThrowError('`didUpdate` was called without setting `userRoleToUpdate` in state')
				expect(onUpdate).toHaveBeenCalledTimes(0)
			})
			it('should throw if no `roleForUpdate` in state', () => {
				expect(() => {
					wrapper.setState({
						userRoleToUpdate: defaultGroupLearner
					})
					instance.didUpdate(true)
				}).toThrowError('`didUpdate` was called without setting `roleForUpdate` in state')
				expect(onUpdate).toHaveBeenCalledTimes(0)
			})
			it('should set failure message and clear state on failure', () => {
				wrapper.setState({
					userRoleToUpdate: defaultGroupLearner,
					roleForUpdate: BASE_ROLE.GROUP_GRADER
				})
				instance.didUpdate(false)
				expect(wrapper.state('failMessage')).toEqual(
					`Oops! There was an error updating Joe Schmoe to a grader${
						entityName ? ` in your ${entityName}` : ''
					}. Please try again.`
				)
				expect(wrapper.find({ id: 'failMessageAlert' }).length).toEqual(1)
				expect(wrapper.state('userRoleToUpdate')).toBeUndefined()
				expect(onUpdate).toHaveBeenCalledTimes(0)
			})
			it('should set success message and clear state on success', () => {
				wrapper.setState({
					userRoleToUpdate: defaultGroupLearner,
					roleForUpdate: BASE_ROLE.GROUP_GRADER
				})
				instance.didUpdate(true)
				expect(wrapper.state('successMessage')).toEqual(
					`Joe Schmoe was successfully updated to a grader${entityName ? ` in your ${entityName}` : ''}.`
				)
				expect(wrapper.find({ id: 'successMessageAlert' }).length).toEqual(1)
				expect(wrapper.state('userRoleToUpdate')).toBeUndefined()
				expect(onUpdate).toHaveBeenCalledWith(defaultGroupLearner, BASE_ROLE.GROUP_GRADER)
				expect(onUpdate).toHaveBeenCalledTimes(1)
			})
		})
		describe('alertRemoveUser', () => {
			it('shows alert when removing learner', () => {
				instance.alertRemoveUserRole(defaultGroupLearner)
				expect(wrapper.state('shouldShowRemoveDialog')).toEqual(true)
				expect(wrapper.state('userRoleToRemove')).toEqual(defaultGroupLearner)
				const removeUserAlert = wrapper.find({ id: 'removeUserAlert' })
				expect(removeUserAlert.prop('isOpen')).toEqual(true)
				expect(removeUserAlert.prop('title')).toEqual('Remove Student')
				expect(shallow(removeUserAlert.prop('description')).text()).toBe(
					'Are you sure you want to remove Joe Schmoe as a student?'
				)
			})
			it('closes alert after onCancel', () => {
				instance.alertRemoveUserRole(defaultGroupLearner)
				const removeUserAlert = wrapper.find({ id: 'removeUserAlert' })
				expect(removeUserAlert.prop('isOpen')).toEqual(true)
				removeUserAlert.prop('onCancel')()
				// userRoleToRemove is cleared, so it is not rendered
				expect(wrapper.find({ id: 'removeUserAlert' }).length).toEqual(0)
			})
			it('closes alert after onDestroy', () => {
				instance.alertRemoveUserRole(defaultGroupLearner)
				let removeUserAlert = wrapper.find({ id: 'removeUserAlert' })
				expect(removeUserAlert.prop('isOpen')).toEqual(true)
				removeUserAlert.prop('onDestroy')()
				// userRoleToRemove still exists, check isOpen
				removeUserAlert = wrapper.find({ id: 'removeUserAlert' })
				expect(removeUserAlert.prop('isOpen')).toEqual(false)
			})
			it('shows alert when removing owner', () => {
				instance.alertRemoveUserRole(defaultGroupOwner)
				expect(wrapper.state('shouldShowRemoveDialog')).toEqual(true)
				expect(wrapper.state('userRoleToRemove')).toEqual(defaultGroupOwner)
				const removeUserAlert = wrapper.find({ id: 'removeUserAlert' })
				expect(removeUserAlert.prop('isOpen')).toEqual(true)
				expect(removeUserAlert.prop('title')).toEqual('Remove Instructor')
			})
		})
		describe('removeUser', () => {
			it('should throw if no `userRoleToRemove` in state', () => {
				expect(() => {
					instance.removeUserRole(false)
				}).toThrowError('`removeUser` was called without setting `userRoleToRemove` in state')
			})
			it('should clear userRoleToRemove on cancel', () => {
				instance.alertRemoveUserRole(defaultGroupLearner)
				instance.removeUserRole(false)
				expect(wrapper.state('shouldShowRemoveDialog')).toEqual(false)
				expect(wrapper.state('userRoleToRemove')).toBeUndefined()
			})
			it('should call props.delete', () => {
				const deleteFn = jest.fn()
				wrapper.setProps({
					delete: deleteFn
				})
				instance.alertRemoveUserRole(defaultGroupLearner)
				instance.removeUserRole(true)
				expect(wrapper.state('userRoleToRemove')).toEqual(defaultGroupLearner)
				expect(deleteFn.mock.calls.length).toEqual(1)
			})
		})
		describe('didRemove', () => {
			it('should throw if no `userRoleToRemove` in state', () => {
				expect(() => {
					instance.didRemove(true)
				}).toThrowError('`didRemove` was called without setting `userRoleToRemove` in state')
				expect(onRemove).toHaveBeenCalledTimes(0)
			})
			it('should clear userRoleToRemove and show error message on failure', () => {
				instance.alertRemoveUserRole(defaultGroupLearner)
				instance.removeUserRole(true)
				instance.didRemove(false)
				expect(wrapper.state('failMessage')).toEqual(
					`Oops! There was an error removing Joe Schmoe${
						entityName ? ` from your ${entityName}` : ''
					}. Please try again.`
				)
				expect(wrapper.find({ id: 'failMessageAlert' }).length).toEqual(1)
				expect(wrapper.state('userRoleToRemove')).toBeUndefined()
				expect(onRemove).toHaveBeenCalledTimes(0)
			})
			it('should clear userRoleToRemove, and show success message', () => {
				instance.alertRemoveUserRole(defaultGroupLearner)
				instance.removeUserRole(true)
				instance.didRemove(true)
				expect(wrapper.state('successMessage')).toEqual(
					`Joe Schmoe was successfully removed${entityName ? ` from your ${entityName}` : ''}.`
				)
				expect(wrapper.find({ id: 'successMessageAlert' }).length).toEqual(1)
				expect(wrapper.state('userRoleToRemove')).toBeUndefined()
				expect(onRemove).toHaveBeenCalledWith(defaultGroupLearner)
				expect(onRemove).toHaveBeenCalledTimes(1)
			})
		})
		describe('renderRemoveUserRoleDescription', () => {
			it('should correctly display user name when removing and user has name', () => {
				const description = instance.renderRemoveUserRoleDescription(defaultNamedGroupLearner)
				expect(shallow(description).find('strong').text()).toBe(
					`remove ${defaultNamedGroupLearner.firstName} ${defaultNamedGroupLearner.lastName}`
				)
			})

			it('should correctly display email address when removing and user has no name', () => {
				const description = instance.renderRemoveUserRoleDescription(defaultNamelessGroupLearner)
				expect(shallow(description).find('strong').text()).toBe(`remove ${defaultNamelessGroupLearner.email}`)
			})
		})
	})
	describe('alerts', () => {
		let wrapper: any
		beforeEach(() => {
			wrapper = setupFunc(
				mockUser([modifyUserRoleActivityName]).userInfo,
				mockEntities ? mockEntities() : undefined,
				defaultRole
			)
		})
		describe('successMessageAlert', () => {
			it('should close after onClose', () => {
				wrapper.setState({
					successMessage: 'foo'
				})
				const successMessageAlert = wrapper.find({ id: 'successMessageAlert' })
				expect(successMessageAlert.length).toEqual(1)
				successMessageAlert.prop('onClose')()
				expect(wrapper.find({ id: 'successMessageAlert' }).length).toEqual(0)
			})
		})
		describe('existingMessageAlert', () => {
			it('should close after onClose', () => {
				wrapper.setState({
					existingMessage: 'foo'
				})
				const existingMessageAlert = wrapper.find({ id: 'existingMessageAlert' })
				expect(existingMessageAlert.length).toEqual(1)
				existingMessageAlert.prop('onClose')()
				expect(wrapper.find({ id: 'existingMessageAlert' }).length).toEqual(0)
			})
		})
		describe('blockedMessageAlert', () => {
			it('should close after onClose', () => {
				wrapper.setState({
					blockedMessage: 'foo'
				})
				const blockedMessageAlert = wrapper.find({ id: 'blockedMessageAlert' })
				expect(blockedMessageAlert.length).toEqual(1)
				blockedMessageAlert.prop('onClose')()
				expect(wrapper.find({ id: 'blockedMessageAlert' }).length).toEqual(0)
			})
		})
		describe('failMessageAlert', () => {
			it('should close after onClose', () => {
				wrapper.setState({
					failMessage: 'foo'
				})
				const failMessageAlert = wrapper.find({ id: 'failMessageAlert' })
				expect(failMessageAlert.length).toEqual(1)
				failMessageAlert.prop('onClose')()
				expect(wrapper.find({ id: 'failMessageAlert' }).length).toEqual(0)
			})
		})
	})
}

describe('UserRoles', () => {
	beforeEach(() => {
		onAdd.mockClear()
		onUpdate.mockClear()
		onRemove.mockClear()
	})
	describe('Admins', () => {
		runTests(
			setupAdmins,
			undefined,
			BASE_ACTIVITY.USER_ROLE_MODIFY_ANY,
			BASE_ACTIVITY.USER_ROLE_MODIFY_ANY,
			undefined,
			BASE_ROLE.ADMIN
		)
	})
	describe('Group Users', () => {
		runTests(
			setupGroup,
			mockGroups,
			BASE_ACTIVITY.GROUP_USER_ROLE_MODIFY,
			BASE_ACTIVITY.GROUP_USER_ROLE_MODIFY,
			'course',
			BASE_ROLE.GROUP_OWNER
		)
	})
})
