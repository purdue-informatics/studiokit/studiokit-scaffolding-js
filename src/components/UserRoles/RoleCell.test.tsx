import { shallow } from 'enzyme'
import { merge } from 'lodash'
import React from 'react'
import * as ReactRedux from 'react-redux'
import { BASE_ROLE } from '../../constants/baseRole'
import {
	defaultExternalGroupLearner,
	defaultGroupLearner,
	defaultGroupOwner,
	defaultGroupOwnerUserInfo,
	mockUser
} from '../../constants/mockData'
import { GroupUserRole, UserInfo } from '../../types'
import { convertToUserWithRoles } from '../../utils/userRole'
import { UserRolesContextType } from './Context'
import { RoleCell, RoleCellProps } from './RoleCell'

const groupOwnerUserWithRoles = convertToUserWithRoles<GroupUserRole>(defaultGroupOwner)

const groupOwnerLearnerGrader = merge({}, groupOwnerUserWithRoles, {
	roles: [
		merge({}, defaultGroupOwner),
		merge({}, defaultGroupOwner, { id: '2', role: BASE_ROLE.GROUP_LEARNER }),
		merge({}, defaultGroupOwner, { id: '3', role: BASE_ROLE.GROUP_GRADER })
	]
})

const groupLearnerUserWithRoles = convertToUserWithRoles<GroupUserRole>(defaultGroupLearner)

const setup = (
	user: Partial<UserInfo>,
	props: Partial<RoleCellProps>,
	contextValue?: Partial<UserRolesContextType>
) => {
	const mockProps: RoleCellProps = Object.assign(
		{
			user,
			shouldRenderRole: true,
			roles: [BASE_ROLE.GROUP_OWNER, BASE_ROLE.GROUP_GRADER, BASE_ROLE.GROUP_LEARNER],
			requiredRoleCount: 2
		},
		props
	)

	const mockContextValue: UserRolesContextType = Object.assign(
		{
			roles: [BASE_ROLE.GROUP_OWNER, BASE_ROLE.GROUP_GRADER, BASE_ROLE.GROUP_LEARNER],
			requiredRole: BASE_ROLE.GROUP_OWNER,
			isUpdating: false,
			updateUserRole: jest.fn(),
			isRemoving: false,
			removeUserRole: jest.fn()
		},
		contextValue
	)

	jest.spyOn(React, 'useContext').mockImplementation(() => mockContextValue)
	jest.spyOn(ReactRedux, 'useSelector').mockImplementation(() => user.id)

	return shallow(<RoleCell {...mockProps} />)
}

describe('RoleCell', () => {
	it('shows multiple roles in correct order', () => {
		const wrapper = setup(
			mockUser().userInfo,
			{
				user: groupOwnerLearnerGrader
			},
			{
				allowMultipleRoles: true
			}
		)
		expect(wrapper.find('li').length).toEqual(3)
		expect(wrapper.find('li').at(0).text()).toBe('Instructor')
		expect(wrapper.find('li').at(1).text()).toBe('Grader')
		expect(wrapper.find('li').at(2).text()).toBe('Student')
	})
	it('disables controls when any user role is being updated', () => {
		const wrapper = setup(
			mockUser().userInfo,
			{
				user: groupOwnerUserWithRoles
			},
			{
				canModify: true,
				isUpdating: true,
				userRoleToUpdate: defaultGroupLearner
			}
		)
		expect(wrapper.find('RoleSelectControl').props().disabled).toEqual(true)
		expect(wrapper.find('.remove-user-button').props().disabled).toEqual(true)
	})
	it('shows refresh indicator when user role is being updated', () => {
		const wrapper = setup(
			mockUser().userInfo,
			{
				user: groupOwnerUserWithRoles
			},
			{
				canModify: true,
				isUpdating: true,
				userRoleToUpdate: defaultGroupOwner
			}
		)
		expect(wrapper.find('RefreshIndicatorInline').length).toEqual(1)
		expect(wrapper.find('RoleSelectControl').length).toEqual(0)
		expect(wrapper.find('.remove-user-button').props().disabled).toEqual(true)
	})
	it('disables controls when another user role is being deleted', () => {
		const wrapper = setup(
			mockUser().userInfo,
			{
				user: groupOwnerUserWithRoles
			},
			{
				canModify: true,
				isRemoving: true,
				userRoleToRemove: defaultGroupLearner
			}
		)
		expect(wrapper.find('RoleSelectControl').props().disabled).toEqual(true)
		expect(wrapper.find('.remove-user-button').props().disabled).toEqual(true)
	})
	it('shows refresh indicator when user role is being removed', () => {
		const wrapper = setup(
			mockUser().userInfo,
			{
				user: groupOwnerUserWithRoles
			},
			{
				canModify: true,
				isRemoving: true,
				userRoleToRemove: defaultGroupOwner
			}
		)
		expect(wrapper.find('RoleSelectControl').props().disabled).toEqual(true)
		expect(wrapper.find('RefreshIndicatorInline').length).toEqual(1)
		expect(wrapper.find('.remove-user-button').length).toEqual(0)
	})
	it('hides remove button, shows external icon, if user role is external', () => {
		const wrapper = setup(defaultGroupOwnerUserInfo, {
			user: convertToUserWithRoles<GroupUserRole>(defaultExternalGroupLearner)
		})
		expect(wrapper.find('.remove-user-button').length).toEqual(0)
		expect(wrapper.find('.external-icon').length).toEqual(1)
	})
	it('hides remove button if isDeleteDisabled = true', () => {
		const wrapper = setup(
			mockUser().userInfo,
			{
				user: groupOwnerUserWithRoles
			},
			{
				isDeleteDisabled: true
			}
		)
		expect(wrapper.find('.remove-user-button').length).toEqual(0)
	})
	it('hides remove button for required role when it is the last required user role', () => {
		const wrapper = setup(
			defaultGroupOwnerUserInfo,
			{
				user: groupOwnerUserWithRoles,
				requiredRoleCount: 1
			},
			{
				canModify: true
			}
		)
		expect(wrapper.find('.remove-user-button').length).toEqual(0)
	})
	it('shows remove button for required role and not the last required user role', () => {
		const wrapper = setup(
			defaultGroupOwnerUserInfo,
			{
				user: merge({}, groupOwnerUserWithRoles, { id: '999' }),
				requiredRoleCount: 2
			},
			{
				canModify: true
			}
		)
		expect(wrapper.find('.remove-user-button').length).toEqual(1)
	})
	it('hides remove button for current user role if canDeleteSelf = false', () => {
		const wrapper = setup(
			defaultGroupOwnerUserInfo,
			{
				user: groupOwnerUserWithRoles
			},
			{
				canDeleteSelf: false
			}
		)
		expect(wrapper.find('.remove-user-button').length).toEqual(0)
	})
	it('shows remove button for current user role if canDeleteSelf = true', () => {
		const wrapper = setup(
			defaultGroupOwnerUserInfo,
			{
				user: groupOwnerUserWithRoles
			},
			{
				canDeleteSelf: true
			}
		)
		expect(wrapper.find('.remove-user-button').length).toEqual(1)
	})
	it('shows remove button for other user role if canModify = true', () => {
		const wrapper = setup(
			defaultGroupOwnerUserInfo,
			{
				user: groupLearnerUserWithRoles
			},
			{
				canModify: true
			}
		)
		expect(wrapper.find('.remove-user-button').length).toEqual(1)
	})
})
