import IconHelp from '@material-ui/icons/Help'
import React, { ChangeEvent, FunctionComponent, ReactElement } from 'react'
import { FormControl, FormGroup, FormLabel, OverlayTrigger } from 'react-bootstrap'
import { OverlayInjectedProps } from 'react-bootstrap/Overlay'
import { textForBaseRole } from '../../utils/baseRole'

const label = (hasMultipleOptions: boolean, labelVisible: boolean, hasTooltip: boolean) => {
	const content = (
		<>
			Role
			{hasTooltip && <IconHelp color="primary" className="ml1 v-mid" tabIndex={0} />}
		</>
	)

	const classes = `f6 mb1 ${!labelVisible ? 'visually-hidden' : ''}`

	return hasMultipleOptions ? (
		<FormLabel className={classes}>{content}</FormLabel>
	) : (
		<span className={classes + ' b'}>{content}</span>
	)
}

const overlay = (hasMultipleOptions: boolean, labelVisible = false, rolePopover?: ReactElement<OverlayInjectedProps>) =>
	rolePopover ? (
		<OverlayTrigger placement="right" trigger={['click', 'hover', 'focus']} overlay={rolePopover}>
			{label(hasMultipleOptions, labelVisible, true)}
		</OverlayTrigger>
	) : (
		label(hasMultipleOptions, labelVisible, false)
	)

export interface RoleSelectControlProps {
	/** CSS class passed to the selector FormControl component */
	className?: string
	/** Array of options the user can choose from */
	options: string[]
	/** Currently selected value */
	value: string
	/** Function that is called when the value changes */
	onChange: (option: string) => void
	/** (Optional) Provide custom user-friendly names for displayed roles */
	textForRole?: (role: string) => string
	/** (Optional) Is the control disabled? */
	disabled?: boolean
}

export const RoleSelectControl: FunctionComponent<RoleSelectControlProps> = ({
	className,
	options,
	value,
	onChange,
	textForRole,
	disabled
}) =>
	options.length > 1 ? (
		<FormControl
			className={className ? className : ''}
			aria-label="Role"
			as="select"
			placeholder="select"
			onChange={(e: ChangeEvent<HTMLSelectElement>) => {
				onChange(e.target.value)
			}}
			value={value}
			disabled={disabled}>
			{options.map((role, i) => (
				<option value={role} key={role}>
					{textForRole ? textForRole(role) : textForBaseRole(role)}
				</option>
			))}
		</FormControl>
	) : (
		<div>{textForRole ? textForRole(options[0]) : textForBaseRole(options[0])}</div>
	)

export interface RoleSelectProps extends RoleSelectControlProps {
	/** Id for the 'select' form control, defaults to 'RoleSelect' */
	controlId?: string
	/** Whether the label text should be visible */
	labelVisible?: boolean
	/** (Optional) Content of the popover tooltip */
	popoverContentComponent?: ReactElement<OverlayInjectedProps>
}

export const RoleSelect: FunctionComponent<RoleSelectProps> = ({
	controlId,
	labelVisible,
	popoverContentComponent,
	options,
	...controlProps
}) => {
	const content = (
		<>
			{overlay(options.length > 1, !!labelVisible, popoverContentComponent)}
			<RoleSelectControl options={options} {...controlProps} />
		</>
	)

	return options.length > 1 ? (
		<FormGroup controlId={controlId ? controlId : 'RoleSelect'}>{content}</FormGroup>
	) : (
		<span>{content}</span>
	)
}
