import Button from '@material-ui/core/Button'
import AddCircle from '@material-ui/icons/AddCircle'
import _ from 'lodash'
import React, { Component, Fragment } from 'react'
import { Col, FormControl, FormGroup, FormLabel, Popover, Row } from 'react-bootstrap'
import { AlertDialog } from '../../components/AlertDialog'
import { RefreshIndicatorInline } from '../../components/RefreshIndicator/Inline'
import { RoleSelect } from '../../components/UserRoles/Select'
import { BASE_ROLE } from '../../constants/baseRole'
import { getAppConfig } from '../../constants/configuration'
import { RoleDescriptions } from '../../types'
import { getDomainIdentifierTypePluralString, getDomainIdentifierTypeString } from '../../utils/domainIdentifier'
import { isPurdueShard } from '../../utils/shard'
import { isEmail, isPurdueAliasOrPuid } from '../../utils/string'
import { AlertWithIcon } from '../AlertWithIcon'

const FERPA_ROLES = [BASE_ROLE.SUPER_ADMIN, BASE_ROLE.ADMIN, BASE_ROLE.GROUP_OWNER, BASE_ROLE.GROUP_GRADER]

export interface UserRolesAddProps {
	id: string

	disabled?: boolean
	entityName?: string
	defaultRole: string
	roleDescriptions: RoleDescriptions
	renderAddDescription?: (entityName?: string) => JSX.Element
	textForRole?: (role: string) => string

	isAddingUsersToRole: boolean
	shouldReset: boolean
	addUserRoles: (identifiers: string[], role: string) => void
}

interface UserRolesAddState {
	identifiers: string
	role: string
	showErrorAlert: boolean
	errorMessage?: string
}

export class UserRolesAdd extends Component<UserRolesAddProps, UserRolesAddState> {
	constructor(props: UserRolesAddProps) {
		super(props)
		this.state = {
			identifiers: '',
			role: this.props.defaultRole,
			showErrorAlert: false,
			errorMessage: undefined
		}
	}

	updateRole = (role: string) => {
		this.setState({
			role
		})
	}

	componentDidUpdate(prevProps: UserRolesAddProps) {
		const { shouldReset: prevShouldReset } = prevProps
		const { shouldReset } = this.props
		if (prevShouldReset === false && shouldReset === true) {
			this.setState({
				identifiers: '',
				role: this.props.defaultRole
			})
		}
	}

	handleSubmit = () => {
		const { identifiers, role } = this.state
		const identifiersArray = identifiers
			.split(/\r\n|\r|\n/g)
			.map(id => id.trim())
			.filter(id => id !== '') // filter identifiers that are empty
		const errorMessage = this.getValidationErrorMessage(identifiersArray)
		if (errorMessage) {
			this.setState({
				showErrorAlert: true,
				errorMessage
			})
		} else {
			this.props.addUserRoles(identifiersArray, role)
		}
	}

	closeErrorAlert = () => {
		this.setState({
			showErrorAlert: false,
			errorMessage: undefined
		})
	}

	updateIdentifiers = (e: any) => {
		this.setState({
			identifiers: e.target.value as string
		})
	}

	getValidationErrorMessage = (identifiers?: string[]) => {
		if (!identifiers || identifiers.length === 0) {
			return `Please enter at least one ${getDomainIdentifierTypeString()}.`
		}

		let invalidIdentifiers
		if (isPurdueShard() && !identifiers.every(id => isPurdueAliasOrPuid(id))) {
			invalidIdentifiers = identifiers.filter(id => !isPurdueAliasOrPuid(id))
		}
		if (!isPurdueShard() && !identifiers.every(id => isEmail(id))) {
			invalidIdentifiers = identifiers.filter(id => !isEmail(id))
		}
		if (invalidIdentifiers) {
			return `Please enter valid ${getDomainIdentifierTypePluralString()}. The following are invalid:\r\n${invalidIdentifiers.join(
				'\r\n'
			)}`
		}
		return undefined
	}

	render() {
		const { identifiers, role, showErrorAlert, errorMessage } = this.state
		const {
			entityName,
			isAddingUsersToRole,
			roleDescriptions,
			renderAddDescription,
			disabled,
			textForRole
		} = this.props
		const roles = _.keys(roleDescriptions)
		const rolePopover = (
			<Popover id="role">
				<h3>Role</h3>
				{roles.map(r => (
					<Fragment key={r}>{roleDescriptions[r]}</Fragment>
				))}
			</Popover>
		)

		const appConfig = getAppConfig()
		const appName = appConfig.APP_NAME
		const gradableDescriptor = appConfig.GRADABLE_DESCRIPTOR

		return (
			<Row>
				{!!renderAddDescription && <Col xs={12}>{renderAddDescription(entityName)}</Col>}
				<Col xs={12} sm={6}>
					<FormGroup controlId="UserEntry">
						<FormLabel className="f6">
							{getDomainIdentifierTypePluralString()} (separated by a new line)
						</FormLabel>
						<FormControl
							type="text"
							as="textarea"
							name="account"
							aria-label="The account names to add"
							onChange={this.updateIdentifiers}
							value={identifiers}
							placeholder={getDomainIdentifierTypeString()}
							disabled={disabled}
						/>
					</FormGroup>
				</Col>
				<Col xs={12} sm={4}>
					<RoleSelect
						options={roles}
						value={role}
						onChange={this.updateRole}
						labelVisible
						popoverContentComponent={rolePopover}
						textForRole={textForRole}
					/>
				</Col>
				<Col xs={12} sm={2} className="mt3-ns pt2-ns">
					{isAddingUsersToRole && <RefreshIndicatorInline className="mr3" />}
					{!isAddingUsersToRole && (
						<Button
							id="userRolesAddButton"
							color="primary"
							className="btn-primary w-100-lt-xs"
							disabled={!identifiers || disabled}
							onClick={this.handleSubmit}>
							<AddCircle className="v-mid" />
							Add
						</Button>
					)}
					<AlertDialog
						isOpen={showErrorAlert}
						title={`Error Adding People to the ${entityName}`}
						description={<div className="pre-wrap">{errorMessage}</div>}
						proceedText="Got it. Thanks!"
						onProceed={this.closeErrorAlert}
						onCancel={this.closeErrorAlert}
					/>
				</Col>
				{_.includes(FERPA_ROLES, role) && (
					<Col xs={12}>
						<AlertWithIcon id="ferpaWarning" variant="warning">
							<p>
								This role includes {gradableDescriptor} scores in {appName}. Please consider whether{' '}
								<a
									href="https://www.purdue.edu/registrar/FERPA/index.html"
									target="_blank"
									rel="noopener noreferrer">
									FERPA
								</a>{' '}
								certification is needed.
							</p>
						</AlertWithIcon>
					</Col>
				)}
			</Row>
		)
	}
}
