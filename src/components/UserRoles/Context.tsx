import React from 'react'
import { UserRole } from '../../types'

export interface UserRolesContextType {
	/** disable all user role select dropdowns, if any */
	isUpdateDisabled?: boolean
	/** disable all user role delete buttons */
	isDeleteDisabled?: boolean
	/** Is the current user allowed to add, update, and delete user roles? */
	canModify?: boolean
	/** Is the current user allowed to delete their own user role? */
	canDeleteSelf?: boolean
	/** If multiple roles allowed, each role is displayed per user, otherwise a select dropdown is displayed. */
	allowMultipleRoles?: boolean
	roles: string[]
	/** (Optional) If provided, the last user role with the required role will be prevented from being removed. */
	requiredRole?: string
	/** (Optional) Provide custom user-friendly names for displayed roles */
	textForRole?: (role: string) => string
	/** The user role being updated */
	userRoleToUpdate?: UserRole
	/** If a user role is being updated */
	isUpdating: boolean
	/** Method to update a user role */
	updateUserRole: (userRoleToUpdate: UserRole, newRole: string) => void
	/** The user role being removed */
	userRoleToRemove?: UserRole
	/** If a user role is being removed */
	isRemoving: boolean
	/** Method to remove a user role */
	removeUserRole: (userRoleToRemove: UserRole) => void
}

/*
 * Context provided to the children of UserRoles.  Default values should be initialized
 * by the context provider.
 */
// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
export const UserRolesContext = React.createContext<UserRolesContextType>(undefined!)
