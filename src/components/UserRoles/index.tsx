import { cloneDeep, Dictionary, isEqual, pickBy } from 'lodash'
import React, { Component } from 'react'
import { Col, Row } from 'react-bootstrap'
import { connect } from 'react-redux'
import { v4 as uuidv4 } from 'uuid'
import { AlertDialog } from '../../components/AlertDialog'
import { Loading } from '../../components/Loading'
import { UserRolesAdd } from '../../components/UserRoles/Add'
import { UserRolesTable } from '../../components/UserRoles/Table'
import { getEndpointMappings } from '../../constants'
import { BASE_ROLE } from '../../constants/baseRole'
import { MODEL_STATUS } from '../../constants/modelStatus'
import { dispatchAction, dispatchModelFetchRequest } from '../../redux/actionCreator'
import {
	MODEL_FETCH_REQUEST_ACTION_TYPE,
	MODEL_FETCH_RESULT_ACTION_TYPE,
	ModelFetchResultAction
} from '../../redux/actions'
import { noStoreHooks } from '../../redux/sagas/noStoreSaga'
import {
	BaseReduxState,
	ExternalProvider,
	FetchErrorData,
	Group,
	Model,
	ModelCollection,
	RoleDescriptions,
	User,
	UserRole,
	UserWithRoles
} from '../../types'
import { canPerformActivityGlobally, canPerformActivityOnEntity, defaultOptions } from '../../utils/baseActivity'
import { singularArticleForBaseRole, textForBaseRole } from '../../utils/baseRole'
import { getDomainIdentifierTypePluralString } from '../../utils/domainIdentifier'
import { getRootModelName } from '../../utils/entityUserRole'
import { isFetchErrorData, prepareFetch } from '../../utils/fetch'
import { sortByNames } from '../../utils/sort'
import { displayName } from '../../utils/user'
import { convertToUserWithRoles, getUserId } from '../../utils/userRole'
import { AlertWithIcon } from '../AlertWithIcon'
import { CollectionComponentWrappedProps } from '../HOC/CollectionComponent'
import { UserRolesContext } from './Context'

export interface UserRolesReduxProps {
	/** Is the current user allowed to add, update, and delete user roles? */
	canModify?: boolean
	/** Is the current user allowed to delete their own user role? */
	canDeleteSelf?: boolean
}

export interface UserRolesOwnProps extends CollectionComponentWrappedProps<UserRole> {
	/** Should the component prevent modifications, independent of permissions? */
	readOnly?: boolean
	/** Is the current user allowed to delete their own user role? Overrides the value from redux. */
	canDeleteSelf?: boolean

	/** If multiple roles allowed, each role is displayed per user, otherwise a select dropdown is displayed. */
	allowMultipleRoles?: boolean
	/** disable adding new user roles */
	isAddDisabled?: boolean
	/** disable all user role select dropdowns, if any */
	isUpdateDisabled?: boolean
	/** disable all user role delete buttons */
	isDeleteDisabled?: boolean

	/** The activity the current user must have in order to add, update, or delete user roles. */
	modifyUserRoleActivityName: string
	/** (Optional) The activity the current user must have in order to delete their own user roles, if they cannot modify all. */
	deleteOwnUserRoleActivityName?: string
	/** The role that will be shown first in the list of available roles. */
	defaultRole: string
	/** (Optional) If provided, the last user role with the required role will be prevented from being removed. */
	requiredRole?: string
	/** A dictionary of all possible roles and their descriptions. */
	roleDescriptions: RoleDescriptions
	/** (Optional) list of roles that should be excluded from the add options. */
	addRoleExcludeList?: string[]

	/** (Optional) The entity that owns the user roles, when targeting EntityUserRoles */
	entity?: Model
	/** (Optional) A user-friendly name for the entity type, when targeting EntityUserRoles  */
	entityName?: string
	/** (Optional) set if `renderRemoveUserRoleDescription` should check for roster sync */
	externalProviders?: ModelCollection<ExternalProvider>

	/** (Optional) Allow users to be excluded from being displayed in the table */
	filterUsers?: (users: UserWithRoles[]) => UserWithRoles[]
	/** (Optional) Callback that is called after user roles are added */
	onAdd?: (userRoles: UserRole[]) => void
	/** (Optional) Callback that is called after a user role is updated */
	onUpdate?: (userRole: UserRole, role: string) => void
	/** (Optional) Callback that is called after a user role is removed */
	onRemove?: (userRole: UserRole) => void
	/** (Optional) Render custom components between the Add and Table components */
	renderTableDescription?: (canModify?: boolean) => JSX.Element
	/** (Optional) Render custom components at the start of the Add component */
	renderAddDescription?: (entityName?: string) => JSX.Element
	/** (Optional) Provide custom user-friendly names for displayed roles */
	textForRole?: (role: string) => string
	/** (Optional) Provide custom user-friendly names for displayed roles that require custom casing */
	lowercaseTextForRole?: (role: string) => string
	/** (Optional) Provide custom user-friendly articles ("a" vs. "an") for displayed roles */
	singularArticleForRole?: (role: string) => string
}

export interface UserRolesProps extends UserRolesReduxProps, UserRolesOwnProps {}

interface UserRolesState {
	sortedUsers: UserWithRoles[]
	// add
	addUsersHookId: any
	isAdding: boolean
	identifiersToAdd?: string[]
	roleForAdd?: string
	shouldResetAddForm: boolean
	// update
	userRoleToUpdate?: UserRole
	roleForUpdate?: string
	isUpdating: boolean
	// remove
	shouldShowRemoveDialog: boolean
	userRoleToRemove?: UserRole
	isRemoving: boolean
	// messages
	successMessage?: string
	existingMessage?: string
	blockedMessage?: string
	failMessage?: string
}

export interface AddBusinessModel {
	addedUserRoles: UserRole[]
	existingUserRoles: UserRole[]
	blockedUserRoles?: UserRole[]
	invalidIdentifiers: string[]
	allowedDomains: string
	invalidDomainIdentifiers: string[]
}

/**
 * Component used to manage either UserRoles (global roles) or EntityUserRoles.
 */
export class UserRoles extends Component<UserRolesProps, UserRolesState> {
	state: UserRolesState = {
		sortedUsers: [],
		// add
		addUsersHookId: uuidv4(),
		isAdding: false,
		isUpdating: false,
		isRemoving: false,
		shouldResetAddForm: false,
		shouldShowRemoveDialog: false
	}

	componentDidMount() {
		this.setStateFromProps(this.props)
		const { addUsersHookId } = this.state
		noStoreHooks.unregisterNoStoreActionHook(addUsersHookId)
	}

	componentDidUpdate(prevProps: UserRolesProps) {
		const { modelArray: prevModelArray, modelStatus: prevModelStatus } = prevProps
		const { modelArray, modelStatus, entityName } = this.props

		if (!isEqual(prevModelArray, modelArray)) {
			this.setStateFromProps(this.props)
		}

		// loading error
		if (
			(prevModelStatus === MODEL_STATUS.UNINITIALIZED || prevModelStatus === MODEL_STATUS.LOADING) &&
			modelStatus === MODEL_STATUS.ERROR
		) {
			this.setState({
				failMessage: `Oops! There was an error loading${
					entityName ? ` the people for your ${entityName}` : ''
				}.\r\nPlease try again.`
			})
		}

		// updating
		if (prevModelStatus === MODEL_STATUS.UPDATING && modelStatus !== MODEL_STATUS.UPDATING) {
			this.didUpdate(modelStatus === MODEL_STATUS.READY)
		}

		// removing
		if (prevModelStatus === MODEL_STATUS.DELETING && modelStatus !== MODEL_STATUS.DELETING) {
			this.didRemove(modelStatus === MODEL_STATUS.READY)
		}
	}

	setStateFromProps = (props: UserRolesProps) => {
		const { modelArray: userRoles, filterUsers } = props
		const usersWithRoles = Object.values(
			userRoles.reduce((result: Dictionary<UserWithRoles>, userRole: UserRole) => {
				const userId = getUserId(userRole)
				let userWithRoles = result[userId]
				if (!userWithRoles) {
					userWithRoles = convertToUserWithRoles(userRole)
				} else {
					userWithRoles.roles.push(userRole)
				}
				result[userId] = userWithRoles
				return result
			}, {})
		)
		const filteredUsers = filterUsers ? filterUsers(usersWithRoles) : usersWithRoles
		const sortedUsers = filteredUsers.sort(sortByNames)
		this.setState({
			sortedUsers
		})
	}

	textForRole = (role: string, lowercase = false) => {
		if (lowercase && this.props.lowercaseTextForRole) {
			return this.props.lowercaseTextForRole(role)
		}
		const roleString = this.props.textForRole ? this.props.textForRole(role) : textForBaseRole(role)
		return lowercase ? roleString.toLowerCase() : roleString
	}

	singularArticleForRole = (role: string) => {
		return this.props.singularArticleForRole
			? this.props.singularArticleForRole(role)
			: singularArticleForBaseRole(role)
	}

	//#region Add Users

	addUserRoles = (identifiers: string[], role: string) => {
		const { entity } = this.props
		this.setState({
			identifiersToAdd: identifiers,
			roleForAdd: role,
			successMessage: undefined,
			failMessage: undefined,
			existingMessage: undefined,
			blockedMessage: undefined,
			shouldResetAddForm: false,
			isAdding: true
		})
		const { addUsersHookId } = this.state
		noStoreHooks.registerNoStoreActionHook(addUsersHookId, (data: AddBusinessModel | FetchErrorData | null) => {
			noStoreHooks.unregisterNoStoreActionHook(addUsersHookId)
			this.didAdd(data)
		})
		const rootModelName = getRootModelName(this.props)
		dispatchModelFetchRequest({
			modelName: `${rootModelName ? `${rootModelName}.` : ''}addUserRoles`,
			pathParams: entity ? [entity.id] : undefined,
			body: {
				entityId: entity ? entity.id : undefined,
				identifiers: identifiers,
				roleName: role
			},
			noStore: true,
			guid: addUsersHookId
		})
	}

	didAdd = (data: AddBusinessModel | FetchErrorData | null) => {
		const { entityName, onAdd, model, modelName, pathParams } = this.props
		const { roleForAdd, identifiersToAdd } = this.state
		if (!roleForAdd || !identifiersToAdd || identifiersToAdd.length === 0) {
			throw new Error('didAdd was called in the incorrect state')
		}

		if (!data || isFetchErrorData(data)) {
			const failMessage = `The following identifiers were not added${
				entityName ? ` to your ${entityName}` : ''
			}:\r\n${identifiersToAdd.join('\r\n')}`

			this.setState({
				shouldResetAddForm: true,
				isAdding: false,
				identifiersToAdd: undefined,
				roleForAdd: undefined,
				failMessage
			})
			return
		}

		const {
			addedUserRoles,
			existingUserRoles,
			blockedUserRoles,
			invalidIdentifiers,
			allowedDomains,
			invalidDomainIdentifiers
		} = data
		const roleString = this.textForRole(roleForAdd, true)
		const singularArticleString = this.singularArticleForRole(roleForAdd)

		const addedNames =
			!!addedUserRoles && addedUserRoles.length > 0
				? addedUserRoles
						.map((au: User) => {
							return `${displayName(au)}${au.uid ? ` (${au.uid})` : ''}`
						})
						.join('\r\n')
				: undefined

		const successMessage = addedNames
			? `The following ${roleString}${addedUserRoles.length > 1 ? 's were' : ' was'} successfully added${
					entityName ? ` to your ${entityName}` : ''
			  }:\r\n${addedNames}`
			: undefined

		const existingNames =
			!!existingUserRoles && existingUserRoles.length > 0
				? existingUserRoles.map((eu: User) => `${displayName(eu)}${eu.uid ? ` (${eu.uid})` : ''}`).join('\r\n')
				: undefined

		const hasMultipleExisting = existingUserRoles.length > 1
		const existingMessage = existingNames
			? `The following ${hasMultipleExisting ? 'people were' : 'person was'} already ${
					entityName
						? `in your ${entityName} as ${hasMultipleExisting ? '' : `${singularArticleString} `}`
						: hasMultipleExisting
						? ''
						: `${singularArticleString} `
			  }${roleString}${hasMultipleExisting ? 's' : ''}:\r\n${existingNames}`
			: undefined

		const blockedNames =
			!!blockedUserRoles && blockedUserRoles.length > 0
				? blockedUserRoles.map((eu: User) => `${displayName(eu)}${eu.uid ? ` (${eu.uid})` : ''}`).join('\r\n')
				: undefined
		const hasMultipleBlocked = !!blockedUserRoles && blockedUserRoles.length > 1
		const blockedMessage = blockedNames
			? `The following ${hasMultipleBlocked ? 'people' : 'person'} could not be added ${
					entityName ? `to your ${entityName}` : ''
			  } as ${hasMultipleBlocked ? '' : `${singularArticleString} `}${roleString}${
					hasMultipleBlocked ? 's' : ''
			  } because they already have a role:\r\n${blockedNames}`
			: undefined

		let failMessage =
			!!invalidIdentifiers && invalidIdentifiers.length > 0
				? `The following ${getDomainIdentifierTypePluralString()} are invalid:\r\n${invalidIdentifiers.join(
						'\r\n'
				  )}`
				: undefined

		if (!!invalidDomainIdentifiers && invalidDomainIdentifiers.length > 0) {
			failMessage = `${
				failMessage ? `${failMessage}\r\n` : ''
			}The following ${getDomainIdentifierTypePluralString()} do not match the allowed domains of ${allowedDomains.replace(
				',',
				', '
			)}:\r\n${invalidDomainIdentifiers.join('\r\n')}`
		}

		this.setState({
			shouldResetAddForm: true,
			isAdding: false,
			identifiersToAdd: undefined,
			roleForAdd: undefined,
			successMessage,
			existingMessage,
			blockedMessage,
			failMessage
		})

		// only reload if we had some successful adds
		if (addedNames) {
			// update redux with added userRoles
			const updatedModel = cloneDeep(model)
			addedUserRoles.forEach(userRole => (updatedModel[userRole.id] = cloneDeep(userRole)))

			// create a dummy action and call `prepareFetch` to get the final `modelPath`
			const { modelPath } = prepareFetch(
				{
					type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
					modelName,
					pathParams
				},
				getEndpointMappings()
			)
			dispatchAction<ModelFetchResultAction>({
				type: MODEL_FETCH_RESULT_ACTION_TYPE.FETCH_RESULT_RECEIVED,
				modelPath,
				data: updatedModel
			})

			if (onAdd) {
				onAdd(addedUserRoles)
			}
		}
	}

	//#endregion Add Users

	//#region Update

	updateUserRole = (userRoleToUpdate: UserRole, newRole: string) => {
		this.setState({
			isUpdating: true,
			userRoleToUpdate,
			roleForUpdate: newRole,
			successMessage: undefined,
			failMessage: undefined
		})
		this.props.update({
			id: userRoleToUpdate.id,
			body: {},
			queryParams: {
				roleName: newRole
			}
		})
	}

	didUpdate = (isSuccess: boolean) => {
		const { entityName, onUpdate } = this.props
		const { userRoleToUpdate, roleForUpdate } = this.state
		if (userRoleToUpdate === undefined) {
			throw new Error('`didUpdate` was called without setting `userRoleToUpdate` in state')
		}
		if (roleForUpdate === undefined) {
			throw new Error('`didUpdate` was called without setting `roleForUpdate` in state')
		}

		const name = displayName(userRoleToUpdate)
		const roleString = this.textForRole(roleForUpdate, true)
		const singularArticleString = this.singularArticleForRole(roleForUpdate)

		if (!isSuccess) {
			const failMessage = `Oops! There was an error updating ${name} to ${singularArticleString} ${roleString}${
				entityName ? ` in your ${entityName}` : ''
			}. Please try again.`
			this.setState({
				isUpdating: false,
				userRoleToUpdate: undefined,
				roleForUpdate: undefined,
				failMessage
			})
			return
		}

		const successMessage = `${name} was successfully updated to ${singularArticleString} ${roleString}${
			entityName ? ` in your ${entityName}` : ''
		}.`

		if (onUpdate) {
			onUpdate(userRoleToUpdate, roleForUpdate)
		}

		this.setState({
			isUpdating: false,
			userRoleToUpdate: undefined,
			roleForUpdate: undefined,
			successMessage
		})
	}

	//#endregion Update

	//#region Remove User

	alertRemoveUserRole = (userRoleToRemove: UserRole) => {
		this.setState({
			userRoleToRemove,
			shouldShowRemoveDialog: true
		})
	}

	removeUserRoleTitle = (roleForRemove: string) => {
		return `Remove ${this.textForRole(roleForRemove)}`
	}

	renderRemoveUserRoleDescription = (userRoleToRemove: UserRole, warning?: JSX.Element) => {
		const { entity, externalProviders } = this.props
		const group = entity && (entity as Group).externalGroups ? (entity as Group) : undefined

		const roleString = this.textForRole(userRoleToRemove.role, true)
		const singularArticleString = this.singularArticleForRole(userRoleToRemove.role)
		const defaultWarning = warning ?? (
			<p className="ma0">
				Are you sure you want to <strong>remove {displayName(userRoleToRemove)}</strong> as{' '}
				{singularArticleString} {roleString}?
			</p>
		)

		const externalGroupsToRemove = group?.externalGroups.filter(eg => eg.userId === userRoleToRemove.id)

		if (
			!externalGroupsToRemove ||
			externalGroupsToRemove.length === 0 ||
			userRoleToRemove.role !== BASE_ROLE.GROUP_OWNER
		) {
			return defaultWarning
		}

		const externalGroupsToRemoveWithRosterSync = externalGroupsToRemove.filter(
			eg => externalProviders?.[eg.externalProviderId]?.rosterSyncEnabled
		)

		return (
			<div>
				<p>
					The following course sections will be <strong>disconnected</strong>.{' '}
					{externalGroupsToRemoveWithRosterSync.length > 0 && (
						<span>
							Any automatically added students will be <strong>removed</strong>.
						</span>
					)}
				</p>
				<ul>
					{externalGroupsToRemove.map(eg => (
						<li key={eg.id}>{eg.description}</li>
					))}
				</ul>
				{defaultWarning}
			</div>
		)
	}

	removeUserRole = (shouldRemove: boolean) => {
		const { userRoleToRemove } = this.state
		if (userRoleToRemove === undefined) {
			throw new Error('`removeUser` was called without setting `userRoleToRemove` in state')
		}
		this.setState({
			isRemoving: shouldRemove,
			shouldShowRemoveDialog: false,
			successMessage: undefined,
			failMessage: undefined,
			// clear if cancelled
			userRoleToRemove: shouldRemove ? userRoleToRemove : undefined
		})
		if (!shouldRemove) {
			return
		}
		this.props.delete({
			id: userRoleToRemove.id
		})
	}

	didRemove = (isSuccess: boolean) => {
		const { entityName, onRemove } = this.props
		const { userRoleToRemove } = this.state
		if (userRoleToRemove === undefined) {
			throw new Error('`didRemove` was called without setting `userRoleToRemove` in state')
		}

		const name = displayName(userRoleToRemove)
		if (!isSuccess) {
			const failMessage = `Oops! There was an error removing ${name}${
				entityName ? ` from your ${entityName}` : ''
			}. Please try again.`
			this.setState({
				isRemoving: false,
				userRoleToRemove: undefined,
				failMessage
			})
			return
		}

		const successMessage = `${name} was successfully removed${entityName ? ` from your ${entityName}` : ''}.`

		if (onRemove) {
			onRemove(userRoleToRemove)
		}

		this.setState({
			isRemoving: false,
			userRoleToRemove: undefined,
			successMessage
		})
	}

	//#endregion Remove User

	render() {
		const {
			modelStatus,
			canModify,
			readOnly,
			canDeleteSelf,
			allowMultipleRoles,
			isAddDisabled,
			isUpdateDisabled,
			isDeleteDisabled,
			defaultRole,
			requiredRole,
			roleDescriptions,
			addRoleExcludeList,
			entityName,
			renderTableDescription,
			renderAddDescription
		} = this.props
		const {
			sortedUsers,
			isAdding,
			isUpdating,
			isRemoving,
			shouldResetAddForm,
			shouldShowRemoveDialog,
			userRoleToUpdate,
			userRoleToRemove,
			successMessage,
			existingMessage,
			blockedMessage,
			failMessage
		} = this.state

		const addableRoleDescriptions = addRoleExcludeList
			? pickBy(roleDescriptions, (_, key) => !addRoleExcludeList.includes(key))
			: roleDescriptions
		const roles = Object.keys(roleDescriptions)
		const shouldRenderAsMutable = !readOnly && canModify

		return (
			<>
				{!!successMessage && (
					<AlertWithIcon
						id="successMessageAlert"
						variant="success"
						dismissible
						onClose={() =>
							this.setState({
								successMessage: undefined
							})
						}>
						<p className="pre-wrap">{successMessage}</p>
					</AlertWithIcon>
				)}
				{!!existingMessage && (
					<AlertWithIcon
						id="existingMessageAlert"
						variant="info"
						dismissible
						onClose={() =>
							this.setState({
								existingMessage: undefined
							})
						}>
						<p className="pre-wrap">{existingMessage}</p>
					</AlertWithIcon>
				)}
				{!!blockedMessage && (
					<AlertWithIcon
						id="blockedMessageAlert"
						variant="warning"
						dismissible
						onClose={() =>
							this.setState({
								blockedMessage: undefined
							})
						}>
						<p className="pre-wrap">{blockedMessage}</p>
					</AlertWithIcon>
				)}
				{!!failMessage && (
					<AlertWithIcon
						id="failMessageAlert"
						variant="warning"
						dismissible
						onClose={() =>
							this.setState({
								failMessage: undefined
							})
						}>
						<p className="pre-wrap">{failMessage}</p>
					</AlertWithIcon>
				)}
				{shouldRenderAsMutable && (
					<UserRolesAdd
						id="entityUserRolesAdd"
						disabled={isAddDisabled}
						entityName={entityName}
						defaultRole={defaultRole}
						roleDescriptions={addableRoleDescriptions}
						renderAddDescription={renderAddDescription}
						textForRole={this.textForRole}
						isAddingUsersToRole={isAdding}
						shouldReset={shouldResetAddForm}
						addUserRoles={this.addUserRoles}
					/>
				)}
				{!!renderTableDescription && renderTableDescription(shouldRenderAsMutable)}
				<Row className="mt3">
					<Col xs={12}>
						{modelStatus === MODEL_STATUS.LOADING ? (
							<Loading />
						) : sortedUsers.length > 0 ? (
							<UserRolesContext.Provider
								value={{
									isUpdateDisabled: isUpdateDisabled || readOnly,
									isDeleteDisabled: isDeleteDisabled || readOnly,
									canModify: shouldRenderAsMutable,
									canDeleteSelf,
									allowMultipleRoles,
									roles,
									requiredRole,
									textForRole: this.textForRole,
									userRoleToUpdate,
									isUpdating,
									updateUserRole: this.updateUserRole,
									userRoleToRemove,
									isRemoving,
									removeUserRole: this.alertRemoveUserRole
								}}>
								<UserRolesTable id="entityUserRolesTable" users={sortedUsers} />
							</UserRolesContext.Provider>
						) : null}
					</Col>
				</Row>
				{!!userRoleToRemove && (
					<AlertDialog
						id="removeUserAlert"
						isOpen={shouldShowRemoveDialog}
						title={this.removeUserRoleTitle(userRoleToRemove.role)}
						description={this.renderRemoveUserRoleDescription(userRoleToRemove)}
						onDestroy={() => this.removeUserRole(true)}
						destroyText="Yes, remove the user"
						onCancel={() => this.removeUserRole(false)}
						cancelText="No, I changed my mind"
					/>
				)}
			</>
		)
	}
}

export const mapStateToProps = (state: BaseReduxState, ownProps: UserRolesOwnProps): UserRolesReduxProps => {
	const canModifyGlobally = canPerformActivityGlobally(ownProps.modifyUserRoleActivityName, defaultOptions(state))

	const canModify =
		canModifyGlobally ||
		(ownProps.entity
			? canPerformActivityOnEntity(ownProps.modifyUserRoleActivityName, defaultOptions(state, ownProps, 'entity'))
			: false)

	return {
		canModify,
		canDeleteSelf:
			canModify ||
			(ownProps.entity && ownProps.deleteOwnUserRoleActivityName
				? canPerformActivityOnEntity(
						ownProps.deleteOwnUserRoleActivityName,
						defaultOptions(state, ownProps, 'entity')
				  )
				: false)
	}
}

export default connect(mapStateToProps)(UserRoles)
