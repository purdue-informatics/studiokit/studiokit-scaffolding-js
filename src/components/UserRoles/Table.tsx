import { isEqual, merge } from 'lodash'
import React, { FunctionComponent, useContext, useEffect, useState } from 'react'
import ReactTable, { Column, Filter } from 'react-table'
import { UserWithRoles } from '../../types'
import { isExternal, sortByHighestAccessRole } from '../../utils/userRole'
import { RoleFilter, roleFilterMethod, roleUserRoleFilterMethod } from '../Tables/RoleFilter'
import { TextFilter, textFilterMethod } from '../Tables/TextFilter'
import { UserRolesContext } from './Context'
import { RoleCell } from './RoleCell'

export interface UserRolesTableProps {
	id?: string
	users: UserWithRoles[]
}

const UserRolesTableComponent: FunctionComponent<UserRolesTableProps> = ({ id, users }) => {
	const { roles, requiredRole, textForRole } = useContext(UserRolesContext)

	const hasExternal = users.some(r => r.roles.some(r => isExternal(r)))
	const columns: Column[] = [
		{
			Header: 'First Name',
			accessor: 'firstName',
			filterMethod: textFilterMethod,
			Filter: TextFilter
		},
		{
			Header: 'Last Name',
			accessor: 'lastName',
			filterMethod: textFilterMethod,
			Filter: TextFilter
		},
		{
			Header: 'Email',
			accessor: 'email',
			filterMethod: textFilterMethod,
			Filter: TextFilter
		}
	]
	if (roles && roles.length > 0) {
		columns.push({
			id: 'role',
			Header: 'Role',
			accessor: u => u,
			Cell: cell => {
				const user: UserWithRoles = cell.value
				return (
					<RoleCell
						user={user}
						requiredRoleCount={
							requiredRole
								? users.filter(u => u.roles.some(r => r.role === requiredRole)).length
								: undefined
						}
					/>
				)
			},
			filterMethod: roleFilterMethod,
			Filter: RoleFilter(roles, textForRole, hasExternal),
			sortMethod: sortByHighestAccessRole(roles),
			style: { padding: '4px' }
		})
	}

	const [usersState, setUsersState] = useState(users)
	const [roleFilter, setRoleFilter] = useState<Filter | undefined>()

	const onFilteredChange = (newFiltering: Filter[], column: any, value: any) => {
		const newRoleFilter = newFiltering.find(f => f.id === 'role')
		setRoleFilter(newRoleFilter)
	}

	// update `usersState` when `users` or `roleFilter` changes
	useEffect(() => {
		let usersToDisplay = users
		// when the role filter is set, only show the matching userRoles in the table
		if (roleFilter) {
			usersToDisplay = users.map(u => {
				const newUser = merge({}, u)
				newUser.roles = u.roles.filter(r => roleUserRoleFilterMethod(roleFilter, r))
				return newUser
			})
		}
		if (!isEqual(usersToDisplay, usersState)) {
			setUsersState(usersToDisplay)
		}
	}, [users, roleFilter, usersState])

	return (
		<div id={id} className="table-container">
			<ReactTable
				className={`-striped`}
				columns={columns}
				data={usersState}
				resizable={false}
				filterable
				onFilteredChange={onFilteredChange}
				defaultSorted={[
					{
						id: 'lastName',
						desc: false
					},
					{
						id: 'firstName',
						desc: false
					}
				]}
				pageSize={Object.keys(usersState).length}
				showPagination={false}
			/>
		</div>
	)
}

// similar to "shouldComponentUpdate", prevent unnecessary renders
export const UserRolesTable = React.memo(
	UserRolesTableComponent,
	(prevProps: UserRolesTableProps, nextProps: UserRolesTableProps) => isEqual(prevProps, nextProps)
)
