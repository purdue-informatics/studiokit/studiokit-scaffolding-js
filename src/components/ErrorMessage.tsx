import IconErrorOutline from '@material-ui/icons/ErrorOutline'
import React, { FunctionComponent } from 'react'

export interface ErrorMessageProps {
	message: string | JSX.Element | undefined
	hideIcon?: boolean
}

export const ErrorMessage: FunctionComponent<ErrorMessageProps> = ({ message, hideIcon }) => {
	if (!message) {
		return null
	}
	return (
		<span className={`color-red b${!hideIcon ? ' flex items-center' : ''}`}>
			{!hideIcon && <IconErrorOutline className="mr1" />}
			{message}
		</span>
	)
}
