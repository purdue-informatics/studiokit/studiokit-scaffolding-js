import Button from '@material-ui/core/Button'
import React, { FunctionComponent, MouseEventHandler } from 'react'
import { Modal } from 'react-bootstrap'
import ConnectedModal from './ConnectedModal'

export interface AlertDialogProps {
	id?: string
	isOpen: boolean
	title: string | JSX.Element
	description?: string | JSX.Element | void
	onProceed?: MouseEventHandler<HTMLElement>
	proceedText?: string
	/** onCancel is needed for closing using the X button, clicking the backdrop, or clicking the cancel button*/
	onCancel: () => void
	cancelText?: string
	hideCancel?: boolean
	onDestroy?: MouseEventHandler<HTMLElement>
	destroyText?: string
	manualBackground?: boolean
}

export const AlertDialog: FunctionComponent<AlertDialogProps> = (props: AlertDialogProps) => {
	const {
		isOpen,
		title,
		description,
		onProceed,
		proceedText,
		onCancel,
		cancelText,
		hideCancel,
		onDestroy,
		destroyText,
		manualBackground
	} = props
	return (
		<ConnectedModal show={isOpen} onHide={onCancel} className={`z-9999${manualBackground ? ' bg-black-50' : ''}`}>
			<Modal.Header closeButton>
				<Modal.Title>{title}</Modal.Title>
			</Modal.Header>
			{!!description && <Modal.Body>{description}</Modal.Body>}
			<Modal.Footer>
				{!!onProceed && (
					<Button
						id="alert-dialog-proceed-btn"
						aria-label={proceedText || 'Confirm'}
						className="btn-primary ttc fr mb1"
						color="primary"
						onClick={onProceed}>
						{proceedText || 'Confirm'}
					</Button>
				)}
				{!hideCancel && (
					<Button
						id="alert-dialog-cancel-btn"
						color="primary"
						aria-label={cancelText || 'Cancel'}
						className={`btn-text ttc${onDestroy ? ' fr' : ' fl'} ${onProceed ? ' mr1' : ''}`}
						onClick={onCancel}>
						{cancelText || 'Cancel'}
					</Button>
				)}
				{!!onDestroy && (
					<Button
						id="alert-dialog-destroy-btn"
						aria-label={destroyText || 'Destroy'}
						color="secondary"
						className="btn-text color-red ttc fl"
						onClick={onDestroy}>
						{destroyText || 'Destroy'}
					</Button>
				)}
			</Modal.Footer>
		</ConnectedModal>
	)
}
