import React, { FunctionComponent } from 'react'

export interface ManageTableNoDataComponentProps {
	entityName: string
	selectedTab?: number
	getTabName: (tab?: number) => string
	queryAll?: boolean
	canQueryAll?: boolean
	isFiltered?: boolean
	hasSearched?: boolean
}

export const ManageTableNoDataComponent: FunctionComponent<ManageTableNoDataComponentProps> = ({
	entityName,
	selectedTab,
	getTabName,
	queryAll,
	canQueryAll,
	isFiltered,
	hasSearched
}) => {
	return (
		<p className="pt4 tc">
			{hasSearched ? (
				<>
					No {entityName.toLocaleLowerCase()} matched your search.
					{canQueryAll && !queryAll && (
						<> You might want to try again with "Search All {entityName}" checked.</>
					)}
				</>
			) : (
				<>
					No {getTabName(selectedTab).toLowerCase()} {entityName.toLocaleLowerCase()}{' '}
					{isFiltered ? 'matched your search' : 'found'}.
				</>
			)}
		</p>
	)
}
