import { debounce } from 'lodash'
import { useCallback, useEffect, useMemo, useState } from 'react'
import React from 'react'
import { Tab, Tabs } from 'react-bootstrap'
import ReactTable, { Column, SortingRule } from 'react-table'
import { TABLE_DEFAULT_PAGE_SIZE } from '../constants/table'
import { Model, Search } from '../types'
import { getPageSizeOptions } from '../utils/table'
import { ManageTableNoDataComponent } from './ManageTableNoDataComponent'
import { PaginationNextButton } from './PaginationNextButton'
import { PaginationPreviousButton } from './PaginationPreviousButton'
import { RefreshIndicator } from './RefreshIndicator'

export interface ManageTableProps<TModel, TSearchDataParams> {
	search: Search
	isFiltered: boolean
	setSelectedTab: (selectedTab: number) => void
	setSortingRules: (newSortingRules: SortingRule[]) => void
	setPageSize: (newPageSize: number, newPage: number) => void
	setPage: (newPage: number) => void
	tabs: number[]
	getTabName: (tab?: number) => string
	searchData: (params: TSearchDataParams) => Record<number, TModel[]>
	searchDataParams: TSearchDataParams
	columns: Column<TModel>[]
	entityName: string
	canQueryAll?: boolean
}

export const ManageTable = <TModel extends Model, TSearchDataParams extends {}>({
	search: { hasSearched, queryAll, selectedTab, sortingRules, pageSize, pageByTab },
	isFiltered,
	setSelectedTab,
	setSortingRules,
	setPageSize,
	setPage,
	tabs,
	getTabName,
	searchData,
	searchDataParams,
	columns,
	entityName,
	canQueryAll
}: ManageTableProps<TModel, TSearchDataParams>) => {
	//#region Data

	const [dataByTab, setDataByTab] = useState<Record<number, TModel[]>>()
	const isInitialized = dataByTab !== undefined

	// search and set state, which can be debounced as a single unit of work
	const searchAndSetData = useCallback(
		(params: TSearchDataParams, setDataByTab: (dataByTab: Record<number, TModel[]>) => void) => {
			setDataByTab(searchData(params))
		},
		[searchData]
	)

	// search is debounced to prevent it being rapidly called
	// useMemo with no dependencies ensures we keep the same debounced function between renders
	const debouncedSearchAndSetData = useMemo(() => debounce(searchAndSetData), [searchAndSetData])

	// search once without debounce to initialize dataByTab state
	useEffect(() => {
		if (!isInitialized) {
			setDataByTab(searchData(searchDataParams))
		}
	}, [isInitialized, searchData, searchDataParams])

	// search using debounced method when searchDataParams changes, after initialization
	// use `isMounted` to not call the callback unless the component is mounted
	useEffect(() => {
		let isMounted = true
		if (isInitialized) {
			debouncedSearchAndSetData(searchDataParams, (dataByTab: Record<number, TModel[]>) => {
				if (isMounted) setDataByTab(dataByTab)
			})
		}
		return () => {
			isMounted = false
		}
	}, [debouncedSearchAndSetData, isInitialized, searchDataParams])

	//#endregion Data

	const noResultsFound = isFiltered && !!dataByTab && Object.values(dataByTab).every(d => d.length === 0)
	const currentTab = selectedTab ?? 1
	const data = dataByTab?.[currentTab]

	//#region Tabs

	const onTabSelect = useCallback(
		(eventKey: string | null) => {
			setSelectedTab(Number(eventKey))
		},
		[setSelectedTab]
	)

	const dataLengthByTab = useMemo(() => {
		const value: Record<number, number> = {}
		tabs.forEach(tab => {
			value[tab] = dataByTab?.[tab].length ?? 0
		})
		return value
	}, [dataByTab, tabs])

	const SearchTabs = useMemo(
		() => (
			<Tabs activeKey={selectedTab} id="search-tabs" onSelect={onTabSelect}>
				{tabs.map(tab => (
					<Tab
						key={tab}
						eventKey={tab.toString()}
						title={`${getTabName(tab)} (${dataLengthByTab[tab]})`}
						disabled={noResultsFound}
					/>
				))}
			</Tabs>
		),
		[dataLengthByTab, getTabName, noResultsFound, onTabSelect, selectedTab, tabs]
	)

	//#endregion Tabs

	//#region Table

	const NoDataComponent = useMemo(
		() => (
			<ManageTableNoDataComponent
				{...{ canQueryAll, entityName, getTabName, hasSearched, isFiltered, queryAll, selectedTab }}
			/>
		),
		[canQueryAll, entityName, getTabName, hasSearched, isFiltered, queryAll, selectedTab]
	)
	const NoDataComponentCallback = useCallback(() => NoDataComponent, [NoDataComponent])

	const Table = useMemo(() => {
		// show pagination if there are more rows than the default page size
		const showPagination = !!data && data.length > TABLE_DEFAULT_PAGE_SIZE
		// if pagination is not enabled, or page is not set, ensure page is 0, the first page
		const pageForTab = showPagination && pageByTab && selectedTab ? pageByTab[selectedTab] : 0
		const pageSizeOptions = getPageSizeOptions(data?.length)

		return (
			<ReactTable
				aria-describedby="search-results-label"
				className={`manage-table -striped cb${data?.length === 1 ? ' single-row-action-button' : ''}`}
				data={data}
				showPagination={showPagination}
				showPaginationTop
				sorted={sortingRules}
				pageSize={pageSize}
				page={pageForTab}
				pageSizeOptions={pageSizeOptions}
				minRows={0} // hide excess empty rows if data is smaller than page size
				columns={columns}
				onSortedChange={setSortingRules}
				onPageSizeChange={setPageSize}
				onPageChange={setPage}
				NextComponent={PaginationNextButton}
				PreviousComponent={PaginationPreviousButton}
				NoDataComponent={NoDataComponentCallback}
			/>
		)
	}, [
		NoDataComponentCallback,
		columns,
		data,
		pageByTab,
		pageSize,
		selectedTab,
		setPage,
		setPageSize,
		setSortingRules,
		sortingRules
	])

	//#endregion Table

	if (!dataByTab) return <RefreshIndicator label="Loading..." labelClassName="color-white" />

	return (
		<>
			<h3 id="search-results-label" className="visually-hidden">
				Search Results
			</h3>
			{SearchTabs}
			{Table}
		</>
	)
}
