import { OAuthTokenOrNull } from '../net'

export interface TokenPersistenceService {
	getPersistedToken: () => OAuthTokenOrNull | Promise<OAuthTokenOrNull>
	persistToken: (oauthToken: OAuthTokenOrNull) => void
}
