export interface LocalLoginRequestBody {
	username: string
	isInstructor?: boolean
	shouldInsertMockLtiClaims?: boolean
}
