export interface CasV1LoginRequestBody {
	username: string
	password: string
}
