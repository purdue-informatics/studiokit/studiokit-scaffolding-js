export interface TicketProviderService {
	getTicket(): string | undefined
	getAppServiceName(): string | undefined
	removeTicket(): void
}
