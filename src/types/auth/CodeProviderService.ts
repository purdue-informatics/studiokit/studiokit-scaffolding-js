export interface CodeProviderService {
	getCode(): string | undefined
	removeCode(): void
}
