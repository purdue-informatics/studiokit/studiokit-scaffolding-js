export interface AuthState {
	isAuthenticated: boolean
	isAuthenticating: boolean
	isInitialized: boolean
	didFail: boolean
}
