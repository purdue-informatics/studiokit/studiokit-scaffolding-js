// Create a type where keys not meeting 'Condition' are set to type 'never'
type FilterFlags<Base, Condition> = { [Key in keyof Base]: Base[Key] extends Condition ? Key : never }

// Remove all the keys of type 'never'
type AllowedNames<Base, Condition> = FilterFlags<Base, Condition>[keyof Base]

/**
 * Create a new type from another type which only includes properties of a specified type.
 * @example
 * // Creates a type with only the string properties of 'SomeType'
 * PropertyOfType<SomeType, string>
 */
export type PropertyOfType<Base, Condition> = Pick<Base, AllowedNames<Base, Condition>>

export default PropertyOfType
