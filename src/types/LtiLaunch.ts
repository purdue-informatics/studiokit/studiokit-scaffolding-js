import { Model } from './net'

export interface LtiLaunch extends Model {
	id: number
	typename: 'LtiLaunch'
	userId: string
	externalProviderId: number
	externalId: string
	groupId: number | null
	name: string
	description: string | null
	roles: string | null
	rosterUrl: string | null
	gradesUrl: string | null
	ltiVersion: string | null
	returnUrl: string | null
	isAutoGradePushEnabled: boolean | null
	courseSectionStartDate: string | null
	courseSectionEndDate: string | null
	deepLinkingReturnUrl: string | null
}
