import { SortingRule } from 'react-table'

export interface Search {
	keywords?: string
	invalidKeywords?: boolean
	hasSearched?: boolean
	dateString?: string
	date?: Date | null
	queryAll?: boolean
	includeIfSoftDeleted?: boolean
	requiredMessage?: string | null
	selectedTab?: number
	/** react-table sorting rules */
	sortingRules?: SortingRule[]
	/** react-table page size, for pagination */
	pageSize?: number
	/** react-table currently selected page for a given tab, for pagination */
	pageByTab?: Record<string, number | undefined>
}
