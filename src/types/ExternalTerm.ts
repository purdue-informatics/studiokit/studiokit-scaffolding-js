import { Model } from './net'

export interface ExternalTerm extends Model {
	id: number
	externalProviderId: number
	externalId: string
	name: string
	startDate: string
	endDate: string
}
