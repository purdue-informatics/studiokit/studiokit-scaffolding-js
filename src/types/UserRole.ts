import { User } from './User'

export interface UserRole extends User {
	role: string
}

export interface EntityUserRole extends UserRole {
	userId: string
	entityId: number
}

export interface GroupUserRole extends EntityUserRole {
	isExternal: boolean
}

export interface UserWithRoles<TUserRole extends UserRole = UserRole> extends User {
	roles: TUserRole[]
}
