import { Model } from './net'

export interface Client extends Model {
	clientId: string
	currentVersion?: string
	minVersion?: string
}
