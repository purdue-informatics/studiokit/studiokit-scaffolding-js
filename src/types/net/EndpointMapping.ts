import { EndpointConfig } from './EndpointConfig'

/**
 * A single model endpoint mapping. Allows for nested mappings.
 */
export interface EndpointMapping extends Record<string, EndpointMapping | EndpointConfig | undefined> {
	_config?: EndpointConfig
}
