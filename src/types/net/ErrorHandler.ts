import { ModelFetchRequestAction, PeriodicModelFetchRequestAction } from '../../redux/actions/ModelAction'
import { FetchConfig } from './FetchConfig'
import { FetchErrorData } from './FetchErrorData'
import { FetchResult } from './FetchResult'

export type ErrorHandler = (
	error: Error,
	modelFetchRequestAction: ModelFetchRequestAction | PeriodicModelFetchRequestAction,
	fetchConfig?: FetchConfig,
	fetchResult?: FetchResult,
	fetchErrorData?: FetchErrorData
) => void
