import { Metadata } from './Metadata'

/**
 * Represents an API request/response that is stored in redux.
 */
export interface Model {
	/** Metadata about the most recent API request, defined once a request starts. */
	_metadata?: Metadata
	/** The id of the model, defined after a successful request, if available. */
	id?: string | number
	/** A unique identifier used for action dispatchers to correlate responses to requests. */
	guid?: string
}
