import { FetchConfig } from './FetchConfig'

/**
 * Configuration for a single model endpoint mapping.
 */
export interface EndpointConfig {
	isCollection?: boolean
	fetch?: FetchConfig
}
