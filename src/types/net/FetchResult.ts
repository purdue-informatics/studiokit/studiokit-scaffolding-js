export interface FetchResult {
	headers: Headers
	ok: boolean
	redirected: boolean
	status: number
	statusText: string
	type: ResponseType
	url: string
	data: any
}
