export enum CUSTOM_ERROR_STATUS_CODE {
	UNKNOWN = -1,
	NO_NETWORK_CONNECTION = -2
}

export interface FetchErrorData {
	status: number
	title: string
	/** human-readable explanation. not always returned from the API, but `fetchService.sendFetch` will default to `null`. */
	detail: string | null
	type?: string
	instance?: string
	traceId?: string
	/** validation errors, e.g. from a BadRequest (400) response */
	errors?: Record<string, string[]>
}
