import { EndpointMapping } from './EndpointMapping'

/**
 * A mapping of models to endpoints, where each key is a *modelName*.
 * Represents the nested structure in which models will be stored in redux.
 */
export interface EndpointMappings extends Record<string, EndpointMapping> {}
