import { CallEffect } from 'redux-saga/effects'
import { OAuthTokenOrNull } from './OAuthTokenOrNull'

export type TokenAccessFunction = (modelName: string) => IterableIterator<OAuthTokenOrNull | CallEffect>
