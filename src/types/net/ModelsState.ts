import { Dictionary } from 'lodash'
import { Model } from './Model'
import { ModelCollection } from './ModelCollection'

/**
 * Defines the base object in redux at `state.models`.
 */
export interface ModelsState extends Dictionary<Model | ModelCollection | undefined> {}
