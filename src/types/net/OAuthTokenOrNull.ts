import { OAuthToken } from './OAuthToken'

export type OAuthTokenOrNull = OAuthToken | null
