import { Metadata } from './Metadata'
import { Model } from './Model'

/**
 * Represents a collection API request/response that is stored in redux.
 *
 * @template T enforce a type on all properties that are defined and not `Metadata`. Defaults to `Model`.
 */
export interface ModelCollection<T extends Model = Model> {
	/** Metadata about the most recent collection API request, defined once a request starts. */
	_metadata?: Metadata
	[key: number]: T | null | undefined
	[key: string]: T | Metadata | null | undefined
}
