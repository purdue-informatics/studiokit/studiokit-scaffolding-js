import { HTTPMethod } from './HTTPMethod'

/**
 * Configuration for a fetch request.
 * Used by EndpointConfig to define defaults for a specific endpoint.
 */
export interface FetchConfig {
	path?: string
	method?: HTTPMethod
	headers?: Record<string, string>
	body?: any
	contentType?: string
	queryParams?: Record<string, string | number | boolean>
}
