import { FetchErrorData } from './FetchErrorData'

/**
 * Fetch related data about an API response.
 */
export interface Metadata {
	isFetching: boolean
	hasError: boolean
	lastFetchErrorData?: FetchErrorData
	fetchedAt?: Date
}
