import { Model } from './net'
/**
 * Simple User object for with general data for displaying users
 */
export interface User extends Model {
	/**
	 * App Id of User
	 */
	id: string
	/**
	 * User's given name
	 */
	firstName: string | null
	/**
	 * User's family name
	 */
	lastName: string | null
	/**
	 * User's email
	 */
	email: string | null
	/**
	 * External email with domain removed. Equivalent to careerAccountAlias for Purdue
	 */
	uid: string | null
}

/**
 * More detailed user object from UserInfo api
 */
export interface UserInfo extends User {
	/**
	 * Equivalent to email
	 */
	userName: string
	/**
	 * Purdue user's career account alias
	 */
	careerAccountAlias: string | null
	/**
	 * External User Id. Equivalent to puid for Purdue
	 */
	employeeNumber: string | null
	/**
	 * User's Purdue Id
	 */
	puid: string | null
	/**
	 * Array of global roles for the User
	 */
	roles: string[]
	/**
	 * Array of global activities for the User
	 */
	activities: string[]
	/**
	 * Date string when user's demo started. Null when not on demo shard
	 */
	dateDemoGenerationSucceeded: string | null
	dateStored: string
	dateLastUpdated: string
	/**
	 * Whether or not the User is being impersonated by another User
	 */
	isImpersonated: boolean

	lockDownBrowserTestLaunchUrl: string | null
}

export interface EntityUser extends User {
	entityId: number
}
