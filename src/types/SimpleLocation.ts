/**
 * A simple interface for a dom `Location`, that can be used for testing.
 */
export interface SimpleLocation {
	/**
	 * Returns the Location object's URL's fragment (includes leading "#" if non-empty).
	 * Can be set, to navigate to the same URL with a changed fragment (ignores leading "#").
	 */
	hash: string
	/**
	 * Returns the Location object's URL's host and port (if different from the default
	 * port for the scheme).
	 * Can be set, to navigate to the same URL with a changed host and port.
	 */
	host: string
	/**
	 * Returns the Location object's URL's host.
	 * Can be set, to navigate to the same URL with a changed host.
	 */
	hostname: string
	/**
	 * Returns the Location object's URL.
	 * Can be set, to navigate to the given URL.
	 */
	href: string
	/**
	 * Returns the Location object's URL's path.
	 * Can be set, to navigate to the same URL with a changed path.
	 */
	pathname: string
	/**
	 * Returns the Location object's URL's port.
	 * Can be set, to navigate to the same URL with a changed port.
	 */
	port: string
	/**
	 * Returns the Location object's URL's scheme.
	 * Can be set, to navigate to the same URL with a changed scheme.
	 */
	protocol: string
	/**
	 * Returns the Location object's URL's query (includes leading "?" if non-empty).
	 * Can be set, to navigate to the same URL with a changed query (ignores leading "?").
	 */
	search: string
}
