import { Model } from './net'

export interface DeletableModel extends Model {
	isDeleted: boolean
}
