import { Model } from './net'

export interface ExternalGroup extends Model {
	id: number
	groupId: number
	externalId: string
	externalProviderId: number
	userId: string
	name: string
	description: string | null
	rosterUrl: string | null
	gradesUrl: string | null
	isAutoGradePushEnabled: boolean | null
	typename: 'ExternalGroup'
}
