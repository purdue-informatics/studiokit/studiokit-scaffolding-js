import { MODEL_STATUS } from '../constants/modelStatus'
import { UniTimeGroup } from './UniTime'
import { User } from './User'

export interface OwnerSchedule {
	hookId: string
	status: MODEL_STATUS
	owner: User
	externalGroups: UniTimeGroup[]
}
