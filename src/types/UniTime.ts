import { Model } from './net'

export interface InstructorSchedule {
	firstName: string | null
	lastName: string | null
	userId: string
	externalGroups: UniTimeGroup[]
}

export interface UniTimeGroup extends Model {
	id: number
	externalProviderId: number
	externalId: string
	userId: string
	name: string
	description: string | null
	isAutoGradePushEnabled: boolean | null
	typename: 'UniTimeGroup'
}
