import { ExternalGroup } from './ExternalGroup'
import { Model, ModelCollection } from './net'
import { EntityUser } from './User'
import { GroupUserRole } from './UserRole'

export interface Group extends Model {
	id: number
	name: string
	externalTermId: number | null
	startDate: string | null
	endDate: string | null
	isDeleted: boolean
	isRosterSyncEnabled: boolean
	roles: string[]
	activities: string[]
	owners: EntityUser[]
	graders: EntityUser[]
	externalGroups: ExternalGroup[]
	groupUserRoles?: ModelCollection<GroupUserRole>
}
