import { Dictionary } from 'lodash'
import { MODEL_STATUS } from '../constants'
import { BaseReduxState } from './BaseReduxState'
import { HTTPMethod, Model, ModelCollection } from './net'

/** The props shared by collection HOCs and hooks. */
export interface CollectionCommonProps {
	/**
	 * The generic path (no Ids) to where the model is stored in redux.
	 * A path relating to an item in defined in `constants/configuration/getEndpointMappings()` (levels separated by a '.').
	 * Can be set in constructor or at render time, e.g. `<C modelName="otherModel" />`
	 */
	modelName: string
	/** The Ids that will be inserted between the components of `modelName`, to link to specific collection items in redux. */
	pathParams: Array<string | number>
	/** Custom queryParams for use in all requests. */
	queryParams?: Record<string, string | number | boolean>
	/** Whether to disable triggering `load()` after mounting. Default is `false`. */
	disableAutoLoad?: boolean
	/** Whether to disable triggering `load()` when `pathParams` or `queryParams` change. Default is `false`. */
	disableAutoLoadOnParamsChange?: boolean
}

/** See `CollectionSelectorMethod` */
export interface CollectionSelectorMethodParams {
	guid: string
	modelName: string
	pathParams: Array<string | number>
	routeMatchParams: Dictionary<any>
	state: BaseReduxState
}

/** See `CollectionSelectorMethod` */
export interface CollectionSelectorMethodResponse<TModel extends Model> {
	modelName: string
	pathParams: Array<string | number>
	model: TModel | ModelCollection<TModel>
	isCollectionItem: boolean
}

/** A method used to select a collection or collection item model from redux. */
export type CollectionSelectorMethod = <TModel extends Model>(
	params: CollectionSelectorMethodParams
) => CollectionSelectorMethodResponse<TModel>

/** The configuration object passed to all collection methods,
 * to encapsulate logic and state from the calling collection HOC or hook. */
export interface CollectionMethodConfiguration extends CollectionCommonProps {
	guid: string
	isInitialized: boolean
	changeModelStatus: (modelStatus: MODEL_STATUS, fetchingId?: string | number) => void
}

export interface CollectionItemLoadParams {
	/** Custom pathParams for use in the request. */
	pathParams?: Array<string | number>
	/** Custom queryParams for use in the request. */
	queryParams?: Record<string, string | number | boolean>
	/** The period (in ms) at which the collection should reload the data. */
	period?: number
	/** The taskId to set for later cancellation. */
	taskId?: string
	/** If true, the fetchReducer will replace the existing value in redux, instead of merging the incoming with existing. */
	replaceValue?: boolean
}

export interface CollectionLoadParams {
	/** Id of an item in the collection to load. Otherwise entire collection is loaded. */
	id?: string | number
	/** Custom pathParams for use in the request. */
	pathParams?: Array<string | number>
	/** Custom queryParams for use in the request. */
	queryParams?: Record<string, string | number | boolean>
	/** The period (in ms) at which the collection should reload the data. */
	period?: number
	/** The taskId to set for later cancellation. */
	taskId?: string
	/** If true, the fetchReducer will replace the existing value in redux, instead of merging the incoming with existing. */
	replaceValue?: boolean
}

export interface CollectionCreateParams {
	/** The body to send in the request. */
	body: any
	/** The 'Content-Type' header value to use. */
	contentType?: string
	/** Custom queryParams for use in the request. */
	queryParams?: Record<string, string | number | boolean>
}

export interface CollectionUpdateParams {
	/** The id of the collection item to update. */
	id: string | number
	/** The body to send in the request. */
	body: any
	/** The 'Content-Type' header value to use. */
	contentType?: string
	/** The HTTP method to use. Defaults to 'PUT'. */
	method?: HTTPMethod
	/** Custom queryParams for use in the request. */
	queryParams?: Record<string, string | number | boolean>
}

export interface CollectionItemUpdateParams {
	/** The body to send in the request. */
	body: any
	/** The 'Content-Type' header value to use. */
	contentType?: string
	/** The HTTP method to use. Defaults to 'PUT'. */
	method?: HTTPMethod
	/** Custom queryParams for use in the request. */
	queryParams?: Record<string, string | number | boolean>
}

export interface CollectionDeleteParams {
	/** The id of the collection item to delete. */
	id: string | number
	/** The body to send in the request. */
	body?: any
	/** Custom queryParams for use in the request. */
	queryParams?: Record<string, string | number | boolean>
}

export interface CollectionItemDeleteParams {
	/** The body to send in the request. */
	body?: any
	/** Custom queryParams for use in the request. */
	queryParams?: Record<string, string | number | boolean>
}

/** The methods returned by `CollectionItemComponent` or `useCollectionItem`. */
export interface CollectionItemMethods {
	/** Dispatch a GET request for the collection item. */
	load: (params?: CollectionItemLoadParams) => void
	/**
	 * Stop periodic fetch of the collection.
	 *
	 * @param taskId The taskId that was initially set when periodic fetch was requested.
	 */
	stopPeriodicLoad: (taskId: string) => void
	/**
	 * Dispatch a POST request to create the collection item.
	 *
	 * The new item can be tracked using the `guid` as its key in the parent collection.
	 */
	create: (params: CollectionCreateParams) => void
	/** Dispatch a PUT request to update the collection item. */
	update: (params: CollectionItemUpdateParams) => void
	/** Dispatch a DELETE request to remove the collection item. */
	delete: (params?: CollectionItemDeleteParams) => void
}

/** The methods returned by `CollectionComponent` or `useCollection`. */
export interface CollectionMethods {
	/** Dispatch a GET request for the collection, or an item in the collection. */
	load: (params?: CollectionLoadParams) => void
	/**
	 * Stop periodic fetch of the collection.
	 *
	 * @param taskId The taskId that was initially set when periodic fetch was requested.
	 */
	stopPeriodicLoad: (taskId: string) => void
	/**
	 * Dispatch a POST request to create an item in the collection.
	 *
	 * The new item can be tracked using the `guid` at `model[guid]`.
	 */
	create: (params: CollectionCreateParams) => void
	/** Dispatch a PUT request to update an item in the collection. */
	update: (params: CollectionUpdateParams) => void
	/** Dispatch a DELETE request to remove an item from the collection. */
	delete: (params: CollectionDeleteParams) => void
}

/** The response from selecting a collection item model from redux. */
export interface CollectionItemReduxResponse<TModel extends Model> {
	/** The collection item model loaded from redux, or loaded into redux using `load()`. */
	model: TModel
}

export interface CollectionItemDerivedProps<TModel extends Model> {
	/** The collection item model, without any of its nested relations. */
	modelMinusRelations: Partial<TModel>
}

/** The response from selecting a collection model from redux. */
export interface CollectionReduxResponse<TModel extends Model> {
	/** The collection model loaded from redux if available, or loaded into redux using `load()`. */
	model: ModelCollection<TModel>
}

export interface CollectionDerivedProps<TModel extends Model> {
	/** The collection model converted to an array of all collection items. */
	modelArray: TModel[]
}

/** The internal state shared by collection HOCs and hooks. */
export interface CollectionCommonState {
	/** The current status of the model. */
	modelStatus: MODEL_STATUS
	/** The previous status of the model. */
	previousModelStatus: MODEL_STATUS
	/** An id that is used when targeting a specific item in a collection. Only applies when `model` is a collection. */
	fetchingId?: string | number
}
