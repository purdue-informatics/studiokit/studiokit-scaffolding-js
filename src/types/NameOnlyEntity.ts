export interface NameOnlyEntity {
	name: string | null
}
