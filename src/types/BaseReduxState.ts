import { RouterState } from 'connected-react-router'
import { PersistedState } from 'redux-persist'
import { ModalsState } from '../redux/reducers/modalsReducer'
import { NotificationState } from '../redux/reducers/notificationsReducer'
import { SearchState } from '../redux/reducers/searchReducer'
import { AuthState } from './auth'
import { Client } from './Client'
import { Configuration } from './Configuration'
import { ExternalProvider } from './ExternalProvider'
import { ExternalTerm } from './ExternalTerm'
import { Group } from './Group'
import { IdentityProvider } from './IdentityProvider'
import { LtiLaunch } from './LtiLaunch'
import { ModelCollection, ModelsState } from './net'
import { User, UserInfo } from './User'
import { UserWithRoles } from './UserRole'

/**
 * Extends the base object in redux at `state.models`, with all scaffolding related collections and models.
 *
 * @template TGroup The type of group stored in `state.models.group`. Defaults to `Group`.
 * @template TSearch The type of search model stored in `state.models.search`
 */
export interface BaseModelsState<
	TGroup extends Group = Group,
	TSearch extends BaseSearchModelsState<TGroup> = BaseSearchModelsState<TGroup>
> extends ModelsState {
	configuration?: Configuration
	client?: Client
	identityProviders?: ModelCollection<IdentityProvider>
	externalTerms?: ModelCollection<ExternalTerm>
	externalProviders?: ModelCollection<ExternalProvider>
	groups?: ModelCollection<TGroup>
	userRoles?: ModelCollection<UserWithRoles>
	ltiLaunches?: ModelCollection<LtiLaunch>
	search?: TSearch
	user?: {
		userInfo?: UserInfo
	}
}

/**
 * Extends the base object in redux at `state.models.search`, with all scaffolding related collections and models.
 *
 * @template TGroup The type of group stored in `state.models.search.group`. Defaults to `Group`.
 */
export interface BaseSearchModelsState<TGroup extends Group = Group> extends ModelsState {
	users?: ModelCollection<User>
	groups?: ModelCollection<TGroup>
}

/**
 * Defines the base redux state of the application.
 *
 * @template TModels The type of state to be stored at `state.models`, allowing for extension.
 */
export interface BaseReduxState<TModels extends BaseModelsState = BaseModelsState> {
	auth: AuthState & PersistedState
	notifications: NotificationState & PersistedState
	modals: ModalsState
	models: TModels
	router: RouterState
	search: SearchState
}
