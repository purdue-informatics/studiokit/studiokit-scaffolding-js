import { Model } from './net'

export enum ARTIFACT_TYPE {
	TEXT_ARTIFACT = 'TextArtifact',
	FILE_ARTIFACT = 'FileArtifact',
	URL_ARTIFACT = 'UrlArtifact'
}

export interface Artifact extends Model {
	id: number
	url: string
	fileName: string
	wordCount: number | null
	typename: ARTIFACT_TYPE
	dateStored: string
	dateLastUpdated: string
}
