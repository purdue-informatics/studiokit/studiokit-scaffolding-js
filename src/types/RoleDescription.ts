export interface RoleDescriptions {
	[role: string]: React.ReactNode
}
