export interface DeepLinkingResponseRequest {
	gradableId: number
	url: string
}
