import { EXTERNAL_PROVIDER_TYPE } from '../constants/externalProviderType'
import { Model } from './net'

export interface ExternalProvider extends Model {
	id: number
	typename: EXTERNAL_PROVIDER_TYPE
	termSyncEnabled: boolean
	rosterSyncEnabled: boolean
	gradePushEnabled: boolean
	shouldDisplayCreateNonLtiGroupAlert: boolean
}
