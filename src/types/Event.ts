/**
 * Event used for tracking in google analytics, app insights or other tracking service
 */
export interface CustomEvent {
	/** Lowercase underscored string to represent the event (i.e. page_view, click_print, etc.) */
	eventName: string
	/** Optional name of the event category, note that it can be used as a filter in google event reports (i.e Print, Help, etc.) */
	category?: string
	/** Optional name of the event action, note that it can be used as a filter in google event reports */
	action?: string
	/** Optional name of the event label, note that it can be used as a filter in google event reports */
	label?: string
}
