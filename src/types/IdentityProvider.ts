import { Model } from './net'

export interface IdentityProvider extends Model {
	id: number
	name: string
	loginUrl: string
	logoutUrl: string
	mobileLoginUrl: string | null
	text: string | null
	/** Base64 encoded image string */
	image: string | null
}
