import { NOTIFICATION_TYPE } from '../constants'

export interface Notification {
	id: string
	text: string | JSX.Element
	type: NOTIFICATION_TYPE
	/** Determines when the notification is shown
	 *
	 * The value can be one of the following, with the given behavior:
	 * * the identifier of a modal => the notification is shown after that specific modal is closed
	 * * `null` => the notification is shown immediately
	 * * `undefined` => the notification is shown after ALL modals are closed (the default behavior)
	 */
	modalId?: string | null
}
