import moment from 'moment-timezone'
import momentUtils from 'moment-timezone/moment-timezone-utils'
import { getDefaultTimeZoneId, guessTimeZoneId } from './timezone'

/**
 * This value is used by time-sensitive functions in this module and other date/time utils. Any time
 * a new Date object is constructed, it will use this value to adjust the time by this offset (to
 * make it consistent with a known time)
 */
let clockOffset = 0

/**
 * When a GroupAssessment comes in the door, it includes the current UTC datetime. This method is
 * used to update the difference between the (accurate) server time and the browser time
 */
export const setClockOffset = function (offset: number) {
	clockOffset = offset
}

/**
 * Get the current UTC time, calibrated to the API server's clock
 */
export const getServerNowUtc = function () {
	return new Date(Date.now() + clockOffset)
}

/**
 * Return the provided date, offset by the clockOffset
 */
export const getOffsetDate = (date: Date) => {
	return new Date(date.getTime() + clockOffset)
}

export const getZonedMomentFromUtc = (
	dateTimeUtc = getServerNowUtc().toISOString(),
	timeZoneId = guessTimeZoneId()
) => {
	return moment.utc(dateTimeUtc).tz(timeZoneId)
}

export const getZonedMomentFromUtcInDefaultZone = (dateTimeUtc = getServerNowUtc().toISOString()) => {
	const timeZoneId = getDefaultTimeZoneId()
	return getZonedMomentFromUtc(dateTimeUtc, timeZoneId)
}

export const getZonedMomentFromLocalDateTime = (localDateTime: Date | string, timeZoneId = guessTimeZoneId()) => {
	const localMoment = moment(localDateTime)
	const zonedMoment = moment.tz(
		[
			localMoment.year(),
			localMoment.month(),
			localMoment.date(),
			localMoment.hour(),
			localMoment.minute(),
			localMoment.second(),
			localMoment.millisecond()
		],
		timeZoneId
	)
	return zonedMoment
}

export const getLocalMomentFromUtc = (
	dateTimeUtc = getServerNowUtc().toISOString(),
	timeZoneId = guessTimeZoneId()
) => {
	const zonedMoment = getZonedMomentFromUtc(dateTimeUtc, timeZoneId)
	const localMoment = moment([
		zonedMoment.year(),
		zonedMoment.month(),
		zonedMoment.date(),
		zonedMoment.hour(),
		zonedMoment.minute(),
		zonedMoment.second(),
		zonedMoment.millisecond()
	])
	return localMoment
}

export const getLocalDateTimeFromUtc = (dateTimeUtc: string) => {
	return getLocalMomentFromUtc(dateTimeUtc).toDate()
}

export const getDateMinusTime = (dateTime: Date) => {
	return new Date(dateTime.getFullYear(), dateTime.getMonth(), dateTime.getDate())
}

export function getEndOfDay(dateTimeUtc: string) {
	return moment(dateTimeUtc)
		.startOf('day')
		.add(1, 'days')
		.subtract(3, 'milliseconds') // must be minus 3 for SQL date
		.toDate()
}

export function getEndOfMinute(dateTimeUtc: string) {
	return moment(dateTimeUtc)
		.startOf('minute')
		.add(1, 'minute')
		.subtract(3, 'milliseconds') // must be minus 3 for SQL date
		.toDate()
}

/**
 * Is the current time, "now", **after or equal to** the given date.
 *
 * @param dateTimeUtc The date to compare the current time, "now", as an ISO date string.
 * @param nowUtc Optional. The current time, "now", as an ISO date string.
 *
 * @returns Whether or not "now" is **after or equal to** `dateTimeUtc`
 */
export function isNowEqualOrAfterDate(dateTimeUtc?: string, nowUtc = getServerNowUtc().toISOString()) {
	if (!dateTimeUtc) {
		return false
	}
	const adjustedDateTimeMoment = getLocalMomentFromUtc(dateTimeUtc)
	const currentMoment = getLocalMomentFromUtc(nowUtc)
	return currentMoment.isSameOrAfter(adjustedDateTimeMoment)
}

/**
 * Is the current time, "now", **before or equal to** the given date.
 *
 * @param dateTimeUtc The date to compare the current time, "now", as an ISO date string.
 * @param nowUtc Optional. The current time, "now", as an ISO date string.
 *
 * @returns Whether or not "now" is **before or equal to** `dateTimeUtc`.
 */
export function isNowEqualOrBeforeDate(dateTimeUtc?: string, nowUtc = getServerNowUtc().toISOString()) {
	if (!dateTimeUtc) {
		return false
	}
	const adjustedDateTimeMoment = getLocalMomentFromUtc(dateTimeUtc)
	const currentMoment = getLocalMomentFromUtc(nowUtc)
	return currentMoment.isSameOrBefore(adjustedDateTimeMoment)
}

/**
 * Is the current time, "now", **after** the given date.
 *
 * @param dateTimeUtc The date to compare the current time, "now", as an ISO date string.
 * @param nowUtc Optional. The current time, "now", as an ISO date string.
 *
 * @returns Whether or not "now" is **after** `dateTimeUtc`.
 */
export function isNowAfterDate(dateTimeUtc?: string, nowUtc = getServerNowUtc().toISOString()) {
	if (!dateTimeUtc) {
		return false
	}
	const adjustedDateTimeMoment = getLocalMomentFromUtc(dateTimeUtc)
	const currentMoment = getLocalMomentFromUtc(nowUtc)
	return currentMoment > adjustedDateTimeMoment
}

/**
 * Is the current time, "now", **before** the given date.
 *
 * @param dateTimeUtc The date to compare the current time, "now", as an ISO date string.
 * @param nowUtc Optional. The current time, "now", as an ISO date string.
 *
 * @returns Whether or not "now" is **before** `dateTimeUtc`.
 */
export function isNowBeforeDate(dateTimeUtc?: string, nowUtc = getServerNowUtc().toISOString()) {
	if (!dateTimeUtc) {
		return false
	}
	const adjustedDateTimeMoment = getLocalMomentFromUtc(dateTimeUtc)
	const currentMoment = getLocalMomentFromUtc(nowUtc)
	return currentMoment < adjustedDateTimeMoment
}

/**
 * Returns a date and time string with full date, e.g. `March 5, 2018 @ 4:05 PM EDT`
 * @param dateTimeUtc A UTC date time string
 * @param withTimeZone (optional) Whether to include the time zone abbreviation. Defaults to `true`.
 */
export function getFormattedFullDateAndTime(dateTimeUtc: string, withTimeZone = true) {
	return applyFormat(dateTimeUtc, `MMMM D, YYYY @ h:mm\u00a0A${withTimeZone ? ' z' : ''}`)
}

/**
 * Returns a date and time string with day of week and American slash dates, e.g. `Mon, 3/5/2018 @ 4:05 PM EDT`
 * @param dateTimeUtc A UTC date time string
 * @param withTimeZone (optional) Whether to include the time zone abbreviation. Defaults to `true`.
 */
export function getFormattedFullDateWithWeek(dateTimeUtc: string, withTimeZone = true) {
	return applyFormat(dateTimeUtc, `ddd, M/D/YYYY @ h:mm\u00a0A${withTimeZone ? ' z' : ''}`)
}

/**
 * Returns a date and time string with American slash dates, and no leading zeroes, e.g. `3/5/18 4:05 PM EDT`
 * @param dateTimeUtc A UTC date time string
 * @param withTimeZone (optional) Whether to include the time zone abbreviation. Defaults to `true`.
 */
export function getFormattedNumberedDateAndTime(dateTimeUtc: string, withTimeZone = true) {
	return applyFormat(dateTimeUtc, `M/D/YY h:mm\u00a0A${withTimeZone ? ' z' : ''}`)
}

/**
 * Returns a date and time string with American slash dates, no leading zeroes, and no year, e.g. `3/5 4:05 PM EDT`
 * @param dateTimeUtc A UTC date time string
 * @param withTimeZone (optional) Whether to include the time zone abbreviation. Defaults to `true`.
 */
export function getFormattedNumberedDateAndTimeWithoutYear(dateTimeUtc: string, withTimeZone = true) {
	return applyFormat(dateTimeUtc, `M/D h:mm\u00a0A${withTimeZone ? ' z' : ''}`)
}

/**
 * Returns a date string with short month and full year, e.g. `Mar 5, 2018`
 * @param dateTimeUtc A UTC date time string
 */
export function getFormattedSimplifiedDate(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'MMM D, YYYY')
}

/**
 * Returns a date string with short month and no year, e.g. `Mar 5`
 * @param dateTimeUtc A UTC date time string
 */
export function getFormattedSimplifiedDateWithoutYear(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'MMM D')
}

/**
 * Returns a date string as either 'MMM D, YYYY' or 'MMM D' if the date's year is the same as the current year
 * @param dateTimeUtc A UTC date time string
 * @param nowUtc (optional) A UTC date time string of "now". Used for testing.
 */
export function getFormattedSimplifiedDateWithYearIfNotCurrent(
	dateTimeUtc: string,
	nowUtc = getServerNowUtc().toISOString()
) {
	const nowUtcMoment = moment(nowUtc)
	const dateTimeMoment = moment(dateTimeUtc)
	return Math.abs(nowUtcMoment.year() - dateTimeMoment.year()) === 0
		? getFormattedSimplifiedDateWithoutYear(dateTimeUtc)
		: getFormattedSimplifiedDate(dateTimeUtc)
}

/**
 * Returns a date string with American slash dates, no leading zeroes, and short year, e.g. `3/5/18`
 * @param dateTimeUtc A UTC date time string
 */
export function getFormattedNumberedDate(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'M/D/YY')
}

/**
 * Returns a date string with American slash dates, no leading zeroes, and full year, e.g. `3/5/2018`
 * @param dateTimeUtc A UTC date time string
 */
export function getFormattedNumberedDateWithFullYear(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'M/D/YYYY')
}

/**
 * Returns a date string for use in HTML inputs, e.g. `2018-03-05`
 * @param dateTimeUtc A UTC date time string
 */
export function getFormattedNumberedDateForInput(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'YYYY-MM-DD')
}

/**
 * Returns a date string with American slash dates and no year and no leading zeroes, e.g. `3/5`
 * @param dateTimeUtc A UTC date time string
 */
export function getFormattedNumberedDateWithoutYear(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'M/D')
}

/**
 * Returns a time string, e.g. `4:05 PM EDT`
 * @param dateTimeUtc A UTC date time string
 * @param withTimeZone (optional) Whether to include the time zone abbreviation. Defaults to `true`.
 */
export function getFormattedNumberedTime(dateTimeUtc: string, withTimeZone = true) {
	return applyFormat(dateTimeUtc, `h:mm\u00a0A${withTimeZone ? ' z' : ''}`)
}

/**
 * Returns a time string with possibly full time zone name, e.g. `4:05 PM EDT` or `4:05 PM CST (Asia/Taipei)`
 * @param dateTimeUtc A UTC date time string
 * @param showEasternZoneId (optional) Whether or not to include the timeZoneId in the result for EST/EDT. Defaults to `false`.
 * @param timeZoneId (optional) A time zone Id. Defaults to the current time zone id, using `guessTimeZoneId()`.
 */
export function getFormattedNumberedTimeWithFullZone(
	dateTimeUtc: string,
	showEasternZoneId = false,
	timeZoneId = guessTimeZoneId()
) {
	const zonedMoment = getZonedMomentFromUtc(dateTimeUtc, timeZoneId)
	const time = applyFormat(dateTimeUtc, 'h:mm\u00a0A', timeZoneId)
	const timezoneAbbreviation = zonedMoment ? zonedMoment.zoneAbbr() : ''
	return (timezoneAbbreviation === 'EDT' || timezoneAbbreviation === 'EST') && !showEasternZoneId
		? // Don't show the full zone name if in EDT or EST, unless told to
		  `${time} ${timezoneAbbreviation}`
		: // If not EDT or EST, show the zone name, replacing underscores with spaces
		  // Ex: "Asia/Taipei"
		  `${time} ${timezoneAbbreviation} (${timeZoneId.replace('_', ' ')})`
}

/**
 * Returns a time string without time zone, e.g. `4:05 PM`
 * @param dateTimeUtc A UTC date time string
 */
export function getFormattedNumberedTimeWithoutZone(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'h:mm\u00a0A')
}

/**
 * Returns a time string without time zone, in 24 hr time, e.g. `16:05`
 * @param dateTimeUtc A UTC date time string
 */
export function getFormattedNumberedTimeWithoutZoneOrMeridian(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'HH:mm')
}

/**
 * Returns a full formatted time zone string for the given `timeZoneId`.
 *
 * Combines all possible abbreviations for the zone and the timeZoneId, e.g. `EST/EDT`, `PST/PDT (America/Los Angeles)` or `CST (Asia/Taipei)`
 *
 * If abbreviations are not alpha characters, e.g. `-04`, then only the timeZoneId is returned.
 *
 * @param timeZoneId (optional) A time zone Id. Defaults to the current time zone id, using `guessTimeZoneId()`.
 * @param showEasternZoneId (optional) Whether or not to include the timeZoneId in the result for EST/EDT. Defaults to `false`.
 * @param nowUtc (optional) The current time, "now", as an ISO date string.
 */
export function getFullFormattedTimeZone(
	timeZoneId = guessTimeZoneId(),
	showEasternZoneId = false,
	nowUtc = getServerNowUtc().toISOString()
) {
	const zone = moment.tz.zone(timeZoneId)
	if (!zone) return ''

	const zonedMoment = getZonedMomentFromUtc(nowUtc, timeZoneId)
	// filter the zone data down to just recent years, instead of the default of 1900 to 2038
	const filteredZone = momentUtils.tz.filterYears(zone, zonedMoment.year() - 1, zonedMoment.year() + 1)
	// find all unique abbreviations for the zone, combined into a string
	const abbreviations = filteredZone.abbrs
		// filter out non-alpha abbreviations
		.filter(abbr => /[a-zA-Z]+/g.test(abbr))
		.reduce((uniqueAbbrs: string[], abbr) => {
			if (!uniqueAbbrs.includes(abbr)) {
				uniqueAbbrs.push(abbr)
			}
			return uniqueAbbrs
		}, [])
	const abbreviationsString = abbreviations.length > 0 ? abbreviations.join('/') : null
	const readableZoneId = timeZoneId.replace('_', ' ')
	return !abbreviationsString
		? readableZoneId
		: abbreviationsString === 'EST/EDT' && !showEasternZoneId
		? // Don't show the zoneId if in Eastern, unless told to
		  abbreviationsString
		: // Show the combined abbreviations plus the friendly zoneId
		  `${abbreviationsString} (${readableZoneId})`
}

/**
 * Returns the basic time zone abbreviation for the given date, e.g. `EDT`.
 *
 * @param dateTimeUtc The date to format.
 */
export function getFormattedTimeZone(dateTimeUtc: string) {
	return applyFormat(dateTimeUtc, 'z')
}

/**
 * Formats a date using the given format. Parses the date as UTC, then converts to the current time zone, then formats.
 * @param {*} dateTimeUtc The date to format. Any valid input for `moment.utc(...)`
 * @param {*} format The format to use
 */
function applyFormat(dateTimeUtc: string, format: string, timeZoneId = guessTimeZoneId()) {
	return getZonedMomentFromUtc(dateTimeUtc, timeZoneId).format(format)
}

const isInputTypeSupported = (type: string) => {
	try {
		const test = document.createElement('input')
		test.type = type
		const isSupported = test.type === type
		return isSupported
	} catch (e) {
		return false
	}
}

export const isDateInputSupported = isInputTypeSupported('date')

export const isTimeInputSupported = isInputTypeSupported('time')

export function isDateTimeValid(dateTime: string | Date) {
	return !!dateTime && moment(dateTime).isValid()
}
