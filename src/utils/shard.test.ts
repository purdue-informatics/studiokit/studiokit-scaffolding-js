import { setAppConfig } from '../constants/configuration'
import { defaultAppConfiguration } from '../constants/mockData'
import { SHARD } from '../constants/shard'
import { getShardKey } from './shard'

describe('utils/shards', () => {
	describe('getShardKey', () => {
		beforeAll(() => {
			setAppConfig(defaultAppConfiguration)
		})
		it('returns localhostShardKey for localhost if localhostShardKey is set', () => {
			expect(getShardKey('localhost', 'custom')).toEqual('custom')
		})
		it('returns PURDUE for localhost', () => {
			expect(getShardKey('localhost')).toEqual(SHARD.PURDUE)
		})
		it('returns PURDUE for Azure Web Apps domain', () => {
			expect(getShardKey('studiokit-web.azurewebsites.net')).toEqual(SHARD.PURDUE)
		})
		it('returns PURDUE for Azure Static Storage domain', () => {
			expect(getShardKey('studiokitstorage.web.core.windows.net')).toEqual(SHARD.PURDUE)
		})
		it('returns PURDUE for Azure Microsoft CDN (classic) domain', () => {
			expect(getShardKey('studiokit-web.azureedge.net')).toEqual(SHARD.PURDUE)
		})
		it('returns PURDUE for Azure Front Door CDN domain', () => {
			expect(getShardKey('studiokit-web.z01.azurefd.net')).toEqual(SHARD.PURDUE)
		})
		it('returns PURDUE for ngrok.io', () => {
			expect(getShardKey('ngrok.io')).toEqual(SHARD.PURDUE)
		})
		it('returns ROOT for www subdomain', () => {
			expect(getShardKey('www.studiokit.org')).toEqual(SHARD.ROOT)
		})
		it('returns ROOT for dev subdomain', () => {
			expect(getShardKey('dev.studiokit.org')).toEqual(SHARD.ROOT)
		})
		it('returns ROOT for no subdomain', () => {
			expect(getShardKey('studiokit.org')).toEqual(SHARD.ROOT)
		})
		it('returns correct subdomain on PROD', () => {
			expect(getShardKey('foo.bar.studiokit.org')).toEqual('foo.bar')
		})
		it('returns correct subdomain on DEV', () => {
			expect(getShardKey('foo.bar.dev.studiokit.org')).toEqual('foo.bar')
		})
	})
})
