import { get, isArray, isUndefined, merge, union } from 'lodash'
import { v4 as uuidv4 } from 'uuid'
import { ModelFetchRequestAction, PeriodicModelFetchRequestAction } from '../redux/actions/ModelAction'
import { EndpointConfig, EndpointMapping, EndpointMappings, FetchConfig, FetchErrorData } from '../types'
import { constructErrorFromCaughtError } from './error'

/**
 * Type check method to confirm if a value is `FetchErrorData`.
 *
 * Checks if `value` is an object, and has `status`, `title`, and `detail` properties.
 */
export const isFetchErrorData = (value: any): value is FetchErrorData =>
	!!value &&
	typeof value === 'object' &&
	Object.prototype.hasOwnProperty.call(value, 'status') &&
	Object.prototype.hasOwnProperty.call(value, 'title') &&
	Object.prototype.hasOwnProperty.call(value, 'detail')

export interface PrepareFetchResult {
	/** The endpoint mapping that was matched using a data request action's 'modelName'. */
	endpointMapping: EndpointMapping
	/** The endpoint config from the endpoint mapping. defaults to an empty object. */
	endpointConfig: EndpointConfig
	/** The fetch config constructed from the data request action and endpoint mapping. Sent to `doFetch`. */
	fetchConfig: FetchConfig
	/** The final object path used to store the result in the redux store. Includes keys and ids. */
	modelPath: string
	isCollectionItemFetch: boolean
	isCollectionItemCreate: boolean
	isCollectionItemDelete: boolean
}

/**
 * Prepare fetchConfig to pass to fetchService. Also set up state to handle response correctly.
 *
 * @param modelFetchRequestAction The action dispatched by the client
 * @param endpointMappings The EndpointMappings object
 */
export function prepareFetch(
	modelFetchRequestAction: ModelFetchRequestAction | PeriodicModelFetchRequestAction,
	endpointMappings: EndpointMappings
) {
	try {
		if (!modelFetchRequestAction) {
			throw new Error("'modelFetchRequestAction' is required")
		}
		if (!modelFetchRequestAction.modelName) {
			throw new Error("'modelFetchRequestAction.modelName' is required")
		}
		if (!endpointMappings) {
			throw new Error("'endpointMappings' is required")
		}

		// Get fetch parameters from global fetch dictionary using the modelName passed in to locate them
		// Combine parameters from global dictionary with any passed in - locals override dictionary
		const endpointMapping: EndpointMapping = get(endpointMappings, modelFetchRequestAction.modelName)
		if (!endpointMapping) {
			throw new Error(`Cannot find '${modelFetchRequestAction.modelName}' in 'endpointMappings'`)
		}

		const endpointConfig: EndpointConfig = merge({}, endpointMapping._config)
		const fetchConfig: FetchConfig = merge({}, endpointConfig.fetch, {
			headers: merge({}, modelFetchRequestAction.headers),
			queryParams: merge({}, modelFetchRequestAction.queryParams)
		})

		// set "method" if defined
		if (modelFetchRequestAction.method) {
			fetchConfig.method = modelFetchRequestAction.method
		}

		// set or merge "body"
		// If the body is a string, we are assuming it's an 'application/x-www-form-urlencoded'
		if (
			modelFetchRequestAction.body &&
			(typeof modelFetchRequestAction.body === 'string' || modelFetchRequestAction.body instanceof FormData)
		) {
			fetchConfig.body = modelFetchRequestAction.body
			fetchConfig.contentType = 'application/x-www-form-urlencoded'
		} else if (fetchConfig.body || modelFetchRequestAction.body) {
			const isBodyArray =
				(fetchConfig.body && isArray(fetchConfig.body)) ||
				(modelFetchRequestAction.body && isArray(modelFetchRequestAction.body))
			fetchConfig.body = isBodyArray
				? union([], fetchConfig.body, modelFetchRequestAction.body)
				: merge({}, fetchConfig.body, modelFetchRequestAction.body)
		}

		// set "contentType" if defined, overriding the default 'application/x-www-form-urlencoded'
		// that may have been set previously
		if (modelFetchRequestAction.contentType) {
			fetchConfig.contentType = modelFetchRequestAction.contentType
		}

		let modelPath: string = modelFetchRequestAction.modelName
		let isCollectionItemFetch = false
		let isCollectionItemCreate = false
		let isCollectionItemDelete = false
		// copy pathParams into two arrays, to manage them separately
		let pathParams = merge([], modelFetchRequestAction.pathParams)
		const modelPathParams = merge([], modelFetchRequestAction.pathParams)

		// find all the model levels from 'modelPath'
		const modelPathLevels = modelPath.split('.')
		let lastModelLevel: EndpointMapping | EndpointConfig = endpointMappings
		const modelLevels = modelPathLevels.map(levelName => {
			const modelLevel = get(lastModelLevel, levelName)
			lastModelLevel = modelLevel
			return modelLevel
		})

		// find the levels that are collections
		const collectionModelLevels = modelLevels.filter(level => level._config && level._config.isCollection)
		const isAnyLevelCollection = collectionModelLevels.length > 0

		// if any level is a collection, we need to concat their fetch paths and modelNames
		if (isAnyLevelCollection) {
			if (modelPathLevels.length > 1) {
				modelLevels.forEach((modelLevel, index) => {
					const levelName: string = modelPathLevels[index]
					const currentModelConfig = merge({}, modelLevel._config)
					const currentFetchConfig = merge({}, currentModelConfig.fetch)
					const currentPath = !isUndefined(currentFetchConfig.path)
						? currentFetchConfig.path
						: index === 0
						? `/api/${levelName}`
						: levelName

					// first level, just use its values
					if (index === 0) {
						fetchConfig.path = currentPath
						modelPath = levelName
						return
					}

					// if previous level isCollection, we need to use "{:id}" hooks when appending new level
					// otherwise, just append using the divider
					const prevModelConfig = merge({}, modelLevels[index - 1]._config)
					const divider =
						!!fetchConfig.path && fetchConfig.path.length > 0 && currentPath.length > 0 ? '/' : ''
					if (prevModelConfig.isCollection) {
						fetchConfig.path = `${fetchConfig.path}${divider}{:id}/${currentPath}`
						modelPath = `${modelPath}.{:id}.${levelName}`
					} else {
						fetchConfig.path = `${fetchConfig.path}${divider}${currentPath}`
						modelPath = `${modelPath}.${levelName}`
					}

					// an absolute path resets the fetch path, and ignores previous pathParams moving forward
					// it does not affect modelPath params for redux
					if (currentPath.indexOf('/') === 0) {
						fetchConfig.path = currentPath
						const collectionLevelIndex = collectionModelLevels.indexOf(modelLevel)
						if (collectionLevelIndex > 0) {
							// since `collectionLevelIndex` is based on the full amount of params,
							// always target `modelPathParams`, not the "current" `pathParams`
							pathParams = modelPathParams.slice(collectionLevelIndex, modelPathParams.length)
						}
					}
				})
			}

			if (!fetchConfig.path) {
				fetchConfig.path = `/api/${modelPath}`
			}

			// determine if we need to append an "{:id}" hook
			const pathLevels = (fetchConfig.path.match(/{:id}/g) || []).length
			// GET, PUT, PATCH, DELETE => append '/{:id}'
			isCollectionItemFetch = !!endpointConfig.isCollection && pathParams.length > pathLevels
			// POST
			isCollectionItemCreate = !!endpointConfig.isCollection && fetchConfig.method === 'POST'
			isCollectionItemDelete = !!endpointConfig.isCollection && fetchConfig.method === 'DELETE'

			// insert pathParam hooks into path and modelPath
			// track collection item requests by id (update, delete) or guid (create)
			if (isCollectionItemFetch && !isCollectionItemCreate) {
				fetchConfig.path = `${fetchConfig.path}/{:id}`
				modelPath = `${modelPath}.{:id}`
			} else if (isCollectionItemCreate) {
				modelPath = `${modelPath}.${modelFetchRequestAction.guid || uuidv4()}`
			}
		}

		if (!fetchConfig.path) {
			throw new Error("'fetchConfig.path' is required for non-collections")
		}

		// substitute any params in `modelPath`, e.g. `groups.{:id}`
		if (/{:.+}/.test(modelPath)) {
			let index = 0
			modelPath = modelPath.replace(/{:(.+?)}/g, () => {
				const value = modelPathParams[index]
				if (value === undefined || value === null) {
					throw new Error(`Could not replace params in 'modelPath' using 'modelPathParams'`)
				}
				index++
				return value
			})
		}

		// substitute any params in `fetchConfig.path`, e.g. `/api/group/{:id}`
		if (!!fetchConfig.path && /{:.+}/.test(fetchConfig.path)) {
			let index = 0
			fetchConfig.path = fetchConfig.path.replace(/{:(.+?)}/g, () => {
				const value = pathParams[index]
				if (value === undefined || value === null) {
					throw new Error(`Could not replace params in 'fetchConfig.path' using 'pathParams'`)
				}
				index++
				return value
			})
		}

		const result: PrepareFetchResult = {
			endpointMapping,
			fetchConfig,
			endpointConfig,
			modelPath,
			isCollectionItemFetch,
			isCollectionItemCreate,
			isCollectionItemDelete
		}

		return result
	} catch (error: unknown) {
		const prepareFetchError = constructErrorFromCaughtError(error, 'PrepareFetchError')
		throw prepareFetchError
	}
}
