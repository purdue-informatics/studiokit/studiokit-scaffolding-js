export const getRootModelName = (props: { modelName: string }) => props.modelName.split('.').slice(0, -1).join('.')
