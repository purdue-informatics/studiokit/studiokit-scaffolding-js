import { DeltaOperation, DeltaStatic } from 'quill'
import { Quill } from 'react-quill'
import { v5 as uuidv5 } from 'uuid'
import { registerQuill } from '../components/Quill'
import { ImageValue } from '../components/Quill/Formats/Image'
import { internalServerErrorData } from '../constants/mockData'
import { HookFunction, noStoreHooks } from '../redux/sagas/noStoreSaga'
import { checkForNonPublicImages } from './quill'

const Delta = Quill.import('delta')

jest.mock('../redux/actionCreator')

registerQuill()

const imageUrl = 'http://foo.bar'
const lowercaseContentTypeImageUrl = 'http://foo.bar.lowercase-content-type'
const noContentTypeImageUrl = 'http://foo.bar.no-content-type'
/** e.g. Brightspace returns the login page html when loading a non-public image */
const loadsAsHtmlImageUrl = 'http://foo.bar.loads-as-html'
/** e.g. the API returns `null` when the URL does not return a successful response */
const nullResponseImageUrl = 'http://foo.bar.null-response'
/** e.g. the API returns `FetchErrorData` for some unknown server error */
const errorResponseImageUrl = 'http://foo.bar.error-response'

const createImageOp = (src: string, nonPublic = false): DeltaOperation => ({
	insert: { image: { src, height: 'auto', width: 'auto', nonPublic } }
})
const newLineOp: DeltaOperation = { insert: '\n' }

const registerNoStoreActionHookMock = jest
	.fn()
	.mockImplementation((hookId: string, hook: HookFunction<Record<string, string[]>>) => {
		if (uuidv5(imageUrl, uuidv5.URL) === hookId) {
			hook({ 'Content-Type': ['image/png'] })
		} else if (uuidv5(lowercaseContentTypeImageUrl, uuidv5.URL) === hookId) {
			hook({ 'content-type': ['image/png'] })
		} else if (uuidv5(loadsAsHtmlImageUrl, uuidv5.URL) === hookId) {
			hook({ 'Content-Type': ['text-html'] })
		}
		// 'Content-Type' header is not mandatory, so we can't say it's not public
		// This simulates a response with headers but no 'Content-Type' header
		else if (uuidv5(noContentTypeImageUrl, uuidv5.URL) === hookId) {
			hook({ 'Cache-Control': ['no-cache'] })
		} else if (uuidv5(nullResponseImageUrl, uuidv5.URL) === hookId) {
			hook(null)
		} else if (uuidv5(errorResponseImageUrl, uuidv5.URL) === hookId) {
			hook(internalServerErrorData)
		}
	})

noStoreHooks.registerNoStoreActionHook = registerNoStoreActionHookMock

describe('utils/quill', () => {
	describe('checkForNonPublicImages', () => {
		let quill: Quill
		let checkedImages: ImageValue[]

		beforeEach(() => {
			quill = new Quill(document.createElement('div'))
			checkedImages = []
			registerNoStoreActionHookMock.mockClear()
		})

		it('does not modify empty contents', () => {
			const contents: DeltaStatic = new Delta([newLineOp])
			quill.setContents(contents)
			checkForNonPublicImages(quill, checkedImages)
			expect(quill.getContents()).toEqual(contents)
			expect(noStoreHooks.registerNoStoreActionHook).not.toHaveBeenCalled()
		})
		it('does not modify contents with some content but no images', () => {
			const contents: DeltaStatic = new Delta([
				{ insert: 'Gandalf', attributes: { bold: true } },
				{ insert: ' the ' },
				{ insert: 'White', attributes: { color: '#ffffff' } },
				newLineOp
			])
			quill.setContents(contents)
			checkForNonPublicImages(quill, checkedImages)
			expect(quill.getContents()).toEqual(contents)
			expect(noStoreHooks.registerNoStoreActionHook).not.toHaveBeenCalled()
		})
		it('does not modify contents if image is public', () => {
			const contents: DeltaStatic = new Delta([createImageOp(imageUrl), newLineOp])
			quill.setContents(contents)
			const updateContentsSpy = jest.spyOn(quill, 'updateContents')
			checkForNonPublicImages(quill, checkedImages)
			expect(quill.getContents()).toEqual(contents)
			expect(noStoreHooks.registerNoStoreActionHook).toHaveBeenCalledTimes(1)
			expect(updateContentsSpy).not.toHaveBeenCalled()
		})
		it('does not modify contents if image is public with a lowercase content-type header', () => {
			const contents: DeltaStatic = new Delta([createImageOp(lowercaseContentTypeImageUrl), newLineOp])
			quill.setContents(contents)
			const updateContentsSpy = jest.spyOn(quill, 'updateContents')
			checkForNonPublicImages(quill, checkedImages)
			expect(quill.getContents()).toEqual(contents)
			expect(noStoreHooks.registerNoStoreActionHook).toHaveBeenCalledTimes(1)
			expect(updateContentsSpy).not.toHaveBeenCalled()
		})
		it('modifies image to be `nonPublic` if the api returns a `null` result, meaning it could not load the image url', () => {
			const contents: DeltaStatic = new Delta([createImageOp(nullResponseImageUrl), newLineOp])
			quill.setContents(contents)
			const updateContentsSpy = jest.spyOn(quill, 'updateContents')
			checkForNonPublicImages(quill, checkedImages)
			const expectedContents: DeltaStatic = new Delta([createImageOp(nullResponseImageUrl, true), newLineOp])
			expect(quill.getContents()).toEqual(expectedContents)
			expect(noStoreHooks.registerNoStoreActionHook).toHaveBeenCalledTimes(1)
			expect(updateContentsSpy).toHaveBeenCalledTimes(1)
		})
		it('does not call the API if image is `nonPublic` but has already been checked', () => {
			const checkedImageOp = createImageOp(nullResponseImageUrl, true)
			const contents: DeltaStatic = new Delta([checkedImageOp, newLineOp])
			quill.setContents(contents)
			const updateContentsSpy = jest.spyOn(quill, 'updateContents')
			checkForNonPublicImages(quill, [checkedImageOp.insert.image])
			expect(noStoreHooks.registerNoStoreActionHook).not.toHaveBeenCalled()
			expect(updateContentsSpy).not.toHaveBeenCalled()
		})
		it('modifies content for multiple images', () => {
			const contents: DeltaStatic = new Delta([
				createImageOp(imageUrl),
				createImageOp(lowercaseContentTypeImageUrl),
				// same image twice
				createImageOp(nullResponseImageUrl),
				createImageOp(nullResponseImageUrl),
				createImageOp(loadsAsHtmlImageUrl),
				createImageOp(noContentTypeImageUrl),
				createImageOp(errorResponseImageUrl),
				newLineOp
			])
			quill.setContents(contents)
			const updateContentsSpy = jest.spyOn(quill, 'updateContents')
			const expectedContents: DeltaStatic = new Delta([
				createImageOp(imageUrl),
				createImageOp(lowercaseContentTypeImageUrl),
				createImageOp(nullResponseImageUrl, true),
				createImageOp(nullResponseImageUrl, true),
				createImageOp(loadsAsHtmlImageUrl, true),
				createImageOp(noContentTypeImageUrl),
				createImageOp(errorResponseImageUrl),
				newLineOp
			])
			checkForNonPublicImages(quill, checkedImages)
			expect(quill.getContents()).toEqual(expectedContents)
			expect(noStoreHooks.registerNoStoreActionHook).toHaveBeenCalledTimes(7)
			expect(updateContentsSpy).toHaveBeenCalledTimes(3)
		})
	})
})
