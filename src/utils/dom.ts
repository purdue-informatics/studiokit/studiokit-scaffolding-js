import { getLogger } from './logger'

const KEYBOARD_ANIMATION_MS = 250

/**
 * Get the nearest vertically scrollable parent of an element
 * @param { HTMLElement } element - The element to start with
 * @returns The nearest vertically scrollable parent element
 */
export const getVerticallyScrollableParent = (element: HTMLElement) => {
	let style = getComputedStyle(element)
	const excludeStaticParent = style.position === 'absolute'
	const overflowRegex = /(auto|scroll|hidden)/

	for (let parent: HTMLElement | null = element; (parent = parent.parentElement); ) {
		style = getComputedStyle(parent)
		if (excludeStaticParent && style.position === 'static') continue
		if (overflowRegex.test(style.overflow + style.overflowY) && parent.scrollHeight > parent.clientHeight)
			return parent
	}
	return document.scrollingElement
}

/**
 * Get the first parent that has the given className in its classList
 * @param element The target element
 * @param className The className to match
 */
export const getFirstParentElementWithClassName = (element: HTMLElement, className: string) => {
	for (let parent: HTMLElement | null = element; (parent = parent.parentElement); ) {
		if (parent.classList.contains(className)) return parent
	}
	return undefined
}

/**
 * Return whether the device being used is one of several mainstream mobile devices
 */
export const isMobileDevice = () => {
	return (
		/android|ipad|ipod|iphone/i.test(navigator.userAgent) ||
		(/macintosh/i.test(navigator.userAgent) && navigator.maxTouchPoints)
	)
}

/** Return whether the device is using an external keyboard vs. the native on-screen OS keyboard. */
export const hasExternalKeyboard = (): boolean | undefined => (window as any).hasExternalKeyboard

/**
 * Sets `window.hasExternalKeyboard`, returned by `hasExternalKeyboard`.
 */
const setHasExternalKeyboard = (hasExternalKeyboard: boolean) => {
	getLogger().debug('hasExternalKeyboard', hasExternalKeyboard)
	const windowWithoutType = window as any
	windowWithoutType.hasExternalKeyboard = hasExternalKeyboard
}

/**
 * 100vh is kind of b⊘rked on mobile safari, we have to set it programmatically here
 * so we can calculate device-independent viewport heights
 * https://allthingssmitty.com/2020/05/11/css-fix-for-100vh-in-mobile-webkit/
 *
 * Sets the CSS variable `--visual-viewport-height` on the `documentElement`/<html>.
 *
 */
const setVisualViewportHeightVar = () => {
	const height = visualViewport ? visualViewport.height : 0
	document.documentElement.style.setProperty('--visual-viewport-height', `${height}px`)
}

/**
 * Is the given element capable of text input?
 *
 * @param el An HTML element
 */
export const isTextInput = (el: HTMLElement) => {
	const tagName = el && el.tagName && el.tagName.toLowerCase()
	if (tagName === 'textarea') return true
	if (tagName === 'input') {
		const inputEl = el as HTMLInputElement
		return inputEl.type !== 'button' && inputEl.type !== 'radio' && inputEl.type !== 'checkbox'
	}
	// Quill editor
	return tagName === 'div' && el.className.includes('ql-editor') && el.contentEditable === 'true'
}

let updateHasExternalKeyboardAfterFocusTimeout: number | undefined = undefined
let scrollToElementAfterFocusTimeout: number | undefined = undefined

const updateHasExternalKeyboardAfterFocus = () => {
	const closedVisualViewportHeight = visualViewport ? visualViewport.height : 0
	// wait for the keyboard animation
	updateHasExternalKeyboardAfterFocusTimeout = window.setTimeout(() => {
		// find the keyboard height based on the difference in viewport height
		const keyboardHeight = closedVisualViewportHeight - (visualViewport ? visualViewport.height : 0)
		// on first-load the input focus can fail
		// when this happens the height is 0, and this ONLY happens when there is NOT an external keyboard
		if (keyboardHeight === 0 && hasExternalKeyboard() === undefined) {
			setHasExternalKeyboard(false)
		} else {
			// if the keyboard height is greater than 100, that means the full onscreen keyboard displayed
			// otherwise, only the small context menu displayed, meaning there is an external keyboard
			setHasExternalKeyboard(keyboardHeight < 100)
		}

		updateHasExternalKeyboardAfterFocusTimeout = undefined
	}, KEYBOARD_ANIMATION_MS)
}

const scrollToElementAfterFocus = (el: HTMLElement) => {
	// wait for the keyboard animation
	scrollToElementAfterFocusTimeout = window.setTimeout(() => {
		// after the keyboard is open, check for the el’s scrollable parent, as it could have just become scrollable
		const scrollParent = getVerticallyScrollableParent(el)
		// if the el has a scrollable parent other than the document, scroll it back into view
		if (scrollParent && scrollParent !== document.documentElement) {
			const scrollRect = scrollParent.getBoundingClientRect()
			const rect = el.getBoundingClientRect()
			// only scroll into view if outside of the scroll parent bounds
			if (rect.bottom < scrollRect.top || rect.top > scrollRect.bottom) {
				const newScrollTop = scrollParent.scrollTop + rect.top - scrollRect.top - 10
				getLogger().debug('scrollToEl', newScrollTop)
				if (scrollParent.scrollTo) {
					scrollParent.scrollTo({ top: newScrollTop, behavior: 'smooth' })
				} else {
					scrollParent.scrollTop = newScrollTop
				}
			}
		}

		scrollToElementAfterFocusTimeout = undefined
	}, KEYBOARD_ANIMATION_MS)
}

/**
 * Event handler for "focusin" event.
 *
 * "focusin" bubbles up from all focus events, so uses `isTextInput` to only react to "keyboard" focus events.
 *
 * Checks for external keyboard and scrolls to the focused element after any animations, e.g. keyboard open.
 *
 */
const onFocusIn = (e: Event) => {
	if (!e.target || !isTextInput(e.target as HTMLElement)) return
	updateHasExternalKeyboardAfterFocus()
	scrollToElementAfterFocus(e.target as HTMLElement)
}

/**
 * Event handler for "focusout" event.
 *
 * "focusout" bubbles up from all focus events, so uses `isTextInput` to only react to "keyboard" focus events.
 *
 * Clears any pending timeouts from "focusin" events.
 *
 */
const onFocusOut = (e: Event) => {
	if (!e.target || !isTextInput(e.target as HTMLElement)) return
	if (updateHasExternalKeyboardAfterFocusTimeout) {
		window.clearTimeout(updateHasExternalKeyboardAfterFocusTimeout)
		updateHasExternalKeyboardAfterFocusTimeout = undefined
	}
	if (scrollToElementAfterFocusTimeout) {
		window.clearTimeout(scrollToElementAfterFocusTimeout)
		scrollToElementAfterFocusTimeout = undefined
	}
}

/**
 * Check if the device is using an external keyboard by appending an `input` and focusing it.
 *
 * This will trigger the `onFocusIn` event handler, which updates the `hasExternalKeyboard` global.
 */
const checkForExternalKeyboard = () => {
	const el = document.createElement('input')
	el.type = 'text'
	el.autofocus = true
	el.addEventListener('focusin', () => {
		// remove the input after the keyboard has displayed, plus a small buffer
		window.setTimeout(() => {
			el.blur()
			el.remove()
		}, KEYBOARD_ANIMATION_MS + 100)
	})
	document.documentElement.append(el)
}

export const setupMobileEventListeners = () => {
	if (!isMobileDevice() || !visualViewport) return

	// set viewport height var at setup
	setVisualViewportHeightVar()

	// add event listeners
	visualViewport.addEventListener('resize', () => {
		setVisualViewportHeightVar()

		// keep the document element scrolled to the top if a ReactModal is open
		if (document.body.classList.contains('ReactModal__Body--open')) {
			document.documentElement.scrollTop = 0
		}
	})
	window.addEventListener('focusin', onFocusIn)
	window.addEventListener('focusout', onFocusOut)

	// check for external keyboard at setup
	checkForExternalKeyboard()
}
