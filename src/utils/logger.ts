import { getAppConfig } from '../constants/configuration'

export interface Logger {
	debug: (message?: any, ...optionalParams: any[]) => void
	info: (message?: any, ...optionalParams: any[]) => void
	warn: (message?: any, ...optionalParams: any[]) => void
	error: (message?: any, ...optionalParams: any[]) => void
}

let logger: Logger

const falseLog = () => {
	return
}

export const dummyLogger: Logger = {
	debug: falseLog,
	info: falseLog,
	warn: falseLog,
	error: falseLog
}

export function createLogger(): Logger {
	if (getAppConfig().NODE_ENV === 'production' || !window.console || !console) {
		return dummyLogger
	}
	return {
		debug: console.debug,
		info: console.info,
		warn: console.warn,
		error: console.error
	}
}

export function setLogger(value: Logger) {
	logger = value
}

export function getLogger() {
	return logger
}
