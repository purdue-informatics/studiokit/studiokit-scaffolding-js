import { GROUP_MANAGE_TABLE_TAB, MANAGE_TABLE_TAB } from '../constants/table'
import { EntityUser, ExternalTerm, Group, ModelCollection } from '../types'
import { getLocalMomentFromUtc, isNowBeforeDate } from './date'
import { getEndDate, getStartDate } from './groupDates'
import { groupsAsAnythingButLearner } from './groupRoles'

export const getKeywordsList = (keywords: string | undefined) => {
	if (!keywords) return []
	return keywords
		.trim()
		.toLowerCase()
		.split(' ')
		.filter(k => !!k)
}

export const isEntityUserMatchedByKeyword = (owner: EntityUser, keyword: string) =>
	owner.id.toString().toLowerCase().includes(keyword) ||
	!!owner.firstName?.toLowerCase().includes(keyword) ||
	!!owner.lastName?.toString().toLowerCase().includes(keyword) ||
	!!owner.email?.toString().toLowerCase().includes(keyword) ||
	!!owner.uid?.toString().toLowerCase().includes(keyword)

export const isEntityMatchedByKeyword = (entity: { id: number; name: string; owners: EntityUser[] }, keyword: string) =>
	entity.id.toString().toLowerCase().includes(keyword) ||
	entity.name.toLowerCase().includes(keyword) ||
	entity.owners.some(owner => isEntityUserMatchedByKeyword(owner, keyword))

export const isEntityMatchedByKeywords = (
	entity: { id: number; name: string; owners: EntityUser[] },
	keywordsList: string[]
) => !keywordsList || keywordsList.length === 0 || keywordsList.every(k => isEntityMatchedByKeyword(entity, k))

export const searchEntities = <T extends { isDeleted: boolean; activities: string[] }>(
	entityArray: T[],
	entityMatchFunction: (entity: T, keywordsList: string[]) => boolean,
	readActivity: string,
	canReadAnyGlobally?: boolean,
	keywords?: string
) => {
	const keywordsList = getKeywordsList(keywords)
	const filteredEntities = entityArray.reduce<Record<number, T[]>>(
		(acc, e) => {
			// exclude if cannot read any entity and does not have the read activity
			if (!canReadAnyGlobally && !e.activities.includes(readActivity)) return acc
			// exclude if does not match, if any
			if (!entityMatchFunction(e, keywordsList)) return acc
			// add entity to the corresponding tab filtered array
			const tab = e.isDeleted ? MANAGE_TABLE_TAB.DELETED : MANAGE_TABLE_TAB.AVAILABLE
			const tabArray: T[] = acc[tab] ?? []
			tabArray.push(e)
			acc[tab] = tabArray
			return acc
		},
		{ [MANAGE_TABLE_TAB.AVAILABLE]: [], [MANAGE_TABLE_TAB.DELETED]: [] }
	)
	return filteredEntities
}

export interface SearchModelArrayParams<TModel> {
	modelArray: TModel[]
	canReadyAnyGlobally?: boolean
	keywords?: string
}

//#region Groups

export interface SearchGroupsParams extends SearchModelArrayParams<Group> {
	groups: ModelCollection<Group>
	externalTerms: ModelCollection<ExternalTerm>
	dateString?: string
}

const getGroupTab = (group: Group, endDate: string) => {
	if (group.isDeleted) {
		return GROUP_MANAGE_TABLE_TAB.DELETED
	} else if (!!group.id && isNowBeforeDate(endDate)) {
		return GROUP_MANAGE_TABLE_TAB.CURRENT
	}
	return GROUP_MANAGE_TABLE_TAB.PAST
}

export const searchGroups = ({
	groups,
	modelArray: groupsArray,
	externalTerms,
	canReadyAnyGlobally,
	keywords,
	dateString
}: SearchGroupsParams) => {
	const keywordsList = getKeywordsList(keywords)
	const visibleGroups = canReadyAnyGlobally ? groupsArray : groupsAsAnythingButLearner(groups)
	const filteredGroups = visibleGroups.reduce<Record<number, Group[]>>(
		(acc, g) => {
			// exclude if does not match keywords, if any
			if (!isEntityMatchedByKeywords(g, keywordsList)) return acc
			const startDate = getStartDate(externalTerms, g)
			const endDate = getEndDate(externalTerms, g)
			// exclude if search dateString is not between group start and end dates
			if (
				!!dateString &&
				(getLocalMomentFromUtc(dateString) < getLocalMomentFromUtc(startDate) ||
					getLocalMomentFromUtc(dateString) > getLocalMomentFromUtc(endDate))
			) {
				return acc
			}
			const tab = getGroupTab(g, endDate)
			const tabArray: Group[] = acc[tab] ?? []
			tabArray.push(g)
			acc[tab] = tabArray
			return acc
		},
		{
			[GROUP_MANAGE_TABLE_TAB.CURRENT]: [],
			[GROUP_MANAGE_TABLE_TAB.PAST]: [],
			[GROUP_MANAGE_TABLE_TAB.DELETED]: []
		}
	)
	return filteredGroups
}

//#endregion Groups
