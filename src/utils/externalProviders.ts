import { EXTERNAL_PROVIDER_TYPE } from '../constants/externalProviderType'
import { ExternalProvider } from '../types/ExternalProvider'

export const shouldDisplayCreateNonLtiGroupAlert = (externalProviders: ExternalProvider[]) => {
	return externalProviders.some(
		ep => ep.typename === EXTERNAL_PROVIDER_TYPE.LTI && ep.shouldDisplayCreateNonLtiGroupAlert
	)
}
