import ga from 'react-ga4'
import { getAppConfig, getAppInsights, getUserId } from '../constants'
import { CustomEvent } from '../types/Event'

/**
 * Send an event to Google Analytics and App Insights
 * @param event
 */
export const trackCustomEvent = (event: CustomEvent) => {
	const { eventName, category, action, label } = event
	const userId = getUserId()
	const appConfig = getAppConfig()
	if (appConfig.GOOGLE_ANALYTICS_TRACKING_ID) {
		ga.event(eventName, {
			event_category: category,
			action: action,
			event_label: label,
			user_id: userId
		})
	}
	if (appConfig.APP_INSIGHTS_KEY) {
		const appInsights = getAppInsights()
		appInsights.trackEvent({
			name: eventName,
			properties: {
				category: category,
				action: action,
				label: label,
				userId: userId
			}
		})
	}
}
