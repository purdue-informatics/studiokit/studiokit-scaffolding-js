import { isArray, isPlainObject } from 'lodash'
import { MODEL_STATUS } from '../constants/modelStatus'
import { Model, ModelCollection } from '../types'

export const getModelArray = <T extends Model>(model: ModelCollection<T>, guid?: string): T[] =>
	Object.keys(model)
		.filter(key => key !== '_metadata' && key !== guid && !!model[key])
		.map(key => model[key]) as T[]

export const getModelMinusRelations = <T extends Model>(model: T) =>
	(Object.keys(model) as Array<keyof T>)
		.filter(key => key !== 'guid' && !isPlainObject(model[key]) && !isArray(model[key]))
		.reduce((retval: Partial<T>, key) => {
			retval[key] = model[key]
			return retval
		}, {})

export const getModelFetchResult = <T extends Model>(model?: T, nextModel?: T) => {
	// from (no model, empty model, fetching model) to (fetched model), e.g. GET
	const didFetch =
		(!model || !model._metadata || model._metadata.isFetching) &&
		!!nextModel &&
		!!nextModel._metadata &&
		!nextModel._metadata.isFetching
	// from (fetching model) to (no model), e.g. DELETED
	const didDelete = !!model && !!model._metadata && model._metadata.isFetching && !nextModel
	const isFinished = didFetch || didDelete
	const isSuccess =
		isFinished && (didDelete || (!!nextModel && !!nextModel._metadata && !nextModel._metadata.hasError))
	const error =
		(isFinished && !didDelete && !!nextModel && !!nextModel._metadata && nextModel._metadata.lastFetchErrorData) ||
		undefined

	return {
		isFinished,
		isSuccess,
		error
	}
}

/**
 * When the model has finished fetching, call `changeModelStatus`.
 * If `fetchingId` is provided, `model` and `prevModel` are assumed to be `ModelCollection`,
 * and the model found at `fetchingId` is used.
 */
export function handleModelFetchFinish<TModel extends Model>(
	model: TModel | ModelCollection<TModel> | undefined,
	prevModel: TModel | ModelCollection<TModel> | undefined,
	fetchingId: string | number | undefined,
	changeModelStatus: (modelStatus: MODEL_STATUS, fetchingId?: string) => void
) {
	const fetchingModel = fetchingId ? ((model as ModelCollection<TModel>)[fetchingId] as TModel | undefined) : model
	const prevFetchingModel = fetchingId
		? ((prevModel as ModelCollection<TModel>)[fetchingId] as TModel | undefined)
		: prevModel
	const modelFetchResult = getModelFetchResult(prevFetchingModel, fetchingModel)
	if (modelFetchResult.isFinished) {
		changeModelStatus(!modelFetchResult.isSuccess ? MODEL_STATUS.ERROR : MODEL_STATUS.READY, undefined)
	}
}
