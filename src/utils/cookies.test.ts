import { getCookies } from './cookies'

describe('utils/cookies', () => {
	describe('getCookies', () => {
		it('returns with no cookies', () => {
			expect(getCookies()).toEqual({})
		})
		it('returns with cookies', () => {
			Object.defineProperty(document, 'cookie', {
				writable: true,
				value: 'someBoolean=1; someValue=blue'
			})
			expect(getCookies()).toEqual({
				someBoolean: '1',
				someValue: 'blue'
			})
		})
	})
})
