import { ExternalTerm, Group, ModelCollection } from '../types'
import { getFormattedNumberedDate, getLocalMomentFromUtc } from './date'

export const getTerm = (externalTerms: ModelCollection<ExternalTerm>, group: Group) => {
	if (!!group.externalTermId && Object.keys(externalTerms).length > 0) {
		const externalTerm = externalTerms[group.externalTermId]
		return externalTerm
	}
	return null
}

export const getTermOrDatesUIText = (externalTerms: ModelCollection<ExternalTerm>, group: Group) => {
	// linked term
	const externalTerm = getTerm(externalTerms, group)
	if (externalTerm) {
		return externalTerm.name
	}
	// group dates
	if (!!group.startDate && !!group.endDate) {
		return `${getFormattedNumberedDate(group.startDate)} - ${getFormattedNumberedDate(group.endDate)}`
	}
	// no term or dates
	return 'N/A'
}

export const getEndDate = (externalTerms: ModelCollection<ExternalTerm>, group: Group) => {
	const externalTerm = getTerm(externalTerms, group)
	if (externalTerm) {
		return externalTerm.endDate
	}
	return group.endDate as string
}

export const getStartDate = (externalTerms: ModelCollection<ExternalTerm>, group: Group) => {
	const externalTerm = getTerm(externalTerms, group)
	if (externalTerm) {
		return externalTerm.startDate
	}
	return group.startDate as string
}

export const filterGroupsByEndDate = (
	groups: Group[],
	externalTerms: ModelCollection<ExternalTerm>,
	filterFunction: (dateTimeUtc?: string, nowUtc?: string) => boolean
) => {
	return groups.filter(g => !!g.id && !g.isDeleted && filterFunction(getEndDate(externalTerms, g)))
}

export const isGroupEnded = (externalTerms: ModelCollection<ExternalTerm>, group: Group) => {
	const groupEndDateMoment = getLocalMomentFromUtc(getEndDate(externalTerms, group))
	const localMoment = getLocalMomentFromUtc()
	return groupEndDateMoment <= localMoment
}
