export const constructErrorFromCaughtError = (caughtError: unknown, name: string, message?: string) => {
	const error = new Error(message)
	error.name = name
	if (caughtError instanceof Error) {
		error.message += `${message ? '.\n' : ''}${caughtError.name !== 'Error' ? `${caughtError.name}: ` : ''}${
			caughtError.message
		}`
		error.stack = caughtError.stack
		// TODO: use `new Error(message, { cause: caughtError })` once we upgrade TS to allow es2022
	} else if (typeof caughtError === 'string') {
		error.message += `${message ? '.\n' : ''}${caughtError}`
	} else if (caughtError && typeof caughtError === 'object') {
		error.message += `${message ? '.\n' : ''}${JSON.stringify(caughtError)}`
	}
	return error
}
