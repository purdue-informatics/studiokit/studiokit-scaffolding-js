import { OPERATING_SYSTEM } from '../constants/operatingSystem'

export const getOperatingSystem = () => {
	const userAgent = window.navigator.userAgent.toLowerCase()
	const macOSPlatforms = /(macintosh|macintel|macppc|mac68k|macos)/i
	const windowsPlatforms = /(win32|win64|windows|wince)/i

	return macOSPlatforms.test(userAgent)
		? OPERATING_SYSTEM.MAC
		: windowsPlatforms.test(userAgent)
		? OPERATING_SYSTEM.WINDOWS
		: OPERATING_SYSTEM.UNKNOWN
}
