import { LocationDescriptor } from 'history'
import { find } from 'lodash'
import { SHARD } from '../constants/shard'
import { TIER } from '../constants/tier'
import { windowService } from '../services/windowService'
import { AppConfiguration } from '../types'
import { getLogger } from './logger'
import { getShardKey } from './shard'

export function getApiBaseUrl(tier: TIER, apiUrlString: string) {
	// skip shard logic on LOCAL or if not using a custom domain (e.g. staging)
	if (tier === TIER.LOCAL || apiUrlString.includes('azurewebsites.net')) {
		return apiUrlString
	}
	let shardKey = getShardKey()

	// TODO: default ROOT to point to Purdue, until we have a landing marketing page
	if (shardKey === SHARD.ROOT) {
		shardKey = SHARD.PURDUE
	}

	const protocol = apiUrlString.substring(0, apiUrlString.indexOf('//'))
	const host = apiUrlString.substring(apiUrlString.indexOf('://') + 3)
	let tierSubdomainPart = ''
	switch (tier) {
		case TIER.DEV:
			tierSubdomainPart = '.dev'
			break
		case TIER.QA:
			tierSubdomainPart = '.qa'
			break
	}
	return `${protocol}//${shardKey}${tierSubdomainPart}.${host}`
}

export function getStorageUrl(config: AppConfiguration, url: string) {
	return config.TIER === TIER.LOCAL && !!config.STORAGE_URL
		? url.replace('http://127.0.0.1:10000', config.STORAGE_URL)
		: url
}

export function hasHttpProtocol(url: string) {
	return /^(http|https):\/\//.test(url)
}

export function isUrlValid(url: string) {
	return /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w.-]+)+[\w\-._~:/?#[\]@!$&'()*+,;=]+$/.test(url)
}

/** The total number of tries for `fetchText`, including the initial request. */
const TRY_LIMIT = 5

function wait(time: number) {
	return new Promise(resolve => {
		setTimeout(resolve, time)
	})
}

export async function fetchText(url: string, options?: RequestInit) {
	let tryCount = 0
	do {
		tryCount++
		try {
			const resp = await fetch(url, options)
			const text = await resp.text()
			return text
		} catch (error) {
			getLogger().error(`Failed to fetch ${url}. attempt: ${tryCount}, error: ${error}`)
			if (tryCount < TRY_LIMIT) {
				await wait(2 ** (tryCount - 1) * 100) // 100, 200, 400, 800
			}
		}
	} while (tryCount < TRY_LIMIT)

	throw new Error(`Failed to fetch ${url} after ${TRY_LIMIT} attempts.`)
}

export enum X_FRAME_OPTIONS {
	DENY = 'DENY',
	SAMEORIGIN = 'SAMEORIGIN',
	ALLOW_FROM = 'ALLOW-FROM'
}

export interface ParsedHeadersResponse {
	isValid: boolean
	iFrameAllowed?: boolean
}

/**
 * Using the given headers, determine if a url is allowed to be displayed in an iFrame.
 * @param headers An array of request headers.
 * @returns The parsed result
 */
export const parseHeadersResponse = (headers?: Record<string, any>) => {
	// default result
	const result: ParsedHeadersResponse = {
		isValid: false,
		iFrameAllowed: undefined
	}

	if (!headers) {
		return result
	}

	result.isValid = true
	result.iFrameAllowed = true

	const location = windowService.getLocation()

	// content-security-policy / x-content-security-policy
	// can contain 'frame-ancestors <source> <source>' with one or many sources
	// iFrameAllowed if it does not contain 'frame-ancestors' or if it does with an acceptable source
	const xContentSecurityPolicy = find(
		headers,
		(value, key) =>
			key.toLowerCase() === 'content-security-policy' || key.toLowerCase() === 'x-content-security-policy'
	)
	if (xContentSecurityPolicy) {
		// define all acceptable frame-ancestors sources
		// protocol, full location host, host, and all wildcard permutations
		const acceptableSources = [location.protocol, `${location.protocol}//${location.host}`, location.host]
		const hostParts = location.host.split('.')
		while (hostParts.length > 2) {
			hostParts.shift()
			const tempHost = `*.${hostParts.join('.')}`
			acceptableSources.push(tempHost)
			acceptableSources.push(`${location.protocol}//${tempHost}`)
		}
		result.iFrameAllowed =
			!xContentSecurityPolicy.some((x: any) => x.indexOf('frame-ancestors ') >= 0) ||
			xContentSecurityPolicy.some((x: any) => {
				/**
				 * Matches
				 * * the string `frame-ancestors `
				 * * one or many of the following, followed by a space, semicolon, or end of string
				 *     * `http:`
				 *     * `https:`
				 *     * `'self'`
				 *     * `'none'`
				 *     * url with optional scheme and optional asterisk wildcards
				 */
				const frameAncestorsRegex = /frame-ancestors ((?:(?:(?:http:)|(?:https:)|(?:'self')|(?:'none')|(?:(?:https?:\/\/)?[\w*.-]+(?:\.[\w*.-]+)+[\w-._~:/?#[\]@!$&'()*+,=]+))(?:[ ;]|$))+)/g
				const results = frameAncestorsRegex.exec(x)
				if (!results) {
					return false
				}
				const sources = results[1].split(' ')
				return sources.some(source => acceptableSources.some(acceptableSource => acceptableSource === source))
			})
	}

	// x-frame-options can be ['SAMEORIGIN', 'DENY', 'ALLOW-FROM...']
	// iFrameAllowed if the the x-frame-options is 'ALLOW-FROM {app web url}'
	const xFrameOptions = find(headers, (value, key) => key.toLowerCase() === 'x-frame-options')
	if (xFrameOptions) {
		result.iFrameAllowed =
			!xFrameOptions.some((x: any) => x === X_FRAME_OPTIONS.DENY || x === X_FRAME_OPTIONS.SAMEORIGIN) &&
			xFrameOptions.some((x: any) => x === `${X_FRAME_OPTIONS.ALLOW_FROM} ${location.protocol}//${location.host}`)
	}
	return result
}

/**
 * Parse a path string into a location with separate pathname, search, and hash components.
 * @param path A path string
 * @returns The parsed result
 */
export const parseLocation = (path: string): LocationDescriptor => {
	const searchStart = path.indexOf('?')
	const hashStart = path.indexOf('#')
	const searchEnd = hashStart > -1 ? hashStart : undefined
	const pathname =
		searchStart > -1 ? path.substring(0, searchStart) : hashStart > -1 ? path.substring(0, hashStart) : path
	const search = searchStart > -1 ? path.substring(searchStart, searchEnd) : undefined
	const hash = hashStart > -1 ? path.substring(hashStart) : undefined
	return {
		pathname: pathname || '/',
		search: search,
		hash: hash
	}
}
