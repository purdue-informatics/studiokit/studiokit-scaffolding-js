import { UserRole, UserWithRoles } from '../types/UserRole'
import { textForBaseRole } from './baseRole'

/**
 * Get the user id for a `userRole`, either a `UserRole` or `EntityUserRole`.
 * @param userRole The user role
 */
export const getUserId = (userRole: UserRole): string =>
	Object.prototype.hasOwnProperty.call(userRole, 'userId') ? (userRole as any).userId : userRole.id

/**
 * Get whether or not the `userRole` has a prop `isExternal` and it is `true`.
 * @param userRole The user role
 */
export const isExternal = (userRole: UserRole): boolean =>
	Object.prototype.hasOwnProperty.call(userRole, 'isExternal') && (userRole as any).isExternal === true

/**
 * Get the text for the given `userRole.role`. Defaults to using `textForBaseRole`.
 * @param textForRole Override method for providing role text
 */
export const getRoleText = (textForRole?: (role: string) => string) => (a: UserRole) =>
	textForRole ? textForRole(a.role) : textForBaseRole(a.role)

/**
 * Sort the userRoles by role, using the provided `roles` order
 * @param roles Roles ordered in descing access level
 */
export const sortByRole = (roles: string[]) => (a: UserRole, b: UserRole) => {
	const aIndex = roles.indexOf(a.role)
	const bIndex = roles.indexOf(b.role)
	if (aIndex !== bIndex) {
		return aIndex > bIndex ? 1 : -1
	}
	return 0
}

/**
 * Sort the users by role, using the provided `roles` order
 * @param roles Roles ordered in descing access level
 */
export const sortByHighestAccessRole = (roles: string[]) => (a: UserWithRoles, b: UserWithRoles) => {
	const aRole = a.roles.sort(sortByRole(roles))[0].role
	const bRole = b.roles.sort(sortByRole(roles))[0].role
	const aIndex = roles.indexOf(aRole)
	const bIndex = roles.indexOf(bRole)
	if (aIndex !== bIndex) {
		return aIndex > bIndex ? 1 : -1
	}
	return 0
}

export const convertToUserWithRoles = <TUserRole extends UserRole = UserRole>(
	userRole: TUserRole
): UserWithRoles<TUserRole> => {
	const userId = getUserId(userRole)
	return {
		id: userId,
		firstName: userRole.firstName,
		lastName: userRole.lastName,
		email: userRole.email,
		uid: userRole.uid,
		roles: [userRole]
	}
}
