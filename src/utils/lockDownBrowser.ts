import { LOCKDOWN_BROWSER_KEY, LOCKDOWN_BROWSER_TRUE } from '../constants/lockDownBrowser'
import { getCookies } from './cookies'

export interface LockDownBrowserInfo {
	isClientLockDownBrowser: boolean
	buildId?: string
	buildDateWindows?: string
	buildDateMac?: string
	buildDateIpad?: string
}

export const getLockDownBrowserInfo = (): LockDownBrowserInfo => {
	const cookies = getCookies()
	return {
		isClientLockDownBrowser: cookies[LOCKDOWN_BROWSER_KEY.CLIENT_IS_LOCK_DOWN_BROWSER] === LOCKDOWN_BROWSER_TRUE,
		buildId: cookies[LOCKDOWN_BROWSER_KEY.BUILD_ID],
		buildDateWindows: cookies[LOCKDOWN_BROWSER_KEY.BUILD_DATE_WINDOWS],
		buildDateMac: cookies[LOCKDOWN_BROWSER_KEY.BUILD_DATE_MAC],
		buildDateIpad: cookies[LOCKDOWN_BROWSER_KEY.BUILD_DATE_IPAD]
	}
}
