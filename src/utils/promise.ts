export interface CancellablePromise<T> {
	promise: Promise<T>
	cancel(): void
}

export const cancelablePromise = <T>(promise: Promise<T>) => {
	let isCancelled = false

	const wrappedPromise = new Promise<T>((resolve, reject) => {
		promise.then(
			val => (isCancelled ? reject({ isCancelled: true }) : resolve(val)),
			error => (isCancelled ? reject({ isCancelled: true }) : reject(error))
		)
	})

	const response: CancellablePromise<T> = {
		promise: wrappedPromise,
		cancel() {
			isCancelled = true
		}
	}
	return response
}
