import { GROUP_MANAGE_TABLE_TAB, MANAGE_TABLE_TAB, TABLE_PAGE_SIZE_OPTIONS } from '../constants/table'

export const getManageTableTabName = (selectedTab?: number) => {
	switch (selectedTab) {
		case MANAGE_TABLE_TAB.AVAILABLE:
			return 'Available'
		case MANAGE_TABLE_TAB.DELETED:
			return 'Deleted'
		default:
			return ''
	}
}

export const getGroupManageTableTabName = (selectedTab?: number) => {
	switch (selectedTab) {
		case GROUP_MANAGE_TABLE_TAB.CURRENT:
			return 'Current'
		case GROUP_MANAGE_TABLE_TAB.DELETED:
			return 'Deleted'
		case GROUP_MANAGE_TABLE_TAB.PAST:
			return 'Past'
		default:
			return ''
	}
}

/**
 * return page size options that are less than or equal to the total data length,
 * and one that is greater (if it exists) to allow viewing all data on one page.
 * */
export const getPageSizeOptions = (dataLength?: number) =>
	TABLE_PAGE_SIZE_OPTIONS.reduce<number[]>((options, option, currentIndex) => {
		const prevOption = currentIndex > 0 ? options[currentIndex - 1] : undefined
		if (!dataLength || option < dataLength || (prevOption && prevOption < dataLength)) {
			options.push(option)
		}
		return options
	}, [])
