import { BASE_ROLE } from '../constants/baseRole'

export const textForBaseRole = (role: string): string => {
	switch (role) {
		case BASE_ROLE.SUPER_ADMIN:
			return 'Super Admin'
		case BASE_ROLE.ADMIN:
			return 'Admin'
		case BASE_ROLE.CREATOR:
			return 'Creator'
		case BASE_ROLE.GROUP_LEARNER:
			return 'Student'
		case BASE_ROLE.GROUP_OWNER:
			return 'Instructor'
		case BASE_ROLE.GROUP_GRADER:
			return 'Grader'
		default:
			throw new Error(`role '${role}' was not found in switch statement`)
	}
}

export const singularArticleForBaseRole = (role: string): string => {
	switch (role) {
		case BASE_ROLE.ADMIN:
		case BASE_ROLE.GROUP_OWNER:
			return 'an'
		case BASE_ROLE.SUPER_ADMIN:
		case BASE_ROLE.GROUP_LEARNER:
		case BASE_ROLE.GROUP_GRADER:
		case BASE_ROLE.CREATOR:
			return 'a'
		default:
			throw new Error(`role '${role}' was not found in switch statement`)
	}
}
