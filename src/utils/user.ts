import { NOTIFICATION_TYPE } from '../constants'
import { addNotification, dispatchModelFetchRequest } from '../redux/actionCreator'
import { registerNoStoreActionHook, unregisterNoStoreActionHook } from '../redux/sagas/noStoreSaga'
import { FetchErrorData, User, UserWithRoles } from '../types'
import { isFetchErrorData } from './fetch'

const makePossessive = (word: string) => (word.endsWith('s') ? `${word}’` : `${word}’s`)

export const displayName = (u: User, shouldReverse = false) =>
	!!u.firstName && !!u.lastName
		? shouldReverse
			? `${u.lastName}, ${u.firstName}`
			: `${u.firstName} ${u.lastName}`
		: u.email

export const displayNamePossessive = (u: User) =>
	!!u.firstName && !!u.lastName ? `${u.firstName} ${makePossessive(u.lastName)}` : `${u.email}’s`

export const displayFirstName = (u: User) => (u.firstName ? u.firstName : u.email)

export const displayFirstNamePossessive = (u: User) => (u.firstName ? makePossessive(u.firstName) : `${u.email}’s`)

export const filterUsersByRole = (role: string) => (userRoles: UserWithRoles[]) => {
	return userRoles.filter(userRole => userRole.roles.some(r => r.role === role))
}

export const startImpersonation = (guid: string, userId: string) => {
	registerNoStoreActionHook(guid, (data: { code: string } | FetchErrorData | null) => {
		unregisterNoStoreActionHook(guid)
		if (!data || isFetchErrorData(data)) {
			addNotification({
				text: data?.detail || 'Could not start impersonation',
				type: NOTIFICATION_TYPE.ERROR
			})
		} else {
			const response = data as { code: string }
			window.location.href = `/?code=${encodeURIComponent(response.code)}`
		}
	})

	dispatchModelFetchRequest({
		modelName: 'startImpersonation',
		noStore: true,
		guid,
		queryParams: { userId }
	})
}

export const stopImpersonation = (guid: string) => {
	registerNoStoreActionHook(guid, (data: { code: string } | FetchErrorData | null) => {
		unregisterNoStoreActionHook(guid)
		if (!data || isFetchErrorData(data)) {
			addNotification({
				text: data?.detail || 'Could not stop impersonation',
				type: NOTIFICATION_TYPE.ERROR
			})
		} else {
			const response = data as { code: string }
			window.location.href = `/?code=${encodeURIComponent(response.code)}`
		}
	})

	dispatchModelFetchRequest({
		modelName: 'stopImpersonation',
		noStore: true,
		guid
	})
}
