import { TABLE_PAGE_SIZE_OPTIONS } from '../constants'
import { getPageSizeOptions } from './table'

describe('utils/table', () => {
	describe('getPageSizeOptions', () => {
		it('returns all options with no data', () => {
			expect(getPageSizeOptions()).toEqual(TABLE_PAGE_SIZE_OPTIONS)
			expect(getPageSizeOptions(0)).toEqual(TABLE_PAGE_SIZE_OPTIONS)
		})
		it('returns no options if data length is less than or equal to the min option', () => {
			expect(getPageSizeOptions(TABLE_PAGE_SIZE_OPTIONS[0])).toEqual([])
			expect(getPageSizeOptions(TABLE_PAGE_SIZE_OPTIONS[0] - 1)).toEqual([])
		})
		it('returns options that are less than the data length, plus one that is greater', () => {
			expect(getPageSizeOptions(TABLE_PAGE_SIZE_OPTIONS[1] + 1)).toEqual(TABLE_PAGE_SIZE_OPTIONS.slice(0, 3))
		})
		it('does not return a greater option if data length equals a specific page size', () => {
			expect(getPageSizeOptions(TABLE_PAGE_SIZE_OPTIONS[1])).toEqual(TABLE_PAGE_SIZE_OPTIONS.slice(0, 2))
		})
		it('returns all options if data length is greater than or equal to max option', () => {
			expect(getPageSizeOptions(TABLE_PAGE_SIZE_OPTIONS[-1])).toEqual(TABLE_PAGE_SIZE_OPTIONS)
			expect(getPageSizeOptions(TABLE_PAGE_SIZE_OPTIONS[-1] + 1)).toEqual(TABLE_PAGE_SIZE_OPTIONS)
		})
	})
})
