import { BASE_ROLE } from '../constants/baseRole'
import { GroupUserRole, UserWithRoles } from '../types'
import {
	displayFirstName,
	displayFirstNamePossessive,
	displayName,
	displayNamePossessive,
	filterUsersByRole
} from './user'

const userWithFullName: UserWithRoles<GroupUserRole> = {
	id: '1',
	firstName: 'Test',
	lastName: 'User',
	email: 'test@test.test',
	uid: 'test',
	roles: [{ userId: '1', role: BASE_ROLE.GROUP_OWNER } as GroupUserRole]
}

const user2WithFullName: UserWithRoles<GroupUserRole> = {
	id: '2',
	firstName: 'Chris',
	lastName: 'Adams',
	email: 'chris@test.test',
	uid: 'chris',
	roles: [
		{ userId: '2', role: BASE_ROLE.GROUP_OWNER } as GroupUserRole,
		{ userId: '2', role: BASE_ROLE.GROUP_LEARNER } as GroupUserRole
	]
}

const userWithNoName: UserWithRoles<GroupUserRole> = {
	id: '3',
	firstName: null,
	lastName: null,
	email: 'test@test.test',
	uid: 'test',
	roles: [{ userId: '3', role: BASE_ROLE.GROUP_LEARNER } as GroupUserRole]
}

const user2WithNoName: UserWithRoles<GroupUserRole> = {
	id: '4',
	firstName: null,
	lastName: null,
	email: 'test@test.tests',
	uid: 'test',
	roles: [{ userId: '4', role: BASE_ROLE.GROUP_LEARNER } as GroupUserRole]
}

const users = [userWithFullName, user2WithFullName, userWithNoName, user2WithNoName]

describe('utils/user', () => {
	describe('displayName', () => {
		it("should display the user's full name when not null", () => {
			const result = displayName(userWithFullName)
			expect(result).toBe('Test User')
		})

		it("should display the user's email address when the name is null", () => {
			const result = displayName(userWithNoName)
			expect(result).toBe('test@test.test')
		})

		describe('displayName reverse', () => {
			it("should display the user's full name in reverse when not null", () => {
				const result = displayName(userWithFullName, true)
				expect(result).toBe('User, Test')
			})

			it("should display the user's email address when the name is null", () => {
				const result = displayName(userWithNoName, true)
				expect(result).toBe('test@test.test')
			})
		})
	})

	describe('displayNamePossessive', () => {
		it("should display the user's full name in possessive form when not null", () => {
			const result = displayNamePossessive(userWithFullName)
			expect(result).toBe('Test User’s')

			const result2 = displayNamePossessive(user2WithFullName)
			expect(result2).toBe('Chris Adams’')
		})

		it("should display the user's email address in possessive form when the name is null", () => {
			const result = displayNamePossessive(userWithNoName)
			expect(result).toBe('test@test.test’s')

			const result2 = displayNamePossessive(user2WithNoName)
			expect(result2).toBe('test@test.tests’s')
		})
	})

	describe('displayFirstName', () => {
		it("should display the user's first name if available", () => {
			const result = displayFirstName(userWithFullName)
			expect(result).toBe('Test')
		})

		it("should display the user's email address if name not available", () => {
			const result = displayFirstName(userWithNoName)
			expect(result).toBe('test@test.test')
		})
	})

	describe('displayFirstNamePossessive', () => {
		it("should display the user's first name in possessive form when name is not null", () => {
			const result = displayFirstNamePossessive(userWithFullName)
			expect(result).toBe('Test’s')

			const result2 = displayFirstNamePossessive(user2WithFullName)
			expect(result2).toBe('Chris’')
		})

		it("should display the user's email address in possessive form when name is null", () => {
			const result = displayFirstNamePossessive(userWithNoName)
			expect(result).toBe('test@test.test’s')

			const result2 = displayFirstNamePossessive(user2WithNoName)
			expect(result2).toBe('test@test.tests’s')
		})
	})

	describe('filterUsersByRole', () => {
		it('should include only users with role', () => {
			const filterByOwner = filterUsersByRole(BASE_ROLE.GROUP_OWNER)
			const results = filterByOwner(users)
			expect(results.length).toEqual(2)

			const filterByLearners = filterUsersByRole(BASE_ROLE.GROUP_LEARNER)
			const results2 = filterByLearners(users)
			expect(results2.length).toEqual(3)
		})
	})
})
