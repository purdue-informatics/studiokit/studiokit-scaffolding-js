import { unknownErrorData } from '../constants/fetchErrorData'
import { MODEL_FETCH_REQUEST_ACTION_TYPE } from '../redux/actions/ModelAction'
import { EndpointMapping, FetchConfig, HTTP_STATUS_CODE } from '../types'
import { isFetchErrorData, prepareFetch, PrepareFetchResult } from './fetch'

jest.mock('uuid', () => {
	return {
		v4: jest.fn().mockImplementation(() => 'new-guid')
	}
})

describe('utils/fetch', () => {
	describe('isFetchErrorData', () => {
		it('should return true for an object with required properties', () => {
			expect(
				isFetchErrorData({
					status: HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR,
					title: 'Error',
					detail: null
				})
			).toEqual(true)
			expect(isFetchErrorData(unknownErrorData)).toEqual(true)
		})
		it('should return false', () => {
			expect(isFetchErrorData(undefined)).toEqual(false)
			expect(isFetchErrorData(null)).toEqual(false)
			expect(isFetchErrorData(1)).toEqual(false)
			expect(isFetchErrorData('foo')).toEqual(false)
			expect(isFetchErrorData({})).toEqual(false)
			expect(isFetchErrorData({ status: HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR })).toEqual(false)
			expect(isFetchErrorData({ status: HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR, title: 'Error' })).toEqual(false)
			expect(isFetchErrorData({ title: 'Error', detail: null })).toEqual(false)
			expect(isFetchErrorData({ detail: null })).toEqual(false)
		})
	})
	describe('prepareFetch', () => {
		it("should throw without 'modelFetchRequestAction'", () => {
			expect(() => {
				prepareFetch(undefined as any, {})
			}).toThrow("'modelFetchRequestAction' is required")
		})
		it("should throw without 'modelName' on the action", () => {
			expect(() => {
				prepareFetch({ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: '' }, {})
			}).toThrow("'modelFetchRequestAction.modelName' is required")
		})
		it("should throw without 'endpointMappings'", () => {
			expect(() => {
				prepareFetch(
					{ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: 'models' },
					undefined as any
				)
			}).toThrow("'endpointMappings' is required")
		})
		it("should throw if 'modelName' cannot be found in 'endpointMappings'", () => {
			expect(() => {
				prepareFetch({ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: 'models' }, {})
			}).toThrow("Cannot find 'models' in 'endpointMappings'")
		})
		it("should throw if 'path' is not set in the 'endpointMapping' for a non-collection model", () => {
			expect(() => {
				prepareFetch(
					{ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: 'model' },
					{
						model: {}
					}
				)
			}).toThrow("'fetchConfig.path' is required for non-collections")
		})
		describe('fetchConfig', () => {
			it("should use all defaults from 'endpointMapping'", () => {
				const fetchConfig: FetchConfig = {
					path: '/api/models/model',
					method: 'POST',
					headers: {
						'some-header': 'some-value'
					},
					body: {
						defaultKey: 'defaultValue'
					},
					contentType: 'application/json; charset=utf-8',
					queryParams: {
						foo: 'bar'
					}
				}
				const endpointMapping: EndpointMapping = {
					_config: {
						fetch: fetchConfig
					}
				}
				const result = prepareFetch(
					{ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: 'models' },
					{
						models: endpointMapping
					}
				)
				expect(result.fetchConfig).toEqual(fetchConfig)
			})
			it("should use 'action.method' if provided", () => {
				const result = prepareFetch(
					{ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: 'model', method: 'POST' },
					{
						model: {
							_config: {
								fetch: {
									path: '/api/endpoint'
								}
							}
						}
					}
				)
				expect(result.fetchConfig.method).toEqual('POST')
			})
			it("should use string 'action.body' and set 'contentType'", () => {
				const result = prepareFetch(
					{
						type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
						modelName: 'model',
						method: 'POST',
						body: 'body'
					},
					{
						model: {
							_config: {
								fetch: {
									path: '/api/endpoint'
								}
							}
						}
					}
				)
				const expectedFetchConfig: FetchConfig = {
					path: '/api/endpoint',
					method: 'POST',
					contentType: 'application/x-www-form-urlencoded',
					headers: {},
					queryParams: {},
					body: 'body'
				}
				expect(result.fetchConfig).toEqual(expectedFetchConfig)
			})
			it("should use JSON object 'action.body'", () => {
				const result = prepareFetch(
					{
						type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
						modelName: 'model',
						method: 'POST',
						body: { foo: 'bar' }
					},
					{
						model: {
							_config: {
								fetch: {
									path: '/api/endpoint'
								}
							}
						}
					}
				)
				const expectedFetchConfig: FetchConfig = {
					path: '/api/endpoint',
					method: 'POST',
					headers: {},
					queryParams: {},
					body: {
						foo: 'bar'
					}
				}
				expect(result.fetchConfig).toEqual(expectedFetchConfig)
			})
			it("should merge 'action.body' and default body as JSON Array if both are arrays", () => {
				const result = prepareFetch(
					{
						type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
						modelName: 'model',
						method: 'POST',
						body: ['foo']
					},
					{
						model: {
							_config: {
								fetch: {
									path: '/api/endpoint',
									body: ['bar']
								}
							}
						}
					}
				)
				const expectedFetchConfig: FetchConfig = {
					path: '/api/endpoint',
					method: 'POST',
					headers: {},
					queryParams: {},
					body: ['bar', 'foo']
				}
				expect(result.fetchConfig).toEqual(expectedFetchConfig)
			})
			it("should merge 'action.body' and default body as JSON object if both are objects", () => {
				const result = prepareFetch(
					{
						type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
						modelName: 'model',
						method: 'POST',
						body: {
							foo: 'bar'
						}
					},
					{
						model: {
							_config: {
								fetch: {
									path: '/api/endpoint',
									body: {
										fizz: 'buzz'
									}
								}
							}
						}
					}
				)
				const expectedFetchConfig: FetchConfig = {
					path: '/api/endpoint',
					method: 'POST',
					headers: {},
					queryParams: {},
					body: {
						fizz: 'buzz',
						foo: 'bar'
					}
				}
				expect(result.fetchConfig).toEqual(expectedFetchConfig)
			})
			it("should use 'action.contentType' if provided", () => {
				const result = prepareFetch(
					{
						type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
						modelName: 'model',
						contentType: 'text/html'
					},
					{
						model: {
							_config: {
								fetch: {
									path: '/api/endpoint'
								}
							}
						}
					}
				)
				expect(result.fetchConfig.contentType).toEqual('text/html')
			})
		})
		describe('populate path params', () => {
			it('should populate basic parameter in path', () => {
				const result = prepareFetch(
					{
						type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
						modelName: 'model',
						pathParams: [1]
					},
					{
						model: {
							_config: {
								fetch: {
									path: '/api/models/{:id}'
								}
							}
						}
					}
				)
				const expectedFetchConfig: FetchConfig = {
					path: '/api/models/1',
					headers: {},
					queryParams: {}
				}
				expect(result.fetchConfig).toEqual(expectedFetchConfig)
			})
			it('should fail to populate basic parameter in path if it is undefined', () => {
				expect(() => {
					prepareFetch(
						{
							type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
							modelName: 'model'
						},
						{
							model: {
								_config: {
									fetch: {
										path: '/api/models/{:id}'
									}
								}
							}
						}
					)
				}).toThrowError(`Could not replace params in 'fetchConfig.path' using 'pathParams'`)
			})
			it('should fail to populate basic parameter in path if it is null', () => {
				expect(() => {
					prepareFetch(
						{
							type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
							modelName: 'model',
							pathParams: [null]
						},
						{
							model: {
								_config: {
									fetch: {
										path: '/api/models/{:id}'
									}
								}
							}
						}
					)
				}).toThrowError(`Could not replace params in 'fetchConfig.path' using 'pathParams'`)
			})
			it('should fail to populate basic parameter in path if it is undefined, for collection', () => {
				expect(() => {
					prepareFetch(
						{
							type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
							modelName: 'models',
							pathParams: [undefined]
						},
						{
							models: {
								_config: {
									isCollection: true
								}
							}
						}
					)
				}).toThrowError(`Could not replace params in 'modelPath' using 'modelPathParams'`)
			})
		})
		describe('collections', () => {
			it('should succeed for collection', () => {
				const result = prepareFetch(
					{ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: 'models' },
					{
						models: {
							_config: {
								isCollection: true
							}
						}
					}
				)
				const expectedResult: PrepareFetchResult = {
					endpointMapping: {
						_config: {
							isCollection: true
						}
					},
					fetchConfig: {
						path: '/api/models',
						headers: {},
						queryParams: {}
					},
					endpointConfig: {
						isCollection: true
					},
					modelPath: 'models',
					isCollectionItemFetch: false,
					isCollectionItemCreate: false,
					isCollectionItemDelete: false
				}
				expect(result).toEqual(expectedResult)
			})
			it('should succeed for collection item fetch', () => {
				const result = prepareFetch(
					{ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: 'models', pathParams: [1] },
					{
						models: {
							_config: {
								isCollection: true
							}
						}
					}
				)
				const expectedResult: PrepareFetchResult = {
					endpointMapping: {
						_config: {
							isCollection: true
						}
					},
					fetchConfig: {
						path: '/api/models/1',
						headers: {},
						queryParams: {}
					},
					endpointConfig: {
						isCollection: true
					},
					modelPath: 'models.1',
					isCollectionItemFetch: true,
					isCollectionItemCreate: false,
					isCollectionItemDelete: false
				}
				expect(result).toEqual(expectedResult)
			})
			it('should succeed for collection item create', () => {
				const result = prepareFetch(
					{
						type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
						modelName: 'models',
						method: 'POST'
					},
					{
						models: {
							_config: {
								isCollection: true
							}
						}
					}
				)
				const expectedResult: PrepareFetchResult = {
					endpointMapping: {
						_config: {
							isCollection: true
						}
					},
					fetchConfig: {
						path: '/api/models',
						method: 'POST',
						headers: {},
						queryParams: {}
					},
					endpointConfig: {
						isCollection: true
					},
					modelPath: 'models.new-guid',
					isCollectionItemFetch: false,
					isCollectionItemCreate: true,
					isCollectionItemDelete: false
				}
				expect(result).toEqual(expectedResult)
			})
			it('should succeed for collection item create, with provided guid', () => {
				const result = prepareFetch(
					{
						type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
						modelName: 'models',
						method: 'POST',
						guid: 'dummy-guid'
					},
					{
						models: {
							_config: {
								isCollection: true
							}
						}
					}
				)
				const expectedResult: PrepareFetchResult = {
					endpointMapping: {
						_config: {
							isCollection: true
						}
					},
					fetchConfig: {
						path: '/api/models',
						method: 'POST',
						headers: {},
						queryParams: {}
					},
					endpointConfig: {
						isCollection: true
					},
					modelPath: 'models.dummy-guid',
					isCollectionItemFetch: false,
					isCollectionItemCreate: true,
					isCollectionItemDelete: false
				}
				expect(result).toEqual(expectedResult)
			})
			it('should succeed for collection item delete', () => {
				const result = prepareFetch(
					{
						type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
						modelName: 'models',
						method: 'DELETE',
						pathParams: [1]
					},
					{
						models: {
							_config: {
								isCollection: true
							}
						}
					}
				)
				const expectedResult: PrepareFetchResult = {
					endpointMapping: {
						_config: {
							isCollection: true
						}
					},
					fetchConfig: {
						path: '/api/models/1',
						method: 'DELETE',
						headers: {},
						queryParams: {}
					},
					endpointConfig: {
						isCollection: true
					},
					modelPath: 'models.1',
					isCollectionItemFetch: true,
					isCollectionItemCreate: false,
					isCollectionItemDelete: true
				}
				expect(result).toEqual(expectedResult)
			})
			it('should construct paths for collections without paths, nested level, with pathParams', () => {
				const result = prepareFetch(
					{
						type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
						modelName: 'models.nestedModels',
						pathParams: [1, 2]
					},
					{
						models: {
							_config: {
								isCollection: true
							},
							nestedModels: {
								_config: {
									isCollection: true
								}
							}
						}
					}
				)
				expect(result.fetchConfig.path).toEqual('/api/models/1/nestedModels/2')
				expect(result.modelPath).toEqual('models.1.nestedModels.2')
			})
			it('should construct paths for collection item action, single level, with pathParams', () => {
				const result = prepareFetch(
					{
						type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
						modelName: 'models.modelAction',
						pathParams: [1]
					},
					{
						models: {
							_config: {
								isCollection: true
							},
							modelAction: {
								_config: {
									fetch: {
										path: 'modelAction',
										method: 'POST'
									}
								}
							}
						}
					}
				)
				expect(result.fetchConfig.path).toEqual('/api/models/1/modelAction')
				expect(result.modelPath).toEqual('models.1.modelAction')
			})
			it('should construct paths for collection item action, nested level, with pathParams', () => {
				const result = prepareFetch(
					{
						type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
						modelName: 'models.nestedModels.modelAction',
						pathParams: [1, 2]
					},
					{
						models: {
							_config: {
								isCollection: true
							},
							nestedModels: {
								_config: {
									isCollection: true
								},
								modelAction: {
									_config: {
										fetch: {
											path: 'modelAction',
											method: 'POST'
										}
									}
								}
							}
						}
					}
				)
				expect(result.fetchConfig.path).toEqual('/api/models/1/nestedModels/2/modelAction')
				expect(result.modelPath).toEqual('models.1.nestedModels.2.modelAction')
			})
			it('should populate id params for multi-level collection paths', () => {
				const result = prepareFetch(
					{
						type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
						modelName: 'foo.bar.baz',
						pathParams: [1, 2]
					},
					{
						foo: {
							_config: {
								isCollection: true
							},
							bar: {
								_config: {
									isCollection: true
								},
								baz: {
									_config: {
										isCollection: true
									}
								}
							}
						}
					}
				)
				expect(result.fetchConfig.path).toEqual('/api/foo/1/bar/2/baz')
			})
			it('should populate id params for collection nested under non-collections', () => {
				const result = prepareFetch(
					{ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: 'foo.bar.baz', pathParams: [1] },
					{
						foo: {
							bar: {
								baz: {
									_config: {
										isCollection: true
									}
								}
							}
						}
					}
				)
				expect(result.fetchConfig.path).toEqual('/api/foo/bar/baz/1')
			})
			it('should populate id params for collection nested under non-collections and honor empty fetch paths', () => {
				const result = prepareFetch(
					{ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: 'foo.bar.baz', pathParams: [1] },
					{
						foo: {
							_config: {
								fetch: {
									path: ''
								}
							},
							bar: {
								_config: {
									fetch: {
										path: '/api/bar'
									},
									isCollection: true
								},
								baz: {
									_config: {
										isCollection: true
									}
								}
							}
						}
					}
				)
				const expectedResult: PrepareFetchResult = {
					endpointMapping: {
						_config: {
							isCollection: true
						}
					},
					fetchConfig: {
						path: '/api/bar/1/baz',
						headers: {},
						queryParams: {}
					},
					endpointConfig: {
						isCollection: true
					},
					modelPath: 'foo.bar.1.baz',
					isCollectionItemFetch: false,
					isCollectionItemCreate: false,
					isCollectionItemDelete: false
				}
				expect(result).toEqual(expectedResult)
			})
			it('should use absolute path for non-collection under collection with absolute path, and not append pathParams to path', () => {
				const result = prepareFetch(
					{ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: 'foo.bar', pathParams: [1] },
					{
						foo: {
							_config: {
								isCollection: true,
								fetch: {
									path: '/api/foo'
								}
							},
							bar: {
								_config: {
									fetch: {
										path: '/api/bar'
									}
								}
							}
						}
					}
				)
				const expectedResult: PrepareFetchResult = {
					endpointMapping: {
						_config: {
							fetch: {
								path: '/api/bar'
							}
						}
					},
					fetchConfig: {
						path: '/api/bar',
						headers: {},
						queryParams: {}
					},
					endpointConfig: {
						fetch: {
							path: '/api/bar'
						}
					},
					modelPath: 'foo.1.bar',
					isCollectionItemCreate: false,
					isCollectionItemFetch: false,
					isCollectionItemDelete: false
				}
				expect(result).toEqual(expectedResult)
			})
			it('should use absolute path for collection under collection with absolute path, and exclude first pathParam from path', () => {
				const result = prepareFetch(
					{ type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST, modelName: 'foo.bar', pathParams: [1, 2] },
					{
						foo: {
							_config: {
								isCollection: true,
								fetch: {
									path: '/api/foo'
								}
							},
							bar: {
								_config: {
									isCollection: true,
									fetch: {
										path: '/api/bar'
									}
								}
							}
						}
					}
				)
				const expectedResult: PrepareFetchResult = {
					endpointMapping: {
						_config: {
							isCollection: true,
							fetch: {
								path: '/api/bar'
							}
						}
					},
					fetchConfig: {
						path: '/api/bar/2',
						headers: {},
						queryParams: {}
					},
					endpointConfig: {
						isCollection: true,
						fetch: {
							path: '/api/bar'
						}
					},
					modelPath: 'foo.1.bar.2',
					isCollectionItemFetch: true,
					isCollectionItemCreate: false,
					isCollectionItemDelete: false
				}
				expect(result).toEqual(expectedResult)
			})
			it('should use absolute path for collection under two collections with absolute path, and exclude correct pathParams from path', () => {
				const result = prepareFetch(
					{
						type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
						modelName: 'foo.bar.baz',
						pathParams: [1, 2, 3]
					},
					{
						foo: {
							_config: {
								isCollection: true,
								fetch: {
									path: '/api/foo'
								}
							},
							bar: {
								_config: {
									isCollection: true,
									fetch: {
										path: '/api/bar'
									}
								},
								baz: {
									_config: {
										isCollection: true
									}
								}
							}
						}
					}
				)
				const expectedResult: PrepareFetchResult = {
					endpointMapping: {
						_config: {
							isCollection: true
						}
					},
					fetchConfig: {
						headers: {},
						path: '/api/bar/2/baz/3',
						queryParams: {}
					},
					endpointConfig: {
						isCollection: true
					},
					modelPath: 'foo.1.bar.2.baz.3',
					isCollectionItemFetch: true,
					isCollectionItemCreate: false,
					isCollectionItemDelete: false
				}
				expect(result).toEqual(expectedResult)
			})
			it('should use absolute path for collection with absolute path under two other collections with absolute paths, and exclude correct pathParams from path', () => {
				const result = prepareFetch(
					{
						type: MODEL_FETCH_REQUEST_ACTION_TYPE.FETCH_REQUEST,
						modelName: 'foo.bar.baz',
						pathParams: [1, 2, 3]
					},
					{
						foo: {
							_config: {
								isCollection: true,
								fetch: {
									path: '/api/foo'
								}
							},
							bar: {
								_config: {
									isCollection: true,
									fetch: {
										path: '/api/bar'
									}
								},
								baz: {
									_config: {
										isCollection: true,
										fetch: {
											path: '/api/baz'
										}
									}
								}
							}
						}
					}
				)
				const expectedResult: PrepareFetchResult = {
					endpointMapping: {
						_config: {
							isCollection: true,
							fetch: {
								path: '/api/baz'
							}
						}
					},
					fetchConfig: {
						headers: {},
						path: '/api/baz/3',
						queryParams: {}
					},
					endpointConfig: {
						isCollection: true,
						fetch: {
							path: '/api/baz'
						}
					},
					modelPath: 'foo.1.bar.2.baz.3',
					isCollectionItemFetch: true,
					isCollectionItemCreate: false,
					isCollectionItemDelete: false
				}
				expect(result).toEqual(expectedResult)
			})
		})
	})
})
