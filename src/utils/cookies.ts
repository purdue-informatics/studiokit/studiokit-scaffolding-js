/** Get the contents of `document.cookie` as a Record with string keys and values. */
export const getCookies = () => {
	const cookies: Record<string, string> = {}
	if (document.cookie && document.cookie !== '') {
		const cookieStrings = document.cookie.split(';')
		cookieStrings.forEach(cookieString => {
			const cookieParts = cookieString.split('=')
			const name = decodeURIComponent(cookieParts[0].replace(/^ /, ''))
			const value = decodeURIComponent(cookieParts[1])
			cookies[name] = value
		})
	}
	return cookies
}
