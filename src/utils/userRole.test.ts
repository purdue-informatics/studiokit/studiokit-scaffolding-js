import { merge } from 'lodash'
import { BASE_ROLE } from '../constants/baseRole'
import {
	defaultExternalGroupLearner,
	defaultGroupGrader,
	defaultGroupLearner,
	defaultGroupOwner
} from '../constants/mockData'
import { GroupUserRole, UserRole } from '../types'
import { convertToUserWithRoles, getRoleText, isExternal, sortByHighestAccessRole, sortByRole } from './userRole'

describe('utils/userRole', () => {
	describe('isExternal', () => {
		it('returns false if no property', () => {
			expect(
				isExternal({
					role: BASE_ROLE.ADMIN
				} as UserRole)
			).toEqual(false)
		})
		it('returns false if false', () => {
			expect(isExternal(defaultGroupOwner)).toEqual(false)
		})
		it('returns true if true', () => {
			expect(isExternal(defaultExternalGroupLearner)).toEqual(true)
		})
	})

	describe('getRoleText', () => {
		it('get roleText using provided method', () => {
			expect(
				getRoleText((role: string) => 'Wizard')({
					role: BASE_ROLE.ADMIN
				} as UserRole)
			).toEqual('Wizard')
		})
		it('get roleText using default method', () => {
			expect(
				getRoleText()({
					role: BASE_ROLE.ADMIN
				} as UserRole)
			).toEqual('Admin')
		})
	})

	describe('sortByRole', () => {
		it('sorts using provided role order', () => {
			const roles = [BASE_ROLE.GROUP_OWNER, BASE_ROLE.GROUP_GRADER]
			const grader: UserRole = defaultGroupGrader
			const owner: UserRole = defaultGroupOwner
			expect(sortByRole(roles)(owner, grader)).toEqual(-1)
			expect(sortByRole(roles)(grader, owner)).toEqual(1)
			expect(sortByRole(roles)(owner, owner)).toEqual(0)
		})
	})

	describe('sortByHighestAccessRole', () => {
		it('sorts using first role provided when multiple roles exist', () => {
			const roles = [BASE_ROLE.GROUP_OWNER, BASE_ROLE.GROUP_GRADER, BASE_ROLE.GROUP_LEARNER]

			const groupGraderUserWithRoles = convertToUserWithRoles<GroupUserRole>(defaultGroupGrader)
			const groupLearnerUserWithRoles = convertToUserWithRoles<GroupUserRole>(defaultGroupLearner)
			const groupOwnerUserWithRoles = convertToUserWithRoles<GroupUserRole>(defaultGroupOwner)
			const groupLearnerWithOwner = merge({}, groupLearnerUserWithRoles)
			groupLearnerWithOwner.roles.push(merge({}, defaultGroupOwner, { userId: groupLearnerUserWithRoles.id }))

			const groupUsersWithRoles = [groupGraderUserWithRoles, groupOwnerUserWithRoles, groupLearnerWithOwner]
			expect(groupUsersWithRoles.sort(sortByHighestAccessRole(roles))).toEqual([
				groupOwnerUserWithRoles,
				groupLearnerWithOwner,
				groupGraderUserWithRoles
			])
		})
	})
})
