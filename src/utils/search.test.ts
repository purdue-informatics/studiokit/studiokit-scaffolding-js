import { merge } from 'lodash'
import { BASE_ACTIVITY, BASE_ROLE, GROUP_MANAGE_TABLE_TAB, MANAGE_TABLE_TAB } from '../constants'
import { defaultExternalTerms, defaultGroup, mockPastAndCurrentGroups } from '../constants/mockData'
import { EntityUser, Group, ModelCollection } from '../types'
import { getModelArray } from './model'
import {
	getKeywordsList,
	isEntityMatchedByKeyword,
	isEntityMatchedByKeywords,
	isEntityUserMatchedByKeyword,
	searchEntities,
	searchGroups
} from './search'

describe('utils/search', () => {
	describe('getKeywordsList', () => {
		it('returns empty array with undefined', () => {
			expect(getKeywordsList(undefined)).toEqual([])
		})
		it('trims spaces from keywords', () => {
			expect(getKeywordsList(' foo  ')).toEqual(['foo'])
		})
		it('splits keywords by space', () => {
			expect(getKeywordsList(' foo  bar ')).toEqual(['foo', 'bar'])
		})
	})
	describe('isEntityUserMatchedByKeyword', () => {
		let entityUser: EntityUser
		beforeEach(() => {
			entityUser = {
				entityId: 1,
				id: 'abc',
				firstName: null,
				lastName: null,
				email: null,
				uid: null
			}
		})
		it('does not match', () => {
			expect(isEntityUserMatchedByKeyword(entityUser, 'zzz')).toEqual(false)
		})
		it('matches by id', () => {
			expect(isEntityUserMatchedByKeyword(entityUser, 'abc')).toEqual(true)
		})
		it('matches by firstName', () => {
			entityUser.firstName = 'George'
			expect(isEntityUserMatchedByKeyword(entityUser, 'org')).toEqual(true)
		})
		it('matches by lastName', () => {
			entityUser.lastName = 'Smith'
			expect(isEntityUserMatchedByKeyword(entityUser, 'smit')).toEqual(true)
		})
		it('matches by email', () => {
			entityUser.email = 'bob@blahblah.lawblog'
			expect(isEntityUserMatchedByKeyword(entityUser, 'bob')).toEqual(true)
		})
		it('matches by uid', () => {
			entityUser.uid = 'paddington'
			expect(isEntityUserMatchedByKeyword(entityUser, 'pad')).toEqual(true)
		})
	})
	describe('isEntityMatchedByKeyword', () => {
		let entity: { id: number; name: string; owners: EntityUser[] }
		beforeEach(() => {
			entity = {
				id: 1,
				name: 'Search 101',
				owners: [{ entityId: 1, id: 'abc', firstName: null, lastName: null, email: null, uid: null }]
			}
		})
		it('does not match', () => {
			expect(isEntityMatchedByKeyword(entity, 'zzz')).toEqual(false)
		})
		it('matches by id', () => {
			expect(isEntityMatchedByKeyword(entity, '1')).toEqual(true)
		})
		it('matches by name', () => {
			expect(isEntityMatchedByKeyword(entity, '101')).toEqual(true)
		})
		it('matches by owner', () => {
			expect(isEntityMatchedByKeyword(entity, 'abc')).toEqual(true)
		})
	})
	describe('searchEntities', () => {
		// use Groups here as a placeholder for other app-specific entities
		let entityArray: Group[]
		let availableEntity: Group
		let deletedEntity: Group
		beforeEach(() => {
			availableEntity = merge({ activities: [BASE_ACTIVITY.GROUP_READ] }, defaultGroup)
			deletedEntity = merge({ activities: [BASE_ACTIVITY.GROUP_READ] }, defaultGroup, {
				name: 'Test Group',
				isDeleted: true
			})
			entityArray = [availableEntity, deletedEntity]
		})
		it('returns filtered entities when can read any', () => {
			// remove activities to test global access
			availableEntity.activities = []
			deletedEntity.activities = []
			expect(searchEntities(entityArray, isEntityMatchedByKeywords, BASE_ACTIVITY.GROUP_READ, true)).toEqual({
				[MANAGE_TABLE_TAB.AVAILABLE]: [availableEntity],
				[MANAGE_TABLE_TAB.DELETED]: [deletedEntity]
			})
		})
		it('returns filtered assessments with read activity', () => {
			// remove activity access
			deletedEntity.activities = []
			expect(searchEntities(entityArray, isEntityMatchedByKeywords, BASE_ACTIVITY.GROUP_READ, false)).toEqual({
				[MANAGE_TABLE_TAB.AVAILABLE]: [availableEntity],
				[MANAGE_TABLE_TAB.DELETED]: []
			})
		})
		it('excludes entity using keywords', () => {
			expect(
				searchEntities(entityArray, isEntityMatchedByKeywords, BASE_ACTIVITY.GROUP_READ, false, 'test')
			).toEqual({
				[MANAGE_TABLE_TAB.AVAILABLE]: [],
				[MANAGE_TABLE_TAB.DELETED]: [deletedEntity]
			})
		})
	})
	describe('groups', () => {
		describe('searchGroups', () => {
			let groups: ModelCollection<Group>
			let groupsArray: Group[]
			let pastGroup: Group
			let currentGroup: Group
			let deletedGroup: Group
			beforeEach(() => {
				groups = mockPastAndCurrentGroups([BASE_ACTIVITY.GROUP_READ], [BASE_ROLE.GROUP_OWNER])
				pastGroup = groups['1'] as Group
				pastGroup.name = 'Past Group ABC'
				currentGroup = groups['2'] as Group
				currentGroup.name = 'Current Group XYZ'
				deletedGroup = merge(
					{ roles: [BASE_ROLE.GROUP_GRADER], activities: [BASE_ACTIVITY.GROUP_READ] },
					defaultGroup,
					{
						id: 3,
						name: 'Deleted Group',
						isDeleted: true
					}
				)
				groups['3'] = deletedGroup
				groupsArray = getModelArray(groups)
			})
			it('returns filtered groups when can read any', () => {
				// remove roles to test global access
				pastGroup.roles = []
				currentGroup.roles = []
				deletedGroup.roles = []
				expect(
					searchGroups({
						groups,
						modelArray: groupsArray,
						externalTerms: defaultExternalTerms,
						canReadyAnyGlobally: true
					})
				).toEqual({
					[GROUP_MANAGE_TABLE_TAB.CURRENT]: [currentGroup],
					[GROUP_MANAGE_TABLE_TAB.PAST]: [pastGroup],
					[GROUP_MANAGE_TABLE_TAB.DELETED]: [deletedGroup]
				})
			})
			it('returns filtered groups with a role other than learner', () => {
				// change roles to learner on 2/3
				pastGroup.roles = [BASE_ROLE.GROUP_LEARNER]
				currentGroup.roles = [BASE_ROLE.GROUP_LEARNER]
				expect(
					searchGroups({
						groups,
						modelArray: groupsArray,
						externalTerms: defaultExternalTerms
					})
				).toEqual({
					[GROUP_MANAGE_TABLE_TAB.CURRENT]: [],
					[GROUP_MANAGE_TABLE_TAB.PAST]: [],
					[GROUP_MANAGE_TABLE_TAB.DELETED]: [deletedGroup]
				})
			})
			it('excludes groups using keywords', () => {
				expect(
					searchGroups({
						groups,
						modelArray: groupsArray,
						externalTerms: defaultExternalTerms,
						keywords: 'ABC'
					})
				).toEqual({
					[GROUP_MANAGE_TABLE_TAB.CURRENT]: [],
					[GROUP_MANAGE_TABLE_TAB.PAST]: [pastGroup],
					[GROUP_MANAGE_TABLE_TAB.DELETED]: []
				})
			})
			it('excludes groups using dateString', () => {
				expect(
					searchGroups({
						groups,
						modelArray: groupsArray,
						externalTerms: defaultExternalTerms,
						dateString: pastGroup.startDate as string
					})
				).toEqual({
					[GROUP_MANAGE_TABLE_TAB.CURRENT]: [],
					[GROUP_MANAGE_TABLE_TAB.PAST]: [pastGroup],
					[GROUP_MANAGE_TABLE_TAB.DELETED]: []
				})
			})
		})
	})
})
