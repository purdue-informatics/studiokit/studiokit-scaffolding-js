import { Dictionary } from 'lodash'
import { LOCKDOWN_BROWSER_KEY, LOCKDOWN_BROWSER_TRUE } from '../constants/lockDownBrowser'
import { getLockDownBrowserInfo, LockDownBrowserInfo } from './lockDownBrowser'

let mockCookies: Dictionary<string> = {}

jest.mock('./cookies', () => {
	return {
		getCookies: () => {
			return mockCookies
		}
	}
})

describe('utils/lockDownBrowser', () => {
	beforeEach(() => {
		mockCookies = {}
	})

	describe('getLockDownBrowserInfo', () => {
		it('returns without any cookie values', () => {
			const expectedInfo: LockDownBrowserInfo = {
				isClientLockDownBrowser: false
			}
			expect(getLockDownBrowserInfo()).toEqual(expectedInfo)
		})
		it('returns with bad cookie value', () => {
			mockCookies = {
				[LOCKDOWN_BROWSER_KEY.CLIENT_IS_LOCK_DOWN_BROWSER]: 'bad'
			}
			const expectedInfo: LockDownBrowserInfo = {
				isClientLockDownBrowser: false
			}
			expect(getLockDownBrowserInfo()).toEqual(expectedInfo)
		})
		it('returns cookie values if they exist', () => {
			mockCookies = {
				[LOCKDOWN_BROWSER_KEY.CLIENT_IS_LOCK_DOWN_BROWSER]: LOCKDOWN_BROWSER_TRUE,
				[LOCKDOWN_BROWSER_KEY.BUILD_ID]: 'foobarfizzbuzz',
				[LOCKDOWN_BROWSER_KEY.BUILD_DATE_WINDOWS]: '220921'
			}
			const expectedInfo: LockDownBrowserInfo = {
				isClientLockDownBrowser: true,
				buildId: 'foobarfizzbuzz',
				buildDateWindows: '220921'
			}
			expect(getLockDownBrowserInfo()).toEqual(expectedInfo)
		})
	})
})
