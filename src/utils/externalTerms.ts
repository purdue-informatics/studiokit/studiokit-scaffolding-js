import { ExternalTerm } from '../types/ExternalTerm'
import { getServerNowUtc } from '../utils/date'
import { isNowBeforeDate } from './date'

export const filterByCurrentAndFuture = (externalTerms: ExternalTerm[], nowUtc = getServerNowUtc().toISOString()) =>
	externalTerms.filter(et => isNowBeforeDate(et.endDate, nowUtc))
