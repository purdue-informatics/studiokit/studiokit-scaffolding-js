import {
	getFormattedNumberedTimeWithFullZone,
	getFormattedSimplifiedDateWithYearIfNotCurrent,
	getFullFormattedTimeZone,
	getOffsetDate,
	setClockOffset
} from './date'

const nowUtc = '2019-01-01T12:00:00Z'

describe('getFormattedNumberedTimeWithFullZone', () => {
	it('does not return timezone name if in EDT or EST', () => {
		const formattedDate = getFormattedNumberedTimeWithFullZone(nowUtc, false, 'America/Indianapolis')
		expect(formattedDate).toEqual('7:00\u00a0AM EST')
	})
	it('does return timezone name if not in EDT or EST', () => {
		const formattedDate = getFormattedNumberedTimeWithFullZone(nowUtc, false, 'America/Chicago')
		expect(formattedDate).toEqual('6:00\u00a0AM CST (America/Chicago)')
	})
	it('does return timezone name if in EDT or EST and alwaysShowZone is true', () => {
		const formattedDate = getFormattedNumberedTimeWithFullZone(nowUtc, true, 'America/Indianapolis')
		expect(formattedDate).toEqual('7:00\u00a0AM EST (America/Indianapolis)')
	})
	it('does remove underscores from timezone names with underscores', () => {
		const formattedDate = getFormattedNumberedTimeWithFullZone(nowUtc, false, 'America/Los_Angeles')
		expect(formattedDate).toEqual('4:00\u00a0AM PST (America/Los Angeles)')
	})

	describe('getFormattedSimplifiedDateWithYearIfNotCurrent', () => {
		it('does not return year if same as current year', () => {
			const dateTimeUtc = '2019-02-02T12:00:00Z'
			const formattedDate = getFormattedSimplifiedDateWithYearIfNotCurrent(dateTimeUtc, nowUtc)
			expect(formattedDate).toEqual('Feb 2')
		})
		it('does return year if not same as current year', () => {
			const dateTimeUtc = '2018-02-02T12:00:00Z'
			const formattedDate = getFormattedSimplifiedDateWithYearIfNotCurrent(dateTimeUtc, nowUtc)
			expect(formattedDate).toEqual('Feb 2, 2018')
		})
	})

	describe('getFullFormattedTimeZone', () => {
		it('returns both abbreviations for Eastern time zone', () => {
			expect(getFullFormattedTimeZone('America/Indianapolis')).toEqual('EST/EDT')
		})
		it('returns both abbreviations for Eastern time zone, with zoneId when requested', () => {
			expect(getFullFormattedTimeZone('America/Indianapolis', true)).toEqual('EST/EDT (America/Indianapolis)')
		})
		it('returns both abbreviations and zoneId for Pacific time zone', () => {
			expect(getFullFormattedTimeZone('America/Los_Angeles')).toEqual('PST/PDT (America/Los Angeles)')
		})
		it('returns abbreviations and zoneId for Asia/Taipei', () => {
			expect(getFullFormattedTimeZone('Asia/Taipei')).toEqual('CST (Asia/Taipei)')
		})
		it('returns only zoneId when no alpha character abbreviations are found', () => {
			expect(getFullFormattedTimeZone('America/Caracas')).toEqual('America/Caracas')
		})
	})

	describe('getOffsetDate', () => {
		it('returns a date offset by a positive clockOffset', () => {
			setClockOffset(1000)
			expect(getOffsetDate(new Date('2021-04-12 00:00:00'))).toEqual(new Date('2021-04-12 00:00:01'))
		})
		it('returns a date offset by a negative clockOffset', () => {
			setClockOffset(-1000)
			expect(getOffsetDate(new Date('2021-04-12 00:00:01'))).toEqual(new Date('2021-04-12 00:00:00'))
		})
		it('returns a date offset a large amount by the clock offset', () => {
			setClockOffset(86400000) // one day in ms
			expect(getOffsetDate(new Date('2021-04-12 00:00:00'))).toEqual(new Date('2021-04-13 00:00:00'))
		})
		it('returns a date offset without mutating the original date', () => {
			setClockOffset(-1000)
			const date = new Date('2021-04-12 00:00:01')
			expect(getOffsetDate(date)).toEqual(new Date('2021-04-12 00:00:00'))
			expect(date).toEqual(new Date('2021-04-12 00:00:01'))
		})
	})
})
