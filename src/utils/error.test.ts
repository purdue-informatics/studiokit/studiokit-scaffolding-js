import { constructErrorFromCaughtError } from './error'

describe('utils/error', () => {
	describe('constructErrorFromCaughtError', () => {
		it('should use default message', () => {
			const expectedError = constructErrorFromCaughtError(null, 'CustomError', 'custom error')
			expect(expectedError.name).toEqual('CustomError')
			expect(expectedError.message).toEqual('custom error')
		})
		it('should handle a caught Error with generic name', () => {
			const caughtError = new Error('some error')
			const expectedError = constructErrorFromCaughtError(caughtError, 'CustomError', 'custom error')
			expect(expectedError.name).toEqual('CustomError')
			expect(expectedError.message).toEqual('custom error.\nsome error')
			expect(expectedError.stack).toEqual(caughtError.stack)
		})
		it('should handle a caught Error with custom name', () => {
			const caughtError = new Error('some error')
			caughtError.name = 'OtherCustomError'
			const expectedError = constructErrorFromCaughtError(caughtError, 'CustomError', 'custom error')
			expect(expectedError.name).toEqual('CustomError')
			expect(expectedError.message).toEqual('custom error.\nOtherCustomError: some error')
			expect(expectedError.stack).toEqual(caughtError.stack)
		})
		it('should handle a caught string', () => {
			const caughtError = 'some error'
			const expectedError = constructErrorFromCaughtError(caughtError, 'CustomError', 'custom error')
			expect(expectedError.name).toEqual('CustomError')
			expect(expectedError.message).toEqual('custom error.\nsome error')
		})
		it('should handle a caught object', () => {
			const caughtError = { message: 'some error' }
			const expectedError = constructErrorFromCaughtError(caughtError, 'CustomError', 'custom error')
			expect(expectedError.name).toEqual('CustomError')
			expect(expectedError.message).toEqual('custom error.\n{"message":"some error"}')
		})
	})
})
