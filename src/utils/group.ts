import { ExternalTerm } from '../types'
import { isNowEqualOrBeforeDate } from './date'

interface FormError {
	name?: string
}

interface FormData {
	externalTerm?: ExternalTerm
	isUsingExternalTerm: boolean
	startDate?: string
	endDate?: string
	groupName: string
	formErrors: FormError
}

// Not validating dates or external sections currently. Assumes data coming from back end has been validated
export const hasFormErrors = (formErrors: FormError) => !!formErrors.name

export const isInvalidForSave = ({
	externalTerm,
	isUsingExternalTerm,
	startDate,
	endDate,
	groupName,
	formErrors
}: FormData) =>
	!groupName ||
	hasFormErrors(formErrors) ||
	(!isUsingExternalTerm && (!startDate || !endDate || isNowEqualOrBeforeDate(startDate, endDate))) ||
	(isUsingExternalTerm && !externalTerm)
