import { BASE_ROLE } from '../constants/baseRole'
import { singularArticleForBaseRole, textForBaseRole } from './baseRole'

describe('textForRole', () => {
	it('returns correct value', () => {
		expect(textForBaseRole(BASE_ROLE.ADMIN)).toEqual('Admin')
	})
})

describe('singularArticleForRole', () => {
	it('returns correct value for Admin', () => {
		expect(singularArticleForBaseRole(BASE_ROLE.ADMIN)).toEqual('an')
	})
	it('returns correct value for Student', () => {
		expect(singularArticleForBaseRole(BASE_ROLE.GROUP_LEARNER)).toEqual('a')
	})
})
