import { EXTERNAL_PROVIDER_TYPE } from '../constants/externalProviderType'
import { ExternalGroup } from '../types/ExternalGroup'
import { ExternalProvider } from '../types/ExternalProvider'

export const hasAnyUniTimeExternalGroup = (externalGroups: ExternalGroup[], externalProviders: ExternalProvider[]) => {
	const uniTimeExternalProvider = externalProviders.find(ep => ep.typename === EXTERNAL_PROVIDER_TYPE.UNITIME)

	if (!uniTimeExternalProvider) {
		return false
	}

	return externalGroups.some(eg => eg.externalProviderId === uniTimeExternalProvider.id)
}

export const hasAnyLtiExternalGroup = (externalGroups: ExternalGroup[], externalProviders: ExternalProvider[]) => {
	const ltiExternalProvider = externalProviders.find(ep => ep.typename === EXTERNAL_PROVIDER_TYPE.LTI)

	if (!ltiExternalProvider) {
		return false
	}

	return externalGroups.some(eg => eg.externalProviderId === ltiExternalProvider.id)
}

export const hasGradePushEnabled = (externalGroups: ExternalGroup[], externalProviders: ExternalProvider[]) => {
	const ltiExternalProvider = externalProviders.find(ep => ep.gradePushEnabled)

	if (!ltiExternalProvider) {
		return false
	}

	return externalGroups.some(eg => eg.externalProviderId === ltiExternalProvider.id && !!eg.gradesUrl)
}
