import { LtiLaunch } from '../types'

// Check if we are inside an iframe
export const isInsideIFrame = () => window !== window.parent

// Check whether this is a regular launch or a Deep Link launch
export const isBasicLaunch = (ltiLaunch: LtiLaunch) => !ltiLaunch.deepLinkingReturnUrl
