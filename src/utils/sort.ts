import OptionalRecord from '../types/OptionalRecord'
import { User } from '../types/User'

export const sortByAttribute = <K extends keyof T, T extends OptionalRecord<K, string>>(
	attributeName: K,
	shouldSortAscending = true
) => (a: T, b: T) => {
	// Typescript doesn't seem to recognize type guards with generics
	const attrA = a[attributeName] ? (a[attributeName] as string).toUpperCase() : ''
	const attrB = b[attributeName] ? (b[attributeName] as string).toUpperCase() : ''
	let sortResult = 0
	if (attrA === '') {
		sortResult = 1
	} else if (attrB === '') {
		sortResult = -1
	} else if (attrA === attrB) {
		return 0
	}
	if (attrA < attrB) {
		sortResult = -1
	} else if (attrA > attrB) {
		sortResult = 1
	}
	return shouldSortAscending ? sortResult : -1 * sortResult
}

export const sortByAttributeNumeric = <T>(attribute: keyof T) => (a: T, b: T) => {
	if (a[attribute] === b[attribute]) {
		return 0
	}
	if (a[attribute] > b[attribute]) {
		return 1
	}
	if (a[attribute] < b[attribute]) {
		return -1
	}
	return 0
}

// Requires at least last name or email address for each user
export const sortByNames = (a: User, b: User) => {
	if ((!a.lastName && !a.email) || (!b.lastName && !b.email)) {
		throw new Error('Last name or email required')
	}
	// (last name or email) => first name
	const lastNameA = a.lastName ? a.lastName.toLowerCase() : a.email ? a.email : ''
	const lastNameB = b.lastName ? b.lastName.toLowerCase() : b.email ? b.email : ''
	if (lastNameA !== lastNameB) {
		return lastNameA > lastNameB ? 1 : -1
	}
	const firstNameA = a.firstName ? a.firstName.toLowerCase() : ''
	const firstNameB = b.firstName ? b.firstName.toLowerCase() : ''
	if (firstNameA !== firstNameB) {
		return firstNameA > firstNameB ? 1 : -1
	}
	return 0
}

export const sortByNameNatural = (a: { name: string }, b: { name: string }) => {
	return a.name.trim().localeCompare(b.name.trim(), undefined, { numeric: true, sensitivity: 'base' })
}

export const sort = (array: any[], attributeName: string, shouldSortAscending: boolean) => {
	return array.slice().sort(sortByAttribute(attributeName, shouldSortAscending))
}
