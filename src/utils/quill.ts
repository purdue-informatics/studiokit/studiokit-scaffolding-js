import Quill, { DeltaOperation } from 'quill'
import { v5 as uuidv5 } from 'uuid'
import { ImageValue } from '../components/Quill/Formats/Image'
import { dispatchModelFetchRequest } from '../redux/actionCreator'
import { noStoreHooks } from '../redux/sagas/noStoreSaga'
import { FetchErrorData } from '../types'
import { isFetchErrorData } from './fetch'

const Delta = Quill.import('delta')

/**
 * Check for non-public in the provided quill contents. Update the ImageValue of an op if an image is found to be
 * non-public.
 * @param quill The Quill instance
 * @param checkedImages A list of all images we have already processed in previous calls to this function.
 * list of processed images
 */
export const checkForNonPublicImages = (quill: Quill, checkedImages: ImageValue[]) => {
	quill
		.getContents()
		?.ops // Is the op an image and have we not seen it before?
		?.filter(
			op =>
				op.insert.image &&
				!checkedImages.find(image => image.src === op.insert.image.src) &&
				op.insert.image.src.startsWith('http')
		)
		.forEach(op => {
			// Check if the image is public. Do not check data:image urls
			// Use the backend to check if the image is public
			// Due to CSP and other security measures, we can't check this in the frontend

			// uuidv5 is a deterministic uuid generator based on a namespace and a name
			// uuidv5.URL is the namespace for urls
			const hookId = uuidv5(op.insert.image.src, uuidv5.URL)
			noStoreHooks.registerNoStoreActionHook(hookId, (data: Record<string, string[]> | FetchErrorData | null) => {
				noStoreHooks.unregisterNoStoreActionHook(hookId)
				let contentTypeKey: string | undefined
				/**
				 * API fetches the URL, and `noStoreSaga` returns either the result or error
				 * * `data` is `null` when the API could not fetch the URL with a successful status code, and returns `null` => `nonPublic`
				 * * `data` is `FetchErrorData` if the API request itself failed, e.g. from some unknown error, we don't know if the image is public => do not update
				 * * `data` is a dictionary of headers when the API does fetch the URL
				 *   * if there is no `Content-Type` header, we don't know if the image is public => do not update
				 *   * if there is a `Content-Type` header, but is NOT an image type (i.e. it was a 302 that, when followed returned HTML)
				 *     (I'm looking at you, Brightspace) => `nonPublic`
				 *   * if there is a `Content-Type` header, and it is an image type, then the image is public => do not update
				 */
				if (
					!data ||
					(!isFetchErrorData(data) &&
						(contentTypeKey = Object.keys(data).find(k => k.toLowerCase() === 'content-type')) &&
						!data[contentTypeKey][0].startsWith('image'))
				) {
					const contents = quill.getContents()
					const ops = contents.map((o: DeltaOperation) =>
						o.insert.image?.src && op.insert.image.src && o.insert.image.src === op.insert.image.src
							? {
									...o,
									insert: {
										...o.insert,
										image: { ...o.insert.image, nonPublic: true }
									}
							  }
							: o
					)
					const updatedContents = contents.diff(new Delta(ops))
					quill.updateContents(updatedContents)
				}
			})
			// This endpoint returns headers for a url. If the url is nonexistent, it will return null
			dispatchModelFetchRequest({
				modelName: 'urlChecker',
				guid: hookId,
				noStore: true,
				queryParams: {
					url: op.insert.image.src
				}
			})
		})
}
