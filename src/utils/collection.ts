import { concat, Dictionary, find, get, includes, isEqual, values } from 'lodash'
import memoizeOne from 'memoize-one'
import { MODEL_STATUS } from '../constants'
import {
	dispatchModelFetchRequest,
	dispatchModelRemoveKeyAction,
	dispatchPeriodicModelFetchRequest,
	dispatchPeriodicModelFetchTerminateAction
} from '../redux/actionCreator'
import {
	CollectionCreateParams,
	CollectionDeleteParams,
	CollectionItemDeleteParams,
	CollectionItemLoadParams,
	CollectionItemUpdateParams,
	CollectionLoadParams,
	CollectionMethodConfiguration,
	CollectionSelectorMethodParams,
	CollectionSelectorMethodResponse,
	CollectionUpdateParams,
	Model,
	ModelCollection
} from '../types'
import { getMinRequiredPathParamsCount, getPathParamsFromRouteMatchParams, getReduxModelName } from './route'

//#region Redux Methods

const getPathParams = memoizeOne(
	(pathParams: Array<string | number>, routeMatchParams: Dictionary<any>, modelName: string) =>
		pathParams || getPathParamsFromRouteMatchParams(routeMatchParams, modelName)
)

export function selectCollectionItemFromState<TModel extends Model>(
	params: CollectionSelectorMethodParams
): CollectionSelectorMethodResponse<TModel> {
	const { guid, modelName, pathParams, routeMatchParams, state } = params
	const modelNameLevelCount = getMinRequiredPathParamsCount(modelName)
	const p = getPathParams(pathParams, routeMatchParams, modelName)
	const reduxModelName = getReduxModelName(p, modelName)

	let model = {} as TModel
	// find `model` using `guid` as its key, to match new item created in `create()`
	if (p.length === modelNameLevelCount) {
		const collection = (get(state.models, reduxModelName) || {}) as ModelCollection<TModel>
		if (Object.prototype.hasOwnProperty.call(collection, guid)) {
			model = collection[guid] as TModel
		} else {
			const modelByGuid = find(values(collection), (m: TModel) => m && m.guid && m.guid === guid) as TModel
			model = modelByGuid || model
		}
	} else {
		// find `model` using pathParams
		const reduxModel = (get(state.models, reduxModelName) || {}) as TModel
		model = (Object.keys(reduxModel).length > 0 ? reduxModel : {}) as TModel
	}

	return {
		modelName,
		pathParams: p,
		model,
		isCollectionItem: true
	}
}

export function selectCollectionFromState<TModel extends Model>(
	params: CollectionSelectorMethodParams
): CollectionSelectorMethodResponse<TModel> {
	const { modelName, pathParams, routeMatchParams, state } = params
	const p = getPathParams(pathParams, routeMatchParams, modelName)
	const reduxModelName = getReduxModelName(p, modelName)

	let model = get(state.models, reduxModelName) as ModelCollection<TModel> | undefined
	model = model && Object.keys(model).length > 0 ? model : {}

	return {
		modelName,
		pathParams: p,
		model,
		isCollectionItem: false
	}
}

//#endregion Redux Methods

//#region Collection/Item CRUD Methods

//#region Load

export function stopCollectionPeriodicLoad(taskId: string) {
	dispatchPeriodicModelFetchTerminateAction({
		taskId
	})
}

export function loadCollection(config: CollectionMethodConfiguration, params: CollectionLoadParams = {}) {
	const {
		modelName,
		isInitialized,
		pathParams: configPathParams,
		queryParams: configQueryParams,
		changeModelStatus
	} = config
	const { id, pathParams, queryParams, period, taskId, replaceValue } = params
	const p = concat([], pathParams || configPathParams, id ? [id] : [])
	if (p.length !== getMinRequiredPathParamsCount(modelName) + (id ? 1 : 0)) {
		throw new Error('pathParams length does not match length of path components')
	}
	if (isInitialized) {
		changeModelStatus(MODEL_STATUS.LOADING, id)
	}
	if (!period) {
		dispatchModelFetchRequest({
			modelName,
			method: 'GET',
			pathParams: p,
			queryParams: queryParams || configQueryParams,
			replaceValue
		})
	} else {
		if (!taskId) throw new Error('taskId is required for periodic data requests')
		dispatchPeriodicModelFetchRequest({
			modelName,
			method: 'GET',
			pathParams: p,
			queryParams: queryParams || configQueryParams,
			period,
			taskId,
			replaceValue
		})
	}
}

export function loadCollectionItem(config: CollectionMethodConfiguration, params: CollectionItemLoadParams = {}) {
	const {
		modelName,
		isInitialized,
		pathParams: configPathParams,
		queryParams: configQueryParams,
		changeModelStatus
	} = config
	const { pathParams, queryParams, period, taskId, replaceValue } = params
	const p = pathParams || configPathParams
	if (p && p.length < getMinRequiredPathParamsCount(modelName) + 1) {
		throw new Error('pathParams length does not match length of path components')
	}
	if (isInitialized) {
		changeModelStatus(MODEL_STATUS.LOADING)
	}
	if (!period) {
		dispatchModelFetchRequest({
			modelName,
			method: 'GET',
			pathParams: p,
			queryParams: queryParams || configQueryParams,
			replaceValue
		})
	} else {
		if (!taskId) throw new Error('taskId is required for periodic data requests')
		dispatchPeriodicModelFetchRequest({
			modelName,
			method: 'GET',
			pathParams: p,
			queryParams: queryParams || configQueryParams,
			period,
			taskId,
			replaceValue
		})
	}
}

//#endregion Load

//#region Create

export function createItemInCollection(config: CollectionMethodConfiguration, params: CollectionCreateParams) {
	const { guid, modelName, pathParams, queryParams: configQueryParams, changeModelStatus } = config
	const { body, contentType, queryParams } = params
	if (pathParams && pathParams.length < getMinRequiredPathParamsCount(modelName)) {
		throw new Error('pathParams length does not match length of path components')
	}
	changeModelStatus(MODEL_STATUS.CREATING, guid)
	dispatchModelFetchRequest({
		modelName,
		guid,
		method: 'POST',
		body,
		pathParams,
		contentType,
		queryParams: queryParams || configQueryParams
	})
}

export function createCollectionItem(
	model: Model,
	config: CollectionMethodConfiguration,
	params: CollectionCreateParams
) {
	if (model.id) {
		throw new Error('model already exists')
	}
	const { guid, modelName, pathParams, queryParams: configQueryParams, changeModelStatus } = config
	const { body, contentType, queryParams } = params
	if (pathParams && pathParams.length < getMinRequiredPathParamsCount(modelName)) {
		throw new Error('pathParams length does not match length of path components')
	}
	changeModelStatus(MODEL_STATUS.CREATING)
	dispatchModelFetchRequest({
		modelName,
		guid,
		method: 'POST',
		body,
		pathParams,
		contentType,
		queryParams: queryParams || configQueryParams
	})
}

//#endregion Create

//#region Update

export function updateItemInCollection(config: CollectionMethodConfiguration, params: CollectionUpdateParams) {
	const { id, body, contentType, method, queryParams } = params
	if (!id) {
		throw new Error(`'id' is required`)
	}
	if (!body) {
		throw new Error(`'body' is required`)
	}
	const { modelName, pathParams: configPathParams, queryParams: configQueryParams, changeModelStatus } = config
	const pathParams = concat([], configPathParams, [id])
	if (pathParams.length !== getMinRequiredPathParamsCount(modelName) + 1) {
		throw new Error('pathParams+id length does not match length of path components')
	}
	const m = method || 'PUT'
	changeModelStatus(MODEL_STATUS.UPDATING, id)
	dispatchModelFetchRequest({
		modelName,
		method: m,
		pathParams,
		queryParams: queryParams || configQueryParams,
		body,
		contentType
	})
}

export function updateCollectionItem(
	model: Model,
	config: CollectionMethodConfiguration,
	params: CollectionItemUpdateParams
) {
	if (!model.id) {
		throw new Error('model does not exist')
	}
	const { body, contentType, method, queryParams } = params
	if (!body) {
		throw new Error(`'body' is required`)
	}
	const { modelName, pathParams, queryParams: configQueryParams, changeModelStatus } = config
	if (pathParams && pathParams.length < getMinRequiredPathParamsCount(modelName) + 1) {
		throw new Error('pathParams length does not match length of path components')
	}
	const m = method || 'PUT'
	changeModelStatus(MODEL_STATUS.UPDATING)
	dispatchModelFetchRequest({
		modelName,
		method: m,
		pathParams,
		body,
		contentType,
		queryParams: queryParams || configQueryParams
	})
}

//#endregion Update

//#region Delete

export function deleteItemFromCollection(config: CollectionMethodConfiguration, params: CollectionDeleteParams) {
	const { id, body, queryParams } = params
	if (!id) {
		throw new Error(`'id' is required`)
	}
	const { modelName, pathParams, queryParams: configQueryParams, changeModelStatus } = config
	const p = concat([], pathParams, [id])
	if (p.length !== getMinRequiredPathParamsCount(modelName) + 1) {
		throw new Error('pathParams+id length does not match length of path components')
	}
	changeModelStatus(MODEL_STATUS.DELETING, id)
	dispatchModelFetchRequest({
		modelName,
		method: 'DELETE',
		pathParams: p,
		queryParams: queryParams || configQueryParams,
		body
	})
}

export function deleteCollectionItem(
	model: Model,
	config: CollectionMethodConfiguration,
	params: CollectionItemDeleteParams = {}
) {
	if (!model.id) {
		throw new Error('model does not exist')
	}
	const { body, queryParams } = params
	const { modelName, pathParams, queryParams: configQueryParams, changeModelStatus } = config
	if (pathParams && pathParams.length < getMinRequiredPathParamsCount(modelName) + 1) {
		throw new Error('pathParams length does not match length of path components')
	}
	changeModelStatus(MODEL_STATUS.DELETING)
	dispatchModelFetchRequest({
		modelName,
		method: 'DELETE',
		pathParams,
		body,
		queryParams: queryParams || configQueryParams
	})
}

//#endregion Delete

//#endregion Collection/Item CRUD Methods

//#region Helper Methods

/**
 * Automatically call `load`, or `changeModelStatus` to `READY`.
 *
 * Does NOT automatically load if:
 * * pathParams does not contain enough items.
 * * model is already fetching.
 * * auto load is disabled.
 */
export function initializeCollectionItem(
	model: Model,
	load: (params?: CollectionItemLoadParams) => void,
	config: CollectionMethodConfiguration
) {
	const { modelName, pathParams, disableAutoLoad, changeModelStatus } = config
	if (
		pathParams.length === getMinRequiredPathParamsCount(modelName) + 1 &&
		!disableAutoLoad &&
		(!model._metadata || model._metadata.isFetching === false)
	) {
		load()
	} else {
		changeModelStatus(MODEL_STATUS.READY)
	}
}

/**
 * Automatically call `load`, or `changeModelStatus` to `READY`.
 *
 * Does NOT automatically load if:
 * * model is already fetching.
 * * auto load is disabled.
 */
export function initializeCollection(
	model: Model,
	load: (params?: CollectionItemLoadParams) => void,
	config: CollectionMethodConfiguration
) {
	const { disableAutoLoad, changeModelStatus } = config
	if (!disableAutoLoad && (!model._metadata || !model._metadata.isFetching)) {
		load()
	} else if (disableAutoLoad) {
		changeModelStatus(MODEL_STATUS.READY)
	}
}

/**
 * When `pathParams` or `queryParams` change, call `load`
 */
export function handleCollectionItemParamsChange(
	modelName: string,
	prevModelName: string,
	pathParams: Array<string | number>,
	prevPathParams: Array<string | number> | undefined,
	queryParams: Record<string, string | number | boolean> | undefined,
	prevQueryParams: Record<string, string | number | boolean> | undefined,
	load: (params?: CollectionItemLoadParams) => void
) {
	if (
		modelName !== prevModelName ||
		(pathParams.length === getMinRequiredPathParamsCount(modelName) + 1 && !isEqual(prevPathParams, pathParams)) ||
		!isEqual(prevQueryParams, queryParams)
	) {
		load({ pathParams, queryParams })
	}
}

/**
 * When `pathParams` or `queryParams` change, call `load`
 */
export function handleCollectionParamsChange(
	modelName: string,
	prevModelName: string,
	pathParams: Array<string | number>,
	prevPathParams: Array<string | number> | undefined,
	queryParams: Record<string, string | number | boolean> | undefined,
	prevQueryParams: Record<string, string | number | boolean> | undefined,
	load: (params?: CollectionLoadParams) => void
) {
	if (
		modelName !== prevModelName ||
		(pathParams.length > 0 && !isEqual(prevPathParams, pathParams)) ||
		!isEqual(prevQueryParams, queryParams)
	) {
		load({ pathParams, queryParams })
	}
}

/**
 * Delete the guid in the model
 */
export function cleanupCollectionGuidKey<TModel extends Model>(
	model: ModelCollection<TModel>,
	config: CollectionMethodConfiguration
) {
	const { guid, modelName, pathParams } = config
	if (includes(Object.keys(model), guid)) {
		const modelNames = modelName.split('.')
		const tempPathParams = pathParams.slice()
		// should include the guid at the end
		tempPathParams.push(guid)
		if (modelNames.length === tempPathParams.length) {
			let path = ''
			modelNames.forEach((m, i) => {
				if (i === 0) {
					path += `${m}.${tempPathParams[i]}`
				} else {
					path += `.${m}.${tempPathParams[i]}`
				}
			})
			dispatchModelRemoveKeyAction({
				modelPath: path
			})
		}
	}
}

//#endregion Helper Methods
