import _ from 'lodash'
import { BASE_ROLE } from '../constants/baseRole'
import { Group, Metadata, ModelCollection, UserWithRoles } from '../types'
import { getModelArray } from './model'
import { isExternal } from './userRole'

export const groupsAsLearner = (groups: ModelCollection<Group>) => {
	return filterGroupsByRole(groups, BASE_ROLE.GROUP_LEARNER)
}

export const groupsAsOwner = (groups: ModelCollection<Group>) => {
	return filterGroupsByRole(groups, BASE_ROLE.GROUP_OWNER)
}

export const groupsAsGrader = (groups: ModelCollection<Group>) => {
	return filterGroupsByRole(groups, BASE_ROLE.GROUP_GRADER)
}

export const groupsAsAnythingButLearner = (groups: ModelCollection<Group>) => {
	return filterGroupsByRoleOtherThanRole(groups, BASE_ROLE.GROUP_LEARNER)
}

const groupHasRole = (value: Group | Metadata | null | undefined, key: string) =>
	key !== '_metadata' && !!value && !!(value as Group).id && !!(value as Group).roles

const filterGroupsByRole = (groups: ModelCollection<Group>, role: string) => {
	return _.values(
		_.omitBy(getModelArray(groups), (value, key) => !groupHasRole(value, key) || !_.includes(value.roles, role))
	)
}

const filterGroupsByRoleOtherThanRole = (groups: ModelCollection<Group>, role: string) => {
	return _.values(
		_.omitBy(
			getModelArray(groups),
			(value, key) => !groupHasRole(value, key) || _.every(value.roles, r => r === role)
		)
	)
}

export const filterManualGroupUsers = (groupUsers: UserWithRoles[]) => {
	return groupUsers.filter(groupUser => !groupUser.roles.some(r => isExternal(r)))
}

export const filterSyncedGroupUsers = (groupUsers: UserWithRoles[]) => {
	return groupUsers.filter(groupUser => groupUser.roles.some(r => isExternal(r)))
}
