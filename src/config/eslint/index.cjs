module.exports = {
	parser: 'babel-eslint',
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: 'module'
	},
	env: {
		es6: true,
		node: true,
		browser: true,
		jest: true
	},
	extends: ['eslint:recommended', './lib/order.cjs', 'prettier', './lib/prettier.cjs'],
	plugins: ['import', 'prettier'],
	overrides: [
		{
			files: ['**/*.ts?(x)'],
			extends: ['./lib/typescript.cjs']
		}
	]
}
