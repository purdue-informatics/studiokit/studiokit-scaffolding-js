module.exports = {
	extends: ['react-app', './index.cjs', 'prettier/react', './lib/prettier.cjs'],
	overrides: [
		{
			files: ['**/*.ts?(x)'],
			extends: ['react-app', './lib/typescript.cjs', 'prettier/react', './lib/prettier.cjs']
		}
	]
}
