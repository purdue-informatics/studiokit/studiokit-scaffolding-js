import { ApplicationInsights } from '@microsoft/applicationinsights-web'
import * as Sentry from '@sentry/react'
import { Action, createBrowserHistory, History, Location, LocationListener } from 'history'
import ga from 'react-ga4'
import { Persistor } from 'redux-persist'
import { getAppInsights, getUserId, setAppConfig, setAppInsights, setEndpointMappings } from './constants/configuration'
import configureStore, { CustomStore } from './redux/configureStore'
import { AppConfiguration, BaseReduxState, EndpointMappings } from './types'
import { createLogger, setLogger } from './utils/logger'
import { getApiBaseUrl } from './utils/url'

export interface StartupResponse<TReduxState extends BaseReduxState> {
	history: History<any>
	store: CustomStore<TReduxState>
	persistor: Persistor
}

/**
 * The main startup method for your app. Should be called before rendering your app or importing other scaffolding modules.
 *
 * Optional startup methods are available to provide additional configuration. Call these **before** `startup`. These methods can all be imported from `studiokit-scaffolding-js`.
 * * `redux/sagas/rootSaga`
 *   * `setOtherDependentSagas` to add more sagas
 * * `redux/sagas/postLoginDataSaga`
 *   * `setOnPostLogin` to load more data after login
 * * `redux/configureReducers`
 *   * `updatePersistBlacklist` and `setOtherReducers` to add more reducers
 *
 * @template TReduxState The type of state to be stored in redux
 *
 * @param appConfig The static configuration for the application.
 * @param endpointMappings The mapping of redux models to api endpoints.
 *
 * @returns An object containing the created `history`, `store`, and `persistor` objects.
 */
export const startup = <TReduxState extends BaseReduxState>(
	appConfig: AppConfiguration,
	endpointMappings: EndpointMappings
) => {
	// set initial config, enabling `getApiBaseUrl` and `getShardKey`
	setAppConfig(appConfig)
	// update API_BASE_URL with final value
	const apiBaseUrl = getApiBaseUrl(appConfig.TIER, appConfig.API_BASE_URL)
	appConfig.API_BASE_URL = apiBaseUrl

	setEndpointMappings(endpointMappings)
	setLogger(createLogger())

	// Google Analytics
	if (appConfig.GOOGLE_ANALYTICS_TRACKING_ID) {
		ga.initialize(appConfig.GOOGLE_ANALYTICS_TRACKING_ID)
		// track initial page
		ga.send({ hitType: 'pageview', page: window.location.pathname })
	}

	// Application Insights
	if (appConfig.APP_INSIGHTS_KEY) {
		const appInsightsConfig = {
			// TODO: change to connectionString
			instrumentationKey: appConfig.APP_INSIGHTS_KEY
		}
		const appInsights = new ApplicationInsights({ config: appInsightsConfig })
		appInsights.loadAppInsights()
		setAppInsights(appInsights)
	}

	// History and Listener
	const history = createBrowserHistory()
	const locationListener: LocationListener = (location: Location, action: Action) => {
		// send pageview to Google Analytics
		if (appConfig.GOOGLE_ANALYTICS_TRACKING_ID) {
			ga.send({ hitType: 'pageview', page: location.pathname, user_id: getUserId() })
		}
		// send pageview to Application Insights
		if (appConfig.APP_INSIGHTS_KEY) {
			const appInsights = getAppInsights()
			appInsights.trackPageView({ uri: location.pathname })
		}
	}
	history.listen(locationListener)

	// Sentry
	if (appConfig.SENTRY_DSN) {
		Sentry.init({
			dsn: appConfig.SENTRY_DSN,
			release: appConfig.VERSION,
			environment: appConfig.TIER,
			debug: appConfig.NODE_ENV !== 'production',
			// limit depth of data attached to breadcrumbs and contexts
			normalizeDepth: 15,
			// increase length of error messages before truncation
			maxValueLength: 450,
			beforeBreadcrumb(breadcrumb, hint) {
				// do not track console logs when running in development mode (locally)
				return breadcrumb.category === 'console' && appConfig.NODE_ENV !== 'production' ? null : breadcrumb
			},
			// enable performance tracing and session replays
			integrations: [
				Sentry.httpClientIntegration({
					failedRequestTargets: [] // disable automatic "fetch" error tracking, since we have our own custom errors
				}),
				Sentry.reactRouterV5BrowserTracingIntegration({ history }),
				Sentry.replayIntegration()
			],
			// set sample rates for performance tracing and session replays
			tracesSampleRate: appConfig.SENTRY_TRACES_SAMPLE_RATE,
			replaysSessionSampleRate: appConfig.SENTRY_REPLAYS_SESSION_SAMPLE_RATE,
			replaysOnErrorSampleRate: appConfig.SENTRY_REPLAYS_ON_ERROR_SAMPLE_RATE
		})
	}

	// Unhandled Errors
	window.onunhandledrejection = (evt: PromiseRejectionEvent) => {
		const message = evt?.reason?.message
		if (
			message &&
			typeof message === 'string' &&
			[
				'Failed to fetch dynamically imported module',
				'error loading dynamically imported module',
				'Importing a module script failed'
			].some(s => message.includes(s))
		) {
			if (!window.navigator.onLine) {
				window.alert(
					`Your network connection appears to be having issues. Please check your connection and refresh your browser.`
				)
			} else {
				window.alert(
					`A new version of ${appConfig.APP_NAME} is available. Your browser will now refresh. If you see this message again, please clear your browser cache and try again.`
				)
				document.location.reload()
			}
		} else {
			Sentry.captureException(evt?.reason)
		}
	}

	// Redux Store and Persistor
	const { store, persistor } = configureStore<TReduxState>(history)
	const response: StartupResponse<TReduxState> = { history, store, persistor }
	return response
}
