import { EndpointMapping, EndpointMappings } from './types'

export const entityUserRoleEndpointMappings = (endpointName: string): EndpointMappings => {
	return {
		[endpointName]: {
			_config: {
				isCollection: true,
				fetch: {
					path: `/api/${endpointName}`
				}
			}
		},
		// use separate model for transient POSTs
		addUserRoles: {
			_config: {
				fetch: {
					path: `/api/${endpointName}`,
					method: 'POST'
				}
			}
		}
	}
}

export const groupEndpointMapping: EndpointMapping = {
	_config: {
		isCollection: true,
		fetch: {
			path: '/api/groups'
		}
	},
	syncRoster: {
		_config: {
			fetch: {
				method: 'POST'
			}
		}
	},
	...entityUserRoleEndpointMappings('groupUserRoles')
}

export const endpointMappings: EndpointMappings = {
	codeFromLocalCredentials: {
		_config: {
			fetch: {
				path: '/api/account/local',
				method: 'POST'
			}
		}
	},
	codeFromCasV1: {
		_config: {
			fetch: {
				path: '/api/account/casV1',
				method: 'POST'
			}
		}
	},
	codeFromCasTicket: {
		_config: { fetch: { path: '/api/account/validateCasTicket' } }
	},
	getToken: {
		_config: {
			fetch: {
				path: '/token',
				method: 'POST',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}
		}
	},
	user: {
		userInfo: {
			_config: { fetch: { path: '/api/account/userInfo' } }
		}
	},
	urlChecker: {
		_config: {
			fetch: {
				path: '/api/url/headers'
			}
		}
	},
	caliperToken: {
		_config: {
			fetch: {
				path: '/api/caliper-eventstore/token'
			}
		}
	},
	configuration: {
		_config: {
			fetch: {
				path: '/api/configuration'
			}
		}
	},
	client: {
		_config: {
			fetch: {
				path: '/api/appClients/web'
			}
		}
	},
	identityProviders: {
		_config: {
			isCollection: true
		}
	},
	externalTerms: {
		_config: {
			isCollection: true
		}
	},
	externalProviders: {
		_config: {
			isCollection: true
		}
	},
	userRoles: {
		_config: {
			isCollection: true
		}
	},
	// use separate model for transient POSTs
	addUserRoles: {
		_config: {
			fetch: {
				path: '/api/userRoles',
				method: 'POST'
			}
		}
	},
	uniTimeInstructorSchedule: {
		_config: {
			fetch: {
				path: '/api/uniTime/instructorSchedule'
			}
		}
	},
	ltiLaunches: {
		_config: {
			isCollection: true
		},
		deepLinkingResponse: {
			_config: {
				fetch: {
					method: 'POST'
				}
			}
		}
	},
	search: {
		_config: {
			fetch: {
				path: ''
			}
		},
		users: {
			_config: {
				isCollection: true,
				fetch: {
					path: '/api/users'
				}
			}
		}
	},
	time: {
		_config: {
			fetch: {
				path: '/api/time'
			}
		}
	},
	startImpersonation: {
		_config: {
			fetch: {
				path: '/api/account/startImpersonation'
			}
		}
	},
	stopImpersonation: {
		_config: {
			fetch: {
				path: '/api/account/stopImpersonation'
			}
		}
	}
}
