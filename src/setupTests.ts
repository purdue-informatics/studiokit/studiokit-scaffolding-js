import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import { configure } from 'enzyme'
import jestFetchMock from 'jest-fetch-mock'
import MockDate from 'mockdate'
import { setEndpointMappings } from './constants/configuration'
import { defaultDate } from './constants/mockData'
import { endpointMappings, groupEndpointMapping } from './endpointMappings'
import { dummyLogger, setLogger } from './utils/logger'

setLogger(dummyLogger)
setEndpointMappings({ ...endpointMappings, groups: groupEndpointMapping })

jestFetchMock.enableMocks()

jest.mock('./utils/timezone', () => {
	return {
		getDefaultTimeZone: () => 'America/Indianapolis',
		guessTimeZoneId: () => 'America/Indianapolis'
	}
})

// globally mock the date
beforeEach(() => {
	MockDate.set(defaultDate)
})
afterEach(() => {
	MockDate.reset()
})

configure({ adapter: new Adapter() })
